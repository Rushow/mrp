<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/','LoginController@index');


/* Login Router */

Route::get('login','LoginController@index');
Route::post('login','LoginController@checkLogin');
Route::get('logout','LoginController@logOut');

Route::group(array('before'=>'super'),function ()
{
    Route::resource('registration', 'RegistrationController');
    Route::get('registration','RegistrationController@index');
    Route::get('registration/create','RegistrationController@create');
});


Route::group(['middleware' => ['auth']], function() 
{
    Route::get('dashboard', array('as' => 'dashboard', 'uses' => 'DashboardController@index'));

    Route::post('storeIn/addStoreInRow','StoreInController@addStoreInRow');
    Route::post('storeIn/orderType','StoreInController@orderType');
    Route::post('storeOut/addStoreOutRow','StoreOutController@addStoreOutRow');

    Route::get('excel/storeIn','ExcelController@storeIn');
    Route::get('excel/storeOut','ExcelController@storeOut');
    Route::get('excel/storeIn/{id}','ExcelController@storeInDetail');
    Route::get('excel/storeOut/{id}','ExcelController@storeOutDetail');
    Route::get('excel/component', 'ExcelController@component');
    Route::get('excel/sampleOrder', 'ExcelController@sampleOrder');
    Route::get('excel/sampleOrder/{id}', 'ExcelController@sampleOrderDetail');
    Route::get('excel/sampleSchedule/', 'ExcelController@sampleSchedule');
    Route::get('excel/sampleStatus/', 'ExcelController@sampleStatus');
    Route::get('excel/article', 'ExcelController@article');
    Route::get('excel/item', 'ExcelController@item');
    Route::get('excel/articleType', 'ExcelController@articleType');
    Route::get('excel/category', 'ExcelController@category');
    Route::get('excel/construction', 'ExcelController@construction');
    Route::get('excel/size', 'ExcelController@size');
    Route::get('excel/sizeGroup', 'ExcelController@sizeGroup');
    Route::get('excel/color', 'ExcelController@color');
    Route::get('excel/itemGroup', 'ExcelController@itemGroup');
    Route::get('excel/itemSubGroup', 'ExcelController@itemSubGroup');
    Route::get('excel/lastNo', 'ExcelController@lastNo');
    Route::get('excel/heelNo', 'ExcelController@heelNo');
    Route::get('excel/outsoleNo', 'ExcelController@outsoleNo');
    Route::get('excel/buyer', 'ExcelController@buyer');
    Route::get('excel/supplier', 'ExcelController@supplier');
    Route::get('excel/parameter', 'ExcelController@parameter');
    Route::get('excel/unit', 'ExcelController@unit');
    Route::get('excel/sampleType', 'ExcelController@sampleType');
    Route::get('excel/destination', 'ExcelController@destination');
    Route::get('excel/season', 'ExcelController@season');
    Route::get('excel/paymentTerm', 'ExcelController@paymentTerm');
    Route::get('excel/department', 'ExcelController@department');
    Route::get('excel/inventory', 'ExcelController@inventory');
    Route::get('excel/user', 'ExcelController@user');

    Route::get('pdf/articleType', 'PdfController@articleType');
    Route::get('pdf/category', 'PdfController@category');
    Route::get('pdf/construction', 'PdfController@construction');
    Route::get('pdf/size', 'PdfController@size');
    Route::get('pdf/sizeGroup', 'PdfController@sizeGroup');
    Route::get('pdf/color', 'PdfController@color');
    Route::get('pdf/itemGroup', 'PdfController@itemGroup');
    Route::get('pdf/itemSubGroup', 'PdfController@itemSubGroup');
    Route::get('pdf/lastNo', 'PdfController@lastNo');
    Route::get('pdf/heelNo', 'PdfController@heelNo');
    Route::get('pdf/outsoleNo', 'PdfController@outsoleNo');
    Route::get('pdf/buyer', 'PdfController@buyer');
    Route::get('pdf/supplier', 'PdfController@supplier');
    Route::get('pdf/parameter', 'PdfController@parameter');
    Route::get('pdf/unit', 'PdfController@unit');
    Route::get('pdf/sampleType', 'PdfController@sampleType');
    Route::get('pdf/destination', 'PdfController@destination');
    Route::get('pdf/season', 'PdfController@season');
    Route::get('pdf/department', 'PdfController@department');
    Route::get('pdf/storeIn','PdfController@storeIn');
    Route::get('pdf/storeOut','PdfController@storeOut');
    Route::get('pdf/storeIn/{id}','PdfController@storeInDetail');
    Route::get('pdf/storeOut/{id}','PdfController@storeOutDetail');
    Route::get('pdf/component', 'PdfController@component');
    Route::get('pdf/sampleOrder', 'PdfController@sampleOrder');
    Route::get('pdf/sampleOrder/{id}', 'PdfController@sampleOrderDetail');
    Route::get('pdf/sampleSchedule/', 'PdfController@sampleSchedule');
    Route::get('pdf/sampleStatus/', 'PdfController@sampleStatus');
    Route::get('pdf/article', 'PdfController@article');
    Route::get('pdf/item', 'PdfController@item');
    Route::get('pdf/inventory', 'PdfController@inventory');
    Route::get('pdf/user', 'PdfController@user');
    Route::get('pdf/productionOrder', 'PdfController@productionOrder');
    Route::get('pdf/productionOrderDetail', 'PdfController@productionOrderDetail');
    Route::get('pdf/orderTODSizeBreakdownReport', 'PdfController@orderTODSizeBreakdownReport');
    Route::get('pdf/productionOrder/details/{id}','PdfController@showProductDetails');

    Route::get('productionOrder/details/{id}','ProductionOrderController@details');
    Route::post('productionOrder/todDetails','ProductionOrderController@todDetails');
    Route::post('productionOrder/todDetailsStore','ProductionOrderController@todDetailsStore');
    Route::post('productionOrder/removeTodDetails','ProductionOrderController@removeTodDetails');
    Route::post('productionOrder/quantityDetails','ProductionOrderController@quantityDetails');
    Route::post('productionOrder/quantityDetailsStore','ProductionOrderController@quantityDetailsStore');
    Route::post('productionOrder/shippingDetails','ProductionOrderController@shippingDetails');
    Route::post('productionOrder/shippingDetailsStore','ProductionOrderController@shippingDetailsStore');
    Route::post('productionOrder/removeShippingBlock','ProductionOrderController@removeShippingBlock');
    Route::post('productionOrder/getSizeQuantityByTod','ProductionOrderController@getSizeQuantityByTod');
    Route::post('productionOrder/paymentDetailsStore','ProductionOrderController@paymentDetailsStore');
    Route::post('productionOrder/getProductionOrderByBuyerArray','ProductionOrderController@getProductionOrderByBuyerArray');
    Route::post('productionOrder/getArticleByOrderArray','ProductionOrderController@getArticleByOrderArray');
    Route::post('productionOrder/getColorByOrderArray','ProductionOrderController@getColorByOrderArray');

    Route::get('productionOrder/search','ProductionOrderController@search');
    Route::post('productionOrder/search','ProductionOrderController@search');
    Route::get('productionOrder/edit/{id}','ProductionOrderController@edit');
    Route::post('productionOrder/edit/{id}','ProductionOrderController@update');
    Route::get('productionOrder/delete/{id}','ProductionOrderController@destroy');
    Route::get('productionOrder/show/{id}','ProductionOrderController@show');
    Route::post('productionOrder/addPOBlock','ProductionOrderController@addPOBlock');
    Route::get('productionOrder/overallTodReport','ProductionOrderController@overallTodReport');
    Route::post('productionOrder/overallTodReport','ProductionOrderController@overallTodReportSearch');
    Route::any('productionOrder/orderTODSizeBreakdownReport','ProductionOrderController@orderTODSizeBreakdownReport');
    Route::get('productionOrder/orderTODSizeBreakdownReportSearch','ProductionOrderController@orderTODSizeBreakdownReportSearch');
    Route::post('productionOrder/orderTODSizeBreakdownReportSearch','ProductionOrderController@orderTODSizeBreakdownReportSearch');
    Route::any('productionOrder/sizeBreakdownProductionReport','ProductionOrderController@sizeBreakdownProductionReport');
    Route::get('productionOrder/sizeBreakdownProductionReportSearch','ProductionOrderController@sizeBreakdownProductionReportSearch');
    Route::post('productionOrder/sizeBreakdownProductionReportSearch','ProductionOrderController@sizeBreakdownProductionReportSearch');
    Route::any('productionOrder/seasonalOrderValueReport','ProductionOrderController@seasonalOrderValueReport');
    Route::any('productionOrder/seasonalOrderValueReportGroupbyOrder','ProductionOrderController@seasonalOrderValueReportGroupbyOrder');
    Route::post('productionOrder/seasonalOrderValueReportGroupbyOrder','ProductionOrderController@seasonalReportSearch');
    Route::any('productionOrder/seasonalTODReport','ProductionOrderController@seasonalTODReport');
    Route::any('productionOrder/seasonalCutOffReport','ProductionOrderController@seasonalCutOffReport');
    Route::post('productionOrder/seasonalReportSearch','ProductionOrderController@seasonalReportSearch');
    Route::get('productionOrder/seasonalConcentrationReport','ProductionOrderController@seasonalConcentrationReport');
    Route::post('productionOrder/seasonalConcentrationReport','ProductionOrderController@seasonalConcentrationReportSearch');

    Route::get('productionOrder/salescontract','ProductionOrderController@salescontract');
    Route::post('productionOrder/salescontract','ProductionOrderController@salescontractSearch');
    Route::get('productionOrder/shipmentschedule','ProductionOrderController@shipmentschedule');
    Route::post('productionOrder/shipmentschedule','ProductionOrderController@shipmentscheduleSearch');
    Route::get('productionOrder/salescontractpdf/{id}','ProductionOrderController@salescontractpdf');
    Route::get('productionOrder/downloadsalescontractpdf/{id}','ProductionOrderController@downloadsalescontractpdf');
    //order breakdown report
    Route::get('productionOrder/orderbreakdownreport','ProductionOrderController@orderbreakdownreport');
    Route::post('productionOrder/orderbreakdownreport','ProductionOrderController@orderbreakdownreportresult');

    //overall production report
    Route::get('productionOrder/overallproductionreport','ProductionOrderController@overallproductionreport');
    Route::post('productionOrder/overallproductionreport','ProductionOrderController@overallproductionreportsearch');
    Route::get('productionOrderDetail/show/{id}','ProductionOrderDetailController@show');

    //Daily production

    Route::get('dailyproduction/storein/{id}','DailyProductionController@storein');
    Route::post('dailyproduction/storein','DailyProductionController@storeinpost');
    Route::get('dailyproduction/storeout/{id}','DailyProductionController@storeout');
    Route::post('dailyproduction/storeout','DailyProductionController@storeoutpost');
    Route::get('dailyproduction/report','DailyProductionController@report');
    Route::get('dailyproduction/summeryin/{id}','DailyProductionController@summeryin');
    Route::post('dailyproduction/summeryin/{id}','DailyProductionController@summeryinSearch');
    Route::get('dailyproduction/summeryout/{id}','DailyProductionController@summeryout');
    Route::post('dailyproduction/summeryout/{id}','DailyProductionController@summeryoutSearch');
    Route::get('dailyproduction/balance/{id}','DailyProductionController@balance');
    Route::post('dailyproduction/balance/{id}','DailyProductionController@balanceSearch');
    Route::get('dailyproduction/totalbalance/{id}','DailyProductionController@totalbalance');
    Route::post('dailyproduction/totalbalance/{id}','DailyProductionController@totalbalanceSearch');
    Route::get('dailyproduction/downloadtotalbalance/{id}','DailyProductionController@downloadtotalbalance');
    Route::get('dailyproduction/componentrecieved/{id}','DailyProductionController@componentrecieved');
    Route::get('dailyproduction/dailyinstruction/{id}','DailyProductionController@dailyinstruction');
    //Route::get('dailyproduction/instruction','DailyProductionController@instruction');
    //Route::get('dailyproduction/instructionjson','DailyProductionController@instructionjson');
    Route::get('dailyproduction/instruction','DailyProductionController@instructionsearch');
    Route::get('dailyproduction/addinstruction','DailyProductionController@addinstruction');
    Route::post('dailyproduction/addinstruction','DailyProductionController@addinstructionpost');
    Route::post('dailyproduction/componentrecieved/{id}','DailyProductionController@componentrecievedSearch');
    Route::post('dailyproduction/getSizesBySizeGroup','DailyProductionController@getSizesBySizeGroup');

    Route::resource('dailyproduction/component','DailyProductionComponentController');
    Route::resource('dailyproduction/componentcombination','DailyProductionComponentCombinationController');
    //end of daily production //

    Route::post('sampleOrder/search','SampleOrderController@search');
    Route::post('sampleOrder/addOrderRow','SampleOrderController@addOrderRow');
    Route::post('sampleSchedule/search','SampleScheduleController@search');
    Route::post('sampleStatus/search','SampleStatusController@search');
    Route::get('sampleOrderSpec/create/{id}','SampleOrderSpecController@create');
    Route::post('sampleOrderSpec/addOrderSpecRow','SampleOrderSpecController@addOrderSpecRow');
    Route::get('sampleOrderMds/create/{id}','SampleOrderMdsController@create');
    Route::post('sampleOrderMds/addMdsDescRow','SampleOrderMdsController@addMdsDescRow');
    Route::post('sampleOrderMds/addTdsRow','SampleOrderMdsController@addTdsRow');

    Route::post('article/search','ArticleController@search');

    Route::post('item/search','ItemController@search');
    Route::post('item/subGroup','ItemController@subGroup');
    Route::post('item/item','ItemController@item');
    Route::post('item/purchaseUnit','ItemController@purchaseUnit');
    Route::post('item/storeUnit','ItemController@storeUnit');

    Route::post('item/items','ItemController@items');

    Route::post('destination/getDestinationCode','DestinationController@getDestinationCode');

    Route::post('buyer/getBuyerInfo','BuyerController@getBuyerInfo');

    Route::post('color/getColorHMCode','ColorController@getColorHMCode');

    Route::get('inventory', 'InventoryController@index');
    Route::post('inventory/search', 'InventoryController@search');

    Route::resource('article', 'ArticleController');
    Route::resource('articleType', 'ArticleTypeController');
    Route::resource('bank', 'BankController');
    Route::resource('buyer', 'BuyerController');
    Route::resource('blockTtNo', 'BlockTtNoController');
    Route::resource('category', 'CategoryController');
    Route::resource('color', 'ColorController');
    Route::resource('component', 'ComponentController');
    Route::resource('construction', 'ConstructionController');
    Route::resource('customerGroup', 'CustomerGroupController');
    Route::resource('department', 'DepartmentController');
    Route::resource('destination', 'DestinationController');
    Route::resource('documentType', 'DocumentTypeController');
    Route::resource('forwarder', 'ForwarderController');
    Route::resource('heelNo', 'HeelNoController');
    Route::resource('item', 'ItemController');
    Route::resource('itemGroup', 'ItemGroupController');
    Route::resource('itemSubGroup', 'ItemSubGroupController');
    Route::resource('lastNo', 'LastNoController');
    Route::resource('outsoleNo', 'OutsoleNoController');
    Route::resource('parameter', 'ParameterController');
    Route::resource('paymentTerm', 'PaymentTermController');
    Route::resource('productionOrder', 'ProductionOrderController');
    Route::resource('productionOrderDetail', 'ProductionOrderDetailController');
    Route::resource('sampleOrder', 'SampleOrderController');
    Route::resource('sampleOrderSpec', 'SampleOrderSpecController');
    Route::resource('sampleOrderMds', 'SampleOrderMdsController');
    Route::resource('sampleType', 'SampleTypeController');
    Route::resource('sampleSchedule', 'SampleScheduleController');
    Route::resource('sampleStatus', 'SampleStatusController');
    Route::resource('season', 'SeasonController');
    Route::resource('shippingMode', 'ShippingModeController');
    Route::resource('size', 'SizeController');
    Route::resource('sizeGroup', 'SizeGroupController');
    Route::resource('supplier', 'SupplierController');
    Route::resource('storeIn', 'StoreInController');
    Route::resource('storeOut', 'StoreOutController');
    Route::resource('termsOfDelivery', 'TermsOfDeliveryController');
    Route::resource('unit', 'UnitController');
    Route::resource('sockPrint', 'SockPrintController');
    Route::resource('liningPrint', 'LiningPrintController');
    Route::resource('additionalPrint', 'AdditionalPrintController');
    Route::resource('polySticker', 'PolyStickerController');
    Route::resource('pictogram', 'PictogramController');
    Route::resource('priceTag', 'PriceTagController');
    Route::resource('pmTag', 'PmTagController');
    Route::resource('additionalStickerInfo', 'AdditionalStickerInfoController');
    Route::resource('labelPrintDetails', 'LabelPrintDetailsController');
    Route::resource('userType', 'UserTypeController');
    Route::resource('buyerTemplate', 'BuyerTemplateController');

    Route::post('sizeGroup/getSizesBySizeGroup','SizeGroupController@getSizesBySizeGroup');
    //Route::get('sizeGroup/getSizesBySizeGroup','SizeGroupController@getSizesBySizeGroup');
    Route::post('productionOrderDetail/getOrderInfo','ProductionOrderDetailController@getOrderInfo');
    Route::post('productionOrderDetail/getOrderByBuyer','ProductionOrderDetailController@getOrderByBuyer');

    //	Route::resource('productionOrderDetail/create/{id}', 'ProductionOrderDetailController@create');

    Route::get('import', 'ImportController@index');
    Route::get('importAppearance', 'ImportController@importAppearance');
    Route::post('importAppearanceSave', 'ImportController@importAppearanceSave');
    Route::get('importBuyer', 'ImportController@importBuyer');
    Route::post('importBuyerSave', 'ImportController@importBuyerSave');
    Route::get('importColor', 'ImportController@importColor');
    Route::post('importColorSave', 'ImportController@importColorSave');
    Route::get('importConstruction', 'ImportController@importConstruction');
    Route::post('importConstructionSave', 'ImportController@importConstructionSave');
    Route::get('importCustomerGroup', 'ImportController@importCustomerGroup');
    Route::post('importCustomerGroupSave', 'ImportController@importCustomerGroupSave');
    Route::get('importDestination', 'ImportController@importDestination');
    Route::post('importDestinationSave', 'ImportController@importDestinationSave');
    Route::get('importSeason', 'ImportController@importSeason');
    Route::post('importSeasonSave', 'ImportController@importSeasonSave');
    
    //planning division

    Route::get('showdivision', 'DivisionController@show');
    Route::get('showdivision/details/{id}', 'DivisionController@showdetail');
    Route::get('editdivision/{id}', 'DivisionController@edit');
    Route::post('editdivision/{id}', 'DivisionController@editdivision');
    Route::get('adddivision', 'DivisionController@add');
    Route::post('adddivision', 'DivisionController@adddivision');
    Route::get('deletedivision/{id}', 'DivisionController@deletedivision');
    
    Route::resource('subdivision', 'SubdivisionController');

    Route::resource('machine', 'MachineController');
    Route::resource('capacity', 'CapacityController');

    Route::resource('assignmachine','AssignMachineController');

    //planning gantt data..............................................

    Route::post('gantt_data/{id}/task',"TaskController@store") ;
    Route::put('gantt_data/{order_id}/task/{id}',"TaskController@update") ;
    Route::put('gantt_data/task/{id}',"TaskController@updatetask") ;
    
    
    Route::post('gantt_data/{id}/link',"LinkController@store") ;
    Route::put('gantt_data/{order_id}/link/{id}',"LinkController@update") ;

    Route::resource('makeschedule',"MakescheduleController") ;
    Route::resource('gantt_data/task',"TaskController") ;
    Route::resource('gantt_data/link',"LinkController") ;
    Route::resource('style',"StyleController") ;
    Route::resource('line',"LineController") ;
    Route::resource('complexity',"ComplexityController") ;


    Route::match(['get','post'], 'gantt_data', "MakescheduleController@data");
    Route::match(['get','post'], 'gantt_data/{id}', "MakescheduleController@individualdata");

    Route::get('listofplan',"MakescheduleController@listofplan") ;
    Route::get('allplanning',"MakescheduleController@allofplan") ;
    Route::post('checkifavailable',"MakescheduleController@checkifavailable") ;
    Route::post('availabledate',"MakescheduleController@availabledate") ;
    Route::get('eventdata',"MakescheduleController@eventdata") ;



});

