<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSampleStatusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sample_statuses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('sample_detail_id');
			$table->string('pattern');
			$table->string('test_sample');
			$table->string('grading');
			$table->string('material_status');			
			$table->dateTime('actual_delivery');
			$table->text('note');

			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamp('created_at')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sample_statuses');
	}

}
