<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDestinationTable2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('destination', function($table)
        {
            $table->dropColumn('sock_print');
            $table->dropColumn('lining_print');
            $table->dropColumn('additional_print');
            $table->dropColumn('poly_sticker');
            $table->dropColumn('pictogram');
            $table->dropColumn('price_tag');
            $table->dropColumn('pm_tag');
            $table->dropColumn('additional_sticker_info');
        });

        Schema::table('destination', function($table)
        {
            $table->integer('sock_print_id')->nullable()->default(0);
            $table->integer('lining_print_id')->nullable()->default(0);
            $table->integer('additional_print_id')->nullable()->unsigned();
            $table->integer('poly_sticker_id')->nullable()->unsigned();
            $table->integer('pictogram_id')->nullable()->unsigned();
            $table->integer('price_tag_id')->nullable()->unsigned();
            $table->integer('pm_tag_id')->nullable()->unsigned();
            $table->integer('additional_sticker_info_id')->nullable()->unsigned();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('destination', function($table)
        {
            $table->dropColumn('sock_print_id');
            $table->dropColumn('lining_print_id');
            $table->dropColumn('additional_print_id');
            $table->dropColumn('poly_sticker_id');
            $table->dropColumn('pictogram_id');
            $table->dropColumn('price_tag_id');
            $table->dropColumn('pm_tag_id');
            $table->dropColumn('additional_sticker_info_id');
        });

        Schema::table('destination', function($table)
        {
            $table->string('sock_print', 45)->nullable();
            $table->string('lining_print', 45)->nullable();
            $table->string('additional_print', 45)->nullable();
            $table->string('poly_sticker', 45)->nullable();
            $table->string('pictogram', 45)->nullable();
            $table->string('price_tag', 45)->nullable();
            $table->string('pm_tag', 45)->nullable();
            $table->string('additional_sticker_info', 45)->nullable();
        });
	}

}

