<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubofsubdivisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subofsubdivisions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subdivisions_id');
            $table->string('subofsubdivision_name');
            $table->string('subofsubdivision_description');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subofsubdivisions');
    }
}
