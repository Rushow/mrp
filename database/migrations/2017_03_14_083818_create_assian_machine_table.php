<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssianMachineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_machines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('machine_id',100);
            $table->string('order_id',100);
            $table->integer('style');
            $table->integer('complexity');
            $table->dateTime('start_time');
            $table->dateTime('end_time');


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assign_machines');
    }
}
