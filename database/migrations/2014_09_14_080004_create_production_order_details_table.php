<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionOrderDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create("production_order_detail",function($table){
            $table->increments('id');
            $table->integer('production_order_id');
            $table->integer('article_id');
            $table->integer('article_color_id');
            $table->double('article_price');
            $table->text('article_note');
            $table->integer('size_group_id');
            $table->integer('size_quantity');
            $table->integer('size_total_value');

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('production_order_detail');
	}

}
