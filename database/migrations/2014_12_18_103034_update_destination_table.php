<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDestinationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('destination', function($table)
        {
            $table->integer('cut_off_diff')->unsigned();
            $table->string('cut_off_day', 45)->nullable();
            $table->integer('etd_diff')->nullable();
            $table->string('etd_day', 45)->nullable();
            $table->string('assort_packing', 45)->nullable();
            $table->string('carton_details_assort', 11)->nullable();
            $table->string('carton_details_primary', 11)->nullable();
            $table->string('carton_details_secondary', 11)->nullable();
            $table->text('consignee_address')->nullable();
            $table->string('sock_print', 45)->nullable();
            $table->string('lining_print', 45)->nullable();
            $table->string('additional_print', 45)->nullable();
            $table->string('poly_sticker', 45)->nullable();
            $table->string('pictogram', 45)->nullable();
            $table->string('price_tag', 45)->nullable();
            $table->string('pm_tag', 45)->nullable();
            $table->string('additional_sticker_info', 45)->nullable();
            $table->text('remarks')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('destination', function($table)
        {
//            $table->dropColumn('cut_off_diff');
//            $table->dropColumn('cut_off_day');
//            $table->dropColumn('etd_diff');
//            $table->dropColumn('etd_day');
//            $table->dropColumn('assort_packing');
//            $table->dropColumn('carton_details_assort');
//            $table->dropColumn('carton_details_primary');
//            $table->dropColumn('carton_details_secondary');
//            $table->dropColumn('consignee_address');
//            $table->dropColumn('sock_print');
//            $table->dropColumn('lining_print');
//            $table->dropColumn('additional_print');
//            $table->dropColumn('poly_sticker');
//            $table->dropColumn('pictogram');
//            $table->dropColumn('price_tag');
//            $table->dropColumn('pm_tag');
//            $table->dropColumn('additional_sticker_info');
//            $table->dropColumn('remarks');
        });
	}

}
