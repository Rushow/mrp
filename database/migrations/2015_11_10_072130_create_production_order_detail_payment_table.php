<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionOrderDetailPaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("production_order_detail_payment",function($table){
			$table->increments('id');
			$table->integer('production_order_id')->nullable();
			$table->integer('payment_term_id')->nullable();
			$table->integer('currency_id')->unsigned()->nullable();
			$table->integer('block_tt_no')->default(0)->nullable();
			$table->integer('block_tt_order_from')->nullable();
			$table->integer('block_tt_order_to')->nullable();
			$table->string('sales_contact_no', 100)->nullable();
			$table->string('purchase_contact_no', 100)->nullable();
			$table->float('commission')->nullable();
			$table->float('total_value')->nullable();
			$table->float('commission_value')->nullable();
			$table->float('invoice_value')->nullable();
			$table->float('less_financial_charge')->nullable();
			$table->float('less_financial_charge_percent')->nullable();
			$table->float('less_claim_bonus')->nullable();
			$table->float('less_claim_bonus_percent')->nullable();
			$table->float('less_novi_deduction')->nullable();
			$table->float('less_novi_deduction_percent')->nullable();
			$table->float('sub_total_value')->nullable();
			$table->float('less_lc_deadline')->nullable();
			$table->float('less_lc_deadline_percent')->nullable();
			$table->float('final_amount')->nullable();
			$table->text('remarks')->nullable();

			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamp('created_at')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('production_order_detail_payment');
	}

}
