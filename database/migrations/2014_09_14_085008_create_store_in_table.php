<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreInTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_ins', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('challan_no');
            $table->integer('warehouse_id');
            $table->string('in_type');
            $table->integer('order_no_id');
            $table->integer('supplier_id');
            $table->dateTime('store_in_date');
            $table->text('remarks');

			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamp('created_at')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_ins');
	}

}
