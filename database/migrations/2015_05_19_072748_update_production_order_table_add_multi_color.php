<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductionOrderTableAddMultiColor extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('production_order', function($table)
		{
			$table->dropColumn('article_id');
			$table->dropColumn('article_type_id');
			$table->dropColumn('category_id');
			$table->dropColumn('size_group_id');
			$table->dropColumn('color_id');
			$table->dropColumn('color_code');
//			$table->dropColumn('block_tt_no');
		});

		Schema::table('production_order', function($table)
		{
			$table->integer('buyer_id')->unsigned();
			$table->integer('status_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('production_order', function($table)
		{
			$table->dropColumn('buyer_id');
		});
	}

}
