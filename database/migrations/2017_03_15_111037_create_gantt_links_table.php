<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGanttLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gantt_links', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id',100);
            $table->integer('source');
            $table->integer('target');
            $table->string('type');
           $table->timestamps();
            $table->softDeletes();
        });

        //INSERT INTO `gantt_links` (`id`, `source`, `target`, `type`) VALUES (1, 10, 11, '3'), (2, 10, 15, '0'), (3, 16, 17, '1'), (4, 17, 19, '1')
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gantt_links');
    }
}
