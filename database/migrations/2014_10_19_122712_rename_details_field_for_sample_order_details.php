<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameDetailsFieldForSampleOrderDetails extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sample_order_spec', function($table)
		{
		    $table->dropColumn('sample_order_details_id');
		});

		Schema::table('sample_order_spec', function($table)
		{
		    $table->integer('sample_order_detail_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('buyer', function($table)
		{
		    $table->integer('sample_order_details_id');
		});
	}

}
