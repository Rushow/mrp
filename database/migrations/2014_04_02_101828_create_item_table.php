<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create("item",function($table){
            $table->increments('id');
            $table->string('item_name', 50);
            $table->integer('group_id');
            $table->integer('sub_group_id');
            $table->integer('parameter_id');
            $table->integer('color_id');
            $table->text('remarks')->nullable();
            $table->string('item_code', 50);
            $table->integer('cutability_factor');
            $table->integer('cutable_width');
            $table->integer('store_unit');
            $table->integer('purchase_unit');
            $table->integer('store_conversion');
            $table->integer('purchase_conversion');
            $table->string('item_pic', 250)->nullable();

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('item');
	}

}
