<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGanttTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gantt_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id',100);
            $table->string('article_no',100);
            $table->integer('division_id');
            $table->integer('sub_division_id');
            $table->integer('line_id');
            $table->integer('complexity_id');
            $table->integer('quantity');
            $table->string('machine_ids',500);
            $table->string('text',255);
            $table->dateTime('start_date');
            $table->integer('duration');
            $table->float('progress');
            $table->double('sortorder');
            $table->integer('parent');
            $table->dateTime('deadline');
            $table->dateTime('planned_start');
            $table->dateTime('planned_end');
            $table->dateTime('end_date');
            $table->timestamps();
            $table->softDeletes();
        });
/*
 INSERT INTO `gantt_tasks` (`id`, `text`, `start_date`, `duration`, `progress`, `sortorder`, `parent`, `deadline`, `planned_start`, `planned_end`, `end_date`) VALUES
(10, 'H&M', '2010-08-08 00:00:00', 4, 0.292857, 0, 0, NULL, NULL, NULL, '2010-08-12 00:00:00'),
(11, 'Cutting', '2010-08-04 00:00:00', 4, 0, 0, 10, NULL, NULL, NULL, '2010-08-08 00:00:00'),
(15, 'Skyving', '2010-08-10 00:00:00', 6, 0, 0, 10, NULL, NULL, NULL, '2010-08-16 00:00:00'),
(16, 'Task 2', '2010-08-18 00:00:00', 7, 0.469388, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00'),
(17, 'Task 2.1', '2010-08-21 00:00:00', 5, 0.168571, 0, 16, NULL, NULL, NULL, '2010-08-26 00:00:00'),
(19, 'Task 2.2', '2010-08-20 00:00:00', 3, 0.652381, 0, 16, NULL, NULL, NULL, '0000-00-00 00:00:00');
 */
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('grantt_tasks');
    }
}
