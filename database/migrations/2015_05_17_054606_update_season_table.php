<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSeasonTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('season', function($table)
		{
			$table->string('start_date', 100);
			$table->string('end_date', 100);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('production_order_detail', function($table)
		{
			$table->string('start_date', 100);
			$table->string('end_date', 100);
		});
	}

}
