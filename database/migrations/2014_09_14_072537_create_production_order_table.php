<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create("production_order",function($table){
            $table->increments('id');
            $table->string('production_order_no', 100);
            $table->string('buyer_order_no', 100);
            $table->integer('destination_id');
            $table->integer('buyer_id');
            $table->string('order_date', 100);
            $table->string('delivery_date', 100);
            $table->integer('season_id');
            $table->integer('payment_term_id');

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('production_order');
	}

}
