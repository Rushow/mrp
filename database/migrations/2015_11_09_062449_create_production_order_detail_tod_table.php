<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionOrderDetailTodTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("production_order_detail_tod",function($table){
			$table->increments('id');
			$table->integer('production_order_id')->nullable();
			$table->integer('article_id')->nullable();
			$table->integer('color_id')->nullable();
			$table->integer('destination_id')->nullable();
			$table->string('despo', 200)->nullable();
			$table->integer('shipping_mode_id')->nullable();
			$table->string('pos', 200)->nullable();
			$table->timestamp('tod')->nullable();
			$table->integer('days_ship_advance')->nullable();
			$table->integer('size_group_id')->nullable();
			$table->integer('total_quantity')->nullable();
			$table->float('unit_value')->nullable();
			$table->text('remarks')->nullable();
			$table->text('quantity_remarks')->nullable();

			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamp('created_at')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('production_order_detail_tod');
	}

}
