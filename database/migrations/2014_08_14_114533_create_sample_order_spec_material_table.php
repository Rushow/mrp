<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSampleOrderSpecMaterialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create("sample_order_spec_material",function($table){
            $table->increments('id');
            $table->integer('sample_spec_id');
            $table->integer('component_id');
            $table->integer('group_id');
            $table->integer('sub_group_id');
            $table->integer('parameter_id');
            $table->integer('item_id');
            $table->integer('color_id_1');
            $table->integer('color_id_2');
            $table->integer('color_id_3');
            $table->text('note');

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('sample_order_spec_material');
	}

}
