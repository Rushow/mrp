<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGanttTasksEnddateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gantt_tasks_enddates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id',100);
            $table->string('text',255);
            $table->dateTime('start_date');
           
            $table->float('progress');
            $table->double('sortorder');
            $table->integer('parent');
            $table->dateTime('end_date');
            $table->dateTime('planned_start');
            $table->dateTime('planned_end');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gantt_tasks_enddates');
    }
}
