<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreOutDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_out_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('store_out_id');
            $table->integer('item_group_id');
            $table->integer('sub_group_id');
            $table->integer('color_id');
            $table->integer('parameter_id');
            $table->double('quantity');
            $table->integer('item_id');
            $table->integer('unit_id');

			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamp('created_at')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_out_details');
	}

}
