<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSampleSpecTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create("sample_order_spec",function($table){
            $table->increments('id');
            $table->integer('sample_order_details_id');
            $table->integer('spec_no');
            $table->integer('last_no_id');
            $table->integer('outsole_no_id');
            $table->integer('heel_no_id');
            $table->string('logo_position', 100);
            $table->string('logo_type', 100);
            $table->string('print_type', 100);
            $table->string('print_color', 100);
            $table->text('note');

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('sample_order_spec');
	}

}
