<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductionOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('production_order', function($table)
        {
            $table->dropColumn('production_order_no');
            $table->dropColumn('buyer_order_no');
            $table->dropColumn('destination_id');
            $table->dropColumn('buyer_id');
            $table->dropColumn('order_date');
            $table->dropColumn('delivery_date');
            $table->dropColumn('payment_term_id');
            $table->dropColumn('season_id');
            $table->dropColumn('created_by');
            $table->dropColumn('updated_by');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
        });

        Schema::table('production_order', function($table)
        {
            $table->string('order_no', 100);
            $table->integer('department_id')->default('0');
            $table->integer('article_id')->unsigned();
            $table->integer('article_type_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('color_id')->unsigned();
            $table->string('color_code', 100);
            $table->integer('size_group_id')->unsigned();
            $table->integer('season_id')->unsigned();
            $table->timestamp('order_date', 100)->nullable();
            $table->timestamp('order_receive_date');
            $table->text('remarks');

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('production_order');

        Schema::create("production_order",function($table){
            $table->increments('id');
            $table->string('production_order_no', 100);
            $table->string('buyer_order_no', 100);
            $table->integer('destination_id');
            $table->integer('buyer_id');
            $table->string('order_date', 100);
            $table->string('delivery_date', 100);
            $table->integer('season_id');
            $table->integer('payment_term_id');

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
	}

}
