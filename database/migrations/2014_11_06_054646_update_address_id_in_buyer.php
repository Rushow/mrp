<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAddressIdInBuyer extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('buyer', function($table)
		{
		    $table->dropColumn('buyer_address');
		});

		Schema::table('buyer', function($table)
		{
		    $table->integer('address_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('buyer', function($table)
		{
		    $table->dropColumn('address_id');
		});

		Schema::table('buyer', function($table)
		{
		    $table->string('buyer_address');
		});
	}

}
