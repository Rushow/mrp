<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('color', function($table)
        {
            $table->dropColumn('color_name');
        });

        Schema::table('color', function($table)
        {
            $table->string('pantone_code', 45)->nullable();
            $table->string('hm_code', 45)->nullable();
            $table->string('color_group_code', 45)->nullable();
            $table->string('color_name_flcl', 45)->nullable();
            $table->string('color_name_hm', 45)->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

        Schema::table('color', function($table)
        {
            $table->dropColumn('pantone_code');
            $table->dropColumn('hm_code');
            $table->dropColumn('color_group_code');
            $table->dropColumn('color_name_flcl');
            $table->dropColumn('color_name_hm');
        });

        Schema::table('color', function($table)
        {
            $table->string('color_name', 50)->nullable();
        });
	}

}
