<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreInDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_in_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('store_in_id');
            $table->integer('item_group_id');
            $table->integer('sub_group_id');
            $table->integer('color_id');
            $table->integer('parameter_id');
            $table->double('quantity');
            $table->integer('item_id');
            $table->double('rate');
            $table->integer('currency_id');
            $table->integer('unit_id');

			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamp('created_at')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_in_details');
	}

}
