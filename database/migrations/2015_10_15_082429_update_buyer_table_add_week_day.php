<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBuyerTableAddWeekDay extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table("buyer",function($table){
//			$table->dropColumn('week_start_day');
//			$table->dropColumn('cut_off_day');
//			$table->dropColumn('cut_off_diff');
//			$table->dropColumn('etd_day');
//			$table->dropColumn('etd_diff');
			$table->dropColumn('created_by');
			$table->dropColumn('updated_by');
		});

		Schema::table("buyer",function($table){
			$table->string('week_start_day', 100)->nullable();
			$table->integer('buyer_template_id')->nullable();
			$table->integer('cut_off_diff')->nullable();
			$table->string('cut_off_day', 45)->nullable();
			$table->integer('etd_diff')->nullable();
			$table->string('etd_day', 45)->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table("buyer",function($table){
			$table->dropColumn('week_start_day');
			$table->dropColumn('cut_off_day');
			$table->dropColumn('cut_off_diff');
			$table->dropColumn('etd_day');
			$table->dropColumn('etd_diff');
			$table->dropColumn('created_by');
			$table->dropColumn('updated_by');
		});
	}

}
