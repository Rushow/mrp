<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductionOrderDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('production_order_detail', function($table)
        {
            $table->dropColumn('production_order_id');
            $table->dropColumn('article_id');
            $table->dropColumn('article_color_id');
            $table->dropColumn('article_price');
            $table->dropColumn('article_note');
            $table->dropColumn('size_group_id');
            $table->dropColumn('size_quantity');
            $table->dropColumn('size_total_value');
            $table->dropColumn('created_by');
            $table->dropColumn('updated_by');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
        });

        Schema::table('production_order_detail', function($table)
        {
            $table->integer('production_order_id')->unsigned();
            $table->integer('color_id')->unsigned();
            $table->integer('destination_id')->unsigned();
            $table->integer('order_date')->nullable();
            $table->integer('days_ship_advance')->unsigned();
            $table->string('cut_off', 100);
            $table->text('remarks');

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('production_order_detail');

        Schema::create("production_order_detail",function($table){
            $table->increments('id');
            $table->integer('production_order_id');
            $table->integer('article_id');
            $table->integer('article_color_id');
            $table->double('article_price');
            $table->text('article_note');
            $table->integer('size_group_id');
            $table->integer('size_quantity');
            $table->integer('size_total_value');

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
	}

}
