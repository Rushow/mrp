<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create("article",function($table){
            $table->increments('id');
            $table->string('article_no', 50);
            $table->integer('article_ref');
            $table->integer('article_type_id');
            $table->text('note')->nullable();
            $table->integer('construction_id');
            $table->integer('customer_group_id');
            $table->string('article_pic', 250)->nullable();

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('article');
	}

}
