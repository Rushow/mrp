<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyProductionsStoreOutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_production_store_out', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_id');
            $table->integer('sub_department_id')->nullable();
            $table->integer('production_order_id');
            $table->integer('article_id');
            $table->integer('color_id');
            $table->dateTime('production_order_detail_tod_id');
            $table->integer('size_group_id');
            $table->integer('quantity');
            $table->integer('store_unit_component_id');
            $table->integer('store_out_division');
            $table->dateTime('store_out_date');
            $table->integer('recieved_confirm');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('daily_production_store_out');
    }
}
