<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAddressIdInSupplier extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('supplier', function($table)
		{
		    $table->dropColumn('supplier_location');
		});

		Schema::table('supplier', function($table)
		{
		    $table->integer('address_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('supplier', function($table)
		{
		    $table->dropColumn('address_id');
		});

		Schema::table('supplier', function($table)
		{
		    $table->string('supplier_location');
		});
	}

}
