<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionOrderDetailShipment extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("production_order_detail_shipment",function($table){
			$table->increments('id');
			$table->integer('production_order_id')->nullable();
			$table->integer('consignee_bank_id')->nullable();
			$table->integer('shipper_bank_id')->nullable();
			$table->integer('terms_of_delivery_id')->nullable();
			$table->integer('forwarder_id')->nullable();

			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamp('created_at')->nullable();
			$table->timestamp('updated_at')->nullable();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('production_order_detail_shipment');
	}

}
