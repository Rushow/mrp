<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDatesFromSampleOrder extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sample_order', function($table)
		{
		    $table->dropColumn(array('order_date', 'delivery_date'));
		});

		Schema::table('sample_order', function($table)
		{
		    $table->timestamp('order_date')->nullable();
		    $table->timestamp('delivery_date')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sample_order', function($table)
		{
		    $table->dropColumn(array('order_date', 'delivery_date'));
		});

		Schema::table('sample_order', function($table)
		{
		    $table->string('order_date', 100);
		    $table->string('delivery_date', 100);
		});
	}

}
