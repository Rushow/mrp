<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSampleOrderMdsDesc2Table extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create("sample_order_mds_desc",function($table){
            $table->increments('id');
            $table->integer('mds_id');
            $table->integer('component_id');
            $table->integer('group_id');
            $table->integer('sub_group_id');
            $table->integer('parameter_id');
            $table->integer('item_id');
            $table->integer('color_id');
            $table->text('area');
            $table->text('wastage');
            $table->text('consumption');
            $table->text('note');

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('sample_order_mds_desc');
	}

}
