<?php
use Illuminate\Database\Seeder;
use App\PolySticker;
class PolyStickerSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('poly_sticker')->delete();

        PolySticker::create(array('poly_sticker' => 'HM 14115'));
        PolySticker::create(array('poly_sticker' => 'HM 14116'));
        PolySticker::create(array('poly_sticker' => 'HM 14117'));

	}

}