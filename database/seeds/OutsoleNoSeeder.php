<?php
use Illuminate\Database\Seeder;
use App\OutsoleNo;
class OutsoleNoSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('outsole_no')->delete();

		for ($i=0; $i < 10; $i++) {
			OutsoleNo::create(array('outsole_no' => 'number ' . $i));
		}
	}

}