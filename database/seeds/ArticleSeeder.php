<?php
use Illuminate\Database\Seeder;

use App\Article;

class ArticleSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('article')->delete();

		Article::create(array('article_no' => '26645', 'article_ref' => '142345', 'article_type_id' => 1, 'construction_id' => 2, 'customer_group_id' => 3));
		Article::create(array('article_no' => '17220', 'article_ref' => '546732', 'article_type_id' => 2, 'construction_id' => 4, 'customer_group_id' => 1));
		Article::create(array('article_no' => '55991', 'article_ref' => '830346', 'article_type_id' => 3, 'construction_id' => 5, 'customer_group_id' => 3));
		Article::create(array('article_no' => '70914', 'article_ref' => '604677', 'article_type_id' => 4, 'construction_id' => 1, 'customer_group_id' => 2));
		Article::create(array('article_no' => '49091', 'article_ref' => '911422', 'article_type_id' => 5, 'construction_id' => 4, 'customer_group_id' => 4));
		Article::create(array('article_no' => '10023', 'article_ref' => '044522', 'article_type_id' => 3, 'construction_id' => 3, 'customer_group_id' => 1));
		Article::create(array('article_no' => '04521', 'article_ref' => '505052', 'article_type_id' => 5, 'construction_id' => 2, 'customer_group_id' => 5));
	}

}