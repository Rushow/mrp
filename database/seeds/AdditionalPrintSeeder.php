<?php
use Illuminate\Database\Seeder;
use App\AdditionalPrint;
class AdditionalPrintSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('additional_print')->delete();

        AdditionalPrint::create(array('additional_print' => 'HM 14115'));
        AdditionalPrint::create(array('additional_print' => 'HM 14116'));
        AdditionalPrint::create(array('additional_print' => 'HM 14117'));

	}

}