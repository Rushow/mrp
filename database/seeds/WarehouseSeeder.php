<?php
use Illuminate\Database\Seeder;
use App\Warehouse;
class WarehouseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('warehouses')->delete();

		Warehouse::create(array('name' => 'East Miami'));
		Warehouse::create(array('name' => 'Taltala Underground'));
		Warehouse::create(array('name' => 'Gulshan Eastside'));
		Warehouse::create(array('name' => 'Gazipur Westside'));
	}

}