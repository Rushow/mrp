<?php
use Illuminate\Database\Seeder;
use App\Construction;
class ConstructionSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Construction::create(array('construction' => 'Surface Mounted'));
		Construction::create(array('construction' => 'Plane Oriented'));
		Construction::create(array('construction' => 'Coated Plating'));
		Construction::create(array('construction' => 'Dual Layer Spotting'));
		Construction::create(array('construction' => 'Single Threading'));
	}

}