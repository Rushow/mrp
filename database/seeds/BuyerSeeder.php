<?php
use Illuminate\Database\Seeder;
use App\Buyer;
class BuyerSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('buyer')->delete();	
		Buyer::create(array('buyer_name' => 'H&M', 'full_name' => 'HnM', 'buyer_email' => 'admin@hnm.mail', 'buyer_phn' => '+880162445789', 'week_start_day' => 'Monday', 'buyer_template_id' => '1'));
		Buyer::create(array('buyer_name' => 'Novi', 'full_name' => 'Novi', 'buyer_email' => 'admin@novi.mail', 'buyer_phn' => '+8801711455577', 'week_start_day' => 'Sunday', 'buyer_template_id' => '2'));
	}
}