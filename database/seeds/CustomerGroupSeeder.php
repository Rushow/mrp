<?php
use Illuminate\Database\Seeder;
use App\CustomerGroup;
class CustomerGroupSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('customer_group')->delete();
		CustomerGroup::create(array('customer_group' => 'Local'));
		CustomerGroup::create(array('customer_group' => 'China'));
		CustomerGroup::create(array('customer_group' => 'Hong kong'));
		CustomerGroup::create(array('customer_group' => 'Germany'));
	}

}