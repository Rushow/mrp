<?php
use Illuminate\Database\Seeder;
use App\TermsOfDelivery;
class TermsOfDeliverySeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('terms_of_delivery')->delete();
		TermsOfDelivery::create(array('terms_of_delivery' => 'term 01'));
		TermsOfDelivery::create(array('terms_of_delivery' => 'term 02'));
		TermsOfDelivery::create(array('terms_of_delivery' => 'term 03'));
	}

}