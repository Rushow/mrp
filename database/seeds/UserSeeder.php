<?php
use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->delete();

		User::create(array('name' => 'atefth', 'first_name' => 'Atef', 'last_name' => 'Haque', 'email' => 'atefth@gmail.com', 'password' => Hash::make('pass'), 'status' => 1, 'user_type_id' => 1));
		User::create(array('name' => 'rushow', 'first_name' => 'Rushow', 'last_name' => 'Khan', 'email' => 'khan.rushow@gmail.com', 'password' => Hash::make('123456'), 'status' => 1, 'user_type_id' => 1));
		User::create(array('name' => 'admin', 'first_name' => 'Admin', 'last_name' => 'Admin', 'email' => 'admin@mrp.com', 'password' => Hash::make('123456'), 'status' => 1, 'user_type_id' => 1));
		User::create(array('name' => 'user', 'first_name' => 'Orindom', 'last_name' => 'Shaha', 'email' => 'user@mrp.com', 'password' => Hash::make('123456'), 'status' => 1, 'user_type_id' => 3));
		User::create(array('name' => 'viewer', 'first_name' => 'Ashik', 'last_name' => 'Iqbal', 'email' => 'viewer@mrp.com', 'password' => Hash::make('123456'), 'status' => 1, 'user_type_id' => 4));
	}
}