<?php
use Illuminate\Database\Seeder;
use App\Component;
class ComponentSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('component')->delete();
		Component::create(array('component_name' => 'Phone Block'));
		Component::create(array('component_name' => 'Sewing Needle'));
		Component::create(array('component_name' => 'Leather Polish'));
		Component::create(array('component_name' => 'Motherboard'));
	}

}