<?php
use Illuminate\Database\Seeder;
use App\SizeGroup;
use App\SizeGroupSize;
class SizeGroupSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('size_group')->delete();
		SizeGroup::create(array('size_group_name' => 'Leather'));
		SizeGroup::create(array('size_group_name' => 'Garments'));
		SizeGroup::create(array('size_group_name' => 'Consumers'));
		SizeGroup::create(array('size_group_name' => 'Plastics'));


		DB::table('size_group_size')->delete();
		SizeGroupSize::create(array('size_group_id' => '1', 'size_id' => '1'));
		SizeGroupSize::create(array('size_group_id' => '1', 'size_id' => '3'));
		SizeGroupSize::create(array('size_group_id' => '1', 'size_id' => '4'));
		SizeGroupSize::create(array('size_group_id' => '2', 'size_id' => '2'));
		SizeGroupSize::create(array('size_group_id' => '2', 'size_id' => '3'));
		SizeGroupSize::create(array('size_group_id' => '2', 'size_id' => '4'));
		SizeGroupSize::create(array('size_group_id' => '2', 'size_id' => '5'));
		SizeGroupSize::create(array('size_group_id' => '3', 'size_id' => '1'));
		SizeGroupSize::create(array('size_group_id' => '3', 'size_id' => '2'));
		SizeGroupSize::create(array('size_group_id' => '3', 'size_id' => '3'));
		SizeGroupSize::create(array('size_group_id' => '4', 'size_id' => '4'));
		SizeGroupSize::create(array('size_group_id' => '4', 'size_id' => '5'));
		SizeGroupSize::create(array('size_group_id' => '4', 'size_id' => '6'));
	}

}