<?php
use Illuminate\Database\Seeder;
use App\Department;
class DepartmentSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('departments')->delete();
		Department::create(array('name' => 'Sales'));
		Department::create(array('name' => 'IT'));
		Department::create(array('name' => 'Marketing'));
		Department::create(array('name' => 'Accounts'));
	}

}