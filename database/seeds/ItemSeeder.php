<?php
use Illuminate\Database\Seeder;
use App\Item;
class ItemSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('item')->delete();
		Item::create(array('item_name' => 'Spicy Chicken', 'group_id' => 1, 'sub_group_id' => 1, 'parameter_id' => 1, 'color_id' => 1, 'item_code' => 'randomcode_13425', 'cutability_factor' => 1, 'cutable_width' => 1, 'store_unit' => 1, 'purchase_unit' => 5, 'store_conversion' => 1, 'purchase_conversion' => 5));
		Item::create(array('item_name' => 'Opium', 'group_id' => 2, 'sub_group_id' => 5, 'parameter_id' => 2, 'color_id' => 2, 'item_code' => 'randomcode_34534', 'cutability_factor' => 2, 'cutable_width' => 2, 'store_unit' => 2, 'purchase_unit' => 4, 'store_conversion' => 2, 'purchase_conversion' => 4));
		Item::create(array('item_name' => 'Cola', 'group_id' => 3, 'sub_group_id' => 9, 'parameter_id' => 3, 'color_id' => 3, 'item_code' => 'randomcode_17899', 'cutability_factor' => 3, 'cutable_width' => 3, 'store_unit' => 3, 'purchase_unit' => 3, 'store_conversion' => 3, 'purchase_conversion' => 3));
		Item::create(array('item_name' => 'Durex', 'group_id' => 2, 'sub_group_id' => 6, 'parameter_id' => 4, 'color_id' => 4, 'item_code' => 'randomcode_78953', 'cutability_factor' => 4, 'cutable_width' => 4, 'store_unit' => 4, 'purchase_unit' => 2, 'store_conversion' => 4, 'purchase_conversion' => 2));
		Item::create(array('item_name' => 'Platinum', 'group_id' => 1, 'sub_group_id' => 3, 'parameter_id' => 2, 'color_id' => 5, 'item_code' => 'randomcode_23456', 'cutability_factor' => 5, 'cutable_width' => 5, 'store_unit' => 5, 'purchase_unit' => 1, 'store_conversion' => 5, 'purchase_conversion' => 1));
	}

}