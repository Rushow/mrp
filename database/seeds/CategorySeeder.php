<?php
use Illuminate\Database\Seeder;
use App\Category;
class CategorySeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('category')->delete();
		Category::create(array('category_name' => 'Shoes'));
		Category::create(array('category_name' => 'Sandal'));
	}

}