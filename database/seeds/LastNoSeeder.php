<?php
use Illuminate\Database\Seeder;
use App\LastNo;
class LastNoSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('last_no')->delete();

		for ($i=0; $i < 10; $i++) {
			LastNo::create(array('last_no' => 'number ' . $i));
		}
	}

}