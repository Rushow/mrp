<?php
use Illuminate\Database\Seeder;
use App\Unit;
class UnitSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('unit')->delete();
		Unit::create(array('unit_name' => 'Kg'));
		Unit::create(array('unit_name' => 'g'));
		Unit::create(array('unit_name' => 'm'));
		Unit::create(array('unit_name' => 'l'));
		Unit::create(array('unit_name' => 'pa'));
	}

}