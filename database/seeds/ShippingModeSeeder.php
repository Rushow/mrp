<?php
use Illuminate\Database\Seeder;
use App\ShippingMode;
class ShippingModeSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('shipping_mode')->delete();
		ShippingMode::create(array('shipping_mode' => 'Sea'));
		ShippingMode::create(array('shipping_mode' => 'Air'));
		ShippingMode::create(array('shipping_mode' => 'Sea-Air'));
	}

}