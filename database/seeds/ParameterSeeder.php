<?php
use Illuminate\Database\Seeder;
use App\Parameter;
class ParameterSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('parameter')->delete();
		Parameter::create(array('parameter_name' => 'Severe'));
		Parameter::create(array('parameter_name' => 'Temporate'));
		Parameter::create(array('parameter_name' => 'Dry'));
		Parameter::create(array('parameter_name' => 'Windy'));
	}

}