<?php
use Illuminate\Database\Seeder;
use App\Color;
class ColorSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('color')->delete();

		Color::create(array('pantone_code' => 'PANTONE 15-4703 TCX', 'hm_code' => '56-314', 'color_name_flcl' => 'Black'));
		Color::create(array('pantone_code' => 'PANTONE 15-4703 TCX', 'hm_code' => '56-315', 'color_name_flcl' => 'White'));
		Color::create(array('pantone_code' => 'PANTONE 15-4101 TCX', 'hm_code' => '56-316', 'color_name_flcl' => 'Yellow'));
		Color::create(array('pantone_code' => 'PANTONE 15-4101 TCX', 'hm_code' => '56-317', 'color_name_flcl' => 'Green'));
		Color::create(array('pantone_code' => 'PANTONE 14-4201 TCX', 'hm_code' => '56-318', 'color_name_flcl' => 'Red'));
		Color::create(array('pantone_code' => 'PANTONE 14-4201 TCX', 'hm_code' => '56-319', 'color_name_flcl' => 'Blue'));
	}

}