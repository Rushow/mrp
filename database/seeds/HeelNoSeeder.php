<?php
use Illuminate\Database\Seeder;
use App\HeelNo;
class HeelNoSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('heel_no')->delete();

		for ($i=0; $i < 10; $i++) {
			HeelNo::create(array('heel_no' => 'number ' . $i));
		}
	}

}