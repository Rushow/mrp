<?php
use Illuminate\Database\Seeder;
use App\UserType;
class UserTypeSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('user_type')->delete();

		UserType::create(array('user_type' => 'Super Admin'));
		UserType::create(array('user_type' => 'Admin'));
		UserType::create(array('user_type' => 'Super User'));
        UserType::create(array('user_type' => 'User'));

	}

}