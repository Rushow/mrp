<?php
use Illuminate\Database\Seeder;
use App\Season;
class SeasonSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('season')->delete();

		Season::create(array('season' => 'Summer', 'start_date' => '1445990400', 'end_date' => '1448323200'));
		Season::create(array('season' => 'Spring', 'start_date' => '1445990400', 'end_date' => '1448323200'));
		Season::create(array('season' => 'Autumn', 'start_date' => '1445990400', 'end_date' => '1448323200'));
		Season::create(array('season' => 'Fall', 'start_date' => '1445990400', 'end_date' => '1448323200'));
	}

}