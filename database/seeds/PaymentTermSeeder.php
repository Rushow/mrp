<?php
use Illuminate\Database\Seeder;
use App\PaymentTerm;
class PaymentTermSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('payment_term')->delete();
		PaymentTerm::create(array('term' => 'Daily'));		
		PaymentTerm::create(array('term' => 'Weekly'));
		PaymentTerm::create(array('term' => 'Monthly'));
		PaymentTerm::create(array('term' => 'Yearly'));
	}

}