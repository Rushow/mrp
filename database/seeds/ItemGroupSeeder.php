<?php
use Illuminate\Database\Seeder;
use App\ItemGroup;
class ItemGroupSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('item_group')->delete();
		ItemGroup::create(array('group_name' => 'Textile'));
		ItemGroup::create(array('group_name' => 'Electronics'));
		ItemGroup::create(array('group_name' => 'Agro'));
	}

}