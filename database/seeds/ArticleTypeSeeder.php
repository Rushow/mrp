<?php
use Illuminate\Database\Seeder;
use App\ArticleType;
class ArticleTypeSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('article_type')->delete();
		ArticleType::create(array('article_type' => 'Elegant'));
		ArticleType::create(array('article_type' => 'Classic'));
		ArticleType::create(array('article_type' => 'Economic'));
		ArticleType::create(array('article_type' => 'Productive'));
		ArticleType::create(array('article_type' => 'Centered'));
	}

}