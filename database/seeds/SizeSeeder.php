<?php
use Illuminate\Database\Seeder;
use App\Size;
class SizeSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('size')->delete();
		Size::create(array('size_no' => '35'));
		Size::create(array('size_no' => '36'));
		Size::create(array('size_no' => '37'));
		Size::create(array('size_no' => '38'));
		Size::create(array('size_no' => '39'));
		Size::create(array('size_no' => '40'));
	}

}