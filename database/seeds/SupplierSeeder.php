<?php
use Illuminate\Database\Seeder;
use App\Supplier;
class SupplierSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('supplier')->delete();

		Supplier::create(array('supplier_name' => 'Martin Dennis'));
		Supplier::create(array('supplier_name' => 'Carlson Morris'));
		Supplier::create(array('supplier_name' => 'Haider Hussain'));
		Supplier::create(array('supplier_name' => 'Dave Robertson'));
		Supplier::create(array('supplier_name' => 'Elizabeth Berthouse'));
	}

}