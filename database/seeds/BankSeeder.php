<?php
use Illuminate\Database\Seeder;
use App\Bank;
class BankSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('bank')->delete();

		Bank::create(array('name' => 'Brac Bank', 'swift_code' => '', 'account_no' => '', 'address_id' => '1', 'created_by' => '1', 'updated_by' => '1'));
		Bank::create(array('name' => 'Dutch Bangla Bank', 'swift_code' => '', 'account_no' => '', 'address_id' => '2'));
		Bank::create(array('name' => 'Standard Chartard Bank', 'swift_code' => '', 'account_no' => '', 'address_id' => '1'));
		Bank::create(array('name' => 'AB Bank', 'swift_code' => '', 'account_no' => '', 'address_id' => '3'));
		Bank::create(array('name' => 'UCB Bank', 'swift_code' => '', 'account_no' => '', 'address_id' => '2'));
	}

}