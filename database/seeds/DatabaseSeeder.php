<?php
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		
		$this->call('ArticleTypeSeeder');
		$this->command->info('Article Type table seeded!');

//		$this->call('BuyerSeeder');
//		$this->command->info('Buyer table seeded!');

		$this->call('CategorySeeder');
		$this->command->info('Category table seeded!');

//		$this->call('ColorSeeder');
//		$this->command->info('Color table seeded!');

		$this->call('ComponentSeeder');
		$this->command->info('Component table seeded!');

		$this->call('ConstructionSeeder');
		$this->command->info('Construction table seeded!');

		$this->call('CustomerGroupSeeder');
		$this->command->info('Customer Group table seeded!');

		$this->call('HeelNoSeeder');
		$this->command->info('Heel No table seeded!');

		$this->call('ItemGroupSeeder');
		$this->command->info('Item Group table seeded!');

		$this->call('ItemSubGroupSeeder');
		$this->command->info('Item Sub Group table seeded!');

		$this->call('LastNoSeeder');
		$this->command->info('Last No table seeded!');

		$this->call('OutsoleNoSeeder');
		$this->command->info('Outsole No table seeded!');

		$this->call('ParameterSeeder');
		$this->command->info('Parameter table seeded!');

		$this->call('SampleTypeSeeder');
		$this->command->info('Sample Type table seeded!');

		$this->call('ShippingModeSeeder');
		$this->command->info('Shipping Mode table seeded!');

		$this->call('SizeGroupSeeder');
		$this->command->info('Size Group table seeded!');

		$this->call('SizeSeeder');
		$this->command->info('Size table seeded!');

		$this->call('SupplierSeeder');
		$this->command->info('Supplier table seeded!');

		$this->call('TermsOfDeliverySeeder');
		$this->command->info('Terms of Delivery table seeded!');

		$this->call('UnitSeeder');
		$this->command->info('Unit table seeded!');

		$this->call('UserSeeder');
		$this->command->info('User table seeded!');

		$this->call('ArticleSeeder');
		$this->command->info('Article table seeded!');

		$this->call('WarehouseSeeder');
		$this->command->info('Warehouse table seeded!');

		$this->call('CurrencySeeder');
		$this->command->info('Currency table seeded!');

		$this->call('ItemSeeder');
		$this->command->info('Item table seeded!');

		$this->call('DepartmentSeeder');
		$this->command->info('Department table seeded!');

		$this->call('SampleOrderSeeder');
		$this->command->info('Sample Order table seeded!');

//		$this->call('SeasonSeeder');
//		$this->command->info('Season table seeded!');

//		$this->call('DestinationSeeder');
//		$this->command->info('Destination table seeded!');

		$this->call('PaymentTermSeeder');
		$this->command->info('Payment Term table seeded!');

		$this->call('UserTypeSeeder');
		$this->command->info('User Type table seeded!');

		$this->call('BuyerTemplateSeeder');
		$this->command->info('Buyer Template table seeded!');

		$this->call('StatusSeeder');
		$this->command->info('Status table seeded!');

		$this->call('BankSeeder');
		$this->command->info('Bank table seeded!');

		$this->call('ForwarderSeeder');
		$this->command->info('Forwarder table seeded!');

		$this->call('DocumentTypeSeeder');
		$this->command->info('Document Type table seeded!');
	}

}