<?php
use Illuminate\Database\Seeder;
use App\Destination;
class DestinationSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('destination')->delete();
		Destination::create(array('destination_name' => 'Los Angeles', 'destination_code' => 'L033', 'cut_off_day' => 'Monday', 'cut_off_diff' => '0'));
		Destination::create(array('destination_name' => 'Dhaka', 'destination_code' => 'D431', 'cut_off_day' => 'Monday', 'cut_off_diff' => '0'));
		Destination::create(array('destination_name' => 'Chennai', 'destination_code' => 'C219', 'cut_off_day' => 'Monday', 'cut_off_diff' => '0'));
		Destination::create(array('destination_name' => 'Dubai', 'destination_code' => 'D110', 'cut_off_day' => 'Monday', 'cut_off_diff' => '0'));
	}

}