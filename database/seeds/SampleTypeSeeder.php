<?php
use Illuminate\Database\Seeder;
use App\SampleType;
class SampleTypeSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('sample_type')->delete();
		SampleType::create(array('sample_type' => 'Original'));
		SampleType::create(array('sample_type' => 'Burmese'));
		SampleType::create(array('sample_type' => 'Colonial'));
		SampleType::create(array('sample_type' => 'Ancient'));
	}

}