<?php
use Illuminate\Database\Seeder;
use App\DocumentType;
class DocumentTypeSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('document_type')->delete();
		DocumentType::create(array('document_type' => 'Image'));
		DocumentType::create(array('document_type' => 'PO File'));
	}

}