<?php
use Illuminate\Database\Seeder;
use App\Forwarder;
class ForwarderSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('bank')->delete();

		Forwarder::create(array('forwarder_name' => 'Forwarder 01'));
		Forwarder::create(array('forwarder_name' => 'Forwarder 02'));
		Forwarder::create(array('forwarder_name' => 'Forwarder 03'));
		Forwarder::create(array('forwarder_name' => 'Forwarder 04'));
		Forwarder::create(array('forwarder_name' => 'Forwarder 05'));
	}

}