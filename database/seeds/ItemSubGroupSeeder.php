<?php
use Illuminate\Database\Seeder;
use App\ItemSubGroup;
class ItemSubGroupSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('item_sub_group')->delete();
		ItemSubGroup::create(array('sub_group_name' => 'T-Shirt', 'group_id' => 1));
		ItemSubGroup::create(array('sub_group_name' => 'Jackets', 'group_id' => 1));
		ItemSubGroup::create(array('sub_group_name' => 'Jeans', 'group_id' => 1));

		ItemSubGroup::create(array('sub_group_name' => 'Mobile', 'group_id' => 2));
		ItemSubGroup::create(array('sub_group_name' => 'PCs', 'group_id' => 2));
		ItemSubGroup::create(array('sub_group_name' => 'Macs', 'group_id' => 2));

		ItemSubGroup::create(array('sub_group_name' => 'Eggs', 'group_id' => 3));
		ItemSubGroup::create(array('sub_group_name' => 'Wheat', 'group_id' => 3));
		ItemSubGroup::create(array('sub_group_name' => 'Oranges', 'group_id' => 3));
	}

}