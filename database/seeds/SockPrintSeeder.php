<?php
use Illuminate\Database\Seeder;
use App\SockPrint;
class SockPrintSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('sock_print')->delete();

        SockPrint::create(array('sock_print' => 'HM 14115'));
        SockPrint::create(array('sock_print' => 'HM 14116'));
        SockPrint::create(array('sock_print' => 'HM 14117'));

	}

}