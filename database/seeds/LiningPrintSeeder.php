<?php
use Illuminate\Database\Seeder;
use App\LiningPrint;
class LiningPrintSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('lining_print')->delete();

        LiningPrint::create(array('lining_print' => 'HM 14115'));
        LiningPrint::create(array('lining_print' => 'HM 14116'));
        LiningPrint::create(array('lining_print' => 'HM 14117'));

	}

}