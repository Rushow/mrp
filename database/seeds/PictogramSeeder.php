<?php
use Illuminate\Database\Seeder;
use App\Pictogram;
class PictogramSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('pictogram')->delete();

        Pictogram::create(array('pictogram' => 'HM 84002'));
        Pictogram::create(array('pictogram' => 'HM 84003'));
        Pictogram::create(array('pictogram' => 'HM 84004'));

	}

}