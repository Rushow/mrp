<?php
use Illuminate\Database\Seeder;
use App\Currency;
class CurrencySeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('currencies')->delete();
		Currency::create(array('name' => 'American Dollar', 'abbr' => 'USD'));
		Currency::create(array('name' => 'Bangladeshi Taka', 'abbr' => 'BDT'));
		Currency::create(array('name' => 'Europian Euro', 'abbr' => 'EUR'));
		Currency::create(array('name' => 'Indian Rupee', 'abbr' => 'RUP'));
	}

}