<?php
use Illuminate\Database\Seeder;
use App\BuyerTemplate;
class BuyerTemplateSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('buyer_template')->delete();
		BuyerTemplate::create(array('buyer_template' => 'Template 01'));
		BuyerTemplate::create(array('buyer_template' => 'Template 02'));
	}
}