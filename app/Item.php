<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'item';
    protected $primaryKey = 'id';

    function Group()
    {
        return $this->belongsTo('App\ItemGroup', 'group_id');
    }

    function SubGroup()
    {
        return $this->belongsTo('App\ItemSubGroup', 'sub_group_id');
    }

    function Parameter()
    {
        return $this->belongsTo('App\Parameter');
    }

    function Color()
    {
        return $this->belongsTo('App\Color', 'color_id');
    }

    function StoreUnit()
    {
        return $this->belongsTo('App\Unit', 'store_unit');
    }

    function PurchaseUnit()
    {
        return $this->belongsTo('App\Unit', 'purchase_unit');
    }

    function scopeWithSubGroup($query, $id)
    {
        return $query->where('sub_group_id', '=', $id);
    }

}