<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyProductionComponentCombination extends Model
{
    protected $table = 'daily_production_component_combination';
    protected $primaryKey = 'id';

    function Comboname(){

        return $this->belongsTo('App\DailyProductionComponent', 'sub_component_id');
    }
}
