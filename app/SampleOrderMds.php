<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class SampleOrderMds extends Model
{
    protected $table = 'sample_order_mds';
    protected $primaryKey = 'id';

    function SampleOrderDetail()
    {
        return $this->belongsTo('App\sampleOrderDetail', 'sample_order_detail_id');
    }

    function LastNo()
    {
        return $this->belongsTo('App\LastNo', 'last_no_id');
    }

    function OutsoleNo()
    {
        return $this->belongsTo('App\OutsoleNo', 'outsole_no_id');
    }

    function HeelNo()
    {
        return $this->belongsTo('App\HeelNo', 'heel_no_id');
    }

}