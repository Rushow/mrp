<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class SampleOrderMdsDesc extends Model
{
    protected $table = 'sample_order_mds_desc';
    protected $primaryKey = 'id';

    function Component()
    {
        return $this->belongsTo('App\Component', 'component_id');
    }

    function Group()
    {
        return $this->belongsTo('App\ItemGroup', 'group_id');
    }

    function SubGroup()
    {
        return $this->belongsTo('App\ItemSubGroup', 'sub_group_id');
    }

    function Parameter()
    {
        return $this->belongsTo('App\Parameter', 'parameter_id');
    }

    function Item()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }

    function Color()
    {
        return $this->belongsTo('App\Color', 'color_id');
    }

}