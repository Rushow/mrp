<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class ProductionOrderDetailQuantity extends Model
{
    protected $table = 'production_order_detail_quantity';
    protected $primaryKey = 'id';

    function ProductionOrder()
    {
        return $this->belongsTo('App\ProductionOrder', 'production_order_id');
    }
    function Article()
    {
        return $this->belongsTo('App\Article', 'article_id');
    }
    function Destination()
    {
        return $this->belongsTo('App\Destination', 'destination_id');
    }
    function Color()
    {
        return $this->belongsTo('App\Color', 'color_id');
    }


}