<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Address extends Model
{
    protected $table = 'addresses';

    function Buyer()
    {
        return $this->hasMany('App\Buyer', 'address_id');
    }

    function Supplier()
    {
        return $this->hasMany('App\Supplier', 'address_id');
    }

    function scopeExists($query, $address_given)
    {
    	$addresses = Address::all();
    	foreach ($addresses as $key => $address) {
    		if (
    			$address->floor === $address_given['floor'] && 
    			$address->street_address === $address_given['street_address'] && 
    			$address->city === $address_given['city'] && 
    			$address->state === $address_given['state'] && 
    			$address->zip === $address_given['zip'] && 
    			$address->country === $address_given['country']
    			) {
    			return $address->id;
    		}
    	}
    	return -1;
    }

}