<?php

class DevelopmentStatus extends Model
{
    protected $table = 'development_statuses';
    // protected $fillable = array('schedule_date', 'sample_detail_id');

    function sample_status()
    {
        return $this->belongsTo('App\SampleStatus', 'sample_status_id');
    }

}