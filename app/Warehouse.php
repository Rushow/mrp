<?php
namespace App;  use Illuminate\Database\Eloquent\Model;
class Warehouse extends Model
{
    protected $table = 'warehouses';
    protected $primaryKey = 'id';

    function StoreIns()
    {
    	return $this->hasMany('App\StoreIn', 'store_in_id');
    }
}