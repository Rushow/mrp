<?php
namespace App;  use Illuminate\Database\Eloquent\Model;
class StoreOut extends Model
{
    protected $table = 'store_outs';
    protected $primaryKey = 'id';
    protected $fillable = array('warehouse_id', 'department_id', 'challan_no', 'order_no_id', 'store_out_date', 'remarks');

    function Details()
    {
    	return $this->hasMany('App\StoreOutDetail', 'store_out_id');
    }
    function Warehouse()
    {
    	return $this->belongsTo('App\Warehouse', 'warehouse_id');
    }
    function Department()
    {
    	return $this->belongsTo('App\Department', 'department_id');
    }

    function SampleOrder()
    {
        return $this->belongsTo('App\SampleOrder', 'order_no_id');
    }

    function ProductionOrder()
    {
        return $this->belongsTo('App\ProductionOrder', 'production_order_no');
    }
}