<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyerTemplate extends Model
{
    protected $table = 'buyer_template';

}