<?php

class StoreOutDetail extends Model
{
    protected $table = 'store_out_details';
    protected $primaryKey = 'id';
    protected $fillable = array('store_out_id', 'item_group_id', 'sub_group_id', 'color_id', 'parameter_id', 'item_id', 'unit_id', 'quantity');

    function StoreOut()
    {
        return $this->belongsTo('App\StoreOut', 'store_out_id');
    }
    function ItemGroup()
    {
        return $this->belongsTo('App\ItemGroup', 'item_group_id');
    }
    function ItemSubGroup()
    {
        return $this->belongsTo('App\ItemSubGroup', 'sub_group_id');
    }
    function Color()
    {
        return $this->belongsTo('App\Color', 'color_id');
    }
    function Parameter()
    {
        return $this->belongsTo('App\Parameter', 'parameter_id');
    }
    function Item()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }
    function Unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id');
    }
}