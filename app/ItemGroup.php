<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class ItemGroup extends Model
{
    protected $table = 'item_group';
    protected $primaryKey = 'id';

    function item(){
        return $this->hasMany('App\Item');
    }

    function subGroup(){
        return $this->hasMany('App\ItemSubGroup');
    }

    function storeInDetails(){
        return $this->hasMany('App\StoreInDetail');
    }

}