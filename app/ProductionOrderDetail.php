<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class ProductionOrderDetail extends Model
{
    protected $table = 'production_order_detail';
    protected $primaryKey = 'id';

    function ProductionOrder()
    {
        return $this->belongsTo('App\ProductionOrder', 'production_order_id');
    }
    function Article()
    {
        return $this->belongsTo('App\Article', 'article_id');
    }
    function Sizes(){
        return $this->belongsToMany('App\Size', 'production_order_detail_size');
    }
    function Destination(){
        return $this->belongsTo('App\Destination', 'destination_id');
    }
    function Color()
    {
        return $this->belongsTo('App\Color', 'color_id');
    }

//    function DetailSizes(){
//        return $this->belongsToMany('ProductionOrderDetailSize');
//    }

}