<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function IsSuperAdmin()
    {
        return ($this->user_type_id == 1);
    }

    public function IsAdmin()
    {
        return ($this->user_type_id == 2);
    }

    public function IsSuperUser()
    {
        return ($this->user_type_id == 3);
    }

    public function isViewer()
    {

        return ($this->user_type_id == 4);
    }

    public function UserType()
    {
        $role = $this->user_type_id;
        switch ($role) {
            case 1:
                return 'Super Admin';
                break;

            case 2:
                return 'Admin';
                break;

            case 3:
                return 'Super User';
                break;

            case 4:
                return 'User';
                break;
        }
    }

}
