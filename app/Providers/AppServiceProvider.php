<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Division;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['dailyproductionmenu.productionmenu'], function ($view)  {

            $division = Division::where("flag_ifshown","=",null)->get();
           
            $view->with('divisonmenu', $division);

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
