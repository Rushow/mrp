<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\SampleOrderSpec;
use App\SampleOrder;
use App\Buyer;
use App\Article;
use App\SampleType;
use Validator;
use View;

class SampleOrderSpecController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index(Request $request)
	{
        $filters = $request->only('sample_order_no','buyer_id','article_id', 'sample_type_id', 'order_date','delivery_date');

        $sample_order = $filters['sample_order_no'];
        $buyer = $filters['buyer_id'];
        $article = $filters['article_id'];
        $sample_type = $filters['sample_type_id'];
        $order_date = $filters['order_date'];
        $delivery_date = $filters['delivery_date'];

        $data['sample_order_no'] = array('' => 'Select Sample Order No') + SampleOrder::pluck('sample_order_no','id')->toArray();
        $data['buyer'] = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $data['article'] = array('' => 'Select Article No') + Article::pluck('article_no','id')->toArray();
        $data['sample_type'] = array('' => 'Select Type of Sample') + SampleType::pluck('sample_type','id')->toArray();
        $data['orders'] = SampleOrderSpec::all();
        return view('sample_order_spec.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($id)
	{
        $data = array();
		$order_details = SampleOrderDetail::find($id);
        $order = SampleOrder::find($order_details['sample_order_id']);
        if($order){
            $buyer = SampleOrder::find($order['id'])->Buyer;
            $order['buyer_name'] = $buyer['buyer_name'];
        }
        if($order_details){
            $article        = SampleOrderDetail::find($order_details['id'])->Article;
            $sampleType     = SampleOrderDetail::find($order_details['id'])->SampleType;
            $size           = SampleOrderDetail::find($order_details['id'])->Sizes;
            $i = 0;
            $sizes = array();
            foreach($size as $s){
                $sizes[$i] = Size::find($s['size_id']);
                $i++;
            }
            $order_details['article_no']    = $article['article_no'];
            $order_details['article_pic']   = $article['article_pic'];
            $order_details['sample_type']   = $sampleType['sample_type'];
            $order_details['size']          = $sizes;
        }

        $data['id'] = $id;
        $data['last_no']        = array('' => 'Select Last No') + LastNo::pluck('last_no','id')->toArray();
        $data['outsole_no']     = array('' => 'Select Outsole No') + OutsoleNo::pluck('outsole_no','id')->toArray();
        $data['heel_no']        = array('' => 'Select Heel No') + HeelNo::pluck('heel_no','id')->toArray();
        $data['component']      = array('' => 'Select Component') + Component::pluck('component_name','id')->toArray();
        $data['item_group']     = array('' => 'Select Group') + ItemGroup::pluck('group_name','id')->toArray();
        $data['item_sub_group'] = array('' => 'Select Sub Group') + ItemSubGroup::pluck('sub_group_name','id')->toArray();
        $data['parameter']      = array('' => 'Select Parameter') + Parameter::pluck('parameter_name','id')->toArray();
        $data['item']           = array('' => 'Select Material') + Item::pluck('item_name','id')->toArray();
        $data['color']          = array('' => 'Select Color') + Color::pluck('color_name_flcl','id')->toArray();



        return view('sample_order_spec.create', $data)->with('order_details', $order_details)->with('order', $order);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $spec_input = $request->only('sample_order_detail_id', 'spec_no', 'last_no_id', 'outsole_no_id', 'heel_no_id', 'logo_position','logo_type', 'print_type', 'print_color', 'note');
        $id = Input::get('id');
        $rules =array(
            'sample_order_detail_id' => array('required'),
            'spec_no' => array('required'),
            'last_no_id' => array('required'),
            'outsole_no_id' => array('required'),
            'heel_no_id' => array('required'),
            'logo_position' => array('required'),
            'logo_type' => array('required'),
            'print_type' => array('required'),
            'print_color' => array('required')
        );


        $validator = Validator::make($spec_input,$rules);
//        $spec_input['created_at'] = new DateTime();
        if ($validator->passes()){
            SampleOrderSpec::insert($spec_input);
            $spec_item = $this->getLastSpec();
            $spec_item_id = $spec_item->id;

            $sample_order_detail_id = $request->get('sample_order_detail_id');
            $size           = SampleOrderDetail::find($sample_order_detail_id)->Sizes;
            $i = 1;

            foreach($size as $s){

                $size_input['size_id'] = $request->get('size_id_'.$i);
                $size_input['quantity'] = $request->get('quantity_'.$i);
//                $size_input['updated_at'] = new DateTime;

                SampleOrderDetailsSize::where('size_id','=',$size_input['size_id'])->where('sample_order_detail_id','=',$sample_order_detail_id, 'AND')->update($size_input);
                $i++;
            }


            $input = Input::only('total_row');
            $total_row = $input['total_row'];
            for($i=1; $i<=$total_row;$i++){
                if(Input::get('component_id_'.$i)){
                    $list_input = array(array('sample_spec_id'=>$spec_item_id, 'component_id'=>$request->get('component_id_'.$i), 'group_id'=>$request->get('group_id_'.$i), 'sub_group_id'=>$request->get('sub_group_id_'.$i), 'parameter_id'=>$request->get('parameter_id_'.$i), 'item_id'=>$request->get('item_id_'.$i), 'color_id_1'=>$request->get('color_id_1_'.$i), 'color_id_2'=>$request->get('color_id_2_'.$i), 'color_id_3'=>$request->get('color_id_3_'.$i), 'note'=>$request->get('note_'.$i), 'created_at'=>new DateTime()));

                    SampleOrderSpecMaterials::insert($list_input);

                }
            }
            Session::flash('message', 'Successfully created a Sample Order!');
            return Redirect::to('sampleOrderSpec/'.$id);
        }
        else{
            return Redirect::to('sampleOrderSpec/create/'.$id)->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = array();
        $sample_spec = SampleOrderSpec::find($id);

        $spec_materials = SampleOrderSpecMaterials::where('sample_spec_id', '=', $id)->get();
        foreach($spec_materials as $material){
            $component = SampleOrderSpecMaterials::find($material['id'])->component;
            $group = SampleOrderSpecMaterials::find($material['id'])->group;
            $sub_group = SampleOrderSpecMaterials::find($material['id'])->subGroup;
            $parameter = SampleOrderSpecMaterials::find($material['id'])->parameter;
            $item = SampleOrderSpecMaterials::find($material['id'])->item;
            $color_1 = SampleOrderSpecMaterials::find($material['id'])->color1;
            $color_2 = SampleOrderSpecMaterials::find($material['id'])->color2;
            $color_3 = SampleOrderSpecMaterials::find($material['id'])->color3;

            $material['component_name'] = $component['component_name'];
            $material['group_name'] = $group['group_name'];
            $material['sub_group_name'] = $sub_group['sub_group_name'];
            $material['parameter_name'] = $parameter['parameter_name'];
            $material['item_name'] = $item['item_name'];
            $material['color_1_name'] = $color_1['color_name_flcl'];
            $material['color_2_name'] = $color_2['color_name_flcl'];
            $material['color_3_name'] = $color_3['color_name_flcl'];
        }

//        echo '<pre>'; print_r($sample_spec);die;
        $order_details = SampleOrderDetail::find($sample_spec->sample_order_detail_id);

        if($order_details){
            $article        = SampleOrderDetail::find($order_details['id'])->Article;
            $sampleType     = SampleOrderDetail::find($order_details['id'])->SampleType;
            $size           = SampleOrderDetail::find($order_details['id'])->Sizes;
            $i = 0;
            $sizes = array();
            foreach($size as $s){
                $sizes[$i] = Size::find($s['size_id']);
                $i++;
            }
            $order_details['article_no']    = $article['article_no'];
            $order_details['article_pic']   = $article['article_pic'];
            $order_details['sample_type']   = $sampleType['sample_type'];
            $order_details['size']          = $sizes;
        }
        $order = SampleOrder::find($order_details->sample_order_id);

        return view('sample_order_spec.show', $data)
                                    ->with('order_details', $order_details)
                                    ->with('order', $order)
                                    ->with('sample_spec', $sample_spec)
                                    ->with('spec_materials', $spec_materials);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    function addOrderSpecRow(Request $request){
        $info = $request->only('last_id');
        if($info){
            $last_id = $info['last_id'] + 1;
            $component      = array('' => 'Select Component') + Component::pluck('component_name','id')->toArray();
            $item_group     = array('' => 'Select Group') + ItemGroup::pluck('group_name','id')->toArray();
            $item_sub_group = array('' => 'Select Sub Group') + ItemSubGroup::pluck('sub_group_name','id')->toArray();
            $parameter      = array('' => 'Select Parameter') + Parameter::pluck('parameter_name','id')->toArray();
            $item           = array('' => 'Select Material') + Item::pluck('item_name','id')->toArray();
            $color          = array('' => 'Select Color') + Color::pluck('color_name','id')->toArray();


            $msg = "Are you sure to delete this item???";

            $html = "<td>".Form::select('component_id_'.$last_id, $component, '', $attributes = array('id'=>'component_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::select('group_id_'.$last_id, $item_group, '', $attributes = array('id'=>'group_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required', 'onChange'=>'generateSubGroup(this);'))."</td>";
            $html .= "<td>".Form::select('sub_group_id_'.$last_id, $item_sub_group, '', $attributes = array('id'=>'sub_group_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required', 'disabled'=>'disabled', 'onChange'=>'generateItem(this);'))."</td>";
            $html .= "<td>".Form::select('parameter_id_'.$last_id, $parameter, '', $attributes = array('id'=>'parameter_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::select('item_id_'.$last_id, $item, '', $attributes = array('id'=>'item_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required', 'disabled'=>'disabled'))."</td>";
            $html .= "<td>".Form::select('color_id_1_'.$last_id, $color, '', $attributes = array('id'=>'color_id_1_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::select('color_id_2_'.$last_id, $color, '', $attributes = array('id'=>'color_id_2_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::select('color_id_3_'.$last_id, $color, '', $attributes = array('id'=>'color_id_3_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::text('note_'.$last_id, '', $attributes = array('class'=>'form-control','id'=>'note_'.$last_id))."</td>";
            $html .= "<td><a href='#' onclick='return checkDelete(this);'><span class='badge badge-important'>X</span></a></td>";

            $data = array();
            $data['error'] = false;
            $data['html'] = $html;
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

    private function getLastSpec()
    {
        return DB::table('sample_order_spec')->orderBy('id', 'desc')->first();
    }

    private function getLastSpecMaterial()
    {
        return DB::table('sample_order_spec_material')->orderBy('id', 'desc')->first();
    }

}