<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\LiningPrint;
use View;
use Validator;

class LiningPrintController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $linings = LiningPrint::all();

        return view('lining_print.index')->with('linings', $linings);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data = array();
        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');
        return view('lining_print.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'lining_print'     => 'required',
            'status'           => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('liningPrint/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type = new LiningPrint;

            $type->lining_print       = $request->get('lining_print');
            $type->status             = $request->get('status');
            $type->enable_date        = strtotime($request->get('enable_date'));

            $type->save();
            Session::flash('message', 'Successfully created a lining print!');
            return Redirect::to('liningPrint');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['lining_print'] = LiningPrint::find($id);

        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');

        $this->layout->content = View::make('lining_print.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'lining_print'       => 'required',
            'status'             => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('liningPrint/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type['lining_print'] = $request->get('lining_print');
            $type['status']       = $request->get('status');
            $type['enable_date']  = strtotime($request->get('enable_date'));
            $type['updated_at'] = new DateTime;

            LiningPrint::where('id','=',$id)->update($type);

            Session::flash('message', 'Successfully updated a lining print!');
            return Redirect::to('liningPrint');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        LiningPrint::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a lining print!');
        return Redirect::to('liningPrint');
	}

}