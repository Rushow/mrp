<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\SizeGroup;
use App\size;
use App\SizeGroupSize;
use Validator;
use View;

class SizeGroupController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $groups = SizeGroup::all();
        return view('size_group.index')->with('groups', $groups);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data['sizes'] = Size::get()->toArray();
        return view('size_group.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'size_group_name'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('sizeGroup/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $size_group = new SizeGroup();
            $size_group->size_group_name       = $request->get('size_group_name');
            $size_group->save();

            $group_id = $size_group->id;

            $sizes       = $request->get('size');

            if(isset($sizes)){
                foreach($sizes as $size){
                    $sgs = new SizeGroupSize();
                    $sgs->size_group_id = $group_id;
                    $sgs->size_id = $size;
                    $sgs->save();
                }
            }

            Session::flash('message', 'Successfully created a size group!');
            return Redirect::to('sizeGroup');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = array();
        $data['size_group'] = SizeGroup::find($id);
        $data['size_group_size'] = SizeGroup::find($id)->size;
        return view('size_group.show', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['size_group'] = SizeGroup::find($id);
        $sizes = SizeGroup::find($id)->size->toArray();
        $i = 0;
        foreach($sizes as $size){
            $data['size_group_size'][$i] = $size['id'];
            $i++;
        }
        $data['sizes'] = Size::get()->toArray();

        return view('size_group.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'size_group_name'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('sizeGroup/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $size_group['size_group_name'] = $request->get('size_group_name');
            $size_group['updated_at'] = new DateTime;

            SizeGroup::where('id','=',$id)->update($size_group);

            $sizes = SizeGroup::find($id)->size->toArray();
            foreach($sizes as $size){
                SizeGroupSize::where('size_id','=',$size['id'])->where('size_group_id','=',$id)->delete();
            }

            $sizes       = $request->get('size');

            if(isset($sizes)){
                foreach($sizes as $size){
                    $sgs = new SizeGroupSize();
                    $sgs->size_group_id = $id;
                    $sgs->size_id = $size;
                    $sgs->save();
                }
            }

            Session::flash('message', 'Successfully updated a size group!');
            return Redirect::to('sizeGroup');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        SizeGroup::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a size group!');
        return Redirect::to('sizeGroup');


	}

    public function getSizesBySizeGroup(Request $request){
        $id = $request->get('size_group_id');
        if(isset($id)){
            $data = array();
            $data['error'] = false;
            $data['sizes'] = SizeGroup::find($id)->size->toArray();
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

}