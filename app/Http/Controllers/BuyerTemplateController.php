<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\AdditionalStickerInfo;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\BuyerTemplate;
use Validator;


class BuyerTemplateController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $templates = BuyerTemplate::all();

        return view('buyer_template.index')->with('templates', $templates);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data = array();
        return view('buyer_template.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request,$id)
	{
        $rules = array(
            'buyer_template'       => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('buyerTemplate/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type = new BuyerTemplate;

            $type->buyer_template   = $request->input('buyer_template');
            $type->created_by 		= Auth::user()->id;

            $type->save();
            Session::flash('message', 'Successfully created a buyer template!');
            return Redirect::to('buyerTemplate');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['buyer_template'] = BuyerTemplate::find($id);

        return view('buyer_template.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'buyer_template'       => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('BuyerTemplate/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type['buyer_template']     = $request->input('buyer_template');
            $buyer['updated_by'] 		= Auth::user()->id;
            $type['updated_at']         = new DateTime;

            BuyerTemplate::where('id','=',$id)->update($type);

            Session::flash('message', 'Successfully updated a buyer template!');
            return Redirect::to('buyerTemplate');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        BuyerTemplate::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a buyer template!');
        return Redirect::to('buyerTemplate');
	}

}