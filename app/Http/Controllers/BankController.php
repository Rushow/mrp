<?php


namespace App\Http\Controllers;

use App\Bank;
use Validator;
use App\Address;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Session;



class BankController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $banks = Bank::all();
        $data = array(
        	'banks' => $banks
        	);
//		print_r($banks);die;
       // $this->layout->content = View::make('bank.index', $data);
		return view('bank.index',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data = array();
        //$this->layout->content = View::make('bank.create', $data);
		return view('bank.create',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'name' => 'required'
        );
        $bank_details = $request->only('name', 'swift_code', 'account_no');
        $validator = Validator::make($bank_details, $rules);

        if ($validator->fails()){
            return Redirect::to('bank/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{
            $bank = new Bank;
            $bank['name'] 		= $request->get('name');
            $bank['swift_code'] 		= $request->get('swift_code');
            $bank['account_no'] 		= $request->get('account_no');
			$bank['created_by'] 		= Auth::user()->id;
			$bank['created_at'] 		= new DateTime();

            $address_input = $request->only('floor', 'street_address', 'city', 'state', 'zip', 'country');
	        $index = Address::Exists($address_input);   
	        if ($index > 0) {
	        	$bank->address_id = $index;
	        }else{
	        	$address = new Address;
	        	$address['floor'] 			= $address_input['floor'];
	        	$address['street_address'] 	= $address_input['street_address'];
	        	$address['city'] 			= $address_input['city'];
	        	$address['state'] 			= $address_input['state'];
	        	$address['zip'] 			= $address_input['zip'];
	        	$address['country'] 		= $address_input['country'];
	        	$address->save();
	            $bank->address_id = $address->id;
	        }
            $bank->save();
            Session::flash('message', 'Successfully created a bank!');
            return Redirect::to('bank');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$bank = Bank::find($id);
		$address = array();
		if ($bank->address_id) {
        		$address['floor'] 			= $bank->Address->floor;
        		$address['street_address'] 	= $bank->Address->street_address;
        		$address['city'] 			= $bank->Address->city;
        		$address['state'] 			= $bank->Address->state;
        		$address['zip'] 			= $bank->Address->zip;
        		$address['country'] 		= $bank->Address->country;
        	}else{
        		$address['floor'] 			= '';
        		$address['street_address'] 	= '';
        		$address['city'] 			= '';
        		$address['state'] 			= '';
        		$address['zip'] 			= '';
        		$address['country'] 		= '';
        	}
        $data = array(
        	'bank' => $bank,
        	'address' => $address
        	);

        //$this->layout->content = View::make('bank.edit', $data);
		return view('bank.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'name' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('bank/edit/'.$id)->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
		else{
	        $address_input = $request->only('floor', 'street_address', 'city', 'state', 'zip', 'country');
	        $index = Address::Exists($address_input);
	        if ($index > 0) {
	        	$bank['address_id'] = $index;
	        }
			else{
	        	$address = new Address;
	        	$address['floor'] 			= $address_input['floor'];
	        	$address['street_address'] 	= $address_input['street_address'];
	        	$address['city'] 			= $address_input['city'];
	        	$address['state'] 			= $address_input['state'];
	        	$address['zip'] 			= $address_input['zip'];
	        	$address['country'] 		= $address_input['country'];
	        	$address->save();
	            $bank['address_id'] = $address->id;
	        }
            $bank['name'] 		= $request->input('name');
            $bank['swift_code'] 		= $request->input('swift_code');
            $bank['account_no'] 		= $request->input('account_no');
            $bank['updated_by'] 		= Auth::user()->id;
            $bank['updated_at'] 		= new DateTime;
		}

            Bank::where('id','=',$id)->update($bank);

            Session::flash('message', 'Successfully updated a bank!');
            return Redirect::to('bank');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Bank::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a bank!');
        return Redirect::to('bank');
	}
}