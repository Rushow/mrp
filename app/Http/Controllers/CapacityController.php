<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Machinelist;
use App\Subdivision;
use App\Division;
use App\Capacity;
use App\Article;
use App\Construction;

class CapacityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $capacity= Capacity::get();
        return view("capacity.index",$data)->with('machine',$capacity);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();
        $machine = Machinelist::pluck('machine_name','id')->toArray();
        $data['article']            = Article::pluck('article_no','id')->toArray();
        $data['construction'] = Construction::pluck('construction','id')->toArray();
        return view("capacity.create",$data)->with('machine',$machine);//->with('subdivision',$subdivision);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array();

        $machine_id = $request->input('machine_id');
        $hours_of_operation = $request->input('hours_of_operation');
        $manpower= $request->input('manpower');
        $product_type = $request->input('product_type');
        $style = $request->input('style');
        $unit = $request->input('unit');

        $capacity =new Capacity;
        $capacity->machine_id = $machine_id;
        $capacity->hours_of_operation =$hours_of_operation;
        $capacity->manpower = $manpower;
        $capacity->product_type =$product_type;
        $capacity->style =$style;
        $capacity->unit =$unit;
        $capacity->save();

        $machine = Machinelist::pluck('machine_name','id')->toArray();
        $data['article']            = Article::pluck('article_no','id')->toArray();
        $data['construction'] = Construction::pluck('construction','id')->toArray();
        return view("capacity.create",$data)->with('machine',$machine);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array();
        $machine = Machinelist::pluck('machine_name','id')->toArray();
        $data['article'] = Article::pluck('article_no','id')->toArray();
        $data['capacity']  =Capacity::find($id);
        $data['construction'] = Construction::pluck('construction','id')->toArray();

        return view("capacity.edit",$data)->with('machine',$machine);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array();

        $machine_id = $request->input('machine_id');
        $hours_of_operation = $request->input('hours_of_operation');
        $manpower= $request->input('manpower');
        $product_type = $request->input('product_type');
        $style = $request->input('style');
        $unit = $request->input('unit');

        $capacity =Capacity::find($id);
        $capacity->machine_id = $machine_id;
        $capacity->hours_of_operation =$hours_of_operation;
        $capacity->manpower = $manpower;
        $capacity->product_type =$product_type;
        $capacity->style =$style;
        $capacity->unit =$unit;
        $capacity->update();

        return Redirect::to('capacity');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
