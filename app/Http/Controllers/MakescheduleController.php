<?php

namespace App\Http\Controllers;

use App\ProductionOrder;
use App\Line;
use App\Complexity;
use Illuminate\Http\Request;
use App\Machinelist;
use App\Subdivision;
use App\Division;
use App\GanttTask;
use App\GanttLink;
use App\Article;

use Dhtmlx\Connector\JSONGanttConnector;
use DB;


class MakescheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
//        $data = array();
//        $data["id"] =0;
//        $data["order"] = ProductionOrder::select('id','order_no')->get();
//        $data["division"] = Division::select('name','id')->get();
//        $data["subdivision"] = Subdivision::select('subdivision_name','id')->get();
//        $data["machine"] = Machinelist::select('id','machine_name','machine_id')->get();
//        return view("schedule.create",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();
        $data["id"] =0;
        $data["order"] = ProductionOrder::select('id','order_no')->get();
        $data["article"] = Article::select('article_no','article_no')->get();
        $data["division"] = Division::select('name','id')->get();
        $data["subdivision"] = Subdivision::select('subdivision_name','id')->get();
        $data["subdivision_a"] = Subdivision::pluck('subdivision_name','id')->toArray();
        $data["line"] = Line::select('id','line_name')->get();
        $data["line_a"] = Line::pluck('line_name','id');
        $data["complexity"] = Complexity::select('id','complexity')->get();
        $data["machine"] = Machinelist::select('id','machine_name','machine_id')->get();
        return view("schedule.create",$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new GanttTask();
        $task->order_id = $request->input("order_id");
        $task->division_id = $request->input("division_id");
        $task->sub_division_id = $request->input("sub_division_id");
        $task->line_id = $request->input("line_id");
        $task->complexity_id = $request->input("complexity_id");
        $task->text = $request->input("text");
        $task->start_date = $request->input("start_date");
        $task->end_date = $request->input("end_date");
        $task->parent = $request->input("parent");
        $task->duration = $request->input("duration");

        $task->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=0)
    {
        $data = array();
        $data['id'] = $id;
        $data["order"] = ProductionOrder::select('id','order_no')->get();
        $data["article"] = Article::select('article_no','article_no')->get();
        $data["division"] = Division::select('name','id')->get();
        $data["subdivision"] = Subdivision::select('subdivision_name','id')->get();
        $data["subdivision_a"] = Subdivision::pluck('subdivision_name','id')->toArray();
        $data["machine"] = Machinelist::select('id','machine_name','machine_id')->get();
        $data["line"] = Line::select('id','line_name')->get();
        $data["line_a"] = Line::pluck('line_name','id');
        $data["complexity"] = Complexity::select('id','complexity')->get();

        $data["planning"]= GanttTask::select(DB::raw('MIN(start_date) as start,MAX(end_date) as end'))->where("order_id","=",$id)->get();

        return view("schedule.edit",$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function data(Request $request) {

        $connector = new JSONGanttConnector(null, "PHPLaravel");
        $connector->render_links(GanttLink::get(), "id", "id,order_id,source,target,type");
        $connector->render_table( GanttTask::get(),"id","id,order_id,article_no,division_id,sub_division_id,line_id,complexity_id,quantity,start_date,duration,text,parent");


    }

    public function individualdata(Request $request,$id) {

        $ids = GanttTask::select('id')->where('order_id', '=', 1)->get()->toArray();
        $connector = new JSONGanttConnector(null, "PHPLaravel");
        $connector->render_links(GanttLink::whereIn('id', $ids)->get(), "id", "order_id,source,target,type");
        $connector->render_table( GanttTask::where('order_id', '=', $id)->get(),"id","sortorder,order_id,article_no,division_id,sub_division_id,line_id,complexity_id,quantity,start_date,duration,text,parent");


    }

    public function listofplan(){

        $data = array();
        $division = Division::pluck('name','id')->toArray();
        $subdivision = Subdivision::pluck('subdivision_name','id')->toArray();

        $data["planning"]= GanttTask::select('order_id','article_no','id','start_date','end_date')->groupBy('order_id')->get();
        return view("schedule.index",$data)->with('division',$division)->with('subdivision',$subdivision);

    }
    public function allofplan()
    {
        $data = array();
        //$data['id'] = $id;
        $data["order"] = ProductionOrder::select('id','order_no')->get();
        $data["article"] = Article::select('article_no','article_no')->get();
        $data["division"] = Division::select('name','id')->get();
        $data["subdivision"] = Subdivision::select('subdivision_name','id')->get();
        $data["subdivision_a"] = Subdivision::pluck('subdivision_name','id')->toArray();
        $data["machine"] = Machinelist::select('id','machine_name','machine_id')->get();
        $data["line_a"] = Line::pluck('line_name','id');
        $data["line"] = Line::select('id','line_name')->get();
        $data["complexity"] = Complexity::select('id','complexity')->get();

        $data["planning"]= GanttTask::select(DB::raw('MIN(start_date) as start,MAX(end_date) as end'))->get();

        return view("schedule.create",$data);
    }

    public function checkifavailable(Request $request){

        $response =array();
        $response["message"]="";
        $data =array();
        $data["start"]=  $request->input("start_date");
        $data["end"] = $request->input("end_date");
        $data["sub_division_id"] = $request->input("sub_division_id");
        
        $recordexist = GanttTask::where("line_id", "=", $request->input("line_id"))->where('start_date', '<=', $request->input("start_date"))->where('end_date', '>=', $request->input("start_date"))->exists();
        if ($recordexist) {
            
            $result = GanttTask::where("line_id", "=", $request->input("line_id"))->where('start_date', '<=',$request->input("start_date"))->where('end_date', '>=', $request->input("start_date"))->get();
            foreach ($result as $k=>$rs){

                $response[$k]["order"]=$rs->order_id;
                $response[$k]["subdivision"]=$rs->subdivision->subdivision_name;
                $response[$k]["article"] = $rs->article_no;
                $response[$k]["start_date"] = $rs->start_date ;
                $response[$k]["end_date"] = $rs->end_date ;
                $response["message"] = $response["message"]."<br>".($k+1).". <b>Order ".$rs->order_id."</b> using <b class='lineblue'>this line</b> for <b>article no ".$rs->article_no."</b> from <b class='lineblue'>".date('d,M-Y',strtotime($rs->start_date))."</b> to <b class='lineblue'>".date('d,M-Y',strtotime($rs->end_date))."</b>";

            }

            echo json_encode($response);
            
        }else{

            $response["message"] = "Available";
            echo json_encode($response);
            
        }

            

    }
    
    public function availabledate(Request $request){

        
        $data =array();
        $data["start"]=  $request->input("start_date");
        $data["end"] = $request->input("end_date");
        $data["sub_division_id"] = $request->input("sub_division_id");
        $data["result"] = GanttTask::where("sub_division_id", "=", $data["sub_division_id"])->where('start_date', '>=',$request->input("start_date"))->where('end_date', '<=', $request->input("end_date"))->get();

        return view("schedule.available",$data);
    }
    public function eventdata(Request $request) {

        $connector = new JSONGanttConnector(null, "PHPLaravel");

//        echo $request->input("from");
//        echo $request->input("to");
        $connector->render_table( GanttTask::where('start_date', '>=', $request->input("from"))->where('start_date', '<=', $request->input("to"))->get(),"id","start_date,end_date,text,sub_division_id");


    }

    public  function test(){

        $ids = GanttTask::select('id')->where('order_id', '=', 1)->get()->toArray();
        print_r($ids);
    }
}
