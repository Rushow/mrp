<?php
namespace App\Http\Controllers;


use App\Article;
use App\ArticleType;
use App\Construction;
use App\CustomerGroup;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use View;
use HTML;
use Validator;
use Session;

class ArticleController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $articles = Article::all();

        foreach($articles as $item){
            $art = Article::find($item['id'])->articleType;
            $cons = Article::find($item['id'])->construction;
            $cat = Article::find($item['id'])->customer_group;
            $item['article_type'] = $art['article_type'];
            $item['construction'] = $cons['construction'];
            $item['customer_group'] = $cat['customer_group'];
        }
//        echo '<pre>';
//        print_r($articles);exit;
        $data['article_no'] = array('' => 'Select Article No') + Article::pluck('article_no','article_no')->toArray();
        $data['article_type'] = array('' => 'Select Appearance') + ArticleType::pluck('article_type','id')->toArray();
        $data['construction'] = array('' => 'Select Construction') + Construction::pluck('construction','id')->toArray();
        $data['customer_group'] = array('' => 'Select Customer Group') + CustomerGroup::pluck('customer_group','id')->toArray();

       // $this->layout->content = View::make('article.index', $data)->with('articles', $articles);
        return view('article.index', $data)->with('articles', $articles);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        // $last_article = $this->getLastArticle();
        $articles = Article::all();
        $last_id = sizeof($articles) + 1;
        $data['article_id'] = $last_id;
        $data['article_type'] = array('' => 'Select Appearance') + ArticleType::pluck('article_type','id')->toArray();
        $data['construction'] = array('' => 'Select Construction') + Construction::pluck('construction','id')->toArray();
        $data['customer_group'] = array('' => 'Select Customer Group') + CustomerGroup::pluck('customer_group','id')->toArray();

        //$this->layout->content = View::make('article.create', $data);
        return view('article.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $article_input = $request->only('article_no','article_ref','article_type_id','note','construction_id','customer_group_id');

        $rules =array(
            'article_no' => array('required'),
            'article_ref' => array('required','numeric'),
            'article_type_id' => array('required'),
            'construction_id'=>array('required'),
            'customer_group_id' => array('required')
        );

        if ($request->hasFile('article_pic')) {
            $article_pic = Input::file('article_pic');

            $destinationPath = 'images/article';
            $filename = time().$article_pic->getClientOriginalName();
            $request->file('article_pic')->move($destinationPath, $filename);
            $img = Image::make('images/article/'.$filename);
            $img->grab(160, 160);
            $img->save('images/article/160X160/'.$filename);

            $img = Image::make('images/article/'.$filename);
            $img->grab(300, 200);
            $img->save('images/article/300X200/'.$filename);

            $article_input['article_pic'] = $filename;
        }

        $validator = Validator::make($article_input,$rules);
        if ($validator->passes()){
            Article::insert($article_input);
            Session::flash('message', 'Successfully created an article!');
            return Redirect::to('article');
        }
        else{
            return Redirect::to('article/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }

    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response3
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['article_type'] = array('' => 'Select Article Type') + ArticleType::pluck('article_type','id')->toArray();
        $data['construction'] = array('' => 'Select Construction') + Construction::pluck('construction','id')->toArray();
        $data['customer_group'] = array('' => 'Select Customer Group') + CustomerGroup::pluck('customer_group','id')->toArray();
        $data['article'] = Article::find($id);

        //$this->layout->content = View::make('article.edit',$data);
        return view('article.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $article_input = $request->only('article_no','article_ref','article_type_id','note','construction_id','customer_group_id');

        $rules =array(
            'article_no' => array('required'),
            'article_ref' => array('required','numeric'),
            'article_type_id' => array('required'),
            'construction_id'=>array('required'),
            'customer_group_id' => array('required')
        );

        if ($request->hasFile('article_pic')) {
            $article_pic = $request->file('article_pic');

            $destinationPath = 'images/article';
            $filename = time().$article_pic->getClientOriginalName();
            $request->file('article_pic')->move($destinationPath, $filename);
            $img = Image::make('images/article/'.$filename);
            $img->grab(160, 160);
            $img->save('images/article/160X160/'.$filename);

            $img = Image::make('images/article/'.$filename);
            $img->grab(300, 200);
            $img->save('images/article/300X200/'.$filename);

            $article_input['article_pic'] = $filename;
        }

        $validator = Validator::make($article_input,$rules);
        if ($validator->passes()){
            Article::where('id','=',$id)->update($article_input);

            Session::flash('message', 'Successfully updated an article!');
            return Redirect::to('article');
        }
        else{
            return Redirect::to('article/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }

    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$article = Article::find($id);
        $article->delete();
        return Redirect::to('article');
	}

    private function getLastArticle()
    {
        return DB::table('article')->orderBy('created_at', 'desc')->first();
    }

    public function search(){
        $data['post_val']['article_no']         = $article_no     = Input::get('article_no', false);
        $data['post_val']['article_type_id']    = $article_type   = Input::get('article_type_id', false);
        $data['post_val']['construction_id']    = $construction   = Input::get('construction_id', false);
        $data['post_val']['customer_group_id']  = $customer_group = Input::get('customer_group_id', false);

        $articles = DB::table('article');
        $articles->select('article.*');

        if($article_no){
            $articles->where('article_no', $article_no);
        }
        if($article_type){
            $articles->where('article_type_id', $article_type);
        }
        if($construction){
            $articles->where('construction_id', $construction);
        }
        if($customer_group){
            $articles->where('customer_group_id', $customer_group);
        }

        $articles = $articles->get();

        if($articles){
            foreach($articles as $item){
                $art = Article::find($item->id)->article_type;
                $cons = Article::find($item->id)->construction;
                $cat = Article::find($item->id)->customer_group;
                $item->article_type = $art['article_type'];
                $item->construction = $cons['construction'];
                $item->customer_group = $cat['customer_group'];
            }
        }

        $data['article_no'] = array('' => 'Select Article No') + Article::pluck('article_no','article_no');
        $data['article_type'] = array('' => 'Select Article Type') + ArticleType::pluck('article_type','id');
        $data['construction'] = array('' => 'Select Construction') + Construction::pluck('construction','id');
        $data['customer_group'] = array('' => 'Select Customer Group') + CustomerGroup::pluck('customer_group','id');

        //$this->layout->content = View::make('article.index', $data)->with('articles', $articles);

        return view('article.index', $data)->with('articles', $articles);
    }

}