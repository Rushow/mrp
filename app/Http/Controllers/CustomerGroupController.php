<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\AdditionalStickerInfo;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\Color;
use View;
use Validator;
use App\CustomerGroup;

class CustomerGroupController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $customer_group = CustomerGroup::all();

        return view('customer_group.index')->with('customer_group', $customer_group);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('customer_group.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'customer_group'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('customerGroup/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $customer_group = new CustomerGroup;

            $customer_group->customer_group       = $request->get('customer_group');

            $customer_group->save();
            Session::flash('message', 'Successfully created a customer group!');
            return Redirect::to('customerGroup');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['customer_group'] = CustomerGroup::find($id);

		return view('customer_group.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'customer_group'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('customer_group/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $customer_group['customer_group'] = $request->get('customer_group');
            $customer_group['updated_at'] = new DateTime;

            CustomerGroup::where('id','=',$id)->update($customer_group);

            Session::flash('message', 'Successfully updated a customer group!');
            return Redirect::to('customerGroup');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        CustomerGroup::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a customer group!');
        return Redirect::to('customerGroup');
	}

}