<?php

namespace App\Http\Controllers;
use App\AdditionalStickerInfo;
use Illuminate\Http\Request;
use DateTime;
use Session;
use Validator;

class AdditionalStickerInfoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $linings = AdditionalStickerInfo::all();

        //$this->layout->content = View::make('additional_sticker_info.index')->with('linings', $linings);

		return view('additional_sticker_info.index')->with('linings', $linings);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data = array();
        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');
        //$this->layout->content = View::make('additional_sticker_info.create', $data);
		return view('additional_sticker_info.create',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = array(
            'sticker_info'     => 'required',
            'status'           => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()){
            return Redirect::to('additionalStickerInfo/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type = new AdditionalStickerInfo;

            $type->sticker_info          = Input::get('sticker_info');
            $type->status             = Input::get('status');
            $type->enable_date        = strtotime(Input::get('enable_date'));

            $type->save();
            Session::flash('message', 'Successfully created a additional sticker info!');
            return Redirect::to('additionalStickerInfo');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['sticker_info'] = AdditionalStickerInfo::find($id);

        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');

        //$this->layout->content = View::make('additional_sticker_info.edit',$data);
		return view('additional_sticker_info.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = array(
            'sticker_info'       => 'required',
            'status'             => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()){
            return Redirect::to('additionalStickerInfo/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type['sticker_info'] = Input::get('sticker_info');
            $type['status']       = Input::get('status');
            $type['enable_date']  = strtotime(Input::get('enable_date'));
            $type['updated_at'] = new DateTime;

            AdditionalStickerInfo::where('id','=',$id)->update($type);

            Session::flash('message', 'Successfully updated a additional sticker info!');
            return Redirect::to('additionalStickerInfo');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        AdditionalStickerInfo::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a additional sticker info!');
        return Redirect::to('additionalStickerInfo');
	}

}