<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\StoreOut;
use App\SampleOrder;
use App\Buyer;
use App\Article;
use App\SampleType;
use Validator;

class StoreOutController extends Controller {

	public $layout = 'layouts.master';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$filters = $request->only('sample_order_no','buyer_id','article_id', 'sample_type_id', 'order_date','delivery_date');

        $sample_order = $filters['sample_order_no'];
        $buyer = $filters['buyer_id'];
        $article = $filters['article_id'];
        $sample_type = $filters['sample_type_id'];
        $order_date = $filters['order_date'];
        $delivery_date = $filters['delivery_date'];

        $data['sample_order_no'] = array('' => 'Select Sample Order No') + SampleOrder::pluck('sample_order_no','id')->toArray();
        $data['buyer'] = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $data['article'] = array('' => 'Select Article No') + Article::pluck('article_no','id')->toArray();
        $data['sample_type'] = array('' => 'Select Type of Sample') + SampleType::pluck('sample_type','id')->toArray();
		$data['store_outs'] = StoreOut::all();

		return view('store_out.index', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$warehouse = array('' => 'Select') + Warehouse::pluck('name','id')->toArray();
		$department = array('' => 'Select') + Department::pluck('name','id')->toArray();
		$item_group = array('' => 'Select') + ItemGroup::pluck('group_name','id')->toArray();
		$item_sub_group = array('' => 'Select') + ItemSubGroup::pluck('sub_group_name','id')->toArray();
		$parameter = array('' => 'Select') + Parameter::pluck('parameter_name','id')->toArray();
		$color = array('' => 'Select') + Color::pluck('color_name_flcl','id')->toArray();
		$item = array('' => 'Select') + Item::pluck('item_name','id')->toArray();
		$unit = "";	
		$order_type = array('sample' => 'Sample', 'production' => 'Production');
		$order_no = array('' => 'Select') + SampleOrder::pluck('sample_order_no','id')->toArray();
		$production_orders = array('' => 'Select') + ProductionOrder::pluck('order_no','id')->toArray();
		$data = array(
			'warehouse' => $warehouse,
			'department' => $department, 
			'item_group' => $item_group, 
			'item_sub_group' => $item_sub_group,
			'parameter' => $parameter,
			'color' => $color,
			'item' => $item,
			'unit' => $unit,
			'order_type' => $order_type,
			'order_no' => $order_no
			);
		return view('store_out.create', $data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$store_out_input = $request->only('warehouse_id','challan_no','department_id', 'out_type','order_no_id', 'store_out_date','remarks');
        $rules =array(
            'warehouse_id' => array('required'),
            'challan_no' => array('required'),
            'department_id' => array('required'),
            'order_no_id' => array('required'),
            'store_out_date' => array('required')
        );

        $store_out = StoreOut::create($store_out_input);

        $validator = Validator::make($store_out_input,$rules);
        if ($validator->passes()){
            $input = $request->only('total_row');
            $total_row = $input['total_row'];
            for($i=1; $i<=$total_row;$i++){
                if($request->get('item_id_'.$i)){
                	$store_out_detail = new StoreOutDetail();
                	$store_out_detail->store_out_id = $store_out->id;
                	$store_out_detail->item_group_id = $request->get('item_group_id_'.$i);
            		$store_out_detail->sub_group_id = $request->get('sub_group_id_'.$i);
            		$store_out_detail->parameter_id = $request->get('parameter_id_'.$i);
        			$store_out_detail->color_id = $request->get('color_id_'.$i);
    				$store_out_detail->item_id = $request->get('item_id_'.$i);
					$store_out_detail->quantity = $request->get('quantity_'.$i);
					$store_out_detail->unit_id = $request->get('unit_id_'.$i);
					$store_out_detail->save();
                }
            }
            Session::flash('message', 'Successfully created a Store Out!');
            return Redirect::to('storeOut');
        }
        else{
            return Redirect::to('storeOut/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$store_out = StoreOut::find($id);
		$store_out_details = $store_out->Details;
		$data = array(
			'store_out' => $store_out,
			'store_out_details' => $store_out_details
			);
		return view('store_out.show', $data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$warehouse = array('' => 'Select') + Warehouse::pluck('name','id')->toArray();
		$department = array('' => 'Select') + Department::pluck('name','id')->toArray();
		$item_group = array('' => 'Select') + ItemGroup::pluck('group_name','id')->toArray();
		$item_sub_group = array('' => 'Select') + ItemSubGroup::pluck('sub_group_name','id')->toArray();
		$parameter = array('' => 'Select') + Parameter::pluck('parameter_name','id')->toArray();
		$color = array('' => 'Select') + Color::pluck('color_name_flcl','id')->toArray();
		$item = array('' => 'Select') + Item::pluck('item_name','id')->toArray();
		$unit = "";
		$order_type = array('sample' => 'Sample', 'production' => 'Production');
		$order_no = array('' => 'Select') + SampleOrder::pluck('sample_order_no','id')->toArray();
		$production_orders = array('' => 'Select') + ProductionOrder::pluck('order_no','id')->toArray();
		
		$store_out = StoreOut::find($id);
		$store_out_details = $store_out->Details;

		$data = array(
			'warehouse' => $warehouse,
			'department' => $department, 
			'item_group' => $item_group, 
			'item_sub_group' => $item_sub_group,
			'parameter' => $parameter,
			'color' => $color,
			'item' => $item,
			'unit' => $unit,
			'store_out' => $store_out,
			'store_out_details' => $store_out_details,
			'order_type' => $order_type,
			'order_no' => $order_no,
			'groups' => count($store_out_details)
			);
		return view('store_out.edit', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$store_out_input = $request->only('warehouse_id', 'challan_no', 'out_type', 'order_no_id', 'department_id', 'store_out_date', 'remarks');

        $rules =array(
            'warehouse_id' => array('required'),
            'challan_no' => array('required'),
            'department_id' => array('required'),
            'store_out_date' => array('required'),
            'order_no_id' => array('required')
        );
        StoreOut::where('id','=',$id)->update($store_out_input);
        $store_out = StoreOut::find($id);
        StoreOutDetail::where('store_out_id', '=', $id)->delete();

        $validator = Validator::make($store_out_input,$rules);
        if ($validator->passes()){
            $input = Input::only('total_row');
            $total_row = $input['total_row'];
            for($i=1; $i<=$total_row;$i++){
                if(Input::get('item_id_'.$i)){
                	$store_out_detail = new StoreOutDetail();
                	$store_out_detail->store_out_id = $store_out->id;
                	$store_out_detail->item_group_id = $request->get('item_group_id_'.$i);
            		$store_out_detail->sub_group_id = $request->get('sub_group_id_'.$i);
            		$store_out_detail->parameter_id = $request->get('parameter_id_'.$i);
        			$store_out_detail->color_id = $request->get('color_id_'.$i);
    				$store_out_detail->item_id = $request->get('item_id_'.$i);
					$store_out_detail->quantity = $request->get('quantity_'.$i);
					$store_out_detail->unit_id = $request->get('unit_id_'.$i);
					$store_out_detail->save();
                }
            }
            Session::flash('message', 'Successfully created a Sample Order!');
            return Redirect::to('storeOut');
        }
        else{
            return Redirect::to('storeOut/'.$id.'/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$store_out = StoreOut::find($id);
		$store_out->delete();
		return Redirect::to('storeOut');
	}

	public function addStoreOutRow(Request $request){
        $info = $request->only('last_id');
        if($info){
            $last_id = $info['last_id'] + 1;
            $warehouse = array('' => 'Select') + Warehouse::pluck('name','id')->toArray();
			$supplier = array('' => 'Select') + Supplier::pluck('supplier_name','id')->toArray();
			$item_group = array('' => 'Select') + ItemGroup::pluck('group_name','id')->toArray();
			$item_sub_group = array('' => 'Select') + ItemSubGroup::pluck('sub_group_name','id')->toArray();
			$parameter = array('' => 'Select') + Parameter::pluck('parameter_name','id')->toArray();
			$color = array('' => 'Select') + Color::pluck('color_name_flcl','id')->toArray();
			$item = array('' => 'Select') + Item::pluck('item_name','id')->toArray();
			$unit = "";	

            $msg = "Are you sure to delete this item???";

            $html = "<td>".$last_id."</td>";
            $html .= "<td>".Form::select('item_group_id_'.$last_id, $item_group, '', $attributes = array('id'=>'item_group_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required', 'onChange'=>'generateSubGroup(this, '.$last_id.');'))."</td>";
            $html .= "<td>".Form::select('sub_group_id_'.$last_id, $item_sub_group, null, $attributes = array('id'=>'sub_group_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required', 'disabled'=>'disabled', 'onChange' => 'generateItems(this, ' . $last_id . ');'))."</td>";
            $html .= "<td>".Form::select('parameter_id_'.$last_id, $parameter, '', $attributes = array('id'=>'parameter_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::select('color_id_'.$last_id, $color, '', $attributes = array('id'=>'color_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::select('item_id_'.$last_id, $item, '', $attributes = array('id'=>'item_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required', 'disabled' => 'disabled', 'onChange'=>'generateUnit(this, '.$last_id.');'))."</td>";
            $html .= '<td>'.Form::text('quantity_'.$last_id, '',array('class'=>'form-control Required','id'=>'quantity_'.$last_id)).'</td>';
            $html .= Form::hidden('unit_id_'.$last_id, '', array('id'=> 'unit_id_'.$last_id));
        	$html .= '<td>'.Form::text('unit_'.$last_id, $unit, array('id'=>'unit_'.$last_id, 'data-rel'=>'chosen', 'class'=>'form-control Required', 'readonly'=>'readonly')).'</td>';
            $html .= "<td><a href='#' onclick='return checkDelete(this);'><span class='badge badge-important'>X</span></a></td>";

            $data = array();
            $data['error'] = false;
            $data['html'] = $html;
            echo json_encode($data);
            die();
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die();
        }
    }
}
