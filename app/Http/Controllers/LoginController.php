<?php
namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use input;
use DB;
use File;
use View;

class LoginController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('login.login');
	}

    public function  checkLogin(Request $request)
    {
        $userData = array(
            'email'=>$request->input('email'),
            'password'=>$request->input('password')
        );

        if(Auth::attempt($userData)){
            return Redirect::route('dashboard');
        }else{
            return Redirect::back()->withInput()->with(array('error_messages'=>array('<div class="alert alert-error">Invalid email/password</div>')));
        }
    }
    public function logout()
    {
        Auth::logout();
        return Redirect::action('LoginController@index');
    }

}