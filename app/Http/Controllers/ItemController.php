<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\Item;
use App\Parameter;
use App\Color;
use App\Unit;
use App\ItemGroup;
use App\ItemSubGroup;
use Validator;
use View;

class ItemController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $items = Item::all();
        foreach($items as $item){
            $color = Item::find($item['id'])->color;
            $parameter = Item::find($item['id'])->parameter;
            $group = Item::find($item['id'])->group;
            $sub_group = Item::find($item['id'])->subGroup;
            $item['color_name'] = $color['color_name_flcl'];
            $item['parameter_name'] = $parameter['parameter_name'];
            $item['group_name'] = $group['group_name'];
            $item['sub_group_name'] = $sub_group['sub_group_name'];
        }
//        echo '<pre>';
//        print_r($items);die();
        $data['item_code'] = array('' => 'Select Material Code') + Item::pluck('item_code','item_code')->toArray();
        $data['item_name'] = array('' => 'Select Material Name') + Item::pluck('item_name','item_name')->toArray();
        $data['parameter'] = array('' => 'Select Parameter') + Parameter::pluck('parameter_name','id')->toArray();
        $data['color'] = array('' => 'Select Color') + Color::pluck('color_name_flcl','id')->toArray();
        $data['item_group'] = array('' => 'Select Group') + ItemGroup::pluck('group_name','id')->toArray();
        $data['item_sub_group'] = array('' => 'Select Sub Group') + ItemSubGroup::pluck('sub_group_name','id')->toArray();

        return view('item.index', $data)->with('items', $items);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        // $last_item = $this->getLastItem();
        $items = Item::all();
        $last_id = sizeof($items) + 1;
        $data['article_id'] = $last_id;
        $data['item_id'] = $last_id;
        $data['item_code'] = 1;
        $data['parameter'] = array('' => 'Select Parameter') + Parameter::pluck('parameter_name','id')->toArray();
        $data['color'] = array('' => 'Select Color') + Color::pluck('color_name_flcl','id')->toArray();
        $data['unit'] = array('' => 'Select Unit') + Unit::pluck('unit_name','id')->toArray();
        $data['item_group'] = array('' => 'Select Group') + ItemGroup::pluck('group_name','id')->toArray();
        $data['item_sub_group'] = array('' => 'Select Sub Group') + ItemSubGroup::pluck('sub_group_name','id')->toArray();

        return view('item.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $item_input = $request->only('item_name','group_id','sub_group_id','parameter_id','color_id','remarks', 'item_code', 'cutability_factor', 'cutable_width', 'cutable_width_unit', 'store_unit', 'purchase_unit', 'store_conversion', 'purchase_conversion');

        $rules =array(
            'group_id' => array('required'),
            'sub_group_id' => array('required'),
            'cutability_factor' => array('required'),
            'store_unit' => array('required'),
            'purchase_unit'=>array('required'),
        );

        if (Input::hasFile('item_pic')) {
            $item_pic = $request->file('item_pic');

            $destinationPath = 'images/item';
            $filename = time().$item_pic->getClientOriginalName();
            Input::file('item_pic')->move($destinationPath, $filename);
            $img = Image::make('images/item/'.$filename);
            $img->grab(160, 160);
            $img->save('images/item/160X160/'.$filename);

            $img = Image::make('images/item/'.$filename);
            $img->grab(300, 200);
            $img->save('images/item/300X200/'.$filename);

            $item_input['item_pic'] = $filename;
        }

        $validator = Validator::make($item_input,$rules);
        if ($validator->passes()){
            Item::insert($item_input);
            Session::flash('message', 'Successfully created an item!');
            return Redirect::to('item');
        }
        else{
            return Redirect::to('item/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$item = Item::find($id);
        $data = array(
            'item' => $item,
            'groups' => array('' => 'Select Group') + ItemGroup::pluck('group_name','id')->toArray(),
            'sub_groups' => array('' => 'Select Sub Group') + ItemSubGroup::pluck('sub_group_name','id')->toArray(),
            'parameters' => array('' => 'Select Parameter') + Parameter::pluck('parameter_name','id')->toArray(),
            'colors' => array('' => 'Select Color') + Color::pluck('color_name_flcl','id')->toArray(),
            'units' => array('' => 'Select Unit') + Unit::pluck('unit_name','id')->toArray()
            );
        return view('item.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$item_input = $request->only('item_name','group_id','sub_group_id','parameter_id','color_id','remarks', 'item_code', 'cutability_factor', 'cutable_width', 'cutable_width_unit', 'store_unit', 'purchase_unit', 'store_conversion', 'purchase_conversion');
        $rules =array(
            'group_id' => array('required'),
            'sub_group_id' => array('required'),
            'cutability_factor' => array('required'),
            'store_unit' => array('required'),
            'purchase_unit'=>array('required')
        );

        if (Input::hasFile('item_pic')) {
            $item_pic = Input::file('item_pic');

            $destinationPath = 'images/item';
            $filename = time().$item_pic->getClientOriginalName();
            Input::file('item_pic')->move($destinationPath, $filename);
            $img = Image::make('images/item/'.$filename);
            $img->grab(160, 160);
            $img->save('images/item/160X160/'.$filename);

            $img = Image::make('images/item/'.$filename);
            $img->grab(300, 200);
            $img->save('images/item/300X200/'.$filename);

            $item_input['item_pic'] = $filename;
        }

        $validator = Validator::make($item_input,$rules);
        if ($validator->passes()){
            Item::where('id','=',$id)->update($item_input);
            Session::flash('message', 'Successfully updated item!');
            return Redirect::to('item');
        }
        else{
            return Redirect::to('item/'.$id.'/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    private function getLastItem()
    {
        return DB::table('item')->orderBy('created_at', 'desc')->first();
    }

    public function search(Request $request){
        $data['post_val']['item_code']      = $item_code    = $request->get('item_code', false);
        $data['post_val']['parameter_id']   = $parameter    = $request->get('parameter_id', false);
        $data['post_val']['group_id']       = $group        = $request->get('group_id', false);
        $data['post_val']['item_name']      = $item_name    = $request->get('item_name', false);
        $data['post_val']['sub_group_id']   = $sub_group    = $request->get('sub_group_id', false);
        $data['post_val']['color_id']       = $color        = $request->get('color_id', false);

        $items = DB::table('item');
        $items->select('item.*');

        if($item_code){
            $items->where('item_code', $item_code);
        }
        if($parameter){
            $items->where('parameter_id', $parameter);
        }
        if($group){
            $items->where('group_id', $group);
        }
        if($item_name){
            $items->where('item_name', $item_name);
        }
        if($sub_group){
            $items->where('sub_group_id', $sub_group);
        }
        if($color){
            $items->where('color_id', $color);
        }

        $items = $items->get();

        if($items){
            foreach($items as $item){
                $color = Item::find($item->id)->Color;
                $parameter = Item::find($item->id)->Parameter;
                $group = Item::find($item->id)->posix_getgroups(oid);
                $sub_group = Item::find($item->id)->SubGroup;
                $item->color_name = $color['color_name_flcl'];
                $item->parameter_name = $parameter['parameter_name'];
                $item->group_name = $group['group_name'];
                $item->sub_group_name = $sub_group['sub_group_name'];
            }
        }

        $data['item_code'] = array('' => 'Select Material Code') + Item::pluck('item_code','item_code')->toArray();
        $data['item_name'] = array('' => 'Select Material Name') + Item::pluck('item_name','item_name')->toArray();
        $data['parameter'] = array('' => 'Select Parameter') + Parameter::pluck('parameter_name','id')->toArray();
        $data['color'] = array('' => 'Select Color') + Color::pluck('color_name_flcl','id')->toArray();
        $data['item_group'] = array('' => 'Select Group') + ItemGroup::pluck('group_name','id')->toArray();
        $data['item_sub_group'] = array('' => 'Select Sub Group') + ItemSubGroup::pluck('sub_group_name','id')->toArray();

        return view('item.index', $data)->with('items', $items);
    }

    function subGroup(Request $request){
        $info = $request->only('group_id');
        $data = array();

        if($info['group_id']){
            $data['group_id'] = $info['group_id'];
            $sub_groups = ItemSubGroup::where('group_id', '=', $info['group_id'])->get();
            $option = '<option value="">Select Sub Group</option>';
            foreach($sub_groups as $sub_group){
                $option = $option."<option value='".$sub_group['id']."'>".$sub_group['sub_group_name']."</option>";
            }
            $data['sub_group'] = $option;
            $data['error'] = false;
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }

    }

    function items(Request $request){
        $info = $request->only('sub_group_id');
        $data = array();

        if($info['sub_group_id']){
            $items = Item::withSubGroup($info['sub_group_id'])->get();
            $option = '<option value="">Select Item</option>';
            foreach($items as $item){
                $option = $option."<option value='".$item['id']."'>".$item['item_name']."</option>";
            }
            $data['items'] = $option;
            $data['error'] = false;
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }

    }

    function purchaseUnit(Request $request){
        $info = $request->only('item_id');
        $item_id = $info['item_id'];
        $data = array();

        if($item_id){
            $item = Item::find($item_id);
            $data['id'] = $item->PurchaseUnit->id;
            $data['unit'] = $item->PurchaseUnit->unit_name;
            $data['error'] = false;
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }

    }

    function storeUnit(Request $request){
        $info = $request->only('item_id');
        $item_id = $info['item_id'];
        $data = array();

        if($item_id){
            $item = Item::find($item_id);
            $data['id'] = $item->StoreUnit->id;
            $data['unit'] = $item->StoreUnit->unit_name;
            $data['error'] = false;
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }

    }

    function item(Request $request){
        $info = $request->only('sub_group_id');
        $data = array();

        if($info['sub_group_id']){
            $data['sub_group_id'] = $info['sub_group_id'];
            $items = Item::where('sub_group_id', '=', $info['sub_group_id'])->get();
            $option = '<option value="">Select Material</option>';
            foreach($items as $item){
                $option = $option."<option value='".$item['id']."'>".$item['item_name']."</option>";
            }
            $data['item'] = $option;
            $data['error'] = false;
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }

    }

}