<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\HeelNo;
use Validator;
use View;

class HeelNoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $heel_no = HeelNo::all();

        return view('heel_no.index')->with('heel_no', $heel_no);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('heel_no.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'heel_no'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()){
            return Redirect::to('heelNo/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $heel_no = new HeelNo;

            $heel_no->heel_no       = Input::get('heel_no');

            $heel_no->save();
            Session::flash('message', 'Successfully created a heel no!');
            return Redirect::to('heelNo');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['heel_no'] = HeelNo::find($id);

        return view('heel_no.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'heel_no'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('heelNo/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $heel_no['heel_no'] = $request->get('heel_no');
            $heel_no['updated_at'] = new DateTime;

            HeelNo::where('id','=',$id)->update($heel_no);

            Session::flash('message', 'Successfully updated a heel no!');
            return Redirect::to('heelNo');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        HeelNo::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a heel no!');
        return Redirect::to('heelNo');
	}

}