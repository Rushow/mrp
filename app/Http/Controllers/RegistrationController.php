<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\User;
use App\UserType;
use Validator;
use View;
use Illuminate\Support\Facades\Hash;

class RegistrationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    protected $layout = 'layouts.master';

    public function index()
	{
        $users = User::all();
        $data = array(
        	'users' => $users
        	);
        return view('registration.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data = array(
        	'roles' => array('' => 'Select User Type') + UserType::pluck('user_type','id')->toArray()
        	);
        return view('registration.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules =array(
            'first_name' => array('required','alpha', 'min:3','max:50'),
            'last_name' => array('required','alpha', 'min:3','max:50'),
            'name' => array('required','alpha_num', 'min:4','max:50','unique:users,name'),
            'password' => array('required','min:6','max:50'),
            'email' => array('required','email','unique:users,email')
        );

        $data = $request->only('first_name','last_name','name','password','email', 'user_type_id');
        //$data['role']++;

        $validator = Validator::make($data,$rules);
        if ($validator->passes()) {

            $data['password'] = Hash::make($data['password']);

            User::insert($data);

            return Redirect::action('RegistrationController@index');
        }else{
            return Redirect::back()->withInput()->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		$data = array(
			'user' => $user,
			'roles' => array('' => 'Select User Type') + UserType::pluck('user_type','id')->toArray()
			);
		return view('registration.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$rules =array(
            'first_name' => array('required','alpha', 'min:3','max:50'),
            'last_name' => array('required','alpha', 'min:3','max:50'),
            'name' => array('required','alpha_num', 'min:4','max:50'),
            'password' => array('required','min:6','max:50'),
            'email' => array('required','email')
        );
        $data = $request->only('first_name','last_name','name','password','email');
        //$data['role']++;

        $validator = Validator::make($data,$rules);
        if ($validator->passes()) {

            $data['password'] = Hash::make($data['password']);

            User::where('id','=',$id)->update($data);

            return Redirect::action('RegistrationController@index');
        }else{
            return Redirect::back()->withInput()->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);
		$user->delete();
		return Redirect::to('registration');
	}

}