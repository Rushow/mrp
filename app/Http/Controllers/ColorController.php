<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\AdditionalStickerInfo;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\Color;
use View;
use Validator;

class ColorController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

	public function index()
	{
        $colors = Color::all();

        return view::make('color.index')->with('colors', $colors);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view::make('color.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $color_input = $request->only('pantone_code', 'hm_code', 'color_group_code', 'color_name_flcl','color_name_hm');

        $rules = array(
            'pantone_code'  => 'required',
            'hm_code'       => 'required',
        );


        $validator = Validator::make($color_input,$rules);
        $color_input['created_at'] = new DateTime();
        if ($validator->passes()){
            Color::insert($color_input);
            Session::flash('message', 'Successfully created a Color!');
            return Redirect::to('color');
        }
        else{
            return Redirect::to('color/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['color'] = Color::find($id);

        return view::make('color.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'pantone_code'  => 'required',
            'hm_code'       => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('color/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $color = $request->only('pantone_code', 'hm_code', 'color_group_code', 'color_name_flcl','color_name_hm');
            $color['updated_at'] = new DateTime;

            Color::where('id','=',$id)->update($color);

            Session::flash('message', 'Successfully updated a color!');
            return Redirect::to('color');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Color::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a color!');
        return Redirect::to('color');
	}

    public function getColorHMCode(Request $request)
    {
        $info = $request->only('id');
        $id = $info['id'];
        if(isset($id)){
            $data = array();
            $data['id'] = $id;
            $color = Color::find($id);
            $data['hm_code'] = $color['hm_code'];
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

}