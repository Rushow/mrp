<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\Season;
use Validator;
use View;

class SeasonController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public $layout = 'layouts.master';

    public function index()
    {
        $seasons = Season::all();

        return view('season.index')->with('seasons', $seasons);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('season.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'season'        => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('season/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $season = new Season;

            $season->season         = $request->get('season');
            $season->start_date     = strtotime($request->get('start_date'));
            $season->end_date       = strtotime($request->get('end_date'));

            $season->save();
            Session::flash('message', 'Successfully created a season!');
            return Redirect::to('season');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = array();
        $data['season'] = Season::find($id);

        return view('season.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $rules = array(
            'season'        => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('season/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $season['season']       = $request->get('season');
            $season['start_date']   = strtotime($request->get('start_date'));
            $season['end_date']     = strtotime($request->get('end_date'));
            $season['updated_at']   = new DateTime;

            Season::where('id','=',$id)->update($season);

            Session::flash('message', 'Successfully updated a season!');
            return Redirect::to('season');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Season::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a season!');
        return Redirect::to('season');
    }

}