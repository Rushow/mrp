<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subdivision;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Redirect;
use View;
use HTML;
use Validator;
use Session;
use Form;
use App\Division;



class SubdivisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data["subdivision"] = Subdivision::get();
        return view('subdivision.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();
        $division = Division::pluck('name','id')->toArray();
        return view('subdivision.create',$data)->with('division',$division);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input  = $request->only('division_name','sub_division_name','description');

        $division = Division::pluck('name','id')->toArray();

        $sub = new Subdivision();
        $sub->division_id = $request->input('division_name');
        $sub->subdivision_name = $request->input('sub_division_name');
        $sub->subdivision_description = $request->input('description');

        $sub->save();

        return Redirect::to('subdivision');
        //return view('subdivision.create')->with('division',$division);;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       echo $id."show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array();
        $data["division"] = Division::pluck('name','id')->toArray();
        $data["subdivision"] =Subdivision::find($id);

        return view('subdivision.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $sub = Subdivision::find($id);
        $sub->division_id = $request->input('division_name');
        $sub->subdivision_name = $request->input('sub_division_name');
        $sub->subdivision_description = $request->input('description');

        $sub->update();
        return Redirect::to('subdivision');
        //return redirect()->action('App\Http\Controllers\SubdivisionController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //echo "Destroy".$id;die();
        $row = Subdivision::find($id);

        $row->delete();
        return Redirect::to('subdivision');
    }
}
