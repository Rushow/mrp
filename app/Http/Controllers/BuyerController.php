<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\AdditionalStickerInfo;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\Buyer;
use App\BuyerTemplate;
use App\Address;
use View;
use Validator;



class BuyerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $buyers = Buyer::all();
        $data = array(
        	'buyers' => $buyers
        	);
        return view('buyer.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data = array(
			'buyer_template' => array('' => 'Select Buyer Template') + BuyerTemplate::pluck('buyer_template','id')->toArray(),
			'cut_off_diff' => array('' => 'Select One', '0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6'),
			'etd_diff' => array('' => 'Select One', '-1' => '-1', '-2' => '-2', '-3' => '-3', '-4' => '-4', '-5' => '-5', '-6' => '-6', '0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6'),
			'week_days' => array('' => 'Select Week Start Day', 'Saturday' => 'Saturday', 'Sunday' => 'Sunday', 'Monday' => 'Monday', 'Tuesday' => 'Tuesday', 'Wednesday' => 'Wednesday', 'Thursday' => 'Thursday', 'Friday' => 'Friday')
		);
        return view('buyer.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'buyer_name' => 'required',
            'buyer_email' => array('email'),

			'street_address'=>'required',
			'city'=>'required',
			'state'=>'required',
			'zip'=>'required',
			'country'=>'required'
        );
        $buyer_details = $request->only('buyer_name', 'full_name', 'buyer_email', 'buyer_phn', 'week_start_day', 'buyer_template');
        $validator = Validator::make($buyer_details, $rules);        

        if ($validator->fails()){
            return Redirect::to('buyer/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{
            $buyer = new Buyer;
            $buyer['buyer_name'] 		= $request->input('buyer_name');
            $buyer['buyer_email'] 		= $request->input('buyer_email');
            $buyer['buyer_phn'] 		= $request->input('buyer_phn');
            $buyer['full_name'] 		= $request->input('full_name');
            $buyer['week_start_day']	= $request->input('week_start_day');
            $buyer['buyer_template_id']	= $request->input('buyer_template_id');
			$buyer['created_by'] 		= Auth::user()->id;
			$buyer['created_at'] 		= new DateTime();

			if($request->input('buyer_template_id') == 2){
				$buyer['cut_off_day'] 		= $request->input('cut_off_day');
				$buyer['cut_off_diff'] 		= $request->input('cut_off_diff');
				$buyer['etd_day']			= $request->input('etd_day');
				$buyer['etd_diff']			= $request->input('etd_diff');
			}
            if ($request->hasFile('logo')) {
	            $logo = $request->file('logo');

	            $destinationPath = 'images/buyer';
	            $filename = time().$logo->getClientOriginalName();
				$request->file('logo')->move($destinationPath, $filename);
	            $img = Image::make('images/buyer/'.$filename);
	            $img->grab(160, 160);
	            $img->save('images/buyer/160X160/'.$filename);

	            $img = Image::make('images/buyer/'.$filename);
	            $img->grab(300, 200);
	            $img->save('images/buyer/300X200/'.$filename);

	            $buyer->logo = $filename;
	        }
            $address_input = $request->only('floor', 'street_address', 'city', 'state', 'zip', 'country');
	        $index = Address::Exists($address_input);   
	        if ($index > 0) {
	        	$buyer->address_id = $index;
	        }else{
	        	$address = new Address;
	        	$address['floor'] 			= $address_input['floor'];
	        	$address['street_address'] 	= $address_input['street_address'];
	        	$address['city'] 			= $address_input['city'];
	        	$address['state'] 			= $address_input['state'];
	        	$address['zip'] 			= $address_input['zip'];
	        	$address['country'] 		= $address_input['country'];
	        	$address->save();
	            $buyer->address_id = $address->id;
	        }
            $buyer->save();
            Session::flash('message', 'Successfully created a buyer!');
            return Redirect::to('buyer');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$buyer = Buyer::find($id);
		$address = array();
		if ($buyer->address_id) {
        		$address['floor'] 			= $buyer->Address->floor;
        		$address['street_address'] 	= $buyer->Address->street_address;
        		$address['city'] 			= $buyer->Address->city;
        		$address['state'] 			= $buyer->Address->state;
        		$address['zip'] 			= $buyer->Address->zip;
        		$address['country'] 		= $buyer->Address->country;
        	}else{
        		$address['floor'] 			= '';
        		$address['street_address'] 	= '';
        		$address['city'] 			= '';
        		$address['state'] 			= '';
        		$address['zip'] 			= '';
        		$address['country'] 		= '';
        	}
        $data = array(
        	'buyer' => $buyer,
        	'address' => $address,
			'buyer_template' => array('' => 'Select Buyer Template') + BuyerTemplate::pluck('buyer_template','id')->toArray(),
			'cut_off_diff' => array('' => 'Select One', '0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6'),
			'etd_diff' => array('' => 'Select One', '-1' => '-1', '-2' => '-2', '-3' => '-3', '-4' => '-4', '-5' => '-5', '-6' => '-6', '0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6'),
			'week_days' => array('' => 'Select Week Start Day', 'Saturday' => 'Saturday', 'Sunday' => 'Sunday', 'Monday' => 'Monday', 'Tuesday' => 'Tuesday', 'Wednesday' => 'Wednesday', 'Thursday' => 'Thursday', 'Friday' => 'Friday')
        	);

        return view::make('buyer.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'buyer_name' => 'required',
            'buyer_email' => array('email'),
			'street_address'=>'required',
			'city'=>'required',
			'state'=>'required',
			'zip'=>'required',
			'country'=>'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('buyer/'.$id.'/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{
	        $address_input = $request->only('floor', 'street_address', 'city', 'state', 'zip', 'country');
	        $index = Address::Exists($address_input);
	        if ($index > 0) {
	        	$buyer['address_id'] = $index;
	        }else{
	        	$address = new Address;
	        	$address['floor'] 			= $address_input['floor'];
	        	$address['street_address'] 	= $address_input['street_address'];
	        	$address['city'] 			= $address_input['city'];
	        	$address['state'] 			= $address_input['state'];
	        	$address['zip'] 			= $address_input['zip'];
	        	$address['country'] 		= $address_input['country'];
	        	$address->save();
	            $buyer['address_id'] = $address->id;
	        }
            $buyer['buyer_name'] 		= $request->input('buyer_name');
            $buyer['full_name'] 		= $request->input('full_name');
            $buyer['buyer_email'] 		= $request->input('buyer_email');
            $buyer['buyer_phn'] 		= $request->input('buyer_phn');
            $buyer['week_start_day'] 	= $request->input('week_start_day');
            $buyer['buyer_template_id'] = $request->input('buyer_template_id');
            $buyer['updated_by'] 		= Auth::user()->id;
            $buyer['updated_at'] 		= new DateTime;

			if($request->input('buyer_template_id') == 2){
				$buyer['cut_off_day'] 		= $request->input('cut_off_day');
				$buyer['cut_off_diff'] 		= $request->input('cut_off_diff');
				$buyer['etd_day']			= $request->input('etd_day');
				$buyer['etd_diff']			= $request->input('etd_diff');
			}

            Buyer::where('id','=',$id)->update($buyer);

            Session::flash('message', 'Successfully updated a buyer!');
            return Redirect::to('buyer');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Buyer::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a buyer!');
        return Redirect::to('buyer');
	}

	public function getBuyerInfo(Request $request)
	{
		$id = $request->input('id');
		if(isset($id)){
			$data = array();
			$data['id'] = $id;
			$buyer = Buyer::find($id);
			$data['buyer_name'] = $buyer['buyer_name'];
			$data['buyer_template'] = Buyer::find($id)->buyerTemplate->buyer_template;
			echo json_encode($data);
			die;
		}
		else{
			$data = array();
			$data['error'] = true;
			$data['message'] = "Something went wrong. Please try again.";
			echo json_encode($data);
			die;
		}
	}
}