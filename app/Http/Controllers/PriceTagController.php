<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\AdditionalStickerInfo;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\PriceTag;
use Validator;
use View;

class PriceTagController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $linings = PriceTag::all();

        return view('price_tag.index')->with('linings', $linings);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data = array();
        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');
        return view('price_tag.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'price_tag'     => 'required',
            'status'           => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('priceTag/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type = new PriceTag;

            $type->price_tag          = $request->get('price_tag');
            $type->status             = $request->get('status');
            $type->enable_date        = strtotime($request->get('enable_date'));

            $type->save();
            Session::flash('message', 'Successfully created a price tag!');
            return Redirect::to('priceTag');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['price_tag'] = PriceTag::find($id);

        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');

        return view('price_tag.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'price_tag'       => 'required',
            'status'             => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('priceTag/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type['price_tag'] = $request->get('price_tag');
            $type['status']       = $request->get('status');
            $type['enable_date']  = strtotime($request->get('enable_date'));
            $type['updated_at'] = new DateTime;

            PriceTag::where('id','=',$id)->update($type);

            Session::flash('message', 'Successfully updated a price tag!');
            return Redirect::to('priceTag');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        PriceTag::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a price tag!');
        return Redirect::to('priceTag');
	}

}