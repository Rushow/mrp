<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\PmTag;
use Validator;
use View;

class PmTagController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $linings = PmTag::all();

        return view('pm_tag.index')->with('linings', $linings);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data = array();
        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');
        return view::make('pm_tag.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'pm_tag'     => 'required',
            'status'           => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('pmTag/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type = new PmTag;

            $type->pm_tag          = $request->get('pm_tag');
            $type->status             = $request->get('status');
            $type->enable_date        = strtotime($request->get('enable_date'));

            $type->save();
            Session::flash('message', 'Successfully created a pm tag!');
            return Redirect::to('pmTag');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['pm_tag'] = PmTag::find($id);

        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');

        return view('pm_tag.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'pm_tag'       => 'required',
            'status'             => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('pmTag/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type['pm_tag'] = $request->get('pm_tag');
            $type['status']       = $request->get('status');
            $type['enable_date']  = strtotime($request->get('enable_date'));
            $type['updated_at'] = new DateTime;

            PmTag::where('id','=',$id)->update($type);

            Session::flash('message', 'Successfully updated a pm tag!');
            return Redirect::to('pmTag');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        PmTag::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a pm tag!');
        return Redirect::to('pmTag');
	}

}