<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\OutsoleNo;
use Validator;
use View;

class OutsoleNoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $outsole_no = OutsoleNo::all();

        return view('outsole_no.index')->with('outsole_no', $outsole_no);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('outsole_no.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'outsole_no'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('outsoleNo/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $outsole_no = new OutsoleNo;

            $outsole_no->outsole_no       = $request->get('outsole_no');

            $outsole_no->save();
            Session::flash('message', 'Successfully created a outsole no!');
            return Redirect::to('outsoleNo');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['outsole_no'] = OutsoleNo::find($id);

        return view('outsole_no.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'outsole_no'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('outsoleNo/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $outsole_no['outsole_no'] = $request->get('outsole_no');
            $outsole_no['updated_at'] = new DateTime;

            OutsoleNo::where('id','=',$id)->update($outsole_no);

            Session::flash('message', 'Successfully updated a outsole no!');
            return Redirect::to('outsoleNo');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        OutsoleNo::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a outsole no!');
        return Redirect::to('outsoleNo');
	}

}