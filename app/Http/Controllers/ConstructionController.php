<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Construction;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\Color;
use View;
use Validator;

class ConstructionController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $constructions = Construction::all();

        return view('construction.index')->with('constructions', $constructions);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('construction.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'construction'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('construction/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $construction = new Construction;

            $construction->construction       = $request->get('construction');

            $construction->save();
            Session::flash('message', 'Successfully created a construction!');
            return Redirect::to('construction');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['construction'] = Construction::find($id);

        return view('construction.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'construction'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('construction/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $construction['construction'] = $request->get('construction');
            $construction['updated_at'] = new DateTime;

            Construction::where('id','=',$id)->update($construction);

            Session::flash('message', 'Successfully updated a construction!');
            return Redirect::to('construction');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Construction::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a construction!');
        return Redirect::to('construction');
	}

}