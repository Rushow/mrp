<?php
namespace App\Http\Controllers;


use App\AdditionalPrint;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use View;
use HTML;
use Validator;


class AdditionalPrintController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $additionals = AdditionalPrint::all();

        //$this->layout->content = View::make('additional_print.index')->with('additionals', $additionals);
		return view('additional_print.index')->with('additionals', $additionals);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data = array();
        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');
        //$this->layout->content = View::make('additional_print.create', $data);
		return view('additional_print.create',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = array(
            'additional_print'       => 'required',
            'status'           => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()){
            return Redirect::to('additionalPrint/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type = new AdditionalPrint;

            $type->additional_print       = Input::get('additional_print');
            $type->status           = Input::get('status');
            $type->enable_date      = strtotime(Input::get('enable_date'));

            $type->save();
            Session::flash('message', 'Successfully created a additional print!');
            return Redirect::to('additionalPrint');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['additional_print'] = AdditionalPrint::find($id);

        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');

        //$this->layout->content = View::make('additional_print.edit',$data);
		return view('additional_print.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = array(
            'additional_print'       => 'required',
            'status'           => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()){
            return Redirect::to('AdditionalPrint/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type['additional_print']   = Input::get('additional_print');
            $type['status']             = Input::get('status');
            $type['enable_date']        = strtotime(Input::get('enable_date'));
            $type['updated_at']         = new DateTime;

            AdditionalPrint::where('id','=',$id)->update($type);

            Session::flash('message', 'Successfully updated a additional print!');
            return Redirect::to('additionalPrint');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        AdditionalPrint::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a additional print!');
        return Redirect::to('additionalPrint');
	}

}