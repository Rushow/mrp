<?php
namespace App\Http\Controllers;

use App\SampleOrder;
use App\Buyer;
use App\Article;
use App\Color;
use App\SampleType;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\SampleOrderDetail;
use Validator;

class SampleScheduleController extends Controller {

	public $layout = 'layouts.master';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $data['sample_order_no'] = array('' => 'Select Sample Order No') + SampleOrder::pluck('sample_order_no','id')->toArray();
        $data['buyer'] = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $data['article'] = array('' => 'Select Article No') + Article::pluck('article_no','id')->toArray();
        $data['sample_type'] = array('' => 'Select Type of Sample') + SampleType::pluck('sample_type','id')->toArray();
		$all_details = SampleOrderDetail::all();
		
		return view('sample_schedule.index', $data)->with('details', $all_details);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$sample_schedule = SampleSchedule::find($id);
		return view('sample_schedule.edit')->with('sample_schedule', $sample_schedule);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$schedule_input = $request->only('schedule_date','note');
		SampleSchedule::where('id','=',$id)->update($schedule_input);
		return Redirect::to('sampleSchedule');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function search(Request $request)
	{
		$filters = $request->only('sample_order_no','buyer_id','article_id', 'sample_type_id', 'order_date','delivery_date');

        $sample_order = $filters['sample_order_no'];
        $buyer = $filters['buyer_id'];
        $article = $filters['article_id'];
        $sample_type = $filters['sample_type_id'];
        $order_date = $filters['order_date'];
        $delivery_date = $filters['delivery_date'];
        $sorted_buyer = array();
		$sorted_sample_type = array();
		$sorted_buyer_sample_type = array();
		$sorted_buyer_article = array();
		$sorted_article = array();
		$sorted_sample_type_article = array();
		$sorted_buyer_sample_type_article = array();
		$sorted_by_date = array();
		$details = array();

        if($sample_order){        	
			$order = SampleOrder::find($sample_order);			
			$data['sample_order_no'] = $order->sample_order_no;
			$order_details = $order->Details;
			foreach ($order_details as $order_detail) {
				array_push($details, $order_detail);
			}
			$all_details = $details; 
        }else{
			$orders = SampleOrder::all();
			foreach ($orders as $order) {
				$order_details = $order->Details;
				foreach ($order_details as $order_detail) {
					array_push($details, $order_detail);
				}
			}
			$all_details = $details; 			
        }
        if($buyer || $article || $sample_type || $order_date || $delivery_date){				         	            
				if ($buyer) {
					$data['buyer_id'] = $buyer;
					foreach ($details as $detail) {
						if ($detail->SampleOrder->Buyer->id == $buyer) {
							array_push($sorted_buyer, $detail);
						}
					}
					$all_details = $sorted_buyer;
				}
				elseif ($sample_type) {
					$data['sample_type_id'] = $sample_type;
					foreach ($details as $detail) {
						if ($detail->SampleType->id == $sample_type) {
							array_push($sorted_sample_type, $detail);
						}
					}
					$all_details = $sorted_sample_type;
				}
				elseif ($article) {
					$data['article_no'] = $article;
					foreach ($details as $detail) {
						if ($detail->Article->id == $article) {
							array_push($sorted_article, $detail);
						}
					}
					$all_details = $sorted_article;
				}
				if ($buyer && $sample_type) {
					$data['buyer_id'] = $buyer;
					$data['sample_type_id'] = $sample_type;
					foreach ($details as $detail) {
						if ($detail->SampleOrder->Buyer->id == $buyer && $detail->SampleType->id == $sample_type) {
							array_push($sorted_buyer_sample_type, $detail);
						}
					}
					$all_details = $sorted_buyer_sample_type;
				}
				elseif ($buyer && $article) {
					$data['buyer_id'] = $buyer;
					$data['article_no'] = $article;
					foreach ($details as $detail) {
						if ($detail->SampleOrder->Buyer->id == $buyer && $detail->Article->id == $article) {
							array_push($sorted_buyer_article, $detail);
						}
					}
					$all_details = $sorted_buyer_article;
				}
				elseif ($sample_type && $article) {
					$data['sample_type_id'] = $sample_type;
					$data['article_no'] = $article;
					foreach ($details as $detail) {
						if ($detail->SampleType->id == $sample_type && $detail->Article->id == $article) {
							array_push($sorted_sample_type_article, $detail);
						}
					}
					$all_details = $sorted_sample_type_article;
				}
				elseif ($sample_type && $article && $buyer) {
					$data['buyer_id'] = $buyer;
					$data['sample_type_id'] = $sample_type;
					$data['article_no'] = $article;
					foreach ($details as $detail) {
						if ($detail->SampleOrder->Buyer->id == $buyer && $detail->SampleType->id == $sample_type && $detail->Article->id == $article) {
							array_push($sorted_buyer_sample_type_article, $detail);
						}
					}
					$all_details = $sorted_buyer_sample_type_article;
				}
				if ($order_date || $delivery_date) {
					if ($order_date) {
						$data['order_date'] = $order_date;
					}elseif ($delivery_date) {
						$data['delivery_date'] = $delivery_date;
					}else{
						$data['order_date'] = $order_date;
						$data['delivery_date'] = $delivery_date;
					}
					foreach ($all_details as $detail) {
						if ($detail->SampleOrder->order_date == $order_date || $detail->SampleOrder->delivery_date == $delivery_date) {
							array_push($sorted_by_date, $detail);
						}
					}
					$all_details = $sorted_by_date;
				}
			}
        $data['sample_order_no'] = array('' => 'Select Sample Order No') + SampleOrder::pluck('sample_order_no','id')->toArray();
        $data['buyer'] = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $data['article'] = array('' => 'Select Article No') + Article::pluck('article_no','id')->toArray();
        $data['sample_type'] = array('' => 'Select Type of Sample') + SampleType::pluck('sample_type','id')->toArray();
		$this->layout->content = View::make('sample_schedule.index', $data)->with('details', $all_details);
	}
}
