<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use App\ArticleType;
use App\Classes\Helpers;
use Illuminate\Http\Request;
use DateTime;
use Session;
use Validator;

class ArticleTypeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $types = ArticleType::all();

        //$this->layout->content = View::make('article_type.index')->with('types', $types);
		return view('article_type.index')->with('types', $types);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

        //$this->layout->content = View::make('article_type.create');
		return view('article_type.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'article_type'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('articleType/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type = new ArticleType;

            $type->article_type       = $request->input('article_type');

            $type->save();
            Session::flash('message', 'Successfully created a appearance!');
            return Redirect::to('articleType');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['article_type'] = ArticleType::find($id);

        //$this->layout->content = View::make('article_type.edit',$data);
		return view('article_type.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'article_type'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('articleType/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type['article_type'] = $request->input('article_type');
            $type['updated_at'] = new DateTime;

            ArticleType::where('id','=',$id)->update($type);

            Session::flash('message', 'Successfully updated a appearance!');
            return Redirect::to('articleType');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        ArticleType::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a appearance!');
        return Redirect::to('articleType');
	}

}