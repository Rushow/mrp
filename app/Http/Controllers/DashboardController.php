<?php
namespace App\Http\Controllers;


use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use View;
use HTML;


class DashboardController extends Controller  {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public $layout = 'layouts.master';

    public function index()
    {

        return view($this->layout, ['content' => 'dashboard.dashboard_view']);

    }
}