<?php

namespace App\Http\Controllers;

use App\DailyProductionComponentCombination;
use Illuminate\Http\Request;
use App\DailyProductionComponent;
use Illuminate\Support\Facades\Redirect;

class DailyProductionComponentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data["dailyProductionComponent"] = DailyProductionComponent::get();
        return view('daily_production_component.show',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();
        $data["sub_component"] = DailyProductionComponent::where("component_type",0)->pluck('component_name','id');
        $data["combination"] =0;
        return view('daily_production_component.create',$data);//->with('dailyProductionComponent',$dailyProductionComponent);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input  = $request->only('component_name');

        $com = new DailyProductionComponent();
        $com->component_name = $request->input('component_name');
        $com->component_type = $request->input('component_type');
        $com->save();

        if($com->component_type){
            $subcomponent = $request->input('sub_component_ids');
            foreach ($subcomponent as $sbcom){
                $comcombi = new DailyProductionComponentCombination();
                $comcombi->component_id = $com->id;
                $comcombi->sub_component_id= $sbcom;
                $comcombi->save();
            }
        }

        return Redirect::to('dailyproduction/component');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo $id."show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array();
        $data["sub_component"] = DailyProductionComponent::where("component_type",0)->pluck('component_name','id');
        $data["dailyProductionComponent"] = DailyProductionComponent::find($id);
        $data["combination"] = $data["dailyProductionComponent"]->component_type;
        return view('daily_production_component.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $com = DailyProductionComponent::find($id);
        $com->component_name = $request->input('component_name');
        $com->component_type = $request->input('component_type');
        $com->update();

        if($com->component_type){
            $subcomponent = $request->input('sub_component_ids');
            DailyProductionComponentCombination::where("component_id","=",$com->id)->delete();
            foreach ($subcomponent as $sbcom){
                $comcombi = new DailyProductionComponentCombination();
                $comcombi->component_id = $com->id;
                $comcombi->sub_component_id= $sbcom;
                $comcombi->save();
            }
        }


        return Redirect::to('dailyproduction/component');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
