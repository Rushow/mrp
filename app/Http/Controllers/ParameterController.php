<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\Parameter;
use Validator;
use View;

class ParameterController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $parameters = Parameter::all();

        return view('parameter.index')->with('parameters', $parameters);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('parameter.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'parameter_name'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('parameter/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $parameter = new Parameter;

            $parameter->parameter_name       = $request->get('parameter_name');

            $parameter->save();
            Session::flash('message', 'Successfully created a parameter!');
            return Redirect::to('parameter');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['parameter'] = Parameter::find($id);

        return view('parameter.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'parameter_name'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('parameter/edit/'.$id)->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $parameter['parameter_name'] = $request->get('parameter_name');
            $parameter['updated_at'] = new DateTime;

            Parameter::where('id','=',$id)->update($parameter);

            Session::flash('message', 'Successfully updated a parameter!');
            return Redirect::to('parameter');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Parameter::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a parameter!');
        return Redirect::to('parameter');
	}

}