<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\AdditionalStickerInfo;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\ItemGroup;
use App\ItemSubGroup;
use Validator;
use View;

class ItemSubGroupController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $groups = ItemSubGroup::all();
        foreach($groups as $item){
            $gn = ItemSubGroup::find($item['id'])->group;
            $item['group_name'] = $gn['group_name'];
        }
        return view('item_sub_group.index')->with('groups', $groups);
	}
	//dd

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data['item_group'] = ItemGroup::pluck('group_name','id')->toArray();
        return view('item_sub_group.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'sub_group_name'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('itemSubGroup/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $sub_group = new ItemSubGroup();

            $sub_group->sub_group_name       = $request->get('sub_group_name');
            $sub_group->group_id       = $request->get('group_id');

            $sub_group->save();
            Session::flash('message', 'Successfully created a sub group!');
            return Redirect::to('itemSubGroup');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['item_group'] = ItemGroup::pluck('group_name','id')->toArray();
        $data['sub_group'] = ItemSubGroup::find($id);

        return view('item_sub_group.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'sub_group_name'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('itemSubGroup/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $sub_group['sub_group_name'] = $request->get('sub_group_name');
            $sub_group['group_id'] = $request->get('group_id');
            $sub_group['updated_at'] = new DateTime;

            ItemSubGroup::where('id','=',$id)->update($sub_group);

            Session::flash('message', 'Successfully updated a sub group!');
            return Redirect::to('itemSubGroup');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        ItemSubGroup::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a sub group!');
        return Redirect::to('itemSubGroup');
	}

}