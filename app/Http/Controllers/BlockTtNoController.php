<?php
namespace App\Http\Controllers;


use App\BlockTtNo;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Classes\Helpers;
use Illuminate\Http\Request;
use DateTime;
use Session;
use View;

class BlockTtNoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $block_tt_no = BlockTtNo::all();

        //$this->layout->content = View::make('block_tt_no.index')->with('block_tt_no', $block_tt_no);
		return view('block_tt_no.index')->with('block_tt_no', $block_tt_no);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        //$this->layout->content = View::make('block_tt_no.create');
		return view('block_tt_no.create');//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'block_tt_no'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('blockTtNo/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $block_tt_no = new BlockTtNo;

            $block_tt_no->block_tt_no       = $request->input('block_tt_no');

            $block_tt_no->save();
            Session::flash('message', 'Successfully created a Block TT No!');
            return Redirect::to('blockTtNo');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['block_tt_no'] = BlockTtNo::find($id);

        return view('block_tt_no.edit',$data);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'block_tt_no'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('block_tt_no/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $block_tt_no['block_tt_no'] = $request->get('block_tt_no');
            $block_tt_no['updated_at'] = new DateTime;

            BlockTtNo::where('id','=',$id)->update($block_tt_no);

            Session::flash('message', 'Successfully updated a Block TT No!');
            return Redirect::to('blockTtNo');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        BlockTtNo::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a Block TT No!');
        return Redirect::to('blockTtNo');
	}

}