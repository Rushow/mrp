<?php

namespace App\Http\Controllers;

use App\ProductionOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Machinelist;
use App\Subdivision;
use App\division;

use App\GanttTask;
use App\GanttLink;

use Dhtmlx\Connector\JSONGanttConnector;



class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
//
        //print_r($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //print_r($request->all());die;
        //
        $task = new GanttLink();
        $task->order_id= $request->input("order_id");
        $task->source = $request->input("source");
        $task->target = $request->input("target");
        $task->type = $request->input("type");

        $task->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $task = GanttLink::find($id);
        //$task->order_id= $request->input("order_id");
        $task->source = $request->input("source");
        $task->target = $request->input("target");
        $task->type = $request->input("type");

        $task->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //print_r($id);
    }


}
