<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\ShippingMode;
use Validator;
use View;

class ShippingModeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $types = ShippingMode::all();

        return view('shipping_mode.index')->with('types', $types);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

        return view('shipping_mode.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'shipping_mode'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('shippingMode/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type = new ShippingMode;

            $type->shipping_mode       = $request->get('shipping_mode');

            $type->save();
            Session::flash('message', 'Successfully created a shipping mode!');
            return Redirect::to('shippingMode');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['shipping_mode'] = ShippingMode::find($id);

        return view('shipping_mode.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'shipping_mode'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('shippingMode/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type['shipping_mode'] = $request->get('shipping_mode');
            $type['updated_at'] = new DateTime;

            ShippingMode::where('id','=',$id)->update($type);

            Session::flash('message', 'Successfully updated a shipping mode!');
            return Redirect::to('shippingMode');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        ShippingMode::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a shipping mode!');
        return Redirect::to('shippingMode');
	}

}