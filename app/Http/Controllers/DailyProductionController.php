<?php

namespace App\Http\Controllers;

use App\SizeGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DateTime;
use Session;
use Validator;
use View;
use App\Article;
use App\Color;
use App\Unit;
use App\Division;
use App\ProductionOrder;
use App\Subdivision;
use App\Size;
use App\DailyProductionComponent;
use App\DailyProductionComponentCombination;
use App\DailyProductionStoreIn;
use App\DailyProductionStoreOut;
use App\DailyProductionDetailSize;
use App\DailyProductionDetailSizeOut;
use App\ProductionOrderDetailTod;
use App\DailyProductionInstruction;
use App\DailyProductionInstructionSize;
use App\Classes\Helpers;
use PDF;
use DB;

class DailyProductionController extends Controller
{
    //

    public function index()
    {

    }
    public function storein($id)
    {
        $data = array();
        $data['did'] = $id;
        $data['sub_division']    = array('' => 'Select Sub Division')+Subdivision::where("division_id",$id)->pluck('subdivision_name','id')->toArray();
        $data['production_order']   = ProductionOrder::pluck('order_no','id')->toArray();
        $data['article']            = Article::pluck('article_no','id')->toArray();
        $data['color'] = array('' => 'Select Color') + Color::pluck('color_name_flcl','id')->toArray();
        $data['unit'] =  DailyProductionComponent::pluck('component_name','id')->toArray();
        $data['sizegroup'] = array('' => 'Select Size Group') + SizeGroup::pluck('size_group_name','id')->toArray();
        $data['division_name'] = Division::find($id);
        $data['tod'] = ProductionOrderDetailTod::select(DB::raw("CONCAT(DATE_FORMAT(tod, '%d-%b,%Y'),',',(SELECT destination.destination_name FROM destination WHERE id=destination_id LIMIT 1)) AS tod"),'id')->pluck('tod','id');//
        return view('daily_production.storein', $data);

    }
    public function storeinpost(Request $request){

        //print_r($_POST);die();
        $data = array();
        $input=$request->only('department_id','sub_department_id','order_no','article_no','color_id','size_group_id','quantity','store_unit_component_id','store_in_date','production_order_detail_tod_id');//item_group_id
        $component =$input['store_unit_component_id'];

        foreach ($component as $comp){
            $DailyProductionStoreIn = new DailyProductionStoreIn();
            $DailyProductionStoreIn->department_id =$input['department_id'];
            $DailyProductionStoreIn->sub_department_id =$input['sub_department_id'];
            $DailyProductionStoreIn->production_order_id = $input['order_no'];
            $DailyProductionStoreIn->article_id = $input['article_no'];
            $DailyProductionStoreIn->color_id = $input['color_id'];
            $DailyProductionStoreIn->size_group_id = $input['size_group_id'];
            $DailyProductionStoreIn->quantity = $input['quantity'];
            $DailyProductionStoreIn->store_unit_component_id = $comp;
            $DailyProductionStoreIn->store_in_date = date ("Y-m-d H:i:s",  strtotime($input['store_in_date']));
            $DailyProductionStoreIn->save();
        }
        //enter detail size
        $allsizes = SizeGroup::find($input['size_group_id'])->size;

        foreach ($allsizes as $size){
            $dailyProductionDetailSize = new DailyProductionDetailSize;
            $dailyProductionDetailSize->daily_production_detail_id =$DailyProductionStoreIn->id;
            $dailyProductionDetailSize->production_order_id = $input['order_no'];
            $dailyProductionDetailSize->production_order_detail_tod_id = $input['production_order_detail_tod_id'];
            $dailyProductionDetailSize->size_id = $size->id;
            $dailyProductionDetailSize->quantity = $request->input('quantity_'.$size->size_no);
            $dailyProductionDetailSize->save();

        }
        //return redirect()->route('dailyproduction/summeryin/'.$input['department_id']);
        //redirect('dailyProduction/summery/'.$input['department_id']);
        return Redirect::to('dailyproduction/summeryin/'.$input['department_id']);
    }
    public function storeout($id)
    {
        $data = array();
        $data['did'] = $id;
        $data['sub_division']    = array('' => 'Select Sub Division')+Subdivision::where("division_id",$id)->pluck('subdivision_name','id')->toArray();
        $data['production_order']   = ProductionOrder::pluck('order_no','id')->toArray();
        $data['article']            = Article::pluck('article_no','id')->toArray();
        $data['color'] = array('' => 'Select Color') + Color::pluck('color_name_flcl','id')->toArray();
        $data['unit'] = DailyProductionComponent::pluck('component_name','id')->toArray();
        $data['sizegroup'] = array('' => 'Select Size Group') + SizeGroup::pluck('size_group_name','id')->toArray();
        $data['division_name'] = Division::find($id);
//        $data['tod'] = ProductionOrderDetailTod::pluck('tod','id','destination_id');
        $data['tod'] = ProductionOrderDetailTod::select(DB::raw("CONCAT(DATE_FORMAT(tod, '%d-%b,%Y'),',',(SELECT destination.destination_name FROM destination WHERE id=destination_id LIMIT 1)) AS tod"),'id')->pluck('tod','id');//
        $data['storeout_division'] = array('' => 'Select Division') + Division::pluck('name','id')->toArray();
        return view('daily_production.storeout', $data);
    }
    public function storeoutpost(Request $request){
        $data = array();
        $input=$request->only('department_id','sub_department_id','order_no','article_no','color_id','size_group_id','quantity','store_unit_component_id','store_out_division','store_out_date','production_order_detail_tod_id');//item_group_id
        $component =$input['store_unit_component_id'];

        foreach ($component as $comp){
            $DailyProductionStoreOut = new DailyProductionStoreOut();
            $DailyProductionStoreOut->department_id =$input['department_id'];
            $DailyProductionStoreOut->sub_department_id =$input['sub_department_id'];
            $DailyProductionStoreOut->production_order_id = $input['order_no'];
            $DailyProductionStoreOut->article_id = $input['article_no'];
            $DailyProductionStoreOut->color_id = $input['color_id'];
            $DailyProductionStoreOut->size_group_id = $input['size_group_id'];
            $DailyProductionStoreOut->quantity = $input['quantity'];
            $DailyProductionStoreOut->store_unit_component_id = $comp;
            $DailyProductionStoreOut->store_out_division = $input['store_out_division'];
            $DailyProductionStoreOut->store_out_date = date ("Y-m-d H:i:s",  strtotime($input['store_out_date']));
            $DailyProductionStoreOut->save();
        }
        $allsizes = SizeGroup::find($input['size_group_id'])->size;

        foreach ($allsizes as $size){
            $dailyProductionDetailSize = new DailyProductionDetailSizeOut;
            $dailyProductionDetailSize->daily_production_detail_id =$DailyProductionStoreOut->id;
            $dailyProductionDetailSize->production_order_id = $input['order_no'];
            $dailyProductionDetailSize->production_order_detail_tod_id = $input['production_order_detail_tod_id'];
            $dailyProductionDetailSize->size_id = $size->id;
            $dailyProductionDetailSize->quantity = $request->input('quantity_'.$size->size_no);
            $dailyProductionDetailSize->save();

        }
        //return redirect()->route('dailyproduction/summeryout/'.$input['department_id']);
        //redirect('dailyProduction/summery/1');
        return Redirect::to('dailyproduction/summeryout/'.$input['department_id']);
    }
    public function summeryin($id){
        $data = array();

        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['sub_division']    = Subdivision::where("division_id",$id)->pluck('subdivision_name','id');
        $data['division'] = Division::find($id); 
        $data['stockin'] = DailyProductionStoreIn::where('department_id',$id)->get();
        $data['size']  = Size::all();
        return view('daily_production.summeryin', $data);
    }
    public function summeryinSearch(Request $request,$id){
        $data = array();
        print_r($_POST);
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['sub_division']    = Subdivision::where("division_id",$id)->pluck('subdivision_name','id');
        $data['division'] = Division::find($id);
        $data['size']  = Size::all();
        //get post val
        $post_val = $request->all();
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';
        $production_date_from    = ($request->has('production_date_from'))?$request->get('production_date_from'):'';
        $production_date_to      = ($request->has('production_date_to'))?$request->get('production_date_to'):'';
        $sub_division     = ($request->has('sub_division'))?$request->get('sub_division'):'';

        $query = DailyProductionStoreIn::where('department_id',$id);

        if($order_no){
            $query->whereIn('production_order_id', $order_no);
        }
        if($article){
            $query->whereIn('article_id',$article);
        }
        if($color){
            $query->whereIn('color_id', $color);
        }
        if($sub_division){
            $query->whereIn('sub_department_id', $sub_division);
        }
        if(($production_date_from)&&($production_date_to)){
            $query->where('store_in_date', '>=',  date('Y-m-d',strtotime($production_date_from)))->where('store_in_date', '<=',  date('Y-m-d',strtotime($production_date_to)));
        }
        $data['stockin'] = $query->get();
        return view('daily_production.summeryin', $data)->with('post_val', $post_val);
    }
    public function summeryout($id){
        $data = array();

        $data['sub_division']    = Subdivision::where("division_id",$id)->pluck('subdivision_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['size']  = Size::all();
        $data['division'] = Division::find($id);
        $data['stockin'] = DailyProductionStoreOut::where('department_id',$id)->get();
        return view('daily_production.summeryout', $data);
    }
    public function summeryoutSearch(Request $request,$id){
        $data = array();

        $data['sub_division']    = Subdivision::where("division_id",$id)->pluck('subdivision_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['division'] = Division::find($id);
        $data['size']  = Size::all();
        //get post val
        $post_val = $request->all();
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';
        $production_date_from    = ($request->has('production_date_from'))?$request->get('production_date_from'):'';
        $production_date_to      = ($request->has('production_date_to'))?$request->get('production_date_to'):'';
        $sub_division     = ($request->has('sub_division'))?$request->get('sub_division'):'';

        $query = DailyProductionStoreOut::where('department_id',$id);
        if($order_no){
            $query->whereIn('production_order_id', $order_no);
        }
        if($article){
            $query->whereIn('article_id',$article);
        }
        if($color){
            $query->whereIn('color_id', $color);
        }
        if($sub_division){
            $query->whereIn('sub_department_id', $sub_division);
        }
        if(($production_date_from)&&($production_date_to)){
            $query->where('store_out_date', '>=', date('Y-m-d',strtotime($production_date_from)))->where('store_out_date', '<=',  date('Y-m-d',strtotime($production_date_to)));
        }
        $data['stockin'] = $query->get();
        //$data['stockin'] = DailyProductionStoreOut::where('department_id',$id)->get();
        return view('daily_production.summeryout', $data)->with('post_val', $post_val);
    }
    public function balance($id){
        
        $data = array();
        $data['sub_division']    = Subdivision::where("division_id",$id)->pluck('subdivision_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['division'] = Division::find($id);
        
        $data["stockin"] =  DailyProductionStoreIn::where('department_id',$id)->get();
        $data["stockout"] = DailyProductionStoreOut::where('department_id',$id)->get();
        $data['component'] = DailyProductionStoreOut::where('store_out_division',$id)->get();
        
        return view('daily_production.balance', $data);

    }
    public function balanceSearch(Request $request,$id){

        $data = array();
        $data['sub_division']    = Subdivision::where("division_id",$id)->pluck('subdivision_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['division'] = Division::find($id);
        //get post val
        $post_val = $request->all();
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';
        $production_date_from    = ($request->has('production_date_from'))?$request->get('production_date_from'):'';
        $production_date_to      = ($request->has('production_date_to'))?$request->get('production_date_to'):'';
        $sub_division     = ($request->has('sub_division'))?$request->get('sub_division'):'';
        $store_component = ($request->has('store_unit_component_id'))?$request->get('store_unit_component_id'):'';

        $query = DailyProductionStoreIn::where('department_id',$id);
        if($order_no){
            $query->whereIn('production_order_id', $order_no);
        }
        if($article){
            $query->whereIn('article_id',$article);
        }
        if($color){
            $query->whereIn('color_id', $color);
        }
        if($sub_division){
            $query->whereIn('sub_department_id', $sub_division);
        }
        if($store_component){
            $query->whereIn('store_unit_component_id', $store_component);
        }
        if(($production_date_from)&&($production_date_to)){
            $query->where('store_in_date', '>=', date('Y-m-d',strtotime($production_date_from)))->where('store_in_date', '<=',  date('Y-m-d',strtotime($production_date_to)));
        }
        $data['stockin'] = $query->get();

        $queryStockout = DailyProductionStoreOut::where('department_id',$id);
        if($order_no){
            $queryStockout->whereIn('production_order_id', $order_no);
        }
        if($article){
            $queryStockout->whereIn('article_id',$article);
        }
        if($color){
            $queryStockout->whereIn('color_id', $color);
        }
        if($sub_division){
            $queryStockout->whereIn('sub_department_id', $sub_division);
        }
        if(($production_date_from)&&($production_date_to)){
            $queryStockout->where('store_out_date', '>=', date('Y-m-d',strtotime($production_date_from)))->where('store_out_date', '<=',  date('Y-m-d',strtotime($production_date_to)));
        }
        $data['stockout'] = $queryStockout->get();


        $component = DailyProductionStoreOut::where('store_out_division',$id);
        if($order_no){
            $component->whereIn('production_order_id', $order_no);
        }
        if($article){
            $component->whereIn('article_id',$article);
        }
        if($color){
            $component->whereIn('color_id', $color);
        }
        if($sub_division){
            $component->whereIn('sub_department_id', $sub_division);
        }
        if(($production_date_from)&&($production_date_to)){
            $component->where('store_out_date', '>=', date('Y-m-d',strtotime($production_date_from)))->where('store_out_date', '<=',  date('Y-m-d',strtotime($production_date_to)));
        }
        $data['component'] = $component->get();
        
        return view('daily_production.balanceresult', $data)->with('post_val', $post_val);

    }
    public function totalbalance($id){

        $data = array();
        $data['sub_division']    = Subdivision::where("division_id",$id)->pluck('subdivision_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['division'] = Division::find($id);
        $data['size']  = Size::all();
        $data["stockin"] =  DailyProductionStoreIn::where('department_id',$id)->get();
        $data["stockout"] = DailyProductionStoreOut::where('department_id',$id)->get();
        $data['component'] = DailyProductionStoreOut::where('store_out_division',$id)->get();

        return view('daily_production.totalbalance', $data);

    }
    public function totalbalanceSearch(Request $request,$id){

        $data = array();
        $data['sub_division']    = Subdivision::where("division_id",$id)->pluck('subdivision_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['division'] = Division::find($id);
        $data['size']  = Size::all();
        //get post val
        $post_val = $request->all();
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';
        $production_date_from    = ($request->has('production_date_from'))?$request->get('production_date_from'):'';
        $production_date_to      = ($request->has('production_date_to'))?$request->get('production_date_to'):'';
        $sub_division     = ($request->has('sub_division'))?$request->get('sub_division'):'';
        $store_component = ($request->has('store_unit_component_id'))?$request->get('store_unit_component_id'):'';


        $order_input = $request->only('sub_division', 'order_no');
        $rules =array(
            'order_no' => array('required'),
            'sub_division' => array('required')
        );
        $validator = Validator::make($order_input,$rules);
        if ($validator->passes()){

            ###################### today's Production #####################################

            $query = DailyProductionStoreIn::select('*')->where('department_id',$id);
            if($order_no){
                $query->whereIn('production_order_id', $order_no);
            }
            if($article){
                $query->whereIn('article_id',$article);
            }
            if($color){
                $query->whereIn('color_id', $color);
            }
            if($sub_division){
                $query->whereIn('sub_department_id', $sub_division);
            }
            if($store_component){
                $query->whereIn('store_unit_component_id', $store_component);
            }

            $query->where('store_in_date', '>=', date('Y-m-d'))->where('store_in_date', '<=',  date('Y-m-d'));

            $tdstockin = $query->get();
            $tdsizequantity =array();

            foreach($data['size'] as $size){
                $tdsizequantity[$size->id] = null;
            }
            foreach ($tdstockin as $tdstkin){

                $qty = DailyProductionDetailSize::where('daily_production_detail_id', $tdstkin->id)->get();
                foreach($qty  as $qt){
                    $tdsizequantity[$qt->size_id] = $tdsizequantity[$qt->size_id]+$qt['quantity'];
                }
            }
            $tdsizequantity['sum'] = $query->sum('quantity');
            $data['tdsizequantity'] = $tdsizequantity;

            ##################### Accumulated Production #################################

            $query = DailyProductionStoreIn::where('department_id',$id);
            if($order_no){
                $query->whereIn('production_order_id', $order_no);
            }
            if($article){
                $query->whereIn('article_id',$article);
            }
            if($color){
                $query->whereIn('color_id', $color);
            }
            if($sub_division){
                $query->whereIn('sub_department_id', $sub_division);
            }
            if($store_component){
                $query->whereIn('store_unit_component_id', $store_component);
            }

            $accumulatestockin = $query->get();
            $acsizequantity = array();
            foreach($data['size'] as $size){
                $acsizequantity[$size->id] = null;
            }
            foreach ($accumulatestockin as $accumulated){

                $qty = DailyProductionDetailSize::where('daily_production_detail_id', $accumulated->id)->get();
                foreach($qty  as $qt){
                    $acsizequantity[$qt->size_id] = $acsizequantity[$qt->size_id]+$qt['quantity'];
                }
            }
            $acsizequantity['sum'] = $query->sum('quantity');
            $data['acsizequantity'] = $acsizequantity;


            ################calculate total order quantity###############################

            $ordsizequantity = array();

            foreach($data['size'] as $size){
                $total_qty = 0;
                $order_details = ProductionOrderDetailTod::where('production_order_id', '=',  $order_no );
                if($article){
                    $order_details->whereIn('article_id',$article);
                }
                if($color){
                    $order_details->whereIn('color_id', $color);
                }

                $orderinfo = $order_details->get();

                foreach($orderinfo as $details){
                    $qty = ($details->id && $size->id)?Helpers::getPOSizeQuantity($details->id, $size->id):0;
                    if(isset($ordsizequantity[$size->id])){
                        $ordsizequantity[$size->id]= $ordsizequantity[$size->id]+ $qty;
                    }else{
                        $ordsizequantity[$size->id]= $qty;
                    }
                }
                $data['order_total'] = $orderinfo->sum('total_quantity');
            }
            $data['ordsizequantity'] = $ordsizequantity;

            ########################## Component ########################################

            $data['component_inventory'] =array();

            $componentlist = DailyProductionStoreOut::where('store_out_division',$id);
            if($order_no){

                $componentlist->whereIn('production_order_id', $order_no);
            }
            if($article){

                $componentlist->whereIn('article_id',$article);
            }
            if($color){

                $componentlist->whereIn('color_id', $color);
            }


            $listofcomponent = $componentlist->groupBy('store_unit_component_id')->get();
            $componentsizequantity =array();
            foreach ($listofcomponent as $list){
                $component_name= $list->Component->component_name;
                $componentsizequantity[$component_name] = array();
                $data['component_inventory'][$component_name] =array();
                foreach($data['size'] as $size){
                    $componentsizequantity[$component_name][$size->id] = null;
                    $data['component_inventory'][$component_name][$size->id] =null;
                }
                $data['component_inventory'][$component_name]['sum'] =0;


                $component = DailyProductionStoreOut::where('store_out_division',$id);
                if($order_no){
                    $component->whereIn('production_order_id', $order_no);
                }
                if($article){
                    $component->whereIn('article_id',$article);
                }
                if($color){
                    $component->whereIn('color_id', $color);

                }
                $accumulatecomponent = $component->where('store_unit_component_id','=',$list->store_unit_component_id)->get();

                foreach ($accumulatecomponent as $accumulated){

                    $qty = DailyProductionDetailSizeOut::where('daily_production_detail_id', $accumulated->id)->get();
                    foreach($qty  as $qt){
                        $componentsizequantity[$component_name][$qt->size_id] = $componentsizequantity[$component_name][$qt->size_id]+$qt['quantity'];
                    }
                }

                //component inventory
                foreach ($componentsizequantity[$component_name] as $k=>$val){
                    $data['component_inventory'][$component_name][$k] = $val - $ordsizequantity[$k];
                    $data['component_inventory'][$component_name]['sum'] +=$data['component_inventory'][$component_name][$k];
                }

                //total sum for component
                $componentsizequantity[$component_name]['sum'] = $component->sum('quantity');

            }
            $data['components'] = $componentsizequantity;

            return view('daily_production.totalbalanceresult', $data)->with('post_val', $post_val);


        }else{
            return Redirect::to('dailyproduction/totalbalance/'.$id)->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
    }
    public function downloadtotalbalance($order_id)
    {
        $data=array();
        $data["order"] = ProductionOrder::find($order_id);
        $pdf = PDF::loadView('dailyproduction/totalbalanceresult/',$data)->setPaper('a4', 'portrait');
        return $pdf->download('dailyproduction_balance_'.date('d-m-Y').'.pdf');
    }
    public function componentrecieved($id){
        
        $data = array();
        $data['sub_division']    = Subdivision::where("division_id",$id)->pluck('subdivision_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['division'] = Division::find($id);

        
        $data['stockin'] = DailyProductionStoreOut::where('store_out_division',$id)->get();
        return view('daily_production.componentrecieved', $data);
        
        
    }
    public function componentrecievedSearch(Request $request,$id){

        $data = array();
        $data['sub_division']    = Subdivision::where("division_id",$id)->pluck('subdivision_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['division'] = Division::find($id);

        //get post val
        $post_val = $request->all();
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';
        $production_date_from    = ($request->has('production_date_from'))?$request->get('production_date_from'):'';
        $production_date_to      = ($request->has('production_date_to'))?$request->get('production_date_to'):'';
        $sub_division     = ($request->has('sub_division'))?$request->get('sub_division'):'';

        $query = DailyProductionStoreOut::where('store_out_division',$id);
        if($order_no){
            $query->whereIn('production_order_id', $order_no);
        }
        if($article){
            $query->whereIn('article_id',$article);
        }
        if($color){
            $query->whereIn('color_id', $color);
        }
        if($sub_division){
            $query->whereIn('sub_department_id', $sub_division);
        }
        if(($production_date_from)&&($production_date_to)){
            $query->where('store_out_date', '>=', date('Y-m-d',strtotime($production_date_from)))->where('store_out_date', '<=',  date('Y-m-d',strtotime($production_date_to)));
        }
        $data['stockin'] = $query->get();
        return view('daily_production.componentrecieved', $data)->with('post_val', $post_val);


    }
    public function dailyinstruction(Request $request,$id){
        $data = array();
        $sub_division    = Subdivision::where("division_id",$id)->get();
        $data['division'] = Division::find($id);
        $data['size']  = Size::all();
        $dailyinstruction = array();
        foreach ($sub_division as $child){
            $dailyinstruction[$child->subdivision_name] =  DailyProductionInstruction::where('department_id',$id)->where('sub_department_id',$child->id)->where('production_in_date', '>=', date('Y-m-d'))->where('production_in_date', '<=',  date('Y-m-d'))->get();
        }
        $data['dailyinstructions']= $dailyinstruction;
        return view('daily_production.dailyinstruction', $data);
    }
    public function instruction(){

        $data = array();
        $data['division'] = Division::pluck('name','id');
        $data['sub_division']    = Subdivision::pluck('subdivision_name','id')->toArray();
        $data['production_order']   = ProductionOrder::pluck('order_no','id')->toArray();
        $data['article']            = Article::pluck('article_no','id')->toArray();
        $data['color'] = Color::pluck('color_name_flcl','id')->toArray();
        $data['unit'] =  DailyProductionComponent::pluck('component_name','id')->toArray();
        $data['sizegroup'] = array('' => 'Select Size Group') + SizeGroup::pluck('size_group_name','id')->toArray();
        //$data['division_name'] = Division::all();
        $data['instruction'] = DailyProductionInstruction::paginate(20);
        $data['size'] =Size::all();
        
        return view('daily_production.instruction', $data);

    }
    public function instructionjson(){
        
        $instruction = DailyProductionInstruction::with('division','article','color')->get();
//        echo "<pre>";
        echo json_encode($instruction);

    }
    public function instructionsearch(Request $request){

        $data = array();
        $data['division'] = Division::pluck('name','id');
        $data['sub_division']    = Subdivision::pluck('subdivision_name','id')->toArray();
        $data['production_order']   = ProductionOrder::pluck('order_no','id')->toArray();
        $data['article']            = Article::pluck('article_no','id')->toArray();
        $data['color'] = Color::pluck('color_name_flcl','id')->toArray();
        $data['unit'] =  DailyProductionComponent::pluck('component_name','id')->toArray();
        $data['sizegroup'] = array('' => 'Select Size Group') + SizeGroup::pluck('size_group_name','id')->toArray();
        $data['size']=Size::all();
        //get post val
        $post_val = $request->all();
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';
        $production_date_from    = ($request->has('production_date_from'))?$request->get('production_date_from'):'';
        $production_date_to      = ($request->has('production_date_to'))?$request->get('production_date_to'):'';
        $sub_division     = ($request->has('sub_division'))?$request->get('sub_division'):'';
        $division     = ($request->has('division'))?$request->get('division'):'';
        
        $query = DailyProductionInstruction::query();
        if($order_no){
            $query->whereIn('production_order_id', $order_no);
        }
        if($article){
            $query->whereIn('article_id',$article);
        }
        if($color){
            $query->whereIn('color_id', $color);
        }
        if($sub_division){
            $query->whereIn('sub_department_id', $sub_division);
        }
        if($division){
            $query->whereIn('department_id', $division);
        }
        if(($production_date_from)&&($production_date_to)){
            $query->where('production_in_date', '>=', date('Y-m-d',strtotime($production_date_from)))->where('production_in_date', '<=',  date('Y-m-d',strtotime($production_date_to)));
        }
        //echo $query->toSql();die();
        $data['instruction'] = $query->paginate(20);
        return view('daily_production.instruction', $data)->with('post_val',$post_val);

    }
    public function addinstruction(){
        $data = array();
        $data['sub_division']    = array('' => 'Select Sub Division')+Subdivision::pluck('subdivision_name','id')->toArray();
        $data['production_order']   = ProductionOrder::pluck('order_no','id')->toArray();
        $data['article']            = Article::pluck('article_no','id')->toArray();
        $data['color'] = array('' => 'Select Color') + Color::pluck('color_name_flcl','id')->toArray();
        $data['unit'] =  DailyProductionComponent::pluck('component_name','id')->toArray();
        $data['sizegroup'] = array('' => 'Select Size Group') + SizeGroup::pluck('size_group_name','id')->toArray();
        $data['division'] = Division::pluck('name','id');


        return view('daily_production.addinstruction', $data);
    }
    public function addinstructionpost(Request $request){
        $data = array();
        $input=$request->only('department_id','sub_department_id','order_no','article_no','color_id','size_group_id','quantity','component_id','production_in_date');//item_group_id
        $component =$input['component_id'];

        foreach ($component as $comp){
            $DailyProductionInstruction = new DailyProductionInstruction();
            $DailyProductionInstruction->department_id =$input['department_id'];
            $DailyProductionInstruction->sub_department_id =$input['sub_department_id'];
            $DailyProductionInstruction->production_order_id = $input['order_no'];
            $DailyProductionInstruction->article_id = $input['article_no'];
            $DailyProductionInstruction->color_id = $input['color_id'];
            $DailyProductionInstruction->size_group_id = $input['size_group_id'];
            $DailyProductionInstruction->quantity = $input['quantity'];
            $DailyProductionInstruction->component_id = $comp;
            $DailyProductionInstruction->production_in_date = date ("Y-m-d H:i:s",  strtotime($input['production_in_date']));
            $DailyProductionInstruction->save();
        }
        //enter detail size
        $allsizes = SizeGroup::find($input['size_group_id'])->size;

        foreach ($allsizes as $size){
            $DailyProductionInstructionSize = new DailyProductionInstructionSize();
            $DailyProductionInstructionSize->daily_production_instruction_id =$DailyProductionInstruction->id;
            //$DailyProductionInstructionSize->production_order_id = $input['order_no'];
            $DailyProductionInstructionSize->size_id = $size->id;
            $DailyProductionInstructionSize->quantity = $request->input('quantity_'.$size->size_no);
            $DailyProductionInstructionSize->save();

        }

        return Redirect::to('dailyproduction/instruction');
    }

    public function report(){
        
        $data = array();
        return view('daily_production.dailyreport', $data);
    }
    public function getSizelistbyGroup(Request $request){

        $id = $request->get('size_group_id');
        if(isset($id)){
            $data = array();
            $data['error'] = false;
            $sizes = SizeGroup::find($id);
            $data['sizes'] = array();
            $i = 0;
            foreach($sizes as $size){
                $data['sizes'][$i]['size_id'] = $size->size_id;
                $data['sizes'][$i]['size_no'] = Size::find($size->size_id)->size_no;
                $data['sizes'][$i]['quantity'] = $size->quantity;
                $i++;
            }
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

    public function getSizesBySizeGroup(Request $request){
        $id = $request->get('size_group_id');
        if(isset($id)){
            $data = array();
            $data['error'] = false;
            $data['sizes'] = SizeGroup::find($id)->size->toArray();
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }
    
}
