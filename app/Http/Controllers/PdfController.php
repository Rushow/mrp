<?php
namespace App\Http\Controllers;
use App\ProductionOrder;
use App\Buyer;
use App\Status;
use App\Destination;
use App\Article;
use App\Color;
use App\Season;
use App\ProductionOrderDespo;
use App\ProductionOrderArticle;
use App\ProductionOrderColor;
use App\ProductionOrderDestination;
use App\ProductionOrderDetail;
use App\PaymentTerm;
use App\Currency;
use App\ShippingMode;
use App\Bank;
use App\TermsOfDelivery;
use App\Forwarder;
use App\SizeGroup;


use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use View;
use HTML;
use App\Classes\Helpers;
use Validator;
use TCPDF;
use Session;

class PdfController extends Controller {
	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function articleType()
	{
		$types = ArticleType::all();

	    // create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Appearance');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Appearance List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Appearance</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($types as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->article_type . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('appearance', 'I');

		return Redirect::to('articleType');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function blockTtNo()
	{
		$blocks = BlockTtNo::all();

	    // create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Block TT No');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Block TT No List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Block TT No</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($blocks as $key => $block) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $block->block_tt_no . '</td>
                <td class="second">' . $block->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('blockTtNo', 'I');

		return Redirect::to('blockTtNo');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function construction()
	{
		$constructions = Construction::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Construction');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Construction List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Construction</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($constructions as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->construction . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('construction', 'I');

		return Redirect::to('construction');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function category()
	{
		$categories = Category::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Category');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Category List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Category</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($categories as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->category_name . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('category', 'I');

		return Redirect::to('category');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function size()
	{
		$sizes = Size::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Size');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Size List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Size</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($sizes as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->size_no . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('size', 'I');

		return Redirect::to('size');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function sizeGroup()
	{
		$sizeGroups = SizeGroup::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Size Group');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Size Group List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Size Group</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($sizeGroups as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->size_group_name . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('size_group', 'I');

		return Redirect::to('sizeGroup');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function color()
	{
		$colors = Color::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Color');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Color List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Color</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($colors as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->color_name . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('colors', 'I');

		return Redirect::to('color');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function itemGroup()
	{
		$itemGroups = ItemGroup::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Item Group');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Item Group List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Item Group</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($itemGroups as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->group_name . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('item_group', 'I');

		return Redirect::to('itemGroup');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function itemSubGroup()
	{
		$itemSubGroups = ItemSubGroup::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Item Sub Group');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Item Sub Group List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Item Sub Group</b></th>
							<th class="first" ><b>Group Name</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($itemSubGroups as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->sub_group_name . '</td>
                <td class="second">' . $type->group()->first()->group_name . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('item_sub_group', 'I');

		return Redirect::to('itemSubGroup');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function lastNo()
	{
		$lastNos = LastNo::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Last No');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Last No List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Last No</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($lastNos as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->last_no . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('last_no', 'I');

		return Redirect::to('lastNo');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function heelNo()
	{
		$heelNos = HeelNo::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Heel No');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Heel No List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Heel No</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($heelNos as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->heel_no . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('heel_no', 'I');

		return Redirect::to('heelNo');
	}

			/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function outsoleNo()
	{
		$outsoleNos = OutsoleNo::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Outsole No');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Outsole No List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Outsole No</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($outsoleNos as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->outsole_no . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('outsole_no', 'I');

		return Redirect::to('outsoleNo');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function buyer()
	{
		$buyers = Buyer::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Buyer');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Buyer List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Buyer</b></th>
							<th class="first" ><b>Name</b></th>
							<th class="first" ><b>Email</b></th>
							<th class="first" ><b>Phone</b></th>
							<th class="first" ><b>Address</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($buyers as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->buyer_name . '</td>
                <td class="second">' . $type->full_name . '</td>
                <td class="second">' . $type->buyer_email . '</td>
                <td class="second">' . $type->buyer_phn . '</td>
                <td class="second">' . $type->buyer_address . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('buyer', 'I');

		return Redirect::to('buyer');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function supplier()
	{
		$suppliers = Supplier::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Supplier');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Supplier List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Supplier Name</b></th>
							<th class="first" ><b>Supplier Location</b></th>
							<th class="first" ><b>Remarks</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($suppliers as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->supplier_name . '</td>
                <td class="second">' . $type->supplier_location . '</td>
                <td class="second">' . $type->remarks . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('supplier', 'I');


		return Redirect::to('supplier');
	}	

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function parameter()
	{
		$parameters = Parameter::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Parameter');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Parameter List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Parameter Name</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($parameters as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->parameter_name . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('parameter', 'I');

		return Redirect::to('parameter');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function unit()
	{
		$units = Unit::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Unit');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Unit List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Unit Name</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($units as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->unit_name . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('unit', 'I');

		return Redirect::to('unit');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function sampleType()
	{
		$sampleTypes = SampleType::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Sample');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Sample List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Sample Type</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($sampleTypes as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->sample_type . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('sample_type', 'I');

		return Redirect::to('sampleType');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function component()
	{
		$components = Component::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Component');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Component List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Component Name</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($components as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->component_name . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('component', 'I');

		return Redirect::to('component');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function season()
	{
		$seasons = Season::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Season');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Season List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Season</b></th>
							<th class="first" ><b>Start Date</b></th>
							<th class="first" ><b>End Date</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($seasons as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->season . '</td>
                <td class="second">' . date('d-m-Y', $type->start_date) . '</td>
                <td class="second">' . date('d-m-Y', $type->end_date) . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('season', 'I');

		return Redirect::to('season');
	}

		/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function destination()
	{
		$destinations = Destination::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Destination');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Destination List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Destination Name</b></th>
							<th class="first" ><b>Destination Code</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($destinations as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->destination_name . '</td>
                <td class="second">' . $type->destination_code . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('destination', 'I');

		return Redirect::to('destination');
	}

		/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function department()
	{
		$departments = Department::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Department');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Department List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Department Name</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($departments as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->name . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('department', 'I');

		return Redirect::to('department');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function paymentTerm()
	{
		$paymentTerms = PaymentTerm::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Payment Term');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Payment Term List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Term</b></th>
							<th class="first" ><b>Date Created</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($paymentTerms as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->term . '</td>
                <td class="second">' . $type->created_at->format('d.m.Y') . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('payment_term', 'I');

		return Redirect::to('paymentTerm');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function item()
	{
		$items = Item::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Item');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Item List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" colspan="2" ><b>Material Code</b></th>
							<th class="first" colspan="2" ><b>Material Name</b></th>
							<th class="first" ><b>Group</b></th>
							<th class="first" ><b>Sub Group</b></th>
							<th class="first" ><b>Parameter</b></th>
							<th class="first" ><b>Color</b></th>
							<th class="first" ><b>Store Unit</b></th>
							<th class="first" ><b>Purchase Unit</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($items as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second" colspan="2" >' . $type->item_code . '</td>
                <td class="second" colspan="2" >' . $type->item_name . '</td>
                <td class="second">' . $type->Group->group_name . '</td>
                <td class="second">' . $type->SubGroup->sub_group_name . '</td>
                <td class="second">' . $type->Parameter->parameter_name . '</td>
                <td class="second">' . $type->Color->color_name . '</td>
                <td class="second">' . $type->StoreUnit->unit_name . '</td>
                <td class="second">' . $type->PurchaseUnit->unit_name . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('sample_order', 'I');

		return Redirect::to('item');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function article()
	{
		$articles = Article::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('article');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">article List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Article No</b></th>
							<th class="first" ><b>Article Ref</b></th>
							<th class="first" colspan="2" ><b>Appearance</b></th>
							<th class="first" colspan="2" ><b>Construction</b></th>
							<th class="first" colspan="2" ><b>Customer Group</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($articles as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->article_no . '</td>
                <td class="second">' . $type->article_ref . '</td>
                <td class="second" colspan="2" >' . $type->ArticleType->article_type . '</td>
                <td class="second" colspan="2" >' . $type->Construction->construction . '</td>
                <td class="second" colspan="2" >' . $type->Category->category_name . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('sample_order', 'I');

		return Redirect::to('article');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function storeIn()
	{
		$storeIns = StoreIn::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Store Ins');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Store In List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Challan No</b></th>
							<th class="first" colspan="2" ><b>Warehouse</b></th>
							<th class="first" colspan="2" ><b>Supplier</b></th>
							<th class="first" colspan="2" ><b>Date</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($storeIns as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->challan_no . '</td>
                <td class="second" colspan="2" >' . $type->Warehouse->name . '</td>
                <td class="second" colspan="2" >' . $type->Supplier->supplier_name . '</td>
                <td class="second" colspan="2" >' . $type->store_in_date . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('store_in', 'I');

		return Redirect::to('storeIn');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function storeOut()
	{
		$storeOuts = StoreOut::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Store Ins');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Store In List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Challan No</b></th>
							<th class="first" colspan="2" ><b>Warehouse</b></th>
							<th class="first" colspan="2" ><b>Department</b></th>
							<th class="first" colspan="2" ><b>Date</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($storeOuts as $key => $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $key . '</td>
                <td class="second">' . $type->challan_no . '</td>
                <td class="second" colspan="2" >' . $type->Warehouse->name . '</td>
                <td class="second" colspan="2" >' . $type->Department->name . '</td>
                <td class="second" colspan="2" >' . $type->store_out_date . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('store_in', 'I');

		return Redirect::to('storeOut');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function storeInDetail($id)
	{
		$storeIn = StoreIn::find($id);
		$storeInDetails = $storeIn->Details;

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Store In Challan No. ' . $storeIn->challan_no);

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Store In Challan No. ' . $storeIn->challan_no . '</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" colspan="1"><b>#</b></th>
							<th class="first" colspan="2"><b>Item Group</b></th>
							<th class="first" colspan="2" ><b>Sub Group</b></th>
							<th class="first" colspan="2" ><b>Parameter</b></th>
							<th class="first" colspan="1" ><b>Color</b></th>
							<th class="first" colspan="3" ><b>Item</b></th>
							<th class="first" colspan="1" ><b>Unit</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($storeInDetails as $type) {
			$tbl = $tbl . '<tr>
                <td class="second" colspan="1">' . $type->id . '</td>
                <td class="second" colspan="2">' . $type->ItemGroup->group_name . '</td>
                <td class="second" colspan="2" >' . $type->ItemSubGroup->sub_group_name . '</td>
                <td class="second" colspan="2" >' . $type->Parameter->parameter_name . '</td>
                <td class="second" colspan="1" >' . $type->Color->color_name . '</td>
                <td class="second" colspan="3" >' . $type->Item->item_name . '</td>
                <td class="second" colspan="1" >' . $type->Unit->unit_name . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('store_in_' . $storeIn->challan_no . '_details', 'I');

		return Redirect::to('storeIn');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function storeOutDetail($id)
	{
		$storeOut = StoreOut::find($id);
		$storeOutDetails = $storeOut->Details;

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Store Out Challan No. ' . $storeOut->challan_no);

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Store Out Challan No. ' . $storeOut->challan_no . '</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" colspan="2"><b>Item Group</b></th>
							<th class="first" colspan="2" ><b>Sub Group</b></th>
							<th class="first" colspan="2" ><b>Parameter</b></th>
							<th class="first" colspan="1" ><b>Color</b></th>
							<th class="first" colspan="3" ><b>Item</b></th>
							<th class="first" colspan="1" ><b>Unit</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($storeOutDetails as $type) {
			$tbl = $tbl . '<tr>
                <td class="second">' . $type->id . '</td>
                <td class="second" colspan="2">' . $type->ItemGroup->group_name . '</td>
                <td class="second" colspan="2" >' . $type->ItemSubGroup->sub_group_name . '</td>
                <td class="second" colspan="2" >' . $type->Parameter->parameter_name . '</td>
                <td class="second" colspan="1" >' . $type->Color->color_name . '</td>
                <td class="second" colspan="3" >' . $type->Item->item_name . '</td>
                <td class="second" colspan="1" >' . $type->Unit->unit_name . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('store_out_' . $storeOut->challan_no . '_details', 'I');

		return Redirect::to('storeOut');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function sampleOrderDetail($id)
	{
		$sampleOrder = SampleOrder::find($id);
		$sampleOrderDetails = $sampleOrder->Details;
		$details = array();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Sample Order No. ' . $sampleOrder->sample_order_no);

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Sample Order No. ' . $sampleOrder->sample_order_no . '</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" colspan="2"><b>Article</b></th>
							<th class="first" colspan="2" ><b>Sample Type</b></th>
							<th class="first" colspan="2" ><b>Size</b></th>
							<th class="first" colspan="1" ><b>Color</b></th>
							<th class="first" colspan="3" ><b>Quantity</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($sampleOrderDetails as $key => $type) {
			$sizes = '';
			$count = 1;
			foreach ($type->Sizes as $size) {
				$sizes = $sizes . $size->size_no;
				if (count($type->Sizes) != $count) {
					$sizes = $sizes . ', ';
				}
				$count++;
			}
			$tbl = $tbl . '<tr>
                <td class="second">' . ($key + 1) . '</td>
                <td class="second" colspan="2">' . $type->Article->article_no . '</td>
                <td class="second" colspan="2" >' . $type->SampleType->sample_type . '</td>
                <td class="second" colspan="2" >' . $sizes . '</td>
                <td class="second" colspan="1" >' . $type->Color->color_name . '</td>
                <td class="second" colspan="3" >' . $type->quantity . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('sample_order_no_' . $sampleOrder->sample_order_no, 'I');

		return Redirect::to('sampleOrder');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function sampleSchedule()
	{
		$sampleSchedules = SampleSchedule::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Sample Schedule List');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Sample Schedule List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" colspan="2"><b>Order No.</b></th>
							<th class="first" colspan="2" ><b>Article No.</b></th>
							<th class="first" colspan="1" ><b>Buyer</b></th>
							<th class="first" colspan="1" ><b>Color</b></th>
							<th class="first" colspan="1" ><b>Sizes</b></th>
							<th class="first" colspan="1" ><b>Qty/Size</b></th>
							<th class="first" colspan="2" ><b>Sample Type</b></th>
							<th class="first" colspan="1" ><b>Order Date</b></th>
							<th class="first" colspan="1" ><b>Customer Req. Delivery Date</b></th>
							<th class="first" colspan="1" ><b>Scheduled Delivery Date</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($sampleSchedules as $key => $schedule) {
			$sizes = '';
			$count = 1;
			foreach ($schedule->SampleOrderDetail->Sizes as $size) {
				$sizes = $sizes . $size->size_no;
				if (count($schedule->Sizes) != $count) {
					$sizes = $sizes . ', ';
				}
				$count++;
			}
			$tbl = $tbl . '<tr>
                <td class="second">' . ($key + 1) . '</td>
                <td class="second" colspan="2">' . $schedule->SampleOrderDetail->SampleOrder->sample_order_no . '</td>
                <td class="second" colspan="2" >' . $schedule->SampleOrderDetail->Article->article_no . '</td>
                <td class="second" colspan="1" >' . $schedule->SampleOrderDetail->SampleOrder->Buyer->buyer_name . '</td>
                <td class="second" colspan="1" >' . $schedule->SampleOrderDetail->Color->color_name . '</td>
                <td class="second" colspan="1" >' . $sizes . '</td>
                <td class="second" colspan="1" >' . $schedule->SampleOrderDetail->quantity . '</td>
                <td class="second" colspan="2" >' . $schedule->SampleOrderDetail->SampleType->sample_type . '</td>
                <td class="second" colspan="1" >' . $schedule->SampleOrderDetail->SampleOrder->order_date . '</td>
                <td class="second" colspan="1" >' . $schedule->SampleOrderDetail->SampleOrder->delivery_date . '</td>
                <td class="second" colspan="1" >' . $schedule->schedule_date . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('sample_schedules', 'I');

		return Redirect::to('sampleSchedule');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function sampleStatus()
	{
		$sampleStatuses = SampleStatus::all();

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Sample Status List');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Sample Status List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" colspan="2"><b>Order No.</b></th>
							<th class="first" colspan="2" ><b>Article No.</b></th>
							<th class="first" colspan="1" ><b>Order Date</b></th>
							<th class="first" colspan="1" ><b>Delivery Date</b></th>
							<th class="first" colspan="1" ><b>Pattern</b></th>
							<th class="first" colspan="1" ><b>Test Sample</b></th>
							<th class="first" colspan="1" ><b>Grading</b></th>
							<th class="first" colspan="1" ><b>Material Status</b></th>
							<th class="first" colspan="1" ><b>Cutting</b></th>
							<th class="first" colspan="1" ><b>Closing</b></th>
							<th class="first" colspan="1" ><b>Assembly</b></th>
							<th class="first" colspan="1" ><b>QC Report</b></th>
							<th class="first" colspan="1" ><b>Actual Delivery Date</b></th>
						</tr>
					</thead>
					<tbody class="">';

		foreach ($sampleStatuses as $key => $status) {
			if ($status->DevelopmentStatus->cutting != null) {
				$cutting = $status->DevelopmentStatus->cutting;
			}else{
				$cutting = 'None';
			}
			if ($status->DevelopmentStatus->closing != null) {
				$closing = $status->DevelopmentStatus->closing;
			}else{
				$closing = 'None';
			}
			if ($status->DevelopmentStatus->assembly != null) {
				$assembly = $status->DevelopmentStatus->assembly;
			}else{
				$assembly = 'None';
			}
			if ($status->DevelopmentStatus->qc_report != null) {
				$qc_report = $status->DevelopmentStatus->qc_report;
			}else{
				$qc_report = 'None';
			}
			if ($status->pattern != null) {
				$pattern = $status->pattern;
			}else{
				$pattern = 'None';
			}
			if ($status->test_sample != null) {
				$test_sample = $status->test_sample;
			}else{
				$test_sample = 'None';
			}
			if ($status->grading != null) {
				$grading = $status->grading;
			}else{
				$grading = 'None';
			}
			if ($status->material_status != null) {
				$material_status = $status->material_status;
			}else{
				$material_status = 'None';
			}
			$tbl = $tbl . '<tr>
                <td class="second">' . ($key + 1) . '</td>
                <td class="second" colspan="2">' . $status->SampleOrderDetail->SampleOrder->sample_order_no . '</td>
                <td class="second" colspan="2" >' . $status->SampleOrderDetail->Article->article_no . '</td>
                <td class="second" colspan="1" >' . $status->SampleOrderDetail->SampleOrder->order_date . '</td>
                <td class="second" colspan="1" >' . $status->SampleOrderDetail->SampleOrder->delivery_date . '</td>
                <td class="second" colspan="1" >' . $pattern . '</td>
                <td class="second" colspan="1" >' . $test_sample . '</td>
                <td class="second" colspan="1" >' . $grading . '</td>
                <td class="second" colspan="1" >' . $material_status . '</td>
                <td class="second" colspan="1" >' . $cutting . '</td>
                <td class="second" colspan="1" >' . $closing . '</td>
                <td class="second" colspan="1" >' . $assembly . '</td>
                <td class="second" colspan="1" >' . $qc_report . '</td>
                <td class="second" colspan="1" >' . $status->actual_delivery . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('sample_statuses', 'I');

		return Redirect::to('sampleStatus');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function inventory()
	{
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Sample Status List');

		// set font
		$pdf->SetFont('helvetica', '', 16);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 24pt;
				        text-decoration: underline;
				    }				
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 16pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Inventory List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" colspan="1"><b>Item</b></th>
							<th class="first" colspan="1" ><b>Group</b></th>
							<th class="first" colspan="1" ><b>Sub Group</b></th>
							<th class="first" colspan="1" ><b>Store Unit</b></th>
							<th class="first" colspan="1" ><b>Store Ins</b></th>
							<th class="first" colspan="1" ><b>Store Outs</b></th>
							<th class="first" colspan="1" ><b>Remainders</b></th>
						</tr>
					</thead>
					<tbody class="">';

		$store_ins = StoreIn::all();
		$store_outs = StoreOut::all();
		$items = Item::all();
		$sorted_items = array();
		$store_in_items = array();
		$store_out_items = array();
		$remainder_items = array();
		$list = array();
		foreach ($items as $count => $item) {
			$store_in_count = 0;
			foreach ($store_ins as $store_in) {
				$store_in_details = $store_in->Details;
				foreach ($store_in_details as $store_in_detail) {
					$item_in_store_in = $store_in_detail->Item;
					if ($item->id == $item_in_store_in->id) {
						$store_in_count += $store_in_detail->quantity;
					}
				}
			}
			$store_in_items['item_'.$item->id] = $store_in_count;
			$store_out_count = 0;
			foreach ($store_outs as $store_out) {
				$store_out_details = $store_out->Details;
				foreach ($store_out_details as $store_out_detail) {
					$item_in_store_out = $store_out_detail->Item;
					if ($item->id == $item_in_store_out->id) {
						$store_out_count += $store_out_detail->quantity;
					}
				}
			}
			$store_out_items['item_'.$item->id] = $store_out_count;
			$remainder_items['item_'.$item->id] = $store_in_count - $store_out_count;
		}
		array_push($list, array(
			'#', 
			'Item', 
			'Group', 
			'Sub Group', 
			'Store Unit', 
			'Store Ins', 
			'Store Outs', 
			'Remainder'
			)
		);
		foreach ($items as $key => $item) {
			array_push($list, array(
				($key + 1),
				$item->item_name,
				$item->Group->group_name,
				$item->SubGroup->sub_group_name,
				$item->StoreUnit->unit_name,
				$store_in_items['item_'.$item->id],
				$store_out_items['item_'.$item->id],
				$remainder_items['item_'.$item->id]
				)
			);
		}

		foreach ($items as $key => $item) {
			$tbl = $tbl . '<tr>
                <td class="second">' . ($key + 1) . '</td>
                <td class="second" colspan="1">' . $item->item_name . '</td>
                <td class="second" colspan="1" >' . $item->Group->group_name . '</td>
                <td class="second" colspan="1" >' . $item->SubGroup->sub_group_name . '</td>
                <td class="second" colspan="1" >' . $item->StoreUnit->unit_name . '</td>
                <td class="second" colspan="1" >' . $store_in_items['item_'.$item->id] . '</td>
                <td class="second" colspan="1" >' . $store_out_items['item_'.$item->id] . '</td>
                <td class="second" colspan="1" >' . $remainder_items['item_'.$item->id] . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('inventory', 'I');

		return Redirect::to('inventory');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function productionOrder(){

		// create new PDF document
		$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Production Order List');

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 10pt;
				        text-decoration: underline;
				    }
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 10pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Production Order List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Order No</b></th>
							<th class="first" ><b>Buying Dept.</b></th>
							<th class="first" ><b>Article No</b></th>
							<th class="first" ><b>Unit Value</b></th>
							<th class="first" ><b>Commission</b></th>
							<th class="first" ><b>Article Type</b></th>
							<th class="first" ><b>Category</b></th>
							<th class="first" ><b>Color</b></th>
							<th class="first" ><b>Color Code</b></th>
							<th class="first" ><b>Size Group</b></th>
							<th class="first" ><b>Season</b></th>
							<th class="first" ><b>Order Date</b></th>
							<th class="first" ><b>Order Receive Date</b></th>
							<th class="first" ><b>Block TT No</b></th>
							<th class="first" ><b>Block TT Ordering Year From</b></th>
							<th class="first" ><b>Block TT Ordering Year To</b></th>
							<th class="first" ><b>Sales Contact No</b></th>
							<th class="first" ><b>Remarks</b></th>
						</tr>
					</thead>
					<tbody class="">';

		$orders = ProductionOrder::all();

		foreach ($orders as $key => $value) {
			$tbl = $tbl . '<tr>
                <td class="second" >' . ($key + 1) . '</td>
                <td class="second" >' . $value->order_no . '</td>
                <td class="second" >' . $value->department->name . '</td>
                <td class="second" >' . $value->article->article_no . '</td>
                <td class="second" >' . $value->unit_val . '</td>
                <td class="second" >' . $value->commission . '</td>
                <td class="second" >' . $value->articleType->article_type . '</td>
                <td class="second" >' . $value->category->category_name . '</td>
                <td class="second" >' . $value->color->color_name_flcl . '</td>
                <td class="second" >' . $value->color_code . '</td>
                <td class="second" >' . $value->sizeGroup->size_group_name . '</td>
                <td class="second" >' . $value->season->season . '</td>
                <td class="second" >' . date('Y-m-d', $value->order_date) . '</td>
                <td class="second" >' . date('Y-m-d', $value->order_receive_date) . '</td>
                <td class="second" >' . $value->block_tt_no . '</td>
                <td class="second" >' . $value->block_tt_order_from . '</td>
                <td class="second" >' . $value->block_tt_order_to . '</td>
                <td class="second" >' . $value->sales_contact_no . '</td>
                <td class="second" >' . $value->remarks . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('production_order', 'I');

		return Redirect::to('productionOrder');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function productionOrderDetail(){

		// create new PDF document
		$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Production Order Detail List');

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 10pt;
				        text-decoration: underline;
				    }
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 10pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Production Order Detail List</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>#</b></th>
							<th class="first" ><b>Order No</b></th>
							<th class="first" ><b>Buying Dept.</b></th>
							<th class="first" ><b>Article No</b></th>
							<th class="first" ><b>Article Type</b></th>
							<th class="first" ><b>Category</b></th>
							<th class="first" ><b>Color</b></th>
							<th class="first" ><b>Color Code</b></th>
							<th class="first" ><b>Size Group</b></th>
							<th class="first" ><b>Season</b></th>
							<th class="first" ><b>Order Date</b></th>
							<th class="first" ><b>Order Receive Date</b></th>
						</tr>
					</thead>
					<tbody class="">';

		$orders = ProductionOrderDetail::all();

		foreach ($orders as $key => $value) {
			$tbl = $tbl . '<tr>
                <td class="second" >' . ($key + 1) . '</td>
                <td class="second" >' . $value->ProductionOrder->order_no . '</td>
                <td class="second" >' . Helpers::getDepartmentNameById($value->ProductionOrder->department_id) . '</td>
                <td class="second" >' . Helpers::getArticleNoById($value->ProductionOrder->article_id) . '</td>
                <td class="second" >' . Helpers::getArticleTypeById($value->ProductionOrder->article_type_id) . '</td>
                <td class="second" >' . Helpers::getCategoryNameById($value->ProductionOrder->category_id) . '</td>
                <td class="second" >' . $value->color->color_name_flcl . '</td>
                <td class="second" >' . $value->color->hm_code . '</td>
                <td class="second" >' . Helpers::getSizeGroupNameById($value->ProductionOrder->size_group_id) . '</td>
                <td class="second" >' . Helpers::getSeasonById($value->ProductionOrder->size_group_id) . '</td>
                <td class="second" >' . date('Y-m-d', $value->order_date) . '</td>
                <td class="second" >' . date('Y-m-d', $value->order_receive_date) . '</td>
            </tr>';
		}
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('production_order_detail', 'I');

		return Redirect::to('productionOrderDetail');
	}

	/**
	 * Download an pdf file of the resource
	 *
	 * @return Response
	 */
	public function orderTODSizeBreakdownReport(){

		// create new PDF document
		$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Production Order Detail List');

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		$sizes = Size::all();

		$tbl = '<style>
				    h1 {
				        color: #333;
				        font-family: times;
				        font-size: 10pt;
				        text-decoration: underline;
				    }
				    .first {
				        color: #333;
				        font-family: helvetica;
				        font-size: 10pt;
				        border: 2px #f9f9f9;
				        background-color: #fff;
				    }
				    td.second {
				        border: 2px #f9f9f9;
				        margin: 4px;
				    }
				</style>
			<h1 align="center" margin="5px;">Fortuna Bangladesh</h1>
			<h2 align="center" margin="5px;">Order and TOD Report with Size breakdown</h2>
			<table class="">
					<thead>
						<tr>
							<th class="first" ><b>Order No</b></th>
							<th class="first" ><b>Article</b></th>
							<th class="first" ><b>Color</b></th>
							<th class="first" ><b>Destination</b></th>
							<th class="first" ><b>Season</b></th>';
		foreach($sizes as $size){
			$tbl = $tbl . '<th class="first" ><b>'. $size->size_no . '</b></th>';
		}

		$tbl = $tbl . ' <th class="first" ><b>Total</b></th>
						<th class="first" ><b>TOD</b></th>
						<th class="first" ><b>Cut Off</b></th>
						<th class="first" ><b>Remarks</b></th>
					</tr>
				</thead>
				<tbody class="">';

		$grand_total = 0;
		foreach($sizes as $size){
			${'s_'.$size->id.'_qty'} = 0;
		}
		$orders = ProductionOrder::all();

		foreach ($orders as $order) {
			$order_details = ProductionOrderDetail::where('production_order_id', '=', $order->id)->get();
			foreach($order_details as $details) {
				$detail_sizes = ProductionOrderDetail::find($details->id)->Sizes;
				$total = 0;
				$tbl = $tbl . '<tr>
					<td class="second" >' . $order->order_no . '</td>
					<td class="second" >' . $order->article->article_no . '</td>
					<td class="second" >' . $order->color->color_name_flcl . '</td>
					<td class="second" >' . Helpers::getDestinationNameById($details->destination_id) . '</td>
					<td class="second" >' . $order->season->season . '</td>';
				foreach($sizes as $size) {
					$qty = 0;
					foreach ($detail_sizes as $ds) {
						if ($size->id == $ds->id) {
							$qty = ($size->id == $ds->id) ? Helpers::getPOSizeQuantity($order->id, $ds->id) : 0;
							break;
						}
					}
					${'s_' . $size->id . '_qty'} += $qty;
					$total += $qty;
					$tbl = $tbl . '<td class="second" >' . $qty . '</td>';
				}
				$grand_total += $total;

				$tbl = $tbl . '<td class="second" >' . $total . '</td>
								<td class="second" >' . date('d.m.Y' ,$order->order_date) . '</td>
								<td class="second" >' . date('d.m.Y' ,$details->cut_off) . '</td>
								<td class="second" >' . $order->remarks . '</td>
							</tr>';
			}
		}
		$tbl = $tbl . '<tr>
                    <td class="second" >&nbsp;</td>
                    <td class="second" >&nbsp;</td>
                    <td class="second" >&nbsp;</td>
                    <td class="second" >&nbsp;</td>
                    <td class="second" ><b>Total Order Quantity</b></td>';
                    foreach($sizes as $size){
                    	$tbl = $tbl . '<td class="second" >' . ${'s_'.$size->id.'_qty'} . '</td>';
                    }
        $tbl = $tbl . '<td class="second" >' . $grand_total . '</td>
                    <td class="second" >&nbsp;</td>
                    <td class="second" >&nbsp;</td>
                    <td class="second" >&nbsp;</td>
                </tr>';
		$tbl = $tbl . '</tbody></table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output('order_tod_size_breakdown_report', 'I');

		return Redirect::to('orderTODSizeBreakdownReport');
	}

	public function showProductDetails($id){
		$data = array();
		$order = ProductionOrder::find($id);
		$order_tod_details      = $order->TodDetails;
		$order_payment_details  = $order->PaymentDetails;
		$order_shipment_details = $order->ShipmentDetails;
		$order_shipping_blocks  = $order->ShippingBlocks;
//        echo count($order_shipping_blocks);die;
		//echo '<pre>';print_r($order_shipment_details);die;

		$data['buyer']              = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
		$data['season']             = array('' => 'Select Season') + Season::pluck('season','id')->toArray();
		$data['status']             = array('' => 'Select Order Status') + Status::pluck('status','id')->toArray();
		$data['payment_term']       = array('' => 'Select Payment Term') + PaymentTerm::pluck('term','id')->toArray();
		$data['currency']           = array('' => 'Select Currency') + Currency::pluck('name','id')->toArray();
		$data['shipping_mode']      = array('' => 'Select Shipping Mode') + ShippingMode::pluck('shipping_mode','id')->toArray();
		$data['consignee_bank']     = array('' => 'Select Consignee Bank') + Bank::pluck('name','id')->toArray();
		$data['shipper_bank']       = array('' => 'Select Shipper Bank') + Bank::pluck('name','id')->toArray();
		$data['terms_of_delivery']  = array('' => 'Select Terms of Delivery') + TermsOfDelivery::pluck('terms_of_delivery','id')->toArray();
		$data['forwarder']          = array('' => 'Select Forwarder') + Forwarder::pluck('forwarder_name','id')->toArray();
		$data['article']            = Article::pluck('article_no','id')->toArray();
		$data['destination']        = Destination::pluck('destination_name','id')->toArray();
		$data['color']              = Color::all()->pluck('color_detail_for_list','id')->toArray();

		$order['destination_list'] = array('' => 'Select Destination');


		$buyer_template = Buyer::find($order['buyer_id'])->buyerTemplate->buyer_template;

		// create article array
		$articles = ProductionOrderArticle::where('production_order_id', '=', $id)->get();
		$i = 0;
		$order['articles'] = array();
		$order['article_list'] = array('' => 'Select Article');
		foreach($articles as $article){
			$a[$i++] =  $article['article_id'];
			$order['articles'] = $a;
			$order['article_list'] = $order['article_list'] + array($article['article_id'] => Helpers::getArticleNoById($article['article_id']));
		}
		// create color array
		$colors = ProductionOrderColor::where('production_order_id', '=', $id)->get();
		$i = 0;
		$order['colors'] = array();
		$order['color_list'] = array('' => 'Select Color');
		foreach($colors as $color){
			$c[$i++] =  $color['color_id'];
			$order['colors'] = $c;
			$order['color_list'] = $order['color_list'] + array($color['color_id'] => Helpers::getColorNameById($color['color_id']));
		}

		if($buyer_template == "Template 01"){
			// create Destination array
			$destinations = ProductionOrderDestination::where('production_order_id', '=', $id)->get();
			$i = 0;
			$order['destinations'] = array();
			foreach($destinations as $destination){
				$des[$i++] =  $destination['destination_id'];
				$order['destinations'] = $des;
				$order['destination_list'] = $order['destination_list'] + array($destination['destination_id'] => Helpers::getDestinationNameById($destination['destination_id']));
			}
		}
		else if($buyer_template == "Template 02"){
			// create Despo array
			$despos = ProductionOrderDespo::where('production_order_id', '=', $id)->get();
			$order['despos'] = '';
			foreach($despos as $despo){
				$order['despos'] = $order['despos'].','.$despo->despo;
			}
		}

		$tod_details = ProductionOrder::find($id)->todDetails;
		$total_value = 0;
		if($tod_details){
			foreach($tod_details as $tod){
				$total = $tod->total_quantity * $tod->unit_value;
				$total_value = $total_value + $total;
			}
		}
		$order['total_value'] = $total_value;

		//-------- PDF Starts from here ------
		// create new PDF document
		$pdf = new TCPDF('s', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Fortuna');
		$pdf->SetTitle('MRP');
		$pdf->SetSubject('Productions order details');

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		/*
		 * PDF Page Starts from here
		 * */
		$buyer_template = Buyer::find($order->buyer_id)->buyerTemplate->buyer_template;
		//Get article list
		$articleData = [];
		foreach($order->articles as $article){
			$articleData[] = Helpers::getArticleNoById($article);
		}
		$articleData = implode(',', $articleData);

		$page = "
			<style>
				.header {
					text-align: center;
					color: #ff0000;
					border: 1px solid #000;
				}
			</style>
			<div class='header'>
				<h3 align='center'>Order No: {$order->order_no}</h3>
				<h4 align='center'>Buyer Name:  {$order->buyer->buyer_name}</h4>
				<div class='product_details' style='margin-top: 10px'>
					<h5><b>Articles:</b> {$articleData}</h5>
		";
		if($order->season_id){
			$page .= "<h5><b>Season: </b>".$order->season->season."</h5>";
		}
		$page .= "<h5>
			<b>Colors: </b>
		";
		$total = count($order->colors);
		$i = 1;
		foreach($order->colors as $color){
			$page .= Helpers::getColorNameById($color);
			$page .= ($total > $i)?', ':'';
			$i++;
		}
		$page .= "</h5>";
		if($buyer_template == "Template 01"){
			$page .= "<h5><b>Destinations: </b>";
			$total = count($order->destinations);
			$i = 1;
			foreach($order->destinations as $destination){
				$page .= Helpers::getDestinationNameById($destination);
				$page .= ($total > $i)?', ':'';
				$i++;
			}
			$page .= "</h5>";
		}
		else if($buyer_template == "Template 02"){
			$page .= "<h5><b>Despo: </b>";
			$total = count($order->despos);
			$i = 1;
			foreach($order->despos as $despo){
				$page .= $despo;
				$page .= ($total > $i)?', ':'';
				$i++;
			}
			$page .= "</h5>";
		}
		$page .= "
				</div>
			</div>
		";
		$page .= "
			<table>
				<tr>
					<td>
					<div class='payment_details'>
						<h3 class=\"box-title\">Payment Details</h3>
						<h5><b>Payment Term: </b>{$order_payment_details->PaymentTerm->term}</h5>
                        <h5><b>Currency: </b>{$order_payment_details->Currency->name}</h5>
                        <h5><b>Commission: </b>{$order_payment_details->commission}%</h5>
                        <h5><b>Total Value: </b>$ {$order_payment_details->total_value}</h5>
                        <h5><b>Commission Value: </b>$ {$order_payment_details->commission_value}</h5>
                        <h5><b>Invoice Value: </b>$ {$order_payment_details->invoice_value}</h5>
		";

		$ttInfo = "";

		 if($order_payment_details->block_tt_no){
			 $ttInfo .= "<h5><b>Block TT No: </b>{$order_payment_details->block_tt_no}</h5>";
		 }
		 if($order_payment_details->block_tt_order_from){
			$ttInfo .= "<h5><b>Block TT From: </b>".date('m/d/Y', $order_payment_details->block_tt_order_from)."</h5>";
		 }
		 if($order_payment_details->block_tt_order_to){
			 $ttInfo .= "<h5><b>Block TT No: </b>".date('m/d/Y', $order_payment_details->block_tt_order_to)."</h5>";
		 }
		 if($order_payment_details->sales_contact_no){
			 $ttInfo .= "<h5><b>Sales Contact No: </b>{$order_payment_details->sales_contact_no}</h5>";
		 }
		if($order_payment_details->remarks){
			$ttInfo .= "<h5><b>Remarks: </b>$ {$order_payment_details->remarks}</h5>";
		}

		$page.= "	</div>			
					</td>
					<td>
						<h3></h3>
						{$ttInfo}			
					</td>
				</tr>
			</table>
		";
		if(count($order_tod_details) > 0) {
			$page .= "
			<div class='tod_details'>
				<h3 class=\"box-title\">TOD Details</h3>";

			$page .= "<table border=\"1\" cellspacing=\"0\" cellpadding=\"4\">
			<tr>
				<th style=\"width: 15px\">#</th>
				<th>Article</th>
				<th style='border: 1px solid #000'>Color</th>
				<th>Destination</th>
				<th>TOD</th>
				<th style='font-size: 10px'>No. of Days Need Ship Advance</th>
				<th>Cut Off</th>
				<th>ETD</th>
			</tr>";
			if(count($order_tod_details) > 0){
				$i = 1;
				foreach($order_tod_details as $key => $value){
					$page .= "<tr>";
						$page .= "<td>".$i++."</td>";
						$page .= "<td>{$value->article->article_no}</td>";
						$page .= "<td>{$value->color->color_name_flcl}</td>";
						$page .= "<td>{$value->destination->destination_name}</td>";
						$page .= "<td>".date('m/d/Y', $value->tod)."</td>";
						$page .= "<td>{$value->days_ship_advance}</td>";
						$page .= "<td>{$value->destination->cut_off_day}</td>";
						$page .= "<td>{$value->destination->etd_day}</td>";
					$page .="</tr>";
				}
			}
			$page .= "</table></div>";

			$page .= "<div class='quantity_details_tod'>
						<h3 class=\"box-title\">Quantity Details</h3>
						<table border=\"1\" cellspacing=\"0\" cellpadding=\"4\">
                        <tr>
                            <th style=\"width: 15px\">#</th>
                            <th>Article</th>
                            <th>Color</th>
                            <th>Destination</th>
                            <th>Total Quantity</th>
                            <th>Unit Value</th>
                            <th>Total Value</th>
                        </tr>
					";

			$i = 1;
			foreach($order_tod_details as $key => $value){
				$page .= "<tr><td>".$i++."</td>";
				$page .= "<td>{$value->article->article_no}</td>";
				$page .= "<td>{$value->color->color_name_flcl}</td>";
				$page .= "<td>{$value->destination->destination_name}</td>";
				$page .= "<td>{$value->total_quantity}</td>";
				$page .= "<td>$ {$value->unit_value}</td>";
				$page .= "<td>$ ".($value->unit_value * $value->total_quantity)."</td></tr>";
			}

			$page .= "</table></div>";
			if(count($order_shipment_details) > 0){
				$page .= "<div>
					<h3 class=\"box-title\">Shipment Details</h3>
					<h5><b>Consignee Bank: </b>{$order_shipment_details->ConsigneeBank->name}</h5>
                    <h5><b>Shipper Bank: </b>{$order_shipment_details->ShipperBank->name}</h5>
                    <h5><b>Terms of Delivery: </b>{$order_shipment_details->TermsOfDelivery->terms_of_delivery}</h5>
                    <h5><b>Forwarder: </b>{$order_shipment_details->Forwarder->forwarder_name}</h5>
				";
				$page .= "</div>";
			}
			if (count($order_shipping_blocks) > 0){
				$page .= "<div>
					<h3 class=\"box-title\">Shipment Details</h3>
					<table border=\"1\" cellspacing=\"0\" cellpadding=\"4\">
					<tr>
                      <th>Shipping Mode</th>
                      <th>Destination</th>
                    </tr>
				";
				$i = 1;
				foreach($order_shipping_blocks as $key => $value){
					$page .= "<tr>";
					$page .= "<td>{$value->ShippingMode->shipping_mode}</td>";
					$data_taz = [];
					$destinations = ProductionOrderDetailShippingBlockDestination::where('production_order_detail_shipping_block_id', '=', $value->id)->get();
					foreach($destinations as $destination){
						$data_taz[] = $destination->destination->destination_name;
					}
					$page .= "<td>".implode(',',$data_taz )."</td>";
					$page .= "</tr>";
				}
				$page .= "</table></div>";
			}
		}

		$pdf->writeHTML($page, true, false, false, false, '');
		$pdf->Output('production_order_detail', 'I');
	}

	public function user(){
		echo "Not Implemented!";
	}

}