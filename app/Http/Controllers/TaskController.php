<?php

namespace App\Http\Controllers;

use App\ProductionOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Machinelist;
use App\Subdivision;
use App\division;

use App\GanttTask;
use App\GanttLink;
use Dhtmlx\Connector\GanttConnector;
use Dhtmlx\Connector\JSONGanttConnector;



class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
//       
        print_r($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //check if line is busy

        $recordexist = GanttTask::where("line_id", "=", $request->input("line_id"))->where('start_date', '<=', $request->start_date)->where('end_date', '>=', $request->start_date)->exists();

        if ($recordexist) {

            $result = GanttTask::where("line_id", "=", $request->input("line_id"))->where('start_date', '<=', $request->start_date)->where('end_date', '>=', $request->start_date)->first();
            $output = "<data><action type='invalid' sid='1' tid='1' message=' Order ".$result->order_id." using ".$result->line->line_name." for article no ".$result->article_no.".' ></action></data>";
            print ($output);

        } else{
            //
            $task = new GanttTask();
            $task->order_id = $request->input("order_id");
            $task->article_no = $request->input("article_no");
            $task->division_id = $request->input("division_id");
            $task->sub_division_id = $request->input("sub_division_id");
            $task->line_id = $request->input("line_id");
            $task->complexity_id = $request->input("complexity_id");
            $task->text = $request->input("text");
            $task->start_date = $request->input("start_date");
            $task->end_date = $request->input("end_date");
            $task->parent = $request->input("parent");
            $task->duration = $request->input("duration");
            $task->quantity = $request->input("quantity");

            $task->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $order_id,$id)//,
    {
        $recordexist = GanttTask::where("line_id", "=", $request->input("line_id"))->where('start_date', '<=', $request->start_date)->where('end_date', '>=', $request->end_date)->where("id","!=",$id)->exists();

        if ($recordexist) {

            $result = GanttTask::where("line_id", "=", $request->input("line_id"))->where('start_date', '<=', $request->start_date)->where('end_date', '>=', $request->end_date)->first();
            //$output = "<data><action type='warning' sid='".$id."' tid='".$id."' message=' Order ".$result->order_id." using ".$result->subdivision->subdivision_name." for article no ".$result->article_no.".' ></action></data>";
            $output = "<data><action type='invalid' sid='".$id."' tid='".$id."' message=' Order ".$result->order_id." using ".$result->line->line_name." for Article no ".$result->article_no." for ".$result->start_date." to ".$result->end_date.".' ></action></data>";

            header('Content-Type: text/xml');
            print ($output);

        }else {

            $task = GanttTask::find($id);


            $task->order_id = $request->input("order_id");

            if ($request->input("division_id")) {
                $task->division_id = $request->input("division_id");
            }
            $task->article_no = $request->input("article_no");
            $task->sub_division_id = $request->input("sub_division_id");
            $task->line_id = $request->input("line_id");
            $task->complexity_id = $request->input("complexity_id");
            $task->text = $request->input("text");
            $task->start_date = $request->input("start_date");
            $task->end_date = $request->input("end_date");
            $task->parent = $request->input("parent");
            $task->duration = $request->input("duration");
            $task->quantity = $request->input("quantity");
            $task->update();

        }
    }
    public function updatetask(Request $request, $id)//,
    {
        $recordexist = GanttTask::where("sub_division_id", "=", $request->input("sub_division_id"))->where('start_date', '<=', $request->start_date)->where('end_date', '>=', $request->end_date)->where("id","!=",$id)->exists();

        if ($recordexist) {

            $result = GanttTask::where("sub_division_id", "=", $request->input("sub_division_id"))->where('start_date', '<=', $request->start_date)->where('end_date', '>=', $request->end_date)->first();
            $output = "<data><action type='invalid' sid='".$id."' tid='".$id."' message=' Order ".$result->order_id." using ".$result->subdivision->subdivision_name." for Article no ".$result->article_no." for ".$result->start_date." to ".$result->end_date.".' ></action></data>";
            header('Content-Type: text/xml');
            print ($output);

        }else {

            $task = GanttTask::find($id);


            $task->order_id = $request->input("order_id");

            if ($request->input("division_id")) {
                $task->division_id = $request->input("division_id");
            }
            $task->article_no = $request->input("article_no");
            $task->sub_division_id = $request->input("sub_division_id");
            $task->line_id = $request->input("line_id");
            $task->complexity_id = $request->input("complexity_id");
            $task->text = $request->input("text");
            $task->start_date = $request->input("start_date");
            $task->end_date = $request->input("end_date");
            $task->parent = $request->input("parent");
            $task->duration = $request->input("duration");
            $task->quantity = $request->input("quantity");
            $task->update();

        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        print_r($id);
    }


}
