<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\SockPrint;
use App\LiningPrint;
use App\AdditionalPrint;
use App\PolySticker;
use App\Pictogram;
use App\PriceTag;
use App\PmTag;
use App\AdditionalStickerInfo;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\Destination;
use Validator;

class DestinationController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public $layout = 'layouts.master';

    public function index()
    {
        $destinations = Destination::all();

        return view('destination.index')->with('destinations', $destinations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data = array();
        $data['cut_off_diff']           = array('' => 'Select One', '0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6');
        $data['etd_diff']           = array('' => 'Select One', '-1' => '-1', '-2' => '-2', '-3' => '-3', '-4' => '-4', '-5' => '-5', '-6' => '-6', '0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6');
        $data['assort_packing']         = array('' => 'Select One', '0' => 'Yes', '1' => 'No');
        $data['carton_details']         = array('N/A' => 'N/A', 'S' => 'S', 'M' => 'M', 'L' => 'L');
        $data['sock_print']             = array('0' => 'Select Sock Print') + SockPrint::pluck('sock_print','id')->toArray();
        $data['lining_print']           = array('0' => 'Select Lining Print') + LiningPrint::pluck('lining_print','id')->toArray();
        $data['additional_print']       = array('0' => 'Select Additional Print') + AdditionalPrint::pluck('additional_print','id')->toArray();
        $data['poly_sticker']           = array('0' => 'Select Poly Sticker') + PolySticker::pluck('poly_sticker','id')->toArray();
        $data['pictogram']              = array('0' => 'Select Pictogram') + Pictogram::pluck('pictogram','id')->toArray();
        $data['price_tag']              = array('0' => 'Select Price Tag') + PriceTag::pluck('price_tag','id')->toArray();
        $data['pm_tag']                 = array('0' => 'Select PM Tag') + PmTag::pluck('pm_tag','id')->toArray();
        $data['additional_sticker_info']= array('0' => 'Select Additional Sticker Info') + AdditionalStickerInfo::pluck('sticker_info','id')->toArray();

        return view('destination.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $destination_input = $request->only('destination_name', 'destination_code', 'cut_off_day', 'cut_off_diff', 'etd_day', 'etd_diff', 'assort_packing', 'carton_details_assort', 'carton_details_primary', 'carton_details_secondary', 'consignee_address', 'sock_print_id', 'lining_print_id', 'additional_print_id', 'poly_sticker_id', 'pictogram_id', 'price_tag_id', 'pm_tag_id', 'additional_sticker_info_id', 'remarks');

        $rules = array(
            'destination_name'  => 'required',
            'destination_code'  => 'required',
        );

        $validator = Validator::make($destination_input, $rules);
        $color_input['created_at'] = new DateTime();
        if ($validator->passes()){
            Destination::insert($destination_input);
            Session::flash('message', 'Successfully created a Country!');
            return Redirect::to('destination');
        }
        else{
            return Redirect::to('destination/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = array();
        $data['cut_off_diff']           = array('' => 'Select One', '0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6');
        $data['etd_diff']           = array('' => 'Select One', '-1' => '-1', '-2' => '-2', '-3' => '-3', '-4' => '-4', '-5' => '-5', '-6' => '-6', '0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6');
        $data['assort_packing']         = array('' => 'Select One', '0' => 'Yes', '1' => 'No');
        $data['carton_details']         = array('N/A' => 'N/A', 'S' => 'S', 'M' => 'M', 'L' => 'L');
        $data['sock_print']             = array('0' => 'Select Sock Print') + SockPrint::pluck('sock_print','id')->toArray();
        $data['lining_print']           = array('0' => 'Select Lining Print') + LiningPrint::pluck('lining_print','id')->toArray();
        $data['additional_print']       = array('0' => 'Select Additional Print') + AdditionalPrint::pluck('additional_print','id')->toArray();
        $data['poly_sticker']           = array('0' => 'Select Poly Sticker') + PolySticker::pluck('poly_sticker','id')->toArray();
        $data['pictogram']              = array('0' => 'Select Pictogram') + Pictogram::pluck('pictogram','id')->toArray();
        $data['price_tag']              = array('0' => 'Select Price Tag') + PriceTag::pluck('price_tag','id')->toArray();
        $data['pm_tag']                 = array('0' => 'Select PM Tag') + PmTag::pluck('pm_tag','id')->toArray();
        $data['additional_sticker_info']= array('0' => 'Select Additional Sticker Info') + AdditionalStickerInfo::pluck('sticker_info','id')->toArray();

        $data['destination'] = Destination::find($id);

        return view('destination.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $rules = array(
            'destination_name'  => 'required',
            'destination_code'  => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('destination/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $destination = $request->only('destination_name', 'destination_code', 'cut_off_day', 'cut_off_diff', 'etd_day', 'etd_diff', 'assort_packing', 'carton_details_assort', 'carton_details_primary', 'carton_details_secondary', 'consignee_address', 'sock_print_id', 'lining_print_id', 'additional_print_id', 'poly_sticker_id', 'pictogram_id', 'price_tag_id', 'pm_tag_id', 'additional_sticker_info_id', 'remarks');
            $destination['updated_at'] = new DateTime;

            Destination::where('id','=',$id)->update($destination);

            Session::flash('message', 'Successfully updated a Country!');
            return Redirect::to('destination');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Destination::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a Country!');
        return Redirect::to('destination');
    }

    public function getDestinationCode(Request $request)
    {
        $info = $request->only('id');
        $id = $info['id'];
        if(isset($id)){
            $data = array();
            $data['id'] = $id;
            $destination = Destination::find($id);
            $data['destination_code'] = $destination['destination_code'];
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

}