<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Http\Request;
use DateTime;
use Session;
use App\SockPrint;
use Validator;
use View;

class SockPrintController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $socks = SockPrint::all();

        return view('sock_print.index')->with('socks', $socks);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data = array();
        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');
        return view('sock_print.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'sock_print'       => 'required',
            'status'           => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('sockPrint/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type = new SockPrint;

            $type->sock_print       = $request->get('sock_print');
            $type->status           = $request->get('status');
            $type->enable_date      = strtotime($request->get('enable_date'));

            $type->save();
            Session::flash('message', 'Successfully created a socks print!');
            return Redirect::to('sockPrint');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['sock_print'] = SockPrint::find($id);

        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');

        return view('sock_print.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'sock_print'       => 'required',
            'status'           => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('sockPrint/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type['sock_print']   = $request->get('sock_print');
            $type['status']       = $request->get('status');
            $type['enable_date']  = strtotime($request->get('enable_date'));
            $type['updated_at']   = new DateTime;

            SockPrint::where('id','=',$id)->update($type);

            Session::flash('message', 'Successfully updated a socks print!');
            return Redirect::to('sockPrint');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        SockPrint::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a sock print!');
        return Redirect::to('sockPrint');
	}

}