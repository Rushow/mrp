<?php
namespace App\Http\Controllers;

use App\SampleOrder;
use App\Buyer;
use App\Article;
use App\Color;
use App\SampleType;
use App\Size;
use App\SampleOrderDetail;


use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use View;
use HTML;
use Validator;
use DateTime;
use Session;
use DB;
class SampleOrderController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index(Request $request)
	{
       // echo Auth::user();die;
        $filters = $request->only('sample_order_no','buyer_id','article_id', 'sample_type_id', 'order_date','delivery_date');

        $sample_order = $filters['sample_order_no'];
        $buyer = $filters['buyer_id'];
        $article = $filters['article_id'];
        $sample_type = $filters['sample_type_id'];
        $order_date = $filters['order_date'];
        $delivery_date = $filters['delivery_date'];

        $data['sample_order_no'] = array('' => 'Select Sample Order No') + SampleOrder::pluck('sample_order_no','id')->toArray();
        $data['buyer'] = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $data['article'] = array('' => 'Select Article No') + Article::pluck('article_no','id')->toArray();
        $data['sample_type'] = array('' => 'Select Type of Sample') + SampleType::pluck('sample_type','id')->toArray();
        $data['orders'] = SampleOrder::all();
        return view('sample_order.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $last_item = $this->getLastOrder();
        $last_id = (isset($last_item)) ? $last_item->id + 1 : 1;
        $last_id = sprintf("%04s", $last_id);
        $data['sample_order_no'] = 'FLCL/SO/'.date('Y')."/".date('M')."/".$last_id;
        $data['buyer'] = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $data['article'] = array('' => 'Select Article') + Article::pluck('article_no','id')->toArray();
        $data['sample_type'] = array('' => 'Select Sample Type') + SampleType::pluck('sample_type','id')->toArray();
        $data['size'] = Size::pluck('size_no','id')->toArray();
        $data['color'] = array('' => 'Select Color') + Color::pluck('color_name_flcl','id')->toArray();

        return view('sample_order.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $order_input = $request->only('sample_order_no','buyer_id', 'order_date', 'delivery_date');
        
        $rules =array(
            'sample_order_no' => array('required'),
            'buyer_id' => array('required'),
            'order_date' => array('required'),
            'order_date' => array('before:delivery_date'),
            'delivery_date' => array('required')
        );


        $validator = Validator::make($order_input, $rules);
        $order_input['created_at'] = new DateTime();
        if ($validator->passes()){
            SampleOrder::insert($order_input);
            $sample_order_item = $this->getLastOrder();
            $sample_order_id = $sample_order_item->id;
            $total_row = $request->get('total_row');
            for($i=1; $i<=$total_row;$i++){
                if($request->get('article_id_'.$i)){
                    $list_input = array(array('sample_order_id'=>$sample_order_id, 'article_id'=>$request->get('article_id_'.$i), 'sample_type_id'=>$request->get('sample_type_id_'.$i), 'color_id'=>$request->get('color_id_'.$i), 'quantity'=>$request->get('quantity_'.$i), 'note'=>$request->get('note_'.$i), 'created_at'=>new DateTime()));

                    SampleOrderDetail::insert($list_input);
                    $sample_order_details_item = $this->getLastOrderDetails();
                    $schedule = SampleSchedule::create(array('schedule_date'=>SampleOrder::find($sample_order_details_item->sample_order_id)->first()->delivery_date, 'sample_detail_id' => $sample_order_details_item->id));
                    $status = SampleStatus::create(array('sample_detail_id' => $sample_order_details_item->id));
                    $development = new DevelopmentStatus();
                    $status->DevelopmentStatus = $development;
                    $sample_order_detail_id = $sample_order_details_item->id;
                    foreach(Input::get('size_id_'.$i) as $size){
                        $size_input = array(array('sample_order_detail_id'=>$sample_order_detail_id, 'size_id'=>$size, 'quantity'=>0, 'created_at'=>new DateTime()));
                        SampleOrderDetailsSize::insert($size_input);
                    }

                }
            }
            Session::flash('message', 'Successfully created a Sample Order!');
            return Redirect::to('sampleOrder');
        }
        else{
            return Redirect::to('sampleOrder/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = array();
		$order_details = SampleOrderDetail::where('sample_order_id', '=', $id)->get();
		$order = SampleOrder::find($id);

        if($order){
            $buyer = SampleOrder::find($order['id'])->buyer;
            $order['buyer_name'] = $buyer['buyer_name'];
        }



        foreach($order_details as $item){
            $sample_order = SampleOrderDetail::find($item['id'])->sampleOrder;
            $article = SampleOrderDetail::find($item['id'])->article;
            $sample_type = SampleOrderDetail::find($item['id'])->sampleType;
            $color = SampleOrderDetail::find($item['id'])->color;
            $size = SampleOrderDetail::find($item['id'])->Sizes;
            $i = 0;
            $item['sample_order_no'] = $sample_order['sample_order_no'];
            $item['article_name'] = $article['article_no'];
            $item['sample_type'] = $sample_type['sample_type'];
            $item['color_name'] = $color['color_name_flcl'];
            $item['size'] = $size;
        }
        return view('sample_order.show', $data)->with('order_details', $order_details)->with('order', $order);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $order = SampleOrder::find($id);
        $order_details = $order->Details;

        $buyers = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $articles = array('' => 'Select Article') + Article::pluck('article_no','id')->toArray();
        $sample_types = array('' => 'Select Sample Type') + SampleType::pluck('sample_type','id')->toArray();
        $sizes = Size::pluck('size_no','id')->toArray();
        $colors = array('' => 'Select Color') + Color::pluck('color_name_flcl','id')->toArray();

        $data = array(
            'buyer' => $buyers,
            'article' => $articles,
            'sample_type' => $sample_types,
            'size' => $sizes,
            'color' => $colors,
            'order' => $order,
            'order_details' => $order_details
            );

        return view('sample_order.edit', $data);
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules =array(
            'sample_order_no' => array('required'),
            'buyer_id' => array('required'),
            'order_date' => array('required'),
            'delivery_date' => array('required')
        );


        $validator = Validator::make($request->all(),$rules);
        if ($validator->passes()){

            $order_input['sample_order_no'] = $request->get('sample_order_no');
            $order_input['buyer_id'] = $request->get('buyer_id');
            $order_input['order_date'] = $request->get('order_date');
            $order_input['delivery_date'] = $request->get('delivery_date');
            $order_input['updated_at'] = new DateTime;

            SampleOrder::where('id','=',$id)->update($order_input);
            $order_details = SampleOrderDetail::where('sample_order_id', '=', $id)->get();
            foreach($order_details as $od){
                SampleOrderDetailsSize::where('sample_order_detail_id', '=', $od['id'])->delete();
            }
            $sample_order_id = $id;
            SampleOrderDetail::where('sample_order_id', '=', $id)->delete();
            $input = $request->only('total_row');
            $total_row = $input['total_row'];
            for($i=1; $i<=$total_row;$i++){
                if($request->get('article_id_'.$i)){
                    $list_input = array(array('sample_order_id'=>$sample_order_id, 'article_id'=>$request->get('article_id_'.$i), 'sample_type_id'=>$request->get('sample_type_id_'.$i), 'color_id'=>$request->get('color_id_'.$i), 'quantity'=>$request->get('quantity_'.$i), 'note'=>$request->get('note_'.$i), 'created_at'=>new DateTime()));

                    SampleOrderDetail::insert($list_input);
                    $sample_order_details_item = $this->getLastOrderDetails();
                    $schedule = SampleSchedule::create(array('schedule_date'=>SampleOrder::find($sample_order_details_item->sample_order_id)->first()->delivery_date, 'sample_detail_id' => $sample_order_details_item->id));
                    $status = SampleStatus::create(array('sample_detail_id' => $sample_order_details_item->id));
                    $development = new DevelopmentStatus();
                    $status->DevelopmentStatus = $development;
                    $sample_order_detail_id = $sample_order_details_item->id;
                    foreach($request->get('size_id_'.$i) as $size){
                        $size_input = array(array('sample_order_detail_id'=>$sample_order_detail_id, 'size_id'=>$size, 'quantity'=>0, 'created_at'=>new DateTime()));
                        SampleOrderDetailsSize::insert($size_input);
                    }
                }
            }
            Session::flash('message', 'Successfully created a Sample Order!');
            return Redirect::to('sampleOrder');
        }
        else{
            return Redirect::to('sampleOrder/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$sample_order = SampleOrder::find($id);
        $sample_order->delete();
        return Redirect::to('sampleOrder');
	}

    private function getLastOrder()
    {
        return DB::table('sample_order')->orderBy('id', 'desc')->first();
    }

    private function getLastOrderDetails()
    {
        return DB::table('sample_order_detail')->orderBy('id', 'desc')->first();
    }

    public function addOrderRow(Request $request){
        $info = $request->only('last_id');
        if($info){
            $last_id = $info['last_id'] + 1;
            $article = array('' => 'Select Article') + Article::pluck('article_no','id')->toArray();
            $sample_type = array('' => 'Select Sample Type') + SampleType::pluck('sample_type','id')->toArray();
            $size = Size::pluck('size_no','id')->toArray();
            $color = array('' => 'Select Color') + Color::pluck('color_name_flcl','id')->toArray();

            $msg = "Are you sure to delete this item???";

            $html = "<td>".$last_id."</td>";
            $html .= "<td>".Form::select('article_id_'.$last_id, $article, '', $attributes = array('id'=>'article_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::select('sample_type_id_'.$last_id, $sample_type, '', $attributes = array('id'=>'sample_type_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::select('size_id_'.$last_id, $size, '', $attributes = array('id'=>'size_id_'.$last_id,'name'=>'size_id_'.$last_id.'[]','data-rel'=>'chosen', 'multiple'=>'', 'class'=>'form-control Required multiselect'))."</td>";
            $html .= "<td>".Form::select('color_id_'.$last_id, $color, '', $attributes = array('id'=>'color_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::text('quantity_'.$last_id, '',array('class'=>'form-control Required','id'=>'quatity_'.$last_id))."</td>";
            $html .= "<td>".Form::text('note_'.$last_id, '',array('class'=>'form-control','id'=>'note_'.$last_id))."</td>";
            $html .= "<td><a href='#' onclick='return checkDelete(this);'><span class='badge badge-important'>X</span></a></td>";

            $data = array();
            $data['error'] = false;
            $data['html'] = $html;
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

    public function search(Request $request)
    {
        $filters = $request->only('sample_order_no','buyer_id','article_id', 'sample_type_id', 'order_date','delivery_date');

        $sample_order = $filters['sample_order_no'];
        $buyer = $filters['buyer_id'];
        $article = $filters['article_id'];
        $sample_type = $filters['sample_type_id'];
        $order_date = $filters['order_date'];
        $delivery_date = $filters['delivery_date'];
        $sorted_buyer = array();
        $sorted_sample_type = array();
        $sorted_article = array();
        $sorted_by_date = array();
        $details = array();
        $orders = array();
        $all_orders = SampleOrder::all();

        if($sample_order){
            $order = SampleOrder::find($sample_order);

            $data['sample_order_no'] = $order->sample_order_no;
            $order_details = $order->Details;
            array_push($orders, $order);
            foreach ($order_details as $order_detail) {
                array_push($details, $order_detail);
            }
        }else{
            if ($buyer) {
                $data['buyer_id'] = $buyer;
                foreach ($all_orders as $order) {
                    if ($order->Buyer->id == $buyer) {
                        array_push($sorted_buyer, $order);
                    }
                }
                $orders = $sorted_buyer;
            }else{                
                foreach ($all_orders as $order) {
                    array_push($orders, $order);
                    $order_details = $order->Details;
                    foreach ($order_details as $order_detail) {
                        array_push($details, $order_detail);
                    }
                }
            }
            if ($order_date || $delivery_date) {
                if ($order_date) {
                    $data['order_date'] = $order_date;
                }elseif ($delivery_date) {
                    $data['delivery_date'] = $delivery_date;
                }else{
                    $data['order_date'] = $order_date;
                    $data['delivery_date'] = $delivery_date;
                }
                foreach ($orders as $key => $order) {
                    if ($order->order_date != $order_date || $order->delivery_date != $delivery_date) {
                        unset($orders[$key]);
                    }
                }
            }
        }

        if($article || $sample_type){
            $orders = array();
            if ($sample_type) {
                $data['sample_type_id'] = $sample_type;                
                foreach ($details as $detail) {                                        
                    if ($detail->SampleType->id == $sample_type) {
                        array_push($sorted_sample_type, $detail);
                    }
                }
            }

            if ($article) {
                $data['article_no'] = $article;
                foreach ($details as $detail) {
                    if ($detail->Article->id == $article) {
                        array_push($sorted_article, $detail);
                    }
                }
            }

            if (sizeof($sorted_sample_type) > 0 || sizeof($sorted_article) > 0 && ($sample_type && $article)) {
                $all_details = array();
                foreach ($sorted_sample_type as $i => $sample_type_detail) {
                    $detail_id = $sample_type_detail->id;
                    foreach ($sorted_article as $j => $article_detail) {
                        if ($article_detail->id == $detail_id) {
                            array_push($all_details, $sample_type_detail);
                            unset($sorted_sample_type[$i]);
                            unset($sorted_article[$j]);
                            break;
                        }
                    }
                }
                $details = $all_details;                
            }

            foreach ($all_orders as $order) {
                $order_id = 0;
                foreach ($details as $key => $detail) {
                    if ($order->id == $detail->SampleOrder->id) {
                        $order_id = $order->id;
                        unset($details[$key]);
                    }
                }
                if ($order_id == $order->id) {
                    array_push($orders, $order);
                }
            }
        }

        // echo "<pre>";
        // dd($orders);

        $data['sample_order_no'] = array('' => 'Select Sample Order No') + SampleOrder::pluck('sample_order_no','id')->toArray();
        $data['buyer'] = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $data['article'] = array('' => 'Select Article No') + Article::pluck('article_no','id')->toArray();
        $data['sample_type'] = array('' => 'Select Type of Sample') + SampleType::pluck('sample_type','id')->toArray();
        return view('sample_order.index', $data)->with('orders', $orders);
    }
}