<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Redirect;
use View;
use HTML;
use Validator;
use Session;
use Form;
use App\Supplier;
use App\Address;
use DateTime;


class SupplierController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $suppliers = Supplier::all();
        return view('supplier.index')->with('suppliers', $suppliers);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('supplier.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'supplier_name' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('supplier/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{
            $supplier = new Supplier;
            $supplier['supplier_name'] = $request->get('supplier_name');
            $supplier['remarks'] = $request->get('remarks');
            $address_input = $request->only('floor', 'street_address', 'city', 'state', 'zip', 'country');
	        $index = Address::Exists($address_input);   
	        if ($index > 0) {
	        	$supplier->address_id = $index;
	        }else{
	        	$address = new Address;
	        	$address['floor'] = $address_input['floor'];
	        	$address['street_address'] = $address_input['street_address'];
	        	$address['city'] = $address_input['city'];
	        	$address['state'] = $address_input['state'];
	        	$address['zip'] = $address_input['zip'];
	        	$address['country'] = $address_input['country'];
	        	$address->save();
	            $supplier->address_id = $address->id;
	        }
            $supplier->save();
            Session::flash('message', 'Successfully created a supplier!');
            return Redirect::to('supplier');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $supplier = Supplier::find($id);
		$address = array();
		if ($supplier->address_id) {
        		$address['floor'] = $supplier->Address->floor;
        		$address['street_address'] = $supplier->Address->street_address;
        		$address['city'] = $supplier->Address->city;
        		$address['state'] = $supplier->Address->state;
        		$address['zip'] = $supplier->Address->zip;
        		$address['country'] = $supplier->Address->country;
        	}else{
        		$address['floor'] = '';
        		$address['street_address'] = '';
        		$address['city'] = '';
        		$address['state'] = '';
        		$address['zip'] = '';
        		$address['country'] = '';
        	}
        $data = array(
        	'supplier' => $supplier,
        	'address' => $address
        	);

        return view('supplier.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'supplier_name' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('supplier/edit/'.$id)->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{
        	$address_input = $request->only('floor', 'street_address', 'city', 'state', 'zip', 'country');
	        $index = Address::Exists($address_input);
	        if ($index > 0) {
	        	$supplier['address_id'] = $index;
	        }else{
	        	$address = new Address;
	        	$address['floor'] = $address_input['floor'];
	        	$address['street_address'] = $address_input['street_address'];
	        	$address['city'] = $address_input['city'];
	        	$address['state'] = $address_input['state'];
	        	$address['zip'] = $address_input['zip'];
	        	$address['country'] = $address_input['country'];
	        	$address->save();
	            $supplier['address_id'] = $address->id;
	        }
            $supplier['supplier_name'] = $request->get('supplier_name');
            //$supplier['full_name'] = $request->get('full_name');
            //$supplier['supplier_email'] = $request->get('supplier_email');
            //$supplier['supplier_phn'] = $request->get('supplier_phn');
            $supplier['updated_at'] = new DateTime;

            $supplier['supplier_name'] = $request->get('supplier_name');
            $supplier['remarks'] = $request->get('remarks');
            $supplier['updated_at'] = new DateTime;

            Supplier::where('id','=',$id)->update($supplier);

            Session::flash('message', 'Successfully updated a supplier!');
            return Redirect::to('supplier');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Supplier::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a supplier!');
        return Redirect::to('supplier');
	}

}