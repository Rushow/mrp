<?
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\AdditionalStickerInfo;
use Illuminate\Http\Request;

use Session;
use App\item;
use App\StoreIn;
use App\StoreOut;
use Validator;

class InventoryController extends Controller {

	public $layout = 'layouts.master';

	public function index(Request $request)
	{		
		$item_list = array('' => 'Select Item') + Item::pluck('item_name', 'id')->toArray();

		$store_ins = StoreIn::all();
		$store_outs = StoreOut::all();
		$items = Item::all();
		$sorted_items = array();
		$store_in_items = array();
		$store_out_items = array();
		$remainder_items = array();
		foreach ($items as $count => $item) {
			$store_in_count = 0;
			foreach ($store_ins as $store_in) {
				$store_in_details = $store_in->Details;
				foreach ($store_in_details as $store_in_detail) {
					$item_in_store_in = $store_in_detail->Item;
					if ($item->id == $item_in_store_in->id) {
						$store_in_count += $store_in_detail->quantity;
					}
				}
			}
			$store_in_items['item_'.$item->id] = $store_in_count;
			$store_out_count = 0;
			foreach ($store_outs as $store_out) {
				$store_out_details = $store_out->Details;
				foreach ($store_out_details as $store_out_detail) {
					$item_in_store_out = $store_out_detail->Item;
					if ($item->id == $item_in_store_out->id) {
						$store_out_count += $store_out_detail->quantity;
					}
				}
			}
			$store_out_items['item_'.$item->id] = $store_out_count;
			$remainder_items['item_'.$item->id] = $store_in_count - $store_out_count;
		}
		$data = array(
			'remainder_items' => $remainder_items,
			'store_in_items' => $store_in_items,
			'store_out_items' => $store_out_items,
			'allItems' => $items,
			'item' => $item_list
			);
		return view('inventory.index', $data);
	}

	public function search(Request $request)
	{
		$filters = $request->only('item_id', 'from_date','to_date');

        $item = $filters['item_id'];
        $from_date = $filters['from_date'];
        $to_date = $filters['to_date'];
        $sorted_results = array();
		$store_ins = StoreIn::all();
		$store_outs = StoreOut::all();
		$items = Item::all();
		$store_in_items = array();
		$store_out_items = array();
		$remainder_items = array();
		$allItems = array();

		if ($item && $from_date && $to_date) {
			$data['item_id'] = $item;
			array_push($allItems, Item::find($item));
			foreach ($store_ins as $store_in) {
				$store_in_date = $store_in->store_in_date;
				if ($store_in_date > $from_date && $store_in_date < $to_date) {
					$store_in_details = $store_in->Details;
					foreach ($store_in_details as $store_in_detail) {
						if ($store_in_detail->Item->id == $item) {
							$store_in_items['item_'.$item] += $store_in_detail->quantity;
							$remainder_items['item_'.$item] += $store_in_items['item_'.$item];
						}
					}
				}
			}
			foreach ($store_outs as $store_out) {
				$store_out_date = $store_out->store_out_date;
				if ($store_out_date > $from_date && $store_out_date < $to_date) {
					$store_out_details = $store_out->Details;
					foreach ($store_out_details as $store_out_detail) {
						if ($store_out_detail->Item->id == $item) {
							$store_out_items['item_'.$item] += $store_out_detail->quantity;
							$remainder_items['item_'.$item] -= $store_out_items['item_'.$item];
						}
					}
				}								
			}
		}else if ($item) {
			array_push($allItems, Item::find($item));
			$data['item_id'] = $item;
			$store_in_items['item_'.$item] = 0;
			$store_out_items['item_'.$item] = 0;
			$remainder_items['item_'.$item] = 0;
			foreach ($store_ins as $store_in) {
				$store_in_details = $store_in->Details;
				foreach ($store_in_details as $store_in_detail) {
					if ($store_in_detail->Item->id == $item) {
						$store_in_items['item_'.$item] += $store_in_detail->quantity;
						$remainder_items['item_'.$item] += $store_in_items['item_'.$item];
					}
				}
			}
			foreach ($store_outs as $store_out) {
				$store_out_details = $store_out->Details;
				foreach ($store_out_details as $store_out_detail) {
					if ($store_out_detail->Item->id == $item) {
						$store_out_items['item_'.$item] += $store_out_detail->quantity;
						$remainder_items['item_'.$item] -= $store_out_items['item_'.$item];
					}
				}
			}
		}else if ($from_date || $to_date) {
			foreach ($store_ins as $store_in) {
				$store_in_date = $store_in->store_in_date;
				if ($store_in_date > $from_date || $store_in_date < $to_date) {
					$store_in_details = $store_in->Details;
					foreach ($store_in_details as $store_in_detail) {						
						$isAdded = false;
						foreach ($allItems as $key => $item) {
							if ($item->id == $store_in_detail->Item->id) {
								$isAdded = true;
								break;
							}
						}
						if (!$isAdded) {
							array_push($allItems, $store_in_detail->Item);
							$store_in_items['item_'.$store_in_detail->Item->id] = 0;
							$remainder_items['item_'.$store_in_detail->Item->id] = 0;
							$store_out_items['item_'.$store_in_detail->Item->id] = 0;
							$remainder_items['item_'.$store_in_detail->Item->id] = 0;
						}
						if (isset($store_in_items['item_'.$store_in_detail->Item->id]) && isset($remainder_items['item_'.$store_in_detail->Item->id])) {
							$store_in_items['item_'.$store_in_detail->Item->id] += $store_in_detail->quantity;
							$remainder_items['item_'.$store_in_detail->Item->id] += $store_in_items['item_'.$store_in_detail->Item->id];
						}						
					}
				}
			}
			foreach ($store_outs as $store_out) {
				$store_out_date = $store_out->store_out_date;
				if ($store_out_date > $from_date || $store_out_date < $to_date) {
					$store_out_details = $store_out->Details;
					foreach ($store_out_details as $store_out_detail) {
						$isAdded = false;
						foreach ($allItems as $key => $item) {
							if ($item->id == $store_out_detail->Item->id) {
								$isAdded = true;
								break;
							}
						}
						if (!$isAdded) {
							array_push($allItems, $store_out_detail->Item);							
						}
						if (isset($store_out_items['item_'.$store_out_detail->Item->id]) && isset($remainder_items['item_'.$store_out_detail->Item->id])) {
							$store_out_items['item_'.$store_out_detail->Item->id] += $store_out_detail->quantity;
							$remainder_items['item_'.$store_out_detail->Item->id] -= $store_out_items['item_'.$store_out_detail->Item->id];
						}						
					}
				}								
			}
		}else{
			$allItems = $items;
			foreach ($items as $count => $item) {
			$store_in_count = 0;
			foreach ($store_ins as $store_in) {
				$store_in_details = $store_in->Details;
				foreach ($store_in_details as $store_in_detail) {
					$item_in_store_in = $store_in_detail->Item;
					if ($item->id == $item_in_store_in->id) {
						$store_in_count += $store_in_detail->quantity;
					}
				}
			}
			$store_in_items['item_'.$item->id] = $store_in_count;
			$store_out_count = 0;
			foreach ($store_outs as $store_out) {
				$store_out_details = $store_out->Details;
				foreach ($store_out_details as $store_out_detail) {
					$item_in_store_out = $store_out_detail->Item;
					if ($item->id == $item_in_store_out->id) {
						$store_out_count += $store_out_detail->quantity;
					}
				}
			}
			$store_out_items['item_'.$item->id] = $store_out_count;
			$remainder_items['item_'.$item->id] = $store_in_count - $store_out_count;
		}
		}
		$item = array('' => 'Select Item') + Item::pluck('item_name', 'id')->toArray();
		$data = array(
			'store_in_items' => $store_in_items,
			'store_out_items' => $store_out_items,
			'remainder_items' => $remainder_items,
			'allItems' => $allItems,
			'item' => $item
			);
		return  view('inventory.index', $data);
	}

}