<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\ItemGroup;
use Validator;
use View;

class ItemGroupController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $groups = ItemGroup::all();
        return view('item_group.index')->with('groups', $groups);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('item_group.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'group_name'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('itemGroup/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $group = new ItemGroup;
            $group->group_name       = $request->get('group_name');
            $group->save();

            Session::flash('message', 'Successfully created an item group!');
            return Redirect::to('itemGroup');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['itemGroup'] = ItemGroup::find($id);

        return view('item_group.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'group_name'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('itemGroup/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $group['group_name'] = $request->get('group_name');
            $group['updated_at'] = new DateTime;

            ItemGroup::where('id','=',$id)->update($group);

            Session::flash('message', 'Successfully updated an item group!');
            return Redirect::to('itemGroup');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        ItemGroup::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted an item group!');
        return Redirect::to('itemGroup');
	}

}