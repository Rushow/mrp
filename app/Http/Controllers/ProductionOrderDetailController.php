<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\AdditionalStickerInfo;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\ProductionOrderDetail;
use Validator;

class ProductionOrderDetailController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public $layout = 'layouts.master';

    public function index()
    {
        $orders = ProductionOrderDetail::all();

        $data = array();
        $data['production_order'] = array('' => 'Select Production Order No') + ProductionOrder::pluck('order_no','id')->toArray();
        $data['destination'] = array('' => 'Select Shipping Destination') + Destination::pluck('destination_name','id')->toArray();
        $data['buyer'] = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $data['article'] = array('' => 'Select Article No') + Article::pluck('article_no','id')->toArray();
        $data['season'] = array('' => 'Select Season') + Season::pluck('season','id')->toArray();

        return view('production_order_detail.index', $data)->with('orders', $orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $data = array();
//        $data['order']          = ProductionOrder::find($id);
        $data['buyer']          = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $data['order']          = array('' => 'Select Order') + ProductionOrder::pluck('order_no','id')->toArray();
        $data['article']        = array('' => 'Select Article') + Article::pluck('article_no','id')->toArray();
        $data['destination']    = Destination::pluck('destination_name','id')->toArray();
        $data['season']         = array('' => 'Select Season') + Season::pluck('season','id')->toArray();
        $data['size_group']     = array('' => 'Select Size Group') + SizeGroup::pluck('size_group_name','id')->toArray();
        $data['size']           = array('' => 'Select Size') + Size::pluck('size_no','id')->toArray();


//        if($data['order']){
//            $data['sizes'] = SizeGroupSize::where('size_group_id', '=', $data['order']->size_group_id)->get();
//
//        }
//        else{
//            return Redirect::to('productionOrderDetail')->with(array('error_messages'=>'<div class="alert alert-error">Order detail not found</div>'));
//        }

        return view('production_order_detail.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $order_input = $request->only('production_order_id', 'color_id', 'destination_id', 'days_ship_advance', 'remarks', 'total_quantity');

        $order_input['order_date'] = strtotime($request->get('order_date'));
        $order_input['cut_off'] = strtotime($request->get('cut_off'));
        $rules =array(
            'production_order_id' => array('required')
        );


        $validator = Validator::make($order_input,$rules);
        $order_input['created_at'] = new DateTime();
        if ($validator->passes()){
            ProductionOrderDetail::insert($order_input);
            $order_detail = $this->getLastProductionOrderDetail();
            $order_detail_id = $order_detail->id;
            $order = ProductionOrder::find($request->get('production_order_id'));

            $size = SizeGroupSize::where('size_group_id', '=', $order->size_group_id)->get();
            $i = 1;
            foreach($size as $s){

                $size_input['size_id'] = $request->get('size_id_'.$i);
                $size_input['quantity'] = $request->get('quantity_'.$i);
                $size_input['production_order_detail_id'] = $order_detail_id;

                ProductionOrderDetailSize::insert($size_input);
                $i++;
            }


            Session::flash('message', 'Successfully created a Productions Order Detail!');
            return Redirect::to('productionOrderDetail');
        }
        else{
            return Redirect::to('productionOrderDetail/create/'.$order_input['production_order_id'])->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        $data = array();
        $order_details = ProductionOrderDetail::where('production_order_id', '=', $id)->get();
        $order = ProductionOrder::find($id);

        if($order){
            $buyer = ProductionOrder::find($order['id'])->buyer;
            $order['buyer_name'] = $buyer['buyer_name'];
        }

        foreach($order_details as $item){
            $item['production_order_no'] = ProductionOrderDetail::find($item['id'])->productionOrder->order_no;
            $item['article_no']          = ProductionOrder::find($id)->article->article_no;
            $item['destination_name']    = ProductionOrderDetail::find($item['id'])->destination->destination_name;
            $item['color_name']          = ProductionOrderDetail::find($item['id'])->color->color_name_flcl;
            $item['size']                = ProductionOrderDetail::find($item['id'])->sizes;
        }

//        dd($order);

        return view('production_order_detail.show', $data)->with('order_details', $order_details)->with('order', $order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $data = array();

        $data['order_detail']   = ProductionOrderDetail::find($id);
        $data['article']        = array('' => 'Select Article') + Article::pluck('article_no','id')->toArray();
        $data['color']          = array('' => 'Select Color') + Color::pluck('color_name_flcl','id')->toArray();
        $data['destination']    = array('' => 'Select Destination') + Destination::pluck('destination_name','id')->toArray();
        $data['season']         = array('' => 'Select Season') + Season::pluck('season','id')->toArray();
        $data['size_group']     = array('' => 'Select Size Group') + SizeGroup::pluck('size_group_name','id')->toArray();
        $data['size']           = array('' => 'Select Size') + Size::pluck('size_no','id')->toArray();


        if($data['order_detail']){
            $data['order']              = ProductionOrder::find($data['order_detail']->production_order_id);
            $data['order_detail_sizes'] = ProductionOrderDetail::find($id)->Sizes;
            $data['sizes']              = SizeGroupSize::where('size_group_id', '=', $data['order']->size_group_id)->get();

        }
        else{
            return Redirect::to('productionOrderDetail')->with(array('error_messages'=>'<div class="alert alert-error">Order detail not found</div>'));
        }

        return view('production_order_detail.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $order_input = $request->only('production_order_id', 'color_id', 'destination_id', 'days_ship_advance', 'remarks', 'total_quantity');

        $order_input['order_date'] = strtotime($request->get('order_date'));
        $order_input['cut_off'] = strtotime($request->get('cut_off'));
        $rules =array(
//            'order_no' => array('required')
        );


        $validator = Validator::make($order_input,$rules);
        $order_input['updated_at'] = new DateTime;
        if ($validator->passes()){
            ProductionOrderDetail::where('id','=',$id)->update($order_input);
            $order_detail = ProductionOrderDetail::find($id);
            $order = ProductionOrder::find($request->get('production_order_id'));

            ProductionOrderDetailSize::where('production_order_detail_id', '=', $order_detail->id)->delete();

            $size  = SizeGroupSize::where('size_group_id', '=', $order->size_group_id)->get();
            $i = 1;
            foreach($size as $s){

                $size_input['size_id'] = $request->get('size_id_'.$i);
                $size_input['quantity'] = $request->get('quantity_'.$i);
                $size_input['production_order_detail_id'] = $id;

                ProductionOrderDetailSize::insert($size_input);
                $i++;
            }


            Session::flash('message', 'Successfully updated a Productions Order Detail!');
            return Redirect::to('productionOrderDetail');
        }
        else{
            return Redirect::to('productionOrderDetail/create/'.$order_input['production_order_id'])->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    private function getLastProductionOrderDetail()
    {
        return DB::table('production_order_detail')->orderBy('id', 'desc')->first();
    }

    function getOrderByBuyer(Request $request){
        $info = $request->only('buyer_id');
        $data = array();

        if($info['buyer_id']){
            $data['buyer_id'] = $info['buyer_id'];
            $data['orders'] = ProductionOrder::where('buyer_id', '=', $info['buyer_id'])->lists('order_no', 'id');
            $data['error'] = false;
            echo json_encode($data);
            die;
        }
        else{
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

    function getOrderInfo(Request $request){
        $info = $request->only('order_id');
        $data = array();

        if($info['order_id']){
            $data['order_id'] = $info['order_id'];
            $data['article_id'] = ProductionOrder::find($info['order_id'])->article_id;
            $data['season_id'] = ProductionOrder::find($info['order_id'])->season_id;
            $data['order_date'] = date('Y-m-d', ProductionOrder::find($info['order_id'])->order_date);


            $sizes = SizeGroupSize::where('size_group_id', '=', ProductionOrder::find($info['order_id'])->size_group_id)->get();
            $size_html = '';
            $i = 1;
            if($sizes){
                foreach($sizes as $s){
                    $size_no = (Helpers::getSizeNoById($s->size_id))?Helpers::getSizeNoById($s->size_id):'';
                    $size_html .= "<div class='form-group'><label class='control-label col-md-4' for='size_no_".$i."'>".$size_no."</label>";
                    $size_html .= "<input type='hidden' name='size_id_".$i."' value='".$s->size_id."' /><div class='controls col-md-8'>";
                    $size_html .= "<input type='text' name='quantity_".$i."' id='quantity_".$i."' class='size_qty form-control Required' onChange='size_change();' /></div><div class='clearfix'></div></div>";
                    $i++;
                }
            }
            $data['size_html'] = $size_html;
            $data['error'] = false;
            echo json_encode($data);
            die;
        }
        else{
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }
}