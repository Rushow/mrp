<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\PaymentTerm;
use Validator;
use View;

class PaymentTermController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public $layout = 'layouts.master';

    public function index()
    {
        $payment_terms = PaymentTerm::all();

        return view('payment_term.index')->with('payment_terms', $payment_terms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('payment_term.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'term'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('payment_term/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $payment_term = new PaymentTerm;

            $payment_term->term       = $request->get('term');

            $payment_term->save();
            Session::flash('message', 'Successfully created a payment term!');
            return Redirect::to('paymentTerm');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = array();
        $data['term'] = PaymentTerm::find($id);

        return view('payment_term.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $rules = array(
            'term'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('payment_term/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $term['term'] = $request->get('term');
            $term['updated_at'] = new DateTime;

            PaymentTerm::where('id','=',$id)->update($term);

            Session::flash('message', 'Successfully updated a payment Term!');
            return Redirect::to('paymentTerm');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Season::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a season!');
        return Redirect::to('season');
    }

}