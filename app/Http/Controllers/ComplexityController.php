<?php

namespace App\Http\Controllers;

use App\Complexity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class ComplexityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data["complexity"] = Complexity::get();
        return view('complexity.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();

        return view('complexity.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input  = $request->only('complexity_name');

        $sub = new Complexity();
        $sub->complexity = $request->input('complexity_name');
        $sub->save();

        return Redirect::to('complexity');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array();

        $data["complexity"] =Complexity::find($id);

        return view('complexity.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sub = Complexity::find($id);
        $sub->complexity = $request->input('complexity_name');

        $sub->update();
        return Redirect::to('complexity');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $row = Complexity::find($id);

        $row->destroy();
    }
}
