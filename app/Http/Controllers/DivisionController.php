<?php

namespace App\Http\Controllers;

use App\Subdivision;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use View;
use HTML;
use Validator;
use Session;
use Form;
use App\Division;
use Illuminate\Support\Facades\Redirect;

class DivisionController extends Controller
{
    //
    public function show(){
        $data = array();
        $data["division"] = Division::get();
        return view('Division.show',$data);
        
    }
    public function showdetail($id){
        $data = array();
        $data["division"] = Division::find($id);

        //print_r($data);die;
        return view('Division.show',$data);

    }
    public function edit($id){
        $data = array();
        $data["division"] = Division::find($id);
        return view('Division.edit',$data);
    }
    public function editdivision(Request $request,$id){

        //$data = array();
        $division = Division::find($id);
        $division->name =$request->input('division_name');
        $division->description = $request->input('description');
        $division->update();

        return Redirect::to('showdivision');
    }
    
    public function add(){
        $data = array();
        $data["division"] = Division::get();
        return view('Division.create',$data);
    
    }
    public function adddivision(Request $request){

        $data = array();
        $input=$request->only('description','division_name');
        $division = new Division();
        $division->name =$input['division_name'];
        $division->description = $input['description'];
        $division->save();


        return view('Division.create',$data);

    }
    public function deletedivision($id){
        $row = Division::find($id);

        $row->delete();
        return Redirect::to('division');
    }

  
}
