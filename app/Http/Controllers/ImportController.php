<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\HeelNo;
use Validator;
use View;

class ImportController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        return view('import.index');
	}

    public function importBuyer()
	{
        return view('import.buyer_import');
	}

    public function importBuyerSave(Request $request)
	{
        $rules = array(
            'import_file'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('importBuyer')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $file = $request->file('import_file');

            Excel::load($file, function($reader) {
                $results = $reader->get();

                foreach($results as $result){
                    $buyer = new Buyer;
                    $buyer['buyer_name'] 		= $result->buyer;
                    $buyer['buyer_email'] 		= $result->email;
                    $buyer['buyer_phn'] 		= $result->phone;
                    $buyer['full_name'] 		= $result->name;
                    $buyer['week_start_day']	= $result->week_start_day;
                    $buyer['buyer_template_id']	= $result->buyer_template;
                    $buyer['created_by'] 		= Auth::user()->id;
                    $buyer['created_at'] 		= new DateTime();

                    if(Input::get('buyer_template_id') == 2){
                        $buyer['cut_off_day'] 		= $result->cut_off_day;
                        $buyer['cut_off_diff'] 		= $result->cut_off_diff;
                        $buyer['etd_day']			= $result->etd_day;
                        $buyer['etd_diff']			= $result->etd_diff;
                    }

                    $address = new Address;
                    $address['floor'] 			= $result->floor;
                    $address['street_address'] 	= $result->street_address;
                    $address['city'] 			= $result->city;
                    $address['state'] 			= $result->state;
                    $address['zip'] 			= $result->zip;
                    $address['country'] 		= $result->country;
                    $address->save();
                    $buyer->address_id = $address->id;

                    $buyer->save();
                }

            });



            Session::flash('message', 'Successfully imported Buyers!');
            return Redirect::to('importBuyer');
        }
	}

    public function importColor()
	{
       return view('import.color_import');
	}

    public function importColorSave(Request $request)
	{
        $rules = array(
            'import_file'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('importColor')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $file = $request->file('import_file');

            Excel::load($file, function($reader) {
                $results = $reader->get();

                foreach($results as $result){
                    $color = new Color;
                    $color['pantone_code'] 		= $result->pantone_code;
                    $color['hm_code'] 		    = $result->buyer_code;
                    $color['color_group_code']  = $result->color_group_code;
                    $color['color_name_flcl']   = $result->color_name_flcl;
                    $color['color_name_hm']	    = $result->color_name_buyer;
                    $color['created_by'] 		= Auth::user()->id;
                    $color['created_at'] 		= new DateTime();

                    $color->save();
                }

            });

            Session::flash('message', 'Successfully imported Colors!');
            return Redirect::to('importColor');
        }
	}

    public function importSeason()
	{
        return view('import.season_import');
	}

    public function importSeasonSave(Request $request)
	{
        $rules = array(
            'import_file'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('importSeason')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $file = $request->file('import_file');

            Excel::load($file, function($reader) {
                $results = $reader->get();

                foreach($results as $result){
                    $season = new Season;
                    $season['season'] 		= $result->season;
                    $season['start_date'] 	= strtotime($result->start_date);
                    $season['end_date'] 	= strtotime($result->end_date);
                    $season['created_by'] 	= Auth::user()->id;
                    $season['created_at'] 	= new DateTime();

                    $season->save();
                }
            });

            Session::flash('message', 'Successfully imported Seasons!');
            return Redirect::to('importSeason');
        }
	}

    public function importDestination()
	{
        return view('import.destination_import');
	}

    public function importDestinationSave(Request $request)
	{
        $rules = array(
            'import_file'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('importDestination')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $file = $request->file('import_file');

            Excel::load($file, function($reader) {
                $results = $reader->get();

//                echo '<pre>';print_r($results);die;
                foreach($results as $result){
                    $destination = new Destination;
                    $destination['destination_name'] 		    = $result->country;
                    $destination['destination_code'] 		    = $result->country_code;
                    $destination['cut_off_diff'] 		        = $result->cut_off_day;
                    $destination['cut_off_day'] 		        = $result->cut_off_day_difference;
                    $destination['etd_diff'] 		            = $result->etd_difference;
                    $destination['etd_day'] 		            = $result->etd_day;
                    $destination['assort_packing'] 		        = $result->assortment_packing;
                    $destination['carton_details_assort'] 		= $result->carton_details_assort;
                    $destination['carton_details_primary'] 		= $result->carton_details_primary;
                    $destination['carton_details_secondary']    = $result->carton_details_secondary;
                    $destination['consignee_address'] 		    = $result->consignee_address;
                    $destination['remarks'] 		            = $result->remarks;
                    $destination['sock_print_id'] 		        = $result->socks_print;
                    $destination['lining_print_id'] 		    = $result->lining_print;
                    $destination['additional_print_id'] 		= $result->additional_print;
                    $destination['poly_sticker_id'] 		    = $result->poly_sticker;
                    $destination['pictogram_id'] 		        = $result->pictogram;
                    $destination['price_tag_id'] 		        = $result->price_tag;
                    $destination['pm_tag_id'] 		            = $result->pm_tag;
                    $destination['additional_sticker_info_id'] 	= $result->additional_information_sticker;
                    $destination['created_by'] 	                = Auth::user()->id;
                    $destination['created_at'] 	                = new DateTime();

                    $destination->save();
                }
            });

            Session::flash('message', 'Successfully imported Destinations!');
            return Redirect::to('importDestination');
        }
	}

    public function importAppearance()
	{
        return view('import.appearance_import');
	}

    public function importAppearanceSave(Request $request)
	{
        $rules = array(
            'import_file'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('importAppearance')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $file = $request->file('import_file');

            Excel::load($file, function($reader) {
                $results = $reader->get();

                foreach($results as $result){
                    $appearance = new ArticleType();
                    $appearance['article_type']     = $result->appearance;
                    $appearance['created_by'] 	    = Auth::user()->id;
                    $appearance['created_at'] 	    = new DateTime();

                    $appearance->save();
                }
            });

            Session::flash('message', 'Successfully imported Appearances!');
            return Redirect::to('importAppearance');
        }
	}

    public function importConstruction()
	{
        return view('import.construction_import');
	}

    public function importConstructionSave(Request $request)
	{
        $rules = array(
            'import_file'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('importConstruction')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $file = $request->file('import_file');

            Excel::load($file, function($reader) {
                $results = $reader->get();

                foreach($results as $result){
                    $construction = new Construction();
                    $construction['construction']     = $result->construction;
                    $construction['created_by'] 	  = Auth::user()->id;
                    $construction['created_at'] 	  = new DateTime();

                    $construction->save();
                }
            });

            Session::flash('message', 'Successfully imported Construction!');
            return Redirect::to('importConstruction');
        }
	}

    public function importCustomerGroup()
	{
        return view('import.customer_group_import');
	}

    public function importCustomerGroupSave(Request $request)
	{
        $rules = array(
            'import_file'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('importCustomerGroup')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $file = $request->file('import_file');

            Excel::load($file, function($reader) {
                $results = $reader->get();

                foreach($results as $result){
                    $customer_group = new CustomerGroup();
                    $customer_group['customer_group']     = $result->customer_group;
                    $customer_group['created_by'] 	  = Auth::user()->id;
                    $customer_group['created_at'] 	  = new DateTime();

                    $customer_group->save();
                }
            });

            Session::flash('message', 'Successfully imported Customer Group!');
            return Redirect::to('importCustomerGroup');
        }
	}

}