<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\Pictogram;
use Validator;
use View;

class PictogramController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $pictogram = Pictogram::all();

        return view('pictogram.index')->with('pictogram', $pictogram);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data = array();
        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');
        return view('pictogram.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'pictogram'     => 'required',
            'status'           => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('pictogram/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type = new Pictogram;

            $type->pictogram       = $request->get('pictogram');
            $type->status             = $request->get('status');
            $type->enable_date        = strtotime($request->get('enable_date'));

            $type->save();
            Session::flash('message', 'Successfully created a pictogram!');
            return Redirect::to('pictogram');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['pictogram'] = Pictogram::find($id);

        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');

        return view('pictogram.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'pictogram'       => 'required',
            'status'             => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('pictogram/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type['pictogram'] = $request->get('pictogram');
            $type['status']       = $request->get('status');
            $type['enable_date']  = strtotime($request->get('enable_date'));
            $type['updated_at'] = new DateTime;

            Pictogram::where('id','=',$id)->update($type);

            Session::flash('message', 'Successfully updated a pictogram!');
            return Redirect::to('pictogram');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Pictogram::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a pictogram!');
        return Redirect::to('pictogram');
	}

}