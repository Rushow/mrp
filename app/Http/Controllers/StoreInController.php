<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Http\Request;
use DateTime;
use Session;

use App\StoreOut;
use App\Buyer;
use App\Article;
use App\SampleType;

use App\StoreIn;
use App\Warehouse;
use App\Supplier;
use App\ItemGroup;
use App\ItemSubGroup;
use App\Parameter;
use App\Color;
use App\Item;
use App\Currency;
use App\SampleOrder;
use App\ProductionOrder;
use Validator;

class StoreInController extends Controller {

	public $layout = 'layouts.master';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$filters = $request->only('sample_order_no','buyer_id','article_id', 'sample_type_id', 'order_date','delivery_date');

        $sample_order = $filters['sample_order_no'];
        $buyer = $filters['buyer_id'];
        $article = $filters['article_id'];
        $sample_type = $filters['sample_type_id'];
        $order_date = $filters['order_date'];
        $delivery_date = $filters['delivery_date'];

        $data['sample_order_no'] = array('' => 'Select Sample Order No') + SampleOrder::pluck('sample_order_no','id')->toArray();
        $data['buyer'] = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $data['article'] = array('' => 'Select Article No') + Article::pluck('article_no','id')->toArray();
        $data['sample_type'] = array('' => 'Select Type of Sample') + SampleType::pluck('sample_type','id')->toArray();
		$data['store_ins'] = StoreIn::all();

		return view('store_in.index', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$warehouse = array('' => 'Select') + Warehouse::pluck('name','id')->toArray();
		$supplier = array('' => 'Select') + Supplier::pluck('supplier_name','id')->toArray();
		$item_group = array('' => 'Select') + ItemGroup::pluck('group_name','id')->toArray();
		$item_sub_group = array('' => 'Select') + ItemSubGroup::pluck('sub_group_name','id')->toArray();
		$parameter = array('' => 'Select') + Parameter::pluck('parameter_name','id')->toArray();
		$color = array('' => 'Select') + Color::pluck('color_name_flcl','id')->toArray();
		$item = array('' => 'Select') + Item::pluck('item_name','id')->toArray();
		$currency = array('' => 'Select') + Currency::pluck('abbr','id')->toArray();
		$unit = "";
		$order_type = array('sample' => 'Sample', 'production' => 'Production');
		$sample_orders = array('' => 'Select') + SampleOrder::pluck('sample_order_no','id')->toArray();
		$production_orders = array('' => 'Select') + ProductionOrder::pluck('order_no','id')->toArray();
		$data = array(
			'warehouse' => $warehouse,
			'supplier' => $supplier, 
			'item_group' => $item_group, 
			'item_sub_group' => $item_sub_group,
			'parameter' => $parameter,
			'color' => $color,
			'item' => $item,
			'currency' => $currency,
			'unit' => $unit,
			'order_type' => $order_type,
			'sample_orders' => $sample_orders,
			'production_orders' => $production_orders,
			);
		return view('store_in.create', $data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$store_in_input = $request->only('warehouse_id', 'challan_no', 'supplier_id', 'order_no_id', 'in_type', 'store_in_date', 'remarks');
        $rules =array(
            'warehouse_id' => array('required'),
            'challan_no' => array('required'),
            'supplier_id' => array('required'),
            'order_no_id' => array('required'),
            'store_in_date' => array('required')
        );

        $validator = Validator::make($store_in_input, $rules);
        if ($validator->passes()){
        	$store_in = StoreIn::create($store_in_input);
            $input = Input::only('total_row');
            $total_row = $input['total_row'];
            for($i=1; $i<=$total_row;$i++){
                if(Input::get('item_id_'.$i)){
                	$store_in_detail = new StoreInDetail();
                	$store_in_detail->store_in_id = $store_in->id;
                	$store_in_detail->item_group_id = Input::get('item_group_id_'.$i);
            		$store_in_detail->sub_group_id = Input::get('sub_group_id_'.$i);
            		$store_in_detail->parameter_id = Input::get('parameter_id_'.$i);
        			$store_in_detail->color_id = Input::get('color_id_'.$i);
    				$store_in_detail->item_id = Input::get('item_id_'.$i);
    				$store_in_detail->quantity = Input::get('quantity_'.$i);
					$store_in_detail->unit_id = Input::get('unit_id_'.$i);
					$store_in_detail->save();
                }
            }
            Session::flash('message', 'Successfully created a Store In!');
            return Redirect::to('storeIn');
        }
        else{
            return Redirect::to('storeIn/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$store_in = StoreIn::find($id);
		$store_in_details = $store_in->Details;
		$data = array(
			'store_in' => $store_in,
			'store_in_details' => $store_in_details
			);
		return view('store_in.show', $data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$warehouse = array('' => 'Select') + Warehouse::pluck('name','id')->toArray();
		$supplier = array('' => 'Select') + Supplier::pluck('supplier_name','id')->toArray();
		$item_group = array('' => 'Select') + ItemGroup::pluck('group_name','id')->toArray();
		$item_sub_group = array('' => 'Select') + ItemSubGroup::pluck('sub_group_name','id')->toArray();
		$parameter = array('' => 'Select') + Parameter::pluck('parameter_name','id')->toArray();
		$color = array('' => 'Select') + Color::pluck('color_name_flcl','id')->toArray();
		$item = array('' => 'Select') + Item::pluck('item_name','id')->toArray();
		// $currency = array('' => 'Select') + Currency::pluck('abbr','id');
		$unit = "";		
		$sample_orders = array('' => 'Select') + SampleOrder::pluck('sample_order_no','id')->toArray();
		$production_orders = array('' => 'Select') + ProductionOrder::pluck('order_no','id')->toArray();
		$store_in = StoreIn::find($id);
		if ($store_in->in_type == 'sample') {
			$order_type = array('sample' => 'Sample', 'production' => 'Production');
			$order_no = $sample_orders;
		}else{
			$order_type = array('production' => 'Production', 'sample' => 'Sample');
			$order_no = $production_orders;
		}
		$store_in_details = $store_in->Details;
		$data = array(
			'warehouse' => $warehouse,
			'supplier' => $supplier, 
			'item_group' => $item_group, 
			'item_sub_group' => $item_sub_group,
			'parameter' => $parameter,
			'color' => $color,
			'item' => $item,
			// 'currency' => $currency,
			'unit' => $unit,
			'store_in' => $store_in,
			'store_in_details' => $store_in_details,
			'order_type' => $order_type,
			'order_no' => $order_no,
			'groups' => count($store_in_details)
			);
		return view('store_in.edit', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$store_in_input = $request->only('warehouse_id', 'challan_no', 'supplier_id', 'in_type', 'order_no_id', 'store_in_date', 'remarks');
        $rules=array(
            'warehouse_id' => array('required'),
            'challan_no' => array('required'),
            'supplier_id' => array('required'),
            'order_no_id' => array('required'),
            'store_in_date' => array('required')
        );
        StoreIn::where('id','=',$id)->update($store_in_input);
        $store_in = StoreIn::find($id);
        StoreInDetail::where('store_in_id', '=', $id)->delete();

        $validator = Validator::make($store_in_input,$rules);
        if ($validator->passes()){
            $input = $request->only('total_row');
            $total_row = $input['total_row'];
            for($i=1; $i<=$total_row;$i++){
                if(Input::get('item_id_'.$i)){
                	$store_in_detail = new StoreInDetail();
                	$store_in_detail->store_in_id = $store_in->id;
                	$store_in_detail->item_group_id = $request->get('item_group_id_'.$i);
            		$store_in_detail->sub_group_id = $request->get('sub_group_id_'.$i);
            		$store_in_detail->parameter_id = $request->get('parameter_id_'.$i);
        			$store_in_detail->color_id = $request->get('color_id_'.$i);
    				$store_in_detail->item_id = $request->get('item_id_'.$i);
					// $store_in_detail->rate = Input::get('rate_'.$i);
					// $store_in_detail->currency_id = Input::get('currency_id_'.$i);
					$store_in_detail->quantity = $request->get('quantity_'.$i);
					$store_in_detail->unit_id = $request->get('unit_id_'.$i);
					$store_in_detail->save();
                }
            }
            Session::flash('message', 'Successfully created a Sample Order!');
            return Redirect::to('storeIn');
        }
        else{
            return Redirect::to('storeIn/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$store_in = StoreIn::find($id);
		$store_in->delete();
		return Redirect::to('storeIn');
	}

    public function addStoreInRow(Request $request){
        $info = $request->only('last_id');
        if($info){
            $last_id = $info['last_id'] + 1;
            $warehouse = array('' => 'Select') + Warehouse::pluck('name','id')->toArray();
			$supplier = array('' => 'Select') + Supplier::pluck('supplier_name','id')->toArray();
			$item_group = array('' => 'Select') + ItemGroup::pluck('group_name','id')->toArray();
			$item_sub_group = array('' => 'Select') + ItemSubGroup::pluck('sub_group_name','id')->toArray();
			$parameter = array('' => 'Select') + Parameter::pluck('parameter_name','id')->toArray();
			$color = array('' => 'Select') + Color::pluck('color_name_flcl','id')->toArray();
			$item = array('' => 'Select') + Item::pluck('item_name','id')->toArray();
			$currency = array('' => 'Select') + Currency::pluck('abbr','id')->toArray();
			$unit = "";	

            $msg = "Are you sure to delete this item???";

            $html = "<td>".$last_id."</td>";
            $html .= "<td>".Form::select('item_group_id_'.$last_id, $item_group, '', $attributes = array('id'=>'item_group_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required', 'onChange'=>'generateSubGroup(this, '.$last_id.');'))."</td>";
            $html .= "<td>".Form::select('sub_group_id_'.$last_id, $item_sub_group, null, $attributes = array('id'=>'sub_group_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required', 'disabled'=>'disabled', 'onChange' => 'generateItems(this, ' . $last_id . ')'))."</td>";
            $html .= "<td>".Form::select('parameter_id_'.$last_id, $parameter, '', $attributes = array('id'=>'parameter_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::select('color_id_'.$last_id, $color, '', $attributes = array('id'=>'color_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::select('item_id_'.$last_id, $item, '', $attributes = array('id'=>'item_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required', 'disabled' => 'disabled', 'onChange'=>'generateUnit(this, '.$last_id.');'))."</td>";
            $html .= '<td>'.Form::text('quantity_'.$last_id, '',array('class'=>'form-control Required','id'=>'quantity_'.$last_id)).'</td>';
        	$html .= Form::hidden('unit_id_'.$last_id, '', array('id'=> 'unit_id_'.$last_id));
            $html .= '<td>'.Form::text('unit_'.$last_id, $unit, array('id'=>'unit_'.$last_id, 'data-rel'=>'chosen', 'class'=>'form-control Required', 'readonly'=>'readonly')).'</td>';
            $html .= "<td><a href='#' onclick='return checkDelete(this);'><span class='badge badge-important'>X</span></a></td>";

            $data = array();
            $data['error'] = false;
            $data['html'] = $html;
            echo json_encode($data);
            die();
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die();
        }
    }

    function orderType(Request $request){
        $order_type = $request->only('order_type');
        $data = array();

        if($order_type['order_type'] == 'sample'){
            $order_no = array('' => 'Select') + SampleOrder::pluck('sample_order_no','id');
            $data['orders'] = Form::select('order_no_id', $order_no, '', $attributes = array('id'=>'order_no_id','data-rel'=>'chosen', 'class'=>'form-control'));
            echo json_encode($data);
	        die;            
        }elseif ($order_type['order_type'] == 'production') {
        	$order_no = array('' => 'Select') + ProductionOrder::pluck('production_order_no','id');
        	$data['orders'] = Form::select('order_no_id', $order_no, '', $attributes = array('id'=>'order_no_id','data-rel'=>'chosen', 'class'=>'form-control'));
        	echo json_encode($data);
	        die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            die();
        }
    }
}
