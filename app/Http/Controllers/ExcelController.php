<?php
namespace App\Http\Controllers;

use App\Buyer;
use App\Destination;
use App\Article;
use App\Color;
use App\Season;
use App\PaymentTerm;
use App\SizeGroup;
use App\Category;
use App\StoreIn;
use App\StoreOut;
use App\Item;
use App\ArticleType;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use Session;
use View;


class ExcelController extends Controller {
	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function articleType()
	{
		$types = ArticleType::all();
		$typesArray = array();
		array_push($typesArray, array('Article Type', 'Date Created'));
		foreach ($types as $type) {
	    	array_push($typesArray, array($type->article_type, $type->created_at->format('d.m.Y')));
	    }
		Excel::create('article_types', function($excel) use($typesArray) {

		    $excel->setTitle('Article Types');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Article Type', function($sheet) use($typesArray) {

	        $sheet->fromArray($typesArray);

	    });

		})->download('xls');

		return Redirect::to('articleType');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function blockTtNo()
	{
		$block_tt_no = BlockTtNo::all();
		$block_tt_no_array = array();
		array_push($block_tt_no_array, array('Block TT No', 'Date Created'));
		foreach ($block_tt_no as $block) {
	    	array_push($block_tt_no_array, array($block->block_tt_no, $block->created_at->format('d.m.Y')));
	    }
		Excel::create('Block TT No', function($excel) use($block_tt_no_array) {

		    $excel->setTitle('Block TT No');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Block TT No', function($sheet) use($block_tt_no_array) {

	        $sheet->fromArray($block_tt_no_array);

	    });

		})->download('xls');

		return Redirect::to('blockTtNo');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function construction()
	{
		$constructions = Construction::all();
		$constructionsArray = array();
		array_push($constructionsArray, array('Construction', 'Date Created'));
		foreach ($constructions as $construction) {
	    	array_push($constructionsArray, array($construction->construction, $construction->created_at->format('d.m.Y')));
	    }
		Excel::create('constructions', function($excel) use($constructionsArray) {

		    $excel->setTitle('Constructions');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');


		    $excel->sheet('Construction', function($sheet) use($constructionsArray) {

	        $sheet->fromArray($constructionsArray);

	    });

		})->download('xls');

		return Redirect::to('construction');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function category()
	{
		$categories = Category::all();
		$categoriesArray = array();
		array_push($categoriesArray, array('Category', 'Date Created'));
		foreach ($categories as $category) {
	    	array_push($categoriesArray, array($category->category_name, $category->created_at->format('d.m.Y')));
	    }

		Excel::create('categories', function($excel) use($categoriesArray) {

		    $excel->setTitle('categories');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Category', function($sheet) use($categoriesArray) {

	        $sheet->fromArray($categoriesArray);

	    });

		})->download('xls');

		return Redirect::to('category');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function size()
	{
		$sizes = Size::all();
		$sizesArray = array();
		array_push($sizesArray, array('Size No', 'Date Created'));
		foreach ($sizes as $size) {
	    	array_push($sizesArray, array($size->size_no, $size->created_at->format('d.m.Y')));
	    }
		Excel::create('sizes', function($excel) use($sizesArray) {

		    $excel->setTitle('Sizes');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Sizes', function($sheet) use($sizesArray) {

	        $sheet->fromArray($sizesArray);

	    });

		})->download('xls');

		return Redirect::to('size');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function sizeGroup()
	{
		$sizeGroups = SizeGroup::all();
		$sizeGroupsArray = array();
		array_push($sizeGroupsArray, array('Size Group', 'Date Created'));
		foreach ($sizeGroups as $sizeGroup) {
	    	array_push($sizeGroupsArray, array($sizeGroup->size_group_name, $sizeGroup->created_at->format('d.m.Y')));
	    }
		Excel::create('size_groups', function($excel) use($sizeGroupsArray) {

		    $excel->setTitle('Size Groups');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Size Group', function($sheet) use($sizeGroupsArray) {

	        $sheet->fromArray($sizeGroupsArray);

	    });

		})->download('xls');

		return Redirect::to('sizeGroup');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function color()
	{
		$colors = Color::all();
		$colorsArray = array();
		array_push($colorsArray, array('Color', 'Date Created'));
		foreach ($colors as $color) {
	    	array_push($colorsArray, array($color->color_name, $color->created_at->format('d.m.Y')));
	    }
		Excel::create('colors', function($excel) use($colorsArray) {

		    $excel->setTitle('Colors');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Color', function($sheet) use($colorsArray) {

	        $sheet->fromArray($colorsArray);

	    });

		})->download('xls');

		return Redirect::to('color');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function itemGroup()
	{
		$itemGroups = ItemGroup::all();
		$itemGroupsArray = array();
		array_push($itemGroupsArray, array('Item Group', 'Date Created'));
		foreach ($itemGroups as $itemGroup) {
	    	array_push($itemGroupsArray, array($itemGroup->group_name, $itemGroup->created_at->format('d.m.Y')));
	    }
		Excel::create('item_groups', function($excel) use($itemGroupsArray) {

		    $excel->setTitle('Item Groups');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Item Group', function($sheet) use($itemGroupsArray) {

	        $sheet->fromArray($itemGroupsArray);

	    });

		})->download('xls');

		return Redirect::to('itemGroup');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function itemSubGroup()
	{
		$itemSubGroups = ItemSubGroup::all();
		$itemSubGroupsArray = array();
		array_push($itemSubGroupsArray, array('Item Sub Group', 'Group Name', 'Date Created'));
		foreach ($itemSubGroups as $itemSubGroup) {
	    	array_push($itemSubGroupsArray, array($itemSubGroup->sub_group_name, $itemSubGroup->group()->first()->group_name, $itemSubGroup->created_at->format('d.m.Y')));
	    }
		Excel::create('item_sub_groups', function($excel) use($itemSubGroupsArray) {

		    $excel->setTitle('Item Sub Groups');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Item Sub Group', function($sheet) use($itemSubGroupsArray) {

	        $sheet->fromArray($itemSubGroupsArray);

	    });

		})->download('xls');

		return Redirect::to('itemSubGroup');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function lastNo()
	{
		$lastNos = LastNo::all();
		$lastNosArray = array();
		array_push($lastNosArray, array('Last No', 'Date Created'));
		foreach ($lastNos as $lastNo) {
	    	array_push($lastNosArray, array($lastNo->last_no, $lastNo->created_at->format('d.m.Y')));
	    }
		Excel::create('last_nos', function($excel) use($lastNosArray) {

		    $excel->setTitle('Last Nos');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Last No', function($sheet) use($lastNosArray) {

	        $sheet->fromArray($lastNosArray);

	    });

		})->download('xls');

		return Redirect::to('lastNo');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function heelNo()
	{
		$heelNos = HeelNo::all();
		$heelNosArray = array();
		array_push($heelNosArray, array('Heel No', 'Date Created'));
		foreach ($heelNos as $heelNo) {
	    	array_push($heelNosArray, array($heelNo->heel_no, $heelNo->created_at->format('d.m.Y')));
	    }
		Excel::create('heel_nos', function($excel) use($heelNosArray) {

		    $excel->setTitle('Heel Nos');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Heel No', function($sheet) use($heelNosArray) {

	        $sheet->fromArray($heelNosArray);

	    });

		})->download('xls');

		return Redirect::to('heelNo');
	}

			/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function outsoleNo()
	{
		$outsoleNos = OutsoleNo::all();
		$outsoleNosArray = array();
		array_push($outsoleNosArray, array('Outsole No', 'Date Created'));
		foreach ($outsoleNos as $outsoleNo) {
	    	array_push($outsoleNosArray, array($outsoleNo->outsole_no, $outsoleNo->created_at->format('d.m.Y')));
	    }
		Excel::create('outsole_nos', function($excel) use($outsoleNosArray) {

		    $excel->setTitle('Outsole Nos');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Outsole No', function($sheet) use($outsoleNosArray) {

	        $sheet->fromArray($outsoleNosArray);

	    });

		})->download('xls');

		return Redirect::to('outsoleNo');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function buyer()
	{
		$buyers = Buyer::all();
		$buyersArray = array();
		array_push($buyersArray, array('Buyer', 'Name', 'Email', 'Phone', 'Address'));
		foreach ($buyers as $buyer) {
	    	array_push($buyersArray, array($buyer->buyer_name, $buyer->full_name, $buyer->buyer_email, $buyer->buyer_phn, $buyer->buyer_address));
	    }
		Excel::create('buyers', function($excel) use($buyersArray) {

		    $excel->setTitle('Buyers');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Buyer', function($sheet) use($buyersArray) {

	        $sheet->fromArray($buyersArray);

	    });

		})->download('xls');

		return Redirect::to('buyer');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function supplier()
	{
		$suppliers = Supplier::all();
		$suppliersArray = array();
		array_push($suppliersArray, array('Supplier Name', 'Supplier Location', 'Remarks'));
		foreach ($suppliers as $supplier) {
	    	array_push($suppliersArray, array($supplier->supplier_name, $supplier->supplier_location, $supplier->remarks));
	    }
		Excel::create('suppliers', function($excel) use($suppliersArray) {

		    $excel->setTitle('Suppliers');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Supplier', function($sheet) use($suppliersArray) {

	        $sheet->fromArray($suppliersArray);

	    });

		})->download('xls');

		return Redirect::to('supplier');
	}	

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function parameter()
	{
		$parameters = Parameter::all();
		$parametersArray = array();
		array_push($parametersArray, array('Parameter Name', 'Date Created'));
		foreach ($parameters as $parameter) {
	    	array_push($parametersArray, array($parameter->parameter_name, $parameter->created_at->format('d.m.Y')));
	    }
		Excel::create('parameters', function($excel) use($parametersArray) {

		    $excel->setTitle('Parameters');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Parameter', function($sheet) use($parametersArray) {

	        $sheet->fromArray($parametersArray);

	    });

		})->download('xls');

		return Redirect::to('parameter');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function unit()
	{
		$units = Unit::all();
		$unitsArray = array();
		array_push($unitsArray, array('Unit Name', 'Date Created'));
		foreach ($units as $unit) {
	    	array_push($unitsArray, array($unit->unit_name, $unit->created_at->format('d.m.Y')));
	    }
		Excel::create('units', function($excel) use($unitsArray) {

		    $excel->setTitle('Units');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Unit', function($sheet) use($unitsArray) {

	        $sheet->fromArray($unitsArray);

	    });

		})->download('xls');

		return Redirect::to('unit');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function sampleType()
	{
		$sampleTypes = SampleType::all();
		$sampleTypesArray = array();
		array_push($sampleTypesArray, array('Sample Type', 'Date Created'));
		foreach ($sampleTypes as $sampleType) {
	    	array_push($sampleTypesArray, array($sampleType->sample_type, $sampleType->created_at->format('d.m.Y')));
	    }
		Excel::create('sample_types', function($excel) use($sampleTypesArray) {

		    $excel->setTitle('Sample Types');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Sample Type', function($sheet) use($sampleTypesArray) {

	        $sheet->fromArray($sampleTypesArray);

	    });

		})->download('xls');

		return Redirect::to('sampleType');
	}

	/**
	 * Download an excel file of the resource
	 *
	 * @return Response
	 */
	public function component()
	{
		$components = Component::all();
		$componentsArray = array();
		array_push($componentsArray, array('Component Name', 'Date Created'));
		foreach ($components as $component) {
	    	array_push($componentsArray, array($component->component_name, $component->created_at->format('d.m.Y')));
	    }
		Excel::create('components', function($excel) use($componentsArray) {

		    $excel->setTitle('Components');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Component', function($sheet) use($componentsArray) {

	        $sheet->fromArray($componentsArray);

	    });

		})->download('xls');

		return Redirect::to('component');
	}

	public function season()
	{
		$seasons = Season::all();
		$seasonsArray = array();
		array_push($seasonsArray, array('Season', 'Start Date', 'End Date'));
		foreach ($seasons as $season) {
	    	array_push($seasonsArray, array($season->season, date('d-m-Y', $season->start_date), date('d-m-Y', $season->end_date)));
	    }
		Excel::create('seasons', function($excel) use($seasonsArray) {

		    $excel->setTitle('Seasons');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Season', function($sheet) use($seasonsArray) {

	        $sheet->fromArray($seasonsArray);

	    });

		})->download('xls');

		return Redirect::to('season');
	}

	public function department()
	{
		$departments = Department::all();
		$departmentsArray = array();
		array_push($departmentsArray, array('Department Name', 'Date Created'));
		foreach ($departments as $department) {
	    	array_push($departmentsArray, array($department->name, $department->created_at->format('d.m.Y')));
	    }
		Excel::create('departments', function($excel) use($departmentsArray) {

		    $excel->setTitle('Departments');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Department', function($sheet) use($departmentsArray) {

	        $sheet->fromArray($departmentsArray);

	    });

		})->download('xls');

		return Redirect::to('department');
	}

	public function destination()
	{
		$destinations = Destination::all();
		$destinationsArray = array();
		array_push($destinationsArray, array('Destination Name', 'Date Created'));
		foreach ($destinations as $destination) {
	    	array_push($destinationsArray, array($destination->destination_name, $destination->created_at->format('d.m.Y')));
	    }
		Excel::create('destinations', function($excel) use($destinationsArray) {

		    $excel->setTitle('Destinations');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('Destination', function($sheet) use($destinationsArray) {

	        $sheet->fromArray($destinationsArray);

	    });

		})->download('xls');

		return Redirect::to('destination');
	}

	public function paymentTerm()
	{
		$paymentTerms = PaymentTerm::all();
		$paymentTermsArray = array();
		array_push($paymentTermsArray, array('Payment Term', 'Date Created'));
		foreach ($paymentTerms as $paymentTerm) {
	    	array_push($paymentTermsArray, array($paymentTerm->term, $paymentTerm->created_at->format('d.m.Y')));
	    }
		Excel::create('payment_terms', function($excel) use($paymentTermsArray) {

		    $excel->setTitle('Payment Terms');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('payment_term', function($sheet) use($paymentTermsArray) {

	        $sheet->fromArray($paymentTermsArray);

	    });

		})->download('xls');

		return Redirect::to('paymentTerm');
	}

	public function sampleOrder()
	{
		$sampleOrders = SampleOrder::all();
		$sampleOrdersArray = array();
		array_push($sampleOrdersArray, array('Sample Order No.', 'Buyer', 'Order Date', 'Delivery Date'));
		foreach ($sampleOrders as $sampleOrder) {
	    	array_push($sampleOrdersArray, array($sampleOrder->sample_order_no, $sampleOrder->Buyer->buyer_name, $sampleOrder->order_date, $sampleOrder->delivery_date));
	    }
		Excel::create('sample_orders', function($excel) use($sampleOrdersArray) {

		    $excel->setTitle('Sample Orders');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('sample_order', function($sheet) use($sampleOrdersArray) {

	        $sheet->fromArray($sampleOrdersArray);

	    });

		})->download('xls');

		return Redirect::to('sampleOrder');
	}

	public function item()
	{
		$items = Item::all();
		$itemsArray = array();
		array_push($itemsArray, array('Material Code', 'Material Name', 'Group', 'Sub Group', 'Parameter', 'Color', 'Store Unit', 'Purchase Unit'));
		foreach ($items as $item) {
	    	array_push($itemsArray, array($item->item_code, $item->item_name, $item->Group->group_name, $item->SubGroup->sub_group_name, $item->Parameter->parameter_name, $item->Color->color_name, $item->StoreUnit->unit_name, $item->PurchaseUnit->unit_name));
	    }
		Excel::create('items', function($excel) use($itemsArray) {

		    $excel->setTitle('Items');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('item', function($sheet) use($itemsArray) {

	        $sheet->fromArray($itemsArray);

	    });

		})->download('xls');

		return Redirect::to('item');
	}

	public function article()
	{
		$articles = Article::all();
		$articlesArray = array();
		array_push($articlesArray, array('Article No', 'Article Ref', 'Appearance', 'Construction', 'Customer Group'));
		foreach ($articles as $article) {
	    	array_push($articlesArray, array($article->article_no, $article->article_ref, $article->ArticleType->article_type, $article->Construction->construction, $article->Category->category_name));
	    }
		Excel::create('articles', function($excel) use($articlesArray) {

		    $excel->setTitle('articles');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('article', function($sheet) use($articlesArray) {

	        $sheet->fromArray($articlesArray);

	    });

		})->download('xls');

		return Redirect::to('article');
	}

	public function storeIn()
	{
		$storeIns = StoreIn::all();
		$storeInsArray = array();
		array_push($storeInsArray, array('Sl No', 'Challan No', 'Warehouse', 'Supplier', 'Date'));
		foreach ($storeIns as $store_in) {
	    	array_push($storeInsArray, array($store_in->id, $store_in->challan_no, $store_in->Warehouse->name, $store_in->Supplier->supplier_name, $store_in->store_in_date));
	    }
		Excel::create('store_ins', function($excel) use($storeInsArray) {

		    $excel->setTitle('Store Ins');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('store_in', function($sheet) use($storeInsArray) {

	        $sheet->fromArray($storeInsArray);

	    });

		})->download('xls');

		return Redirect::to('storeIn');
	}

	public function storeOut()
	{
		$storeOuts = StoreOut::all();
		$storeOutsArray = array();
		array_push($storeOutsArray, array('Sl No', 'Challan No', 'Warehouse', 'Department', 'Date'));
		foreach ($storeOuts as $store_out) {
	    	array_push($storeOutsArray, array($store_out->id, $store_out->challan_no, $store_out->Warehouse->name, $store_out->Department->name, $store_out->store_out_date));
	    }
		Excel::create('store_outs', function($excel) use($storeOutsArray) {

		    $excel->setTitle('Store Ins');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('store_out', function($sheet) use($storeOutsArray) {

	        $sheet->fromArray($storeOutsArray);

	    });

		})->download('xls');

		return Redirect::to('storeOut');
	}

	public function storeInDetail($id)
	{
		$storeIn = StoreIn::find($id);
		$storeInDetails = $storeIn->Details;
		$details = array();
		array_push($details, array('Store In Challan No', $storeIn->challan_no));
		array_push($details, array('#', 'Item Group', 'Sub Group', 'Parameter', 'Color', 'Item', 'Unit'));
		foreach ($storeInDetails as $detail) {
	    	array_push($details, array($detail->id, $detail->ItemGroup->group_name, $detail->ItemSubGroup->sub_group_name, $detail->Parameter->parameter_name, $detail->Color->color_name, $detail->Item->item_name, $detail->Unit->unit_name));
	    }
	    $data = array(
			'storeIn' => $storeIn,
			'details' => $details
			);
	    // echo "<pre>";
		   //  dd($storeIn);
		Excel::create('store_in_' . $data['storeIn']->challan_no . '_details', function($excel) use($data) {

		    $excel->setTitle('Store In Challan No. ' . $data['storeIn']->challan_no);

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('store_in_' . $data['storeIn']->challan_no . '_details', function($sheet) use($data) {

	        $sheet->fromArray($data['details']);

	    });		    

		})->download('xls');

		return Redirect::to('storeIn');
	}

	public function storeOutDetail($id)
	{
		$storeOut = StoreOut::find($id);
		$storeOutDetails = $storeOut->Details;
		$details = array();
		array_push($details, array('Store Out Challan No', $storeOut->challan_no));
		array_push($details, array('#', 'Item Group', 'Sub Group', 'Parameter', 'Color', 'Item', 'Unit'));
		foreach ($storeOutDetails as $detail) {
	    	array_push($details, array($detail->id, $detail->ItemGroup->group_name, $detail->ItemSubGroup->sub_group_name, $detail->Parameter->parameter_name, $detail->Color->color_name, $detail->Item->item_name, $detail->Unit->unit_name));
	    }
	    $data = array(
			'storeOut' => $storeOut,
			'details' => $details
			);
		Excel::create('store_out_' . $data['storeOut']->challan_no . '_details', function($excel) use($data) {

		    $excel->setTitle('Store Out Challan No. ' . $data['storeOut']->challan_no);

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('store_out_' . $data['storeOut']->challan_no . '_details', function($sheet) use($data) {

	        $sheet->fromArray($data['details']);

	    });

		})->download('xls');

		return Redirect::to('storeOut');
	}

	public function sampleOrderDetail($id)
	{
		$sampleOrder = SampleOrder::find($id);
		$sampleOrderDetails = $sampleOrder->Details;
		$details = array();
		array_push($details, array('Sample Order No', 'Buyer', 'Order Date', 'Delivery Date'));
		array_push($details, array($sampleOrder->sample_order_no, $sampleOrder->Buyer->buyer_name, $sampleOrder->order_date, $sampleOrder->delivery_date));
		array_push($details, array('', '', '', ''));
		array_push($details, array('#', 'Article', 'Sample Type', 'Size', 'Color', 'Quantity'));		
		foreach ($sampleOrderDetails as $key => $detail) {
			$sizes = '';
			$count = 1;
			foreach ($detail->Sizes as $size) {
				$sizes = $sizes . $size->size_no;
				if (count($detail->Sizes) != $count) {
					$sizes = $sizes . ', ';
				}
				$count++;
			}
	    	array_push($details, array(($key + 1), $detail->Article->article_no, $detail->SampleType->sample_type, $sizes, $detail->Color->color_name, $detail->quantity));
	    }
	    $data = array(
			'sampleOrder' => $sampleOrder,
			'details' => $details
			);
		Excel::create('sample_order_' . $data['sampleOrder']->id . '_details', function($excel) use($data) {

		    $excel->setTitle('Sample Order No. ' . $data['sampleOrder']->id);

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('sample_order_' . $data['sampleOrder']->id . '_details', function($sheet) use($data) {

	        $sheet->fromArray($data['details']);

	    });

		})->download('xls');

		return Redirect::to('sampleOrder');
	}

	public function sampleSchedule()
	{
		$sampleSchedules = SampleSchedule::all();
		$schedules = array();

		array_push($schedules, array(
			'#', 
			'Order No', 
			'Article', 
			'Buyer', 
			'Color', 
			'Sizes', 
			'Qty/Size', 
			'Sample Type', 
			'Order Date', 
			'Customer Req. Delivery Date', 
			'Scheduled Delivery Date'
			)
		);
		foreach ($sampleSchedules as $key => $schedule) {
			$sizes = '';
			$count = 1;
			foreach ($schedule->SampleOrderDetail->Sizes as $size) {
				$sizes = $sizes . $size->size_no;
				if (count($schedule->SampleOrderDetail->Sizes) != $count) {
					$sizes = $sizes . ', ';
				}
				$count++;
			}
			if ($schedule != null) {
				$schedule_date = $schedule->schedule_date;
			}else{
				$schedule_date = $order->delivery_date;
			} 
	    	array_push($schedules, array(
	    		($key + 1), 
	    		$schedule->SampleOrderDetail->SampleOrder->sample_order_no, 
	    		$schedule->SampleOrderDetail->Article->article_no, 
	    		$schedule->SampleOrderDetail->SampleOrder->Buyer->buyer_name,
	    		$schedule->SampleOrderDetail->Color->color_name,
	    		$sizes,
	    		$schedule->SampleOrderDetail->quantity,
	    		$schedule->SampleOrderDetail->SampleType->sample_type,
	    		$schedule->SampleOrderDetail->SampleOrder->order_date,
	    		$schedule->SampleOrderDetail->SampleOrder->delivery_date,
	    		$schedule_date
	    		)
	    	);
	    }

		Excel::create('sample_schedules', function($excel) use($schedules) {

		    $excel->setTitle('Sample Schedule List');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('sample_schedules', function($sheet) use($schedules) {

	        $sheet->fromArray($schedules);

	    });

		})->download('xls');

		return Redirect::to('sampleSchedule');
	}

	public function sampleStatus()
	{
		$sampleStatuses = SampleStatus::all();
		$statuses = array();

		array_push($statuses, array(
			'#', 
			'Order No', 
			'Article', 
			'Order Date', 
			'Delivery Date', 
			'Pattern',
			'Test Sample', 
			'Grading', 
			'Material Status', 
			'Cutting', 
			'Closing',
			'Assembly',
			'QC Report',
			'Actual Delivery'
			)
		);
		foreach ($sampleStatuses as $key => $status) {
			if ($status->DevelopmentStatus->cutting != null) {
				$cutting = $status->DevelopmentStatus->cutting;
			}else{
				$cutting = 'None';
			}
			if ($status->DevelopmentStatus->closing != null) {
				$closing = $status->DevelopmentStatus->closing;
			}else{
				$closing = 'None';
			}
			if ($status->DevelopmentStatus->assembly != null) {
				$assembly = $status->DevelopmentStatus->assembly;
			}else{
				$assembly = 'None';
			}
			if ($status->DevelopmentStatus->qc_report != null) {
				$qc_report = $status->DevelopmentStatus->qc_report;
			}else{
				$qc_report = 'None';
			}
			if ($status->pattern != null) {
				$pattern = $status->pattern;
			}else{
				$pattern = 'None';
			}
			if ($status->test_sample != null) {
				$test_sample = $status->test_sample;
			}else{
				$test_sample = 'None';
			}
			if ($status->grading != null) {
				$grading = $status->grading;
			}else{
				$grading = 'None';
			}
			if ($status->material_status != null) {
				$material_status = $status->material_status;
			}else{
				$material_status = 'None';
			}
	    	array_push($statuses, array(
	    		($key + 1), 
	    		$status->SampleOrderDetail->SampleOrder->sample_order_no, 
	    		$status->SampleOrderDetail->Article->article_no, 
	    		$status->SampleOrderDetail->SampleOrder->order_date,
	    		$status->SampleOrderDetail->SampleOrder->delivery_date,
	    		$pattern,
	    		$test_sample,
	    		$grading,
	    		$material_status,
	    		$cutting,
	    		$closing,
	    		$assembly,
	    		$qc_report,
	    		$status->actual_delivery
	    		)
	    	);
	    }

		Excel::create('sample_statuses', function($excel) use($statuses) {

		    $excel->setTitle('Sample Status List');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('sample_statuses', function($sheet) use($statuses) {

	        $sheet->fromArray($statuses);

	    });

		})->download('xls');

		return Redirect::to('sampleStatus');
	}

	public function inventory()
	{
		$store_ins = StoreIn::all();
		$store_outs = StoreOut::all();
		$items = Item::all();
		$sorted_items = array();
		$store_in_items = array();
		$store_out_items = array();
		$remainder_items = array();
		$list = array();
		foreach ($items as $count => $item) {
			$store_in_count = 0;
			foreach ($store_ins as $store_in) {
				$store_in_details = $store_in->Details;
				foreach ($store_in_details as $store_in_detail) {
					$item_in_store_in = $store_in_detail->Item;
					if ($item->id == $item_in_store_in->id) {
						$store_in_count += $store_in_detail->quantity;
					}
				}
			}
			$store_in_items['item_'.$item->id] = $store_in_count;
			$store_out_count = 0;
			foreach ($store_outs as $store_out) {
				$store_out_details = $store_out->Details;
				foreach ($store_out_details as $store_out_detail) {
					$item_in_store_out = $store_out_detail->Item;
					if ($item->id == $item_in_store_out->id) {
						$store_out_count += $store_out_detail->quantity;
					}
				}
			}
			$store_out_items['item_'.$item->id] = $store_out_count;
			$remainder_items['item_'.$item->id] = $store_in_count - $store_out_count;
		}
		array_push($list, array(
			'#', 
			'Item', 
			'Group', 
			'Sub Group', 
			'Store Unit', 
			'Store Ins', 
			'Store Outs', 
			'Remainder'
			)
		);
		foreach ($items as $key => $item) {
			array_push($list, array(
				($key + 1),
				$item->item_name,
				$item->Group->group_name,
				$item->SubGroup->sub_group_name,
				$item->StoreUnit->unit_name,
				$store_in_items['item_'.$item->id],
				$store_out_items['item_'.$item->id],
				$remainder_items['item_'.$item->id]
				)
			);
		}

		Excel::create('inventory', function($excel) use($list) {

		    $excel->setTitle('Inventory List');

		    $excel->setCreator('Fortuna')
		          ->setCompany('www.bluebd.com');

		    
		    $excel->sheet('inventory', function($sheet) use($list) {

	        $sheet->fromArray($list);

	    });

		})->download('xls');

		return Redirect::to('inventory');
	}

}