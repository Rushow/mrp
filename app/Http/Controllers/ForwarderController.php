<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\Forwarder;
use Validator;
use View;

class ForwarderController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $forwarder = Forwarder::all();

        return view('forwarder.index')->with('forwarder', $forwarder);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('forwarder.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'forwarder_name'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('forwarder/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $forwarder = new Forwarder;

            $forwarder->forwarder_name       = $request->get('forwarder_name');

            $forwarder->save();
            Session::flash('message', 'Successfully created a forwarder!');
            return Redirect::to('forwarder');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['forwarder'] = Forwarder::find($id);

        return view('forwarder.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'forwarder_name'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('forwarder/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $forwarder['forwarder_name'] = $request->get('forwarder_name');
            $forwarder['updated_at'] = new DateTime;

            Forwarder::where('id','=',$id)->update($forwarder);

            Session::flash('message', 'Successfully updated a forwarder!');
            return Redirect::to('forwarder');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Forwarder::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a forwarder!');
        return Redirect::to('forwarder');
	}

}