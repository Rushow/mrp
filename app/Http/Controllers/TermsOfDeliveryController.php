<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subdivision;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Redirect;
use View;
use HTML;
use Validator;
use Session;
use Form;
use App\TermsOfDelivery;

class TermsOfDeliveryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $terms = TermsOfDelivery::all();

        return view('terms_of_delivery.index')->with('terms', $terms);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data = array();
        return view('terms_of_delivery.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'terms_of_delivery'     => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('termsOfDelivery/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $term = new TermsOfDelivery;

            $term->terms_of_delivery          = $request->get('terms_of_delivery');

            $term->save();
            Session::flash('message', 'Successfully created a terms of delivery!');
            return Redirect::to('termsOfDelivery');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['terms_of_delivery'] = TermsOfDelivery::find($id);

        return view('terms_of_delivery.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'terms_of_delivery'       => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('termsOfDelivery/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $term['terms_of_delivery'] = $request->get('terms_of_delivery');
            $term['updated_at'] = new DateTime;

            TermsOfDelivery::where('id','=',$id)->update($term);

            Session::flash('message', 'Successfully updated a terms of delivery!');
            return Redirect::to('termsOfDelivery');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        TermsOfDelivery::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a terms of delivery!');
        return Redirect::to('termsOfDelivery');
	}

}