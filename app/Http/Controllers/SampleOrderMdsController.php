<?php
namespace App\Http\Controllers;
use App\SampleOrder;
use App\Buyer;
use App\Article;
use App\Color;
use App\SampleType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\SampleOrderMds;
use Validator;

class SampleOrderMdsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public $layout = 'layouts.master';

    public function index(Request $request)
    {
        $filters = $request->only('sample_order_no','buyer_id','article_id', 'sample_type_id', 'order_date','delivery_date');

        $sample_order = $filters['sample_order_no'];
        $buyer = $filters['buyer_id'];
        $article = $filters['article_id'];
        $sample_type = $filters['sample_type_id'];
        $order_date = $filters['order_date'];
        $delivery_date = $filters['delivery_date'];

        $data['sample_order_no'] = array('' => 'Select Sample Order No') + SampleOrder::pluck('sample_order_no','id')->toArray();
        $data['buyer'] = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $data['article'] = array('' => 'Select Article No') + Article::pluck('article_no','id')->toArray();
        $data['sample_type'] = array('' => 'Select Type of Sample') + SampleType::pluck('sample_type','id')->toArray();
        $data['orders'] = SampleOrderMds::all();
        return view('sample_order_mds.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($id)
    {
        $data = array();
        $last_mds = $this->getLastMds();
        $last_id = (isset($last_mds)) ? $last_mds->id + 1 : 1;
        $last_id = sprintf("%04s", $last_id);        
        $order_details = SampleOrderDetail::find($id);
        $order = SampleOrder::find($order_details['sample_order_id']);
        if($order){
            $buyer = SampleOrder::find($order['id'])->buyer;
            $order['buyer_name'] = $buyer['buyer_name'];
        }
        if($order_details){
            $article        = SampleOrderDetail::find($order_details['id'])->Article;
            $sampleType     = SampleOrderDetail::find($order_details['id'])->SampleType;
            $size           = SampleOrderDetail::find($order_details['id'])->Sizes;
            $i = 0;
            $sizes = array();
            foreach($size as $s){
                $sizes[$i] = Size::find($s['size_id']);
                $i++;
            }
            $order_details['article_no']    = $article['article_no'];
            $order_details['article_pic']   = $article['article_pic'];
            $order_details['sample_type']   = $sampleType['sample_type'];
            $order_details['size']          = $sizes;

            $sample_spec = array();
            $sample_specs = SampleOrderSpec::where('sample_order_detail_id', '=', $id)->get();
            if($sample_specs){
                foreach($sample_specs as $spec){
                    $sample_spec['id']              = $spec['id'];
                    $sample_spec['spec_no']         = $spec['spec_no'];
                }
            }else{
                $spec_count = count(SampleOrderSpec::all());
                for ($i=0; $i < $spec_count; $i++) { 
                    $sample_spec['id']              = $i;
                    $sample_spec['spec_no']         = $i;
                }
            }
        }

        $data = array(
            'id' => $id,
            'last_no' => array('' => 'Select Last No') + LastNo::pluck('last_no','id')->toArray(),
            'outsole_no' => array('' => 'Select Outsole No') + OutsoleNo::pluck('outsole_no','id')->toArray(),
            'heel_no' => array('' => 'Select Heel No') + HeelNo::pluck('heel_no','id')->toArray(),
            'component' => array('' => 'Select Component') + Component::pluck('component_name','id')->toArray(),
            'item_group' => array('' => 'Select Group') + ItemGroup::pluck('group_name','id')->toArray(),
            'item_sub_group' => array('' => 'Select Sub Group') + ItemSubGroup::pluck('sub_group_name','id')->toArray(),
            'parameter' => array('' => 'Select Parameter') + Parameter::pluck('parameter_name','id')->toArray(),
            'item' => array('' => 'Select Material') + Item::pluck('item_name','id')->toArray(),
            'color' => array('' => 'Select Color') + Color::pluck('color_name_flcl','id')->toArray(),
            'size' => array('' => 'Select Size') + Size::pluck('size_no','id')->toArray(),
            'order_details' => $order_details,
            'sample_spec' => $sample_spec,
            'order' => $order,
            'mds_no' => 'FLCL/MDS/'.date('Y')."/".date('M')."/".$last_id
            );

        return view('sample_order_mds.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $mds_input = $request->only('sample_order_detail_id', 'mds_no', 'last_no_id', 'outsole_no_id', 'heel_no_id', 'logo_position','logo_type', 'print_type', 'print_color', 'note');
        $id = $request->get('id');
        // dd($id);
        $rules =array(
            'mds_no' => array('required'),
            'sample_order_detail_id' => array('required'),
            'last_no_id' => array('required'),
            'outsole_no_id' => array('required'),
            'heel_no_id' => array('required'),
            'logo_position' => array('required'),
            'logo_type' => array('required'),
            'print_type' => array('required'),
            'print_color' => array('required')
        );


        $validator = Validator::make($mds_input,$rules);
//        $spec_input['created_at'] = new DateTime();
        if ($validator->passes()){
            SampleOrderMds::insert($mds_input);
            $mds_item = $this->getLastMds();
            $mds_item_id = $mds_item->id;

            $sample_order_detail_id = $request->get('sample_order_detail_id');
            $size           = SampleOrderDetail::find($sample_order_detail_id)->Sizes;
            $i = 1;

            foreach($size as $s){

                $size_input['size_id'] = $request->get('size_id_'.$i);
                $size_input['quantity'] = $request->get('quantity_'.$i);
                $size_input['sample_order_mds_id'] = $mds_item_id;
//                $size_input['updated_at'] = new DateTime;

                SampleOrderMdsSize::insert($size_input);
                $i++;
            }


            $input = $request->only('total_row');
            $total_row = $input['total_row'];
            for($i=1; $i<=$total_row;$i++){
                if(Input::get('component_id_'.$i)){
                    $list_input = array(array('mds_id'=>$mds_item_id, 'component_id'=>$request->get('component_id_'.$i), 'group_id'=>$request->get('group_id_'.$i), 'sub_group_id'=>$request->get('sub_group_id_'.$i), 'parameter_id'=>$request->get('parameter_id_'.$i), 'item_id'=>$request->get('item_id_'.$i), 'color_id'=>$request->get('color_id_'.$i), 'area'=>$request->get('area_'.$i), 'wastage'=>$request->get('wastage_'.$i), 'consumption'=>$request->get('consumption_'.$i), 'note'=>$request->get('note_'.$i), 'created_at'=>new DateTime()));

                    SampleOrderMdsDesc::insert($list_input);

                }
            }


            $tds_input = $request->only('tds_total_row');
            $tds_total_row = $tds_input['tds_total_row'];
            for($i=1; $i<=$tds_total_row;$i++){
                if($request->get('component_id_'.$i)){
                    $tds_list_input = array(array('mds_id'=>$mds_item_id, 'particulars'=>$request->get('tds_particulars_'.$i), 'tolerance'=>$request->get('tds_tolerance_'.$i), 'remarks'=>$request->get('tds_remarks_'.$i), 'created_at'=>new DateTime()));

                    SampleOrderMdsTds::insert($tds_list_input);

                }
            }
            Session::flash('message', 'Successfully created a Sample Order MDS!');
            return Redirect::to('sampleOrderMds/'.$id);
        }
        else{
            return Redirect::to('sampleOrderMds/create/'.$id)->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $data = array();
        $sample_mds = SampleOrderMds::find($id);

        $mds_desc = SampleOrderMdsDesc::where('mds_id', '=', $id)->get();
        foreach($mds_desc as $desc){
            $component = SampleOrderMdsDesc::find($desc['id'])->component;
            $group = SampleOrderMdsDesc::find($desc['id'])->group;
            $sub_group = SampleOrderMdsDesc::find($desc['id'])->subGroup;
            $parameter = SampleOrderMdsDesc::find($desc['id'])->parameter;
            $item = SampleOrderMdsDesc::find($desc['id'])->item;
            $color = SampleOrderMdsDesc::find($desc['id'])->color;

            $desc['component_name'] = $component['component_name'];
            $desc['group_name'] = $group['group_name'];
            $desc['sub_group_name'] = $sub_group['sub_group_name'];
            $desc['parameter_name'] = $parameter['parameter_name'];
            $desc['item_name'] = $item['item_name'];
            $desc['color_name'] = $color['color_name_flcl'];
        }

        $tds = SampleOrderMdsTds::where('mds_id', '=', $id)->get();

        $order_details = SampleOrderDetail::find($sample_mds->sample_order_detail_id);
        $sample_spec = SampleOrderSpec::find($sample_mds->sample_order_detail_id);

        if($order_details){
            $article        = SampleOrderDetail::find($order_details['id'])->Article;
            $sampleType     = SampleOrderDetail::find($order_details['id'])->SampleType;
            $size           = SampleOrderDetail::find($order_details['id'])->Sizes;
            $i = 0;
            $sizes = array();
            foreach($size as $s){
                $sizes[$i] = Size::find($s['size_id']);
                $i++;
            }
            $order_details['article_no']    = $article['article_no'];
            $order_details['article_pic']   = $article['article_pic'];
            $order_details['sample_type']   = $sampleType['sample_type'];
            $order_details['size']          = $sizes;
        }
        $order = SampleOrder::find($order_details->sample_order_id);



        return view('sample_order_mds.show', $data)
                                    ->with('order_details', $order_details)
                                    ->with('order', $order)
                                    ->with('sample_spec', $sample_spec)
                                    ->with('sample_mds', $sample_mds)
                                    ->with('sample_tds', $tds)
                                    ->with('mds_desc', $mds_desc);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    function addMdsDescRow(Request $request){
        $info = $request->only('last_id');
        if($info){
            $last_id = $info['last_id'] + 1;
            $component      = array('' => 'Select Component') + Component::pluck('component_name','id')->toArray();
            $item_group     = array('' => 'Select Group') + ItemGroup::pluck('group_name','id')->toArray();
            $item_sub_group = array('' => 'Select Sub Group') + ItemSubGroup::pluck('sub_group_name','id')->toArray();
            $parameter      = array('' => 'Select Parameter') + Parameter::pluck('parameter_name','id')->toArray();
            $item           = array('' => 'Select Material') + Item::pluck('item_name','id')->toArray();
            $color          = array('' => 'Select Color') + Color::pluck('color_name','id')->toArray();


            $msg = "Are you sure to delete this item???";

            $html = "<td>".Form::select('component_id_'.$last_id, $component, '', $attributes = array('id'=>'component_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::select('group_id_'.$last_id, $item_group, '', $attributes = array('id'=>'group_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required', 'onChange'=>'generateSubGroup(this);'))."</td>";
            $html .= "<td>".Form::select('sub_group_id_'.$last_id, $item_sub_group, '', $attributes = array('id'=>'sub_group_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required', 'disabled'=>'disabled', 'onChange'=>'generateItem(this);'))."</td>";
            $html .= "<td>".Form::select('parameter_id_'.$last_id, $parameter, '', $attributes = array('id'=>'parameter_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::select('item_id_'.$last_id, $item, '', $attributes = array('id'=>'item_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required', 'disabled'=>'disabled'))."</td>";
            $html .= "<td>".Form::select('color_id_'.$last_id, $color, '', $attributes = array('id'=>'color_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::text('area_'.$last_id, '', $attributes = array('class'=>'form-control Required','id'=>'area_'.$last_id))."</td>";
            $html .= "<td>".Form::text('wastage_'.$last_id, '', $attributes = array('class'=>'form-control Required','id'=>'wastage_'.$last_id))."</td>";
            $html .= "<td>".Form::text('consumption_'.$last_id, '', $attributes = array('class'=>'form-control Required','id'=>'consumption_'.$last_id))."</td>";
            $html .= "<td>".Form::text('note_'.$last_id, '', $attributes = array('class'=>'form-control','id'=>'note_'.$last_id))."</td>";
            $html .= "<td><a href='#' onclick='return checkDelete(this);'><span class='badge badge-important'>X</span></a></td>";

            $data = array();
            $data['error'] = false;
            $data['html'] = $html;
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

    function addTdsRow(Request $request){
        $info = $request->only('last_id');
        if($info){
            $last_id = $info['last_id'] + 1;

            $msg = "Are you sure to delete this item???";

            $html = "<td>".Form::text('tds_particulars_'.$last_id, '', $attributes = array('class'=>'form-control Required','id'=>'tds_particulars_'.$last_id))."</td>";
            $html .= "<td>".Form::text('tds_tolerance_'.$last_id, '', $attributes = array('class'=>'form-control Required','id'=>'tds_tolerance_'.$last_id))."</td>";
            $html .= "<td>".Form::text('tds_remarks_'.$last_id, '', $attributes = array('class'=>'form-control Required','id'=>'tds_remarks_'.$last_id))."</td>";
            $html .= "<td><a href='#' onclick='return checkDelete(this);'><span class='badge badge-important'>X</span></a></td>";

            $data = array();
            $data['error'] = false;
            $data['html'] = $html;
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

    private function getLastMds()
    {
        return DB::table('sample_order_mds')->orderBy('id', 'desc')->first();
    }

    private function getLastSpecMaterial()
    {
        return DB::table('sample_order_spec_material')->orderBy('id', 'desc')->first();
    }

}