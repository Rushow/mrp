<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\SampleType;
use Validator;
use View;

class SampleTypeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $sampleType = SampleType::all();

        return view('sample_type.index')->with('sampleType', $sampleType);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('sample_type.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'sample_type'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('sampleType/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $sampleType = new SampleType;

            $sampleType->sample_type       = $request->get('sample_type');

            $sampleType->save();
            Session::flash('message', 'Successfully created a sample type!');
            return Redirect::to('sampleType');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['sampleType'] = SampleType::find($id);

        return view('sample_type.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'sample_type'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('sampleType/edit/'.$id)->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $sampleType['sample_type'] = $request->get('sample_type');
            $sampleType['updated_at'] = new DateTime;

            SampleType::where('id','=',$id)->update($sampleType);

            Session::flash('message', 'Successfully updated a sample type!');
            return Redirect::to('sampleType');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        SampleType::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a sample type!');
        return Redirect::to('sampleType');
	}

}