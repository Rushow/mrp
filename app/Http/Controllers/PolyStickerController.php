<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\PolySticker;
use Validator;
use View;

class PolyStickerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $linings = PolySticker::all();

        return view('poly_sticker.index')->with('linings', $linings);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data = array();
        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');
        return view('poly_sticker.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'poly_sticker'     => 'required',
            'status'           => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('polySticker/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type = new PolySticker;

            $type->poly_sticker       = $request->get('poly_sticker');
            $type->status             = $request->get('status');
            $type->enable_date        = strtotime($request->get('enable_date'));

            $type->save();
            Session::flash('message', 'Successfully created a polysticker!');
            return Redirect::to('polySticker');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['poly_sticker'] = PolySticker::find($id);

        $data['status'] = array(''=>'Select One', '0'=>'Disable', '1'=>'Enable');

        $this->layout->content = View::make('poly_sticker.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'poly_sticker'       => 'required',
            'status'             => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('polySticker/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $type['poly_sticker'] = $request->get('poly_sticker');
            $type['status']       = $request->get('status');
            $type['enable_date']  = strtotime($request->get('enable_date'));
            $type['updated_at'] = new DateTime;

            PolySticker::where('id','=',$id)->update($type);

            Session::flash('message', 'Successfully updated a poly sticker!');
            return Redirect::to('polySticker');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        PolySticker::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a poly sticker!');
        return Redirect::to('polySticker');
	}

}