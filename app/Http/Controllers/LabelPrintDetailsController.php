<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DateTime;
use Session;
use App\LabelPrintDetails;
use App\ItemSubGroup;
use Validator;

class LabelPrintDetailsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $label_print_details = LabelPrintDetails::all();

        $this->layout->content = View::make('label_print_details.index')->with('label_print_details', $label_print_details);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $this->layout->content = View::make('label_print_details.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = array(
            'print_code' => 'required',
            'type'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()){
            return Redirect::to('labelPrintDetails/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $label_print_details = new LabelPrintDetails;

            $label_print_details->print_code = Input::get('print_code');
            $label_print_details->type       = Input::get('type');
            $label_print_details->remarks    = Input::get('remarks');
            $label_print_details->created_at = new DateTime();

            $label_print_details->save();
            Session::flash('message', 'Successfully created a Label & Print Details!');
            return Redirect::to('labelPrintDetails');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['label_print_details'] = LabelPrintDetails::find($id);

        $this->layout->content = View::make('label_print_details.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = array(
            'print_code' => 'required',
            'type'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()){
            return Redirect::to('labelPrintDetails/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $label_print_details['print_code']  = Input::get('print_code');
            $label_print_details['type']        = Input::get('type');
            $label_print_details['remarks']     = Input::get('remarks');
            $label_print_details['updated_at']  = new DateTime;

            LabelPrintDetails::where('id','=',$id)->update($label_print_details);

            Session::flash('message', 'Successfully updated a Label & Print Details!');
            return Redirect::to('labelPrintDetails');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        LabelPrintDetails::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a Label & Print Details!');
        return Redirect::to('labelPrintDetails');
	}

}