<?php
namespace App\Http\Controllers;


use App\Division;
use App\ProductionOrder;
use App\Buyer;
use App\Status;
use App\Destination;
use App\Article;
use App\Color;
use App\Season;
use App\ProductionOrderDespo;
use App\ProductionOrderArticle;
use App\ProductionOrderColor;
use App\ProductionOrderDestination;
use App\ProductionOrderDetail;
use App\PaymentTerm;
use App\Currency;
use App\ShippingMode;
use App\Bank;
use App\TermsOfDelivery;
use App\Forwarder;
use App\ProductionOrderDetailTod;
use App\SizeGroup;
use App\Size;
use App\ProductionOrderDetailPayment;
use App\SizeGroupSize;
use App\ProductionOrderDetailSize;
use App\ProductionOrderDetailShipment;
use App\ProductionOrderDetailShippingBlock;
use App\ProductionOrderDetailShippingBlockDestination;


use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use View;
use HTML;
use App\Classes\Helpers;
use Validator;
use DateTime;
use Session;
use DB;
use PDF;


class ProductionOrderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public $layout = 'layouts.master';

    public function index()
    {

        $orders = ProductionOrder::all();

        $data['buyer']              = Buyer::pluck('buyer_name','id')->toArray();
        $data['production_order']   = ProductionOrder::pluck('order_no','id')->toArray();
        $data['article']            = Article::pluck('article_no','id')->toArray();
        $data['color']              = Color::pluck('color_name_flcl','id')->toArray();
        $data['season']             = Season::pluck('season','id')->toArray();
        $data['destination']        = Destination::pluck('destination_name','id')->toArray();
        $data['despo']              = ProductionOrderDespo::pluck('despo','id')->toArray();

//        foreach($orders as $order){
//            $buyer_template = Buyer::find($order['buyer_id'])->buyerTemplate->buyer_template;
            // create article array
//            $articles = ProductionOrderArticle::where('production_order_id', '=', $order['id'])->get();
//            $i = 0;
//            $order['articles'] = array();
//            foreach($articles as $article){
//                $a[$i++] =  $article['article_id'];
//                $order['articles'] = $a;
//            }
//            // create color array
//            $colors = ProductionOrderColor::where('production_order_id', '=', $order['id'])->get();
//            $i = 0;
//            $order['colors'] = array();
//            foreach($colors as $color){
//                $c[$i++] =  $color['color_id'];
//                $order['colors'] = $c;
//            }

//            if($buyer_template == "Template 01"){
                // create Destination array
//                $destinations = ProductionOrderDestination::where('production_order_id', '=', $order['id'])->get();
//                $i = 0;
//                $order['destinations'] = array();
//                foreach($destinations as $destination){
//                    $des[$i++] =  $destination['destination_id'];
//                    $order['destinations'] = $des;
//                }
//            }
//            else if($buyer_template == "Template 02"){
                // create Despo array
//                $despos = ProductionOrderDespo::where('production_order_id', '=', $order['id'])->get();
//                $i = 0;
//                $order['despos'] = array();
//                foreach($despos as $despo){
//                    $dsp[$i++] =  $despo['despo'];
//                    $order['despos'] = $dsp;
//                }
//            }
//        }
//        echo '<pre>';
//            print_r($orders);
//            die;


        return view('production_order.index', $data)->with('orders', $orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
//       
        $data['buyer']          = array(0 => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();//array('' => 'Select Buyer') +
        $data['season']         = array(0 => 'Select Season') + Season::pluck('season','id')->toArray();
        $data['status']         = array(0 => 'Select Order Status') + Status::pluck('status','id')->toArray();
        $data['article']        = Article::pluck('article_no','id')->toArray();
        $data['destination']    = Destination::select(DB::raw("CONCAT(destination_code,'-',destination_name) AS name"),'id')->pluck("name","id")->toArray();
        $data['color']          = Color::all()->pluck('color_detail_for_list','id')->toArray();
//
//        $block_tt_order_from = array(''=>'Select Block TT Ordering Year From');
//        $block_tt_order_to = array(''=>'Select Block TT Ordering Year To');
//        for($i = 2000; $i <= 2050; $i++){
//            $block_tt_order_from[$i] = $i;
//            $block_tt_order_to[$i] = $i;
//        }
//        $data['block_tt_order_from']   = $block_tt_order_from;
//        $data['block_tt_order_to']     = $block_tt_order_to;

//        echo "<pre>";
//        print_r($data['buyer']);die;

        //$this->layout->content = View::make('production_order.create', $data);
        return view('production_order.create', $data);

        //return view('production_order.create', ['buyer' =>  $data['buyer'],'season'=>$data['season'],'status'=>$data['status'],'article'=>$data['article'],'destination'=>$data['destination'],'color'=>$data['color'] ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $order_input = $request->only('buyer_id', 'order_no', 'order_receive_date','season_id', 'status_id', 'remarks');

        //$order_input['order_receive_date'] = strtotime($request->get('order_receive_date'));
        $rules =array(
            'order_no' => array('required'),
            'buyer_id' => array('required'),
            'season_id' => array('required'),
            'status_id' => array('required'),
            'order_receive_date' => array('required')
        );
        $buyer_template = Buyer::find($request->get('buyer_id'))->buyerTemplate->buyer_template;
        $validator = Validator::make($order_input,$rules);
        $order_input['created_at'] = new DateTime();
        if ($validator->passes()){
            ProductionOrder::insert($order_input);
            $production_order_item = $this->getLastOrder();
            $production_order_id = $production_order_item->id;

            if(count($request->get('article_id'))){
                foreach($request->get('article_id') as $article){
                    $article_input = array(array('production_order_id'=>$production_order_id, 'article_id'=>$article, 'created_at'=>new DateTime()));
                    ProductionOrderArticle::insert($article_input);
                }
            }

            if(count($request->get('color_id'))){
                foreach($request->get('color_id') as $color){
                    $color_input = array(array('production_order_id'=>$production_order_id, 'color_id'=>$color, 'created_at'=>new DateTime()));
                    ProductionOrderColor::insert($color_input);
                }
            }

            if($buyer_template == "Template 01"){
                if(count($request->get('destination_id'))){
                    foreach($request->get('destination_id') as $destination){
                        $destination_input = array(array('production_order_id'=>$production_order_id, 'destination_id'=>$destination, 'created_at'=>new DateTime()));
                        ProductionOrderDestination::insert($destination_input);
                    }
                }
            }
            else if($buyer_template == "Template 02"){
                $despos = explode(',', $request->get('despo'));
                foreach($despos as $despo){
                    $despo_input = array(array('production_order_id'=>$production_order_id, 'despo'=>$despo, 'created_at'=>new DateTime()));
                    ProductionOrderDespo::insert($despo_input);
                }
            }

            Session::flash('message', 'Successfully created a Productions Order!');
            return Redirect::to('productionOrder');
        }
        else{
            return Redirect::to('productionOrder/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $data = array();
        $order_details = ProductionOrderDetail::where('production_order_id', '=', $id)->get();
        $order = ProductionOrder::find($id);

        if($order){
            $buyer = ProductionOrder::find($order['id'])->buyer;
            $order['buyer_name'] = $buyer['buyer_name'];
        }

        foreach($order_details as $item){
            $production_order = ProductionOrderDetail::find($item['id'])->productionOrder;
            $article = ProductionOrderDetail::find($item['id'])->article;
            $sample_type = ProductionOrderDetail::find($item['id'])->sampleType;
            $color = ProductionOrderDetail::find($item['id'])->color;
            $size = ProductionOrderDetail::find($item['id'])->size;
            $i = 0;
            $sizes = array();
            foreach($size as $s){
                $sizes[$i] = Size::find($s['size_id']);
                $i++;
            }
            $item['sample_order_no'] = $production_order['production_order_no'];
            $item['article_name'] = $article['article_no'];
            $item['sample_type'] = $sample_type['sample_type'];
            $item['color_name'] = $color['color_name_flcl'];
            $item['size'] = $sizes;
        }

        //$this->layout->content = View::make('production_order.show', $data)->with('order_details', $order_details)->with('order', $order);

         
        return view('production_order.show', $data)->with('order_details', $order_details)->with('order', $order);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = array();
        $order = ProductionOrder::find($id);

        $data['buyer']          = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $data['season']         = array('' => 'Select Season') + Season::pluck('season','id')->toArray();
        $data['status']         = array('' => 'Select Order Status') + Status::pluck('status','id')->toArray();
        $data['article']        = Article::pluck('article_no','id')->toArray();
        $data['destination']    = Destination::pluck('destination_name','id')->toArray();
        $data['color']          = Color::all()->pluck('color_detail_for_list','id')->toArray();

//        $block_tt_order_from = array(''=>'Select Block TT Ordering Year From');
//        $block_tt_order_to = array(''=>'Select Block TT Ordering Year To');
//
//        for($i = 2000; $i <= 2050; $i++){
//            $block_tt_order_from[$i] = $i;
//            $block_tt_order_to[$i] = $i;
//        }
//        $data['block_tt_order_from']   = $block_tt_order_from;
//        $data['block_tt_order_to']     = $block_tt_order_to;
        $buyer_template = Buyer::find($order->buyer_id)->buyerTemplate->buyer_template;

        // create article array
        $articles = ProductionOrderArticle::where('production_order_id', '=', $id)->get();
        $i = 0;
        $order['articles'] = array();
        foreach($articles as $article){
            $a[$i++] =  $article['article_id'];
            $order['articles'] = $a;
        }
        // create color array
        $colors = ProductionOrderColor::where('production_order_id', '=', $id)->get();
        $i = 0;
        $order['colors'] = array();
        foreach($colors as $color){
            $c[$i++] =  $color['color_id'];
            $order['colors'] = $c;
        }

        if($buyer_template == "Template 01"){
            // create Destination array
            $destinations = ProductionOrderDestination::where('production_order_id', '=', $id)->get();
            $i = 0;
            $order['destinations'] = array();
            foreach($destinations as $destination){
                $des[$i++] =  $destination['destination_id'];
                $order['destinations'] = $des;
            }
        }
        else if($buyer_template == "Template 02"){
            // create Despo array
            $despos = ProductionOrderDespo::where('production_order_id', '=', $id)->get();
            $i = 0;
            $order['despos'] = '';
            foreach($despos as $despo){
                $order['despos'] = $order['despos'].','.$despo->despo;
            }
        }

        return view('production_order.edit', $data)->with('order', $order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $order_input = $request->only('buyer_id', 'order_no', 'order_receive_date','season_id', 'status_id', 'remarks');
//        echo "<pre>";
//        print_r($order_input);
       // echo $request->input('order_receive_date');die;
        //$order_input['order_receive_date'] = strtotime($request->input('order_receive_date'));
        $rules =array(
            'order_no' => array('required'),
            'buyer_id' => array('required'),
            'season_id' => array('required'),
            'order_receive_date' => array('required')
        );
        $buyer_template = Buyer::find($request->input('buyer_id'))->buyerTemplate->buyer_template;

        $validator = Validator::make($order_input,$rules);
        $order_input['updated_at'] = new DateTime();
        if ($validator->passes()){
            ProductionOrder::where('id','=',$id)->update($order_input);

            ProductionOrderArticle::where('production_order_id', '=', $id)->delete();
            if(count($request->input('article_id'))){
                foreach($request->input('article_id') as $article){
                    $article_input = array(array('production_order_id'=>$id, 'article_id'=>$article, 'created_at'=>new DateTime()));
                    ProductionOrderArticle::insert($article_input);
                }
            }

            ProductionOrderColor::where('production_order_id', '=', $id)->delete();
            if(count($request->input('color_id'))){
                foreach($request->input('color_id') as $color){
                    $color_input = array(array('production_order_id'=>$id, 'color_id'=>$color, 'created_at'=>new DateTime()));
                    ProductionOrderColor::insert($color_input);
                }
            }

            if($buyer_template == "Template 01"){
                ProductionOrderDestination::where('production_order_id', '=', $id)->delete();
                if(count($request->input('destination_id'))){
                    foreach($request->input('destination_id') as $destination){
                        $destination_input = array(array('production_order_id'=>$id, 'destination_id'=>$destination, 'created_at'=>new DateTime()));
                        ProductionOrderDestination::insert($destination_input);
                    }
                }
            }
            else if($buyer_template == "Template 02"){
                ProductionOrderDespo::where('production_order_id', '=', $id)->delete();
                $despos = explode(',', $request->input('despo'));
                foreach($despos as $despo){
                    $despo_input = array(array('production_order_id'=>$id, 'despo'=>$despo, 'created_at'=>new DateTime()));
                    ProductionOrderDespo::insert($despo_input);
                }
            }

            Session::flash('message', 'Successfully updated a Production Order!');
            return Redirect::to('productionOrder');
        }
        else{
            return Redirect::to('productionOrder/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        echo "del";
    }

    private function getLastOrder()
    {
        return DB::table('production_order')->orderBy('id', 'desc')->first();
    }

    private function getLastOrderDetails()
    {
        return DB::table('production_order_details')->orderBy('id', 'desc')->first();
    }

    function addPOBlock(Request $request){
        $info = $request->only('last_id');
        if($info){
            $last_id = $info['last_id'] + 1;
            $article = array('' => 'Select Article') + Article::pluck('article_no','id');
            $size_group = array('' => 'Select Size Group') + SizeGroup::pluck('size_group_name','id');
            $color = array('' => 'Select Color') + Color::pluck('color_name_flcl','id');

            $msg = "Are you sure to delete this item???";


            $html = "<a href='#' onclick='return checkPODelete(this);'><span class='badge badge-important'>X</span></a><div><p class='title'>Article Details</p><table><thead><td class='col-md-2'>Article No</td><td class='col-md-2'>Article Ref</td><td class='col-md-2'>Article Color</td><td class='col-md-2'>Unit Price</td><td class='col-md-2'>Note</td></thead><tbody>";
            $html .= "<td>".Form::select('article_id_'.$last_id, $article, '', $attributes = array('id'=>'article_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::select('article_color_id_'.$last_id, $color, '', $attributes = array('id'=>'article_color_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::text('article_price_'.$last_id, '',array('class'=>'form-control Required','id'=>'article_price_'.$last_id))."</td>";
            $html .= "<td>".Form::text('article_note_'.$last_id, '',array('class'=>'form-control Required','id'=>'article_note_'.$last_id))."</td>";
            $html .= "</tbody></table><p class='title'>Size & Quantity Details</p><table><thead><td class='col-md-2'>Size Range</td><td class='col-md-2'>Total Quantity</td><td class='col-md-2'>Total Value</td></thead><tbody>";
            $html .= "<td>".Form::select('size_group_id_'.$last_id, $size_group, '', $attributes = array('id'=>'size_group_id_'.$last_id,'data-rel'=>'chosen', 'class'=>'form-control Required'))."</td>";
            $html .= "<td>".Form::text('size_quantity_'.$last_id, '',array('class'=>'form-control Required','id'=>'size_quantity_'.$last_id))."</td>";
            $html .= "<td>".Form::text('size_total_value_'.$last_id, '',array('class'=>'form-control Required','id'=>'size_total_value_'.$last_id))."</td>";
            $html .= "</tbody></table></div>";

            $data = array();
            $data['error'] = false;
            $data['html'] = $html;
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

    public function details($id)
    {
        $data = array();
        $order = ProductionOrder::find($id);
        $order_tod_details      = $order->TodDetails;echo count($order_tod_details);
        $order_payment_details  = $order->PaymentDetails;
        $order_shipment_details = $order->ShipmentDetails;
        //$order_shipping_blocks  = $order->ShippingBlocks;


        $data['buyer']              = array('' => 'Select Buyer') + Buyer::pluck('buyer_name','id')->toArray();
        $data['season']             = array('' => 'Select Season') + Season::pluck('season','id')->toArray();
        $data['status']             = array('' => 'Select Order Status') + Status::pluck('status','id')->toArray();
        $data['payment_term']       = array('' => 'Select Payment Term') + PaymentTerm::pluck('term','id')->toArray();
        $data['currency']           = array('' => 'Select Currency') + Currency::pluck('name','id')->toArray();
        $data['shipping_mode']      = array('' => 'Select Shipping Mode') + ShippingMode::pluck('shipping_mode','id')->toArray();
        $data['consignee_bank']     = array('' => 'Select Consignee Bank') + Bank::pluck('name','id')->toArray();
        $data['shipper_bank']       = array('' => 'Select Shipper Bank') + Bank::pluck('name','id')->toArray();
        $data['terms_of_delivery']  = array('' => 'Select Terms of Delivery') + TermsOfDelivery::pluck('terms_of_delivery','id')->toArray();
        $data['forwarder']          = array('' => 'Select Forwarder') + Forwarder::pluck('forwarder_name','id')->toArray();
        $data['article']            = Article::pluck('article_no','id')->toArray();
        $data['destination']        = Destination::pluck('destination_name','id')->toArray();
        $data['color']              = Color::all()->pluck('color_detail_for_list','id')->toArray();

        $order['destination_list'] = array('' => 'Select Destination');


        $buyer_template = Buyer::find($order['buyer_id'])->buyerTemplate->buyer_template;

        // create article array
        $articles = ProductionOrderArticle::where('production_order_id', '=', $id)->get();
        $i = 0;
        $order['articles'] = array();
        $order['article_list'] = array('' => 'Select Article');
        foreach($articles as $article){
            $a[$i++] =  $article['article_id'];
            $order['articles'] = $a;
            $order['article_list'] = $order['article_list'] + array($article['article_id'] => Helpers::getArticleNoById($article['article_id']));
        }
        // create color array
        $colors = ProductionOrderColor::where('production_order_id', '=', $id)->get();
        $i = 0;
        $order['colors'] = array();
        $order['color_list'] = array('' => 'Select Color');
        foreach($colors as $color){
            $c[$i++] =  $color['color_id'];
            $order['colors'] = $c;
            $order['color_list'] = $order['color_list'] + array($color['color_id'] => Helpers::getColorNameById($color['color_id']));
        }

        if($buyer_template == "Template 01"){
            // create Destination array
            $destinations = ProductionOrderDestination::where('production_order_id', '=', $id)->get();
            $i = 0;
            $order['destinations'] = array();
            foreach($destinations as $destination){
                $des[$i++] =  $destination['destination_id'];
                $order['destinations'] = $des;
                $order['destination_list'] = $order['destination_list'] + array($destination['destination_id'] => Helpers::getDestinationNameById($destination['destination_id']));
            }
        }
        else if($buyer_template == "Template 02"){
            // create Despo array
            $despos = ProductionOrderDespo::where('production_order_id', '=', $id)->get();
            $order['despos'] = '';
            foreach($despos as $despo){
                $order['despos'] = $order['despos'].','.$despo->despo;
            }
        }

        $tod_details = ProductionOrder::find($id)->todDetails;
        $invoice_value = 0;
        if($tod_details){
            foreach($tod_details as $tod){
                $total = $tod->total_quantity * $tod->unit_value;
                $invoice_value = $invoice_value + $total;
            }
        }
        $order['invoice_value'] = $invoice_value;

         return view('production_order.details',$data)
            ->with('order', $order)
            ->with('order_tod_details', $order_tod_details)
            ->with('order_payment_details', $order_payment_details)
            ->with('order_shipment_details', $order_shipment_details)
            //->with('order_shipping_blocks', $order_shipping_blocks)
            ->with('id',$id);
    }

    public function todDetails(Request $request)
    {

        $data = array();
        $order_id = $request->input('id');
        $id = $request->input('id');//$order_id['id'];


        $data['tod_block_no'] = $request->input('tod_block_no');
        if($request->input('tod_id') > 0){
            $tod_id = $request->input('tod_id');

            $tod_details = ProductionOrderDetailTod::find($tod_id);
        }
        $order = ProductionOrder::find($id);

        $order['destination_list'] = array('' => 'Select Destination');
        $order['despo_list'] = array('' => 'Select Dispo');
        $order['shipping_mode_list']=array('' => 'Select Shipping Mode') + ShippingMode::pluck('shipping_mode','id')->toArray();

        $buyer_template = Buyer::find($order['buyer_id'])->buyerTemplate->buyer_template;

        // create article array
        $articles = ProductionOrderArticle::where('production_order_id', '=', $id)->get();
        $i = 0;
        $order['articles'] = array();
        $order['article_list'] = array('' => 'Select Article');
        foreach($articles as $article){
            $a[$i++] =  $article['article_id'];
            $order['articles'] = $a;
            $order['article_list'] = $order['article_list'] + array($article['article_id'] => Helpers::getArticleNoById($article['article_id']));
        }
        // create color array
        $colors = ProductionOrderColor::where('production_order_id', '=', $id)->get();
        $i = 0;
        $order['colors'] = array();
        $order['color_list'] = array('' => 'Select Color');
        foreach($colors as $color){
            $c[$i++] =  $color['color_id'];
            $order['colors'] = $c;
            $order['color_list'] = $order['color_list'] + array($color['color_id'] => Helpers::getColorNameById($color['color_id']));
        }

        if($buyer_template == "Template 01"){
            // create Destination array
            $destinations = ProductionOrderDestination::where('production_order_id', '=', $id)->get();
            $i = 0;
            $order['destinations'] = array();
            foreach($destinations as $destination){
                $des[$i++] =  $destination['destination_id'];
                $order['destinations'] = $des;
                $order['destination_list'] = $order['destination_list'] + array($destination['destination_id'] => Helpers::getDestinationNameById($destination['destination_id']));
            }
        }
        else if($buyer_template == "Template 02"){
            // create Despo array
            $despos = ProductionOrderDespo::where('production_order_id', '=', $id)->get();
            $order['despos'] = '';
            foreach($despos as $despo){
                $order['despos'] = $order['despos'].','.$despo->despo;
                $order['despo_list'] = $order['despo_list'] + array($despo['despo'] => $despo->despo);
            }
        }

        if($request->input('tod_id') > 0) {
            return View::make('production_order.todDetails', $data)->with('order', $order)->with('tod_details', $tod_details);
        }
        else{
            $tod_details = array();
            return View::make('production_order.todDetails', $data)->with('order', $order)->with('tod_details', $tod_details);
        }
    }

    function todDetailsStore(Request $request)
    {
        $production_order_id = $request->input('order_id');
        $buyer_template = $request->input('buyer_template');
        $created_at = new DateTime();

        $total_block = $request->input('total_tod_block');
        for($i=1; $i<=$total_block; $i++){
            if($request->input('article_id_'.$i) || $request->input('color_id_'.$i) || $request->input('tod_'.$i) || $request->input('days_ship_advance_'.$i)){
                if($buyer_template == "Template 01"){
                    $tod_input = array(
                        'production_order_id'=>$production_order_id,
                        'article_id'=>($request->input('article_id_'.$i))?$request->input('article_id_'.$i):NULL,
                        'color_id'=>($request->input('color_id_'.$i))?$request->input('color_id_'.$i):NULL,
                        'destination_id'=>($request->input('destination_id_'.$i))?$request->input('destination_id_'.$i):NULL,
                        'shipping_mode_id'=>($request->input('shipping_mode_id_'.$i))?$request->input('shipping_mode_id_'.$i):0,
                        'tod'=>$request->input('tod_'.$i),
                        'days_ship_advance'=>($request->input('days_ship_advance_'.$i))?$request->input('days_ship_advance_'.$i):NULL,
                        'remarks'=>$request->input('remarks_'.$i),
                        'created_at'=>$created_at
                    );
                    $tod_detail_id = $request->input('tod_detail_id_'.$i);
                    if($tod_detail_id){
                        ProductionOrderDetailTod::where('id','=',$tod_detail_id)->update($tod_input);
                    }
                    else{
                        ProductionOrderDetailTod::insert($tod_input);
                    }
                }
                else if($buyer_template == "Template 02"){
                    $tod_input = array(
                        'production_order_id'=>$production_order_id,
                        'article_id'=>$request->input('article_id_'.$i),
                        'color_id'=>$request->input('color_id_'.$i),
                        'despo'=>$request->input('despo_'.$i),
                        'shipping_mode_id'=>($request->input('shipping_mode_id_'.$i))?$request->input('shipping_mode_id_'.$i):0,
                        'pos'=>$request->input('pos_'.$i),
                        'tod'=>$request->input('tod_'.$i),
                        'days_ship_advance'=>($request->input('days_ship_advance_'.$i))?$request->input('days_ship_advance_'.$i):NULL,
                        'remarks'=>$request->input('remarks_'.$i),
                        'created_at'=>$created_at
                    );
                    $tod_detail_id = $request->input('tod_detail_id_'.$i);
                    if($tod_detail_id){
                        ProductionOrderDetailTod::where('id','=',$tod_detail_id)->update($tod_input);
                    }
                    else{
                        ProductionOrderDetailTod::insert($tod_input);
                    }
                }
            }
        }

        Session::flash('message', 'Successfully created Production Order TOD Detail!');
        return Redirect::to('productionOrder/details/'.$production_order_id);
    }

    public function removeTodDetails(Request $request){
        $data = array();
        $tod_detail_id = $request->input('tod_detail_id');
        if($tod_detail_id){
            ProductionOrderDetailTod::where('id', '=', $tod_detail_id)->delete();

            $data['error'] = false;
            echo json_encode($data);
            die;
        }
        else{
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

    public function removeShippingBlock(Request $request){
        $data = array();
        $shipping_block_id = $request->input('shipping_block_id');
        if($shipping_block_id){
            ProductionOrderDetailShippingBlock::where('id', '=', $shipping_block_id)->delete();

            $data['error'] = false;
            echo json_encode($data);
            die;
        }
        else{
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

    public function quantityDetails(Request $request)
    {

        $data = array();
        $order_id = $request->input('id');
        $id = $order_id;//$order_id['id'];
        $data['size_group']   = array('' => 'Select Size Group') + SizeGroup::pluck('size_group_name','id')->toArray();
        $data['quantity_block_no'] = $request->input('quantity_block_no');
        if($request->input('quantity_id') > 0){
            $quantity_id = $request->input('quantity_id');

            $quantity_details = ProductionOrderDetailTod::find($quantity_id);
        }
        $order = ProductionOrder::find($id);

        $order['destination_list'] = array('' => 'Select Destination');
        $order['despo_list'] = array('' => 'Select Dispo');

        $buyer_template = Buyer::find($order['buyer_id'])->buyerTemplate->buyer_template;

        // create article array
        $articles = ProductionOrderArticle::where('production_order_id', '=', $id)->get();
        $i = 0;
        $order['articles'] = array();
        $order['article_list'] = array('' => 'Select Article');
        foreach($articles as $article){
            $a[$i++] =  $article['article_id'];
            $order['articles'] = $a;
            $order['article_list'] = $order['article_list'] + array($article['article_id'] => Helpers::getArticleNoById($article['article_id']));
        }
        // create color array
        $colors = ProductionOrderColor::where('production_order_id', '=', $id)->get();
        $i = 0;
        $order['colors'] = array();
        $order['color_list'] = array('' => 'Select Color');
        foreach($colors as $color){
            $c[$i++] =  $color['color_id'];
            $order['colors'] = $c;
            $order['color_list'] = $order['color_list'] + array($color['color_id'] => Helpers::getColorNameById($color['color_id']));
        }

        if($buyer_template == "Template 01"){
            // create Destination array
            $destinations = ProductionOrderDestination::where('production_order_id', '=', $id)->get();
            $i = 0;
            $order['destinations'] = array();
            foreach($destinations as $destination){
                $des[$i++] =  $destination['destination_id'];
                $order['destinations'] = $des;
                $order['destination_list'] = $order['destination_list'] + array($destination['destination_id'] => Helpers::getDestinationNameById($destination['destination_id']));
            }
        }
        else if($buyer_template == "Template 02"){
            // create Despo array
            $despos = ProductionOrderDespo::where('production_order_id', '=', $id)->get();
            $order['despos'] = '';
            foreach($despos as $despo){
                $order['despos'] = $order['despos'].','.$despo->despo;
                $order['despo_list'] = $order['despo_list'] + array($despo['despo'] => $despo->despo);
            }
        }

        if($request->input('quantity_id') > 0) {
            return View::make('production_order.quantityDetails', $data)->with('order', $order)->with('quantity_details', $quantity_details);
        }
        else{
            $quantity_details = array();
            return View::make('production_order.quantityDetails', $data)->with('order', $order)->with('quantity_details', $quantity_details);
        }
    }

    function quantityDetailsStore(Request $request)
    {

        $total_block =0;
        foreach($_POST as $key => $value) {
            if (strpos($key, 'tod_detail_id_') === 0) {
                $total_block =$total_block+1;
            }
        }
//
//        echo $total_block;

        $production_order_id = $request->get('order_id');
        $buyer_template = $request->get('buyer_template');
        $created_at = new DateTime();

        //$total_block = $request->get('total_quantity_block');
        for($i=1; $i<=$total_block; $i++){
            if($request->get('size_group_id_'.$i)){
                if($buyer_template == "Template 01"){

                    $tod_detail_id = $request->get('tod_detail_id_'.$i);
                    if($tod_detail_id){

                        $size = SizeGroupSize::where('size_group_id', '=', $request->get('size_group_id_'.$i))->get();
                        $j = 1;
                        $total_quantity = 0;
                        if($request->get('size_'.$i.'_'.$j,0)){
                            ProductionOrderDetailSize::where('production_order_detail_id', '=', $tod_detail_id)->delete();
                            foreach($size as $s){
                                $size_input['size_id'] = $request->get('size_'.$i.'_'.$j);
                                $size_input['quantity'] = ($request->get('quantity_'.$i.'_'.$j))?$request->get('quantity_'.$i.'_'.$j):0;
                                $size_input['production_order_detail_id'] = $tod_detail_id;

                                $total_quantity = $total_quantity + $size_input['quantity'];
                                //echo 'size_'.$i.'_'.$j;
                                //print_r($size_input);//die;
                                if($size_input['size_id']){
                                    ProductionOrderDetailSize::insert($size_input);

                                }

                                $j++;


                            }
                            //echo "#tod_detail_id ".$tod_detail_id."<br/>";
                            $tod_input = array(
                                'size_group_id'=>$request->get('size_group_id_'.$i),
                                'quantity_remarks'=>$request->get('remarks_'.$i),
                                'unit_value'=>$request->get('unit_value_'.$i),
                                'total_quantity'=>$total_quantity,
                                'updated_at'=>$created_at
                            );


                            ProductionOrderDetailTod::where('id','=',$tod_detail_id)->update($tod_input);

                        }else{
                            //fix for editing unit_value
                            //echo "tod_detail_id ".$tod_detail_id."<br/>";
                            $tod_input = array(
                                //'size_group_id'=>$request->get('size_group_id_'.$i),
                                'quantity_remarks'=>$request->get('remarks_'.$i),
                                'unit_value'=>$request->get('unit_value_'.$i),
                                //'total_quantity'=>$total_quantity,
                                'updated_at'=>$created_at
                            );


                            ProductionOrderDetailTod::where('id','=',$tod_detail_id)->update($tod_input);

                        }



                    }
                }
                else if($buyer_template == "Template 02"){

                    $tod_detail_id = $request->get('tod_detail_id_'.$i);
                    if($tod_detail_id){

                        $size = SizeGroupSize::where('size_group_id', '=', $request->get('size_group_id_'.$i))->get();
                        $j = 1;
                        $total_quantity = 0;
                        ProductionOrderDetailSize::where('production_order_detail_id', '=', $tod_detail_id)->delete();
                        foreach($size as $s){
                            $size_input['size_id'] = $request->get('size_'.$i.'_'.$j);
                            $size_input['quantity'] = ($request->get('quantity_'.$i.'_'.$j))?$request->get('quantity_'.$i.'_'.$j):0;
                            $size_input['production_order_detail_id'] = $tod_detail_id;

                            $total_quantity = (int)$total_quantity + $size_input['quantity'];
                            ProductionOrderDetailSize::insert($size_input);
                            $j++;
                        }
                        $tod_input = array(
                            'size_group_id'=>$request->get('size_group_id_'.$i),
                            'quantity_remarks'=>$request->get('remarks_'.$i),
                            'unit_value'=>$request->get('unit_value_'.$i),
                            'total_quantity'=>$total_quantity,
                            'updated_at'=>$created_at
                        );
                        ProductionOrderDetailTod::where('id','=',$tod_detail_id)->update($tod_input);
                    }
                }
            }
        }

        Session::flash('message', 'Successfully updated Production Order Quantity Detail!');
        return Redirect::to('productionOrder/details/'.$production_order_id);
    }


    function paymentDetailsStore(Request $request)
    {
        $production_order_id = $request->get('order_id');
        $buyer_template =$request->get('buyer_template');
        $updated_at = new DateTime();

        if($buyer_template == "Template 01"){
            $payment_input = array(
                'production_order_id'=>$production_order_id,
                'payment_term_id'=>($request->get('payment_term_id'))?$request->get('payment_term_id'):NULL,
                'currency_id'=>($request->get('currency_id'))?$request->get('currency_id'):NULL,
                'commission'=>$request->get('commission'),
                'total_value'=>$request->get('total_value'),
                'commission_value'=>$request->get('commission_value'),
                'invoice_value'=>$request->get('invoice_value'),
                'block_tt_no'=>($request->get('block_tt_no'))?$request->get('block_tt_no'):NULL,
                'block_tt_order_from'=>strtotime($request->get('block_tt_order_from')),
                'block_tt_order_to'=>strtotime($request->get('block_tt_order_to')),
                'sales_contact_no'=>$request->get('sales_contact_no'),
                'remarks'=>$request->get('remarks'),
                'updated_at'=>$updated_at
            );
        }
        else if($buyer_template == "Template 02"){
            $payment_input = array(
                'production_order_id'=>$production_order_id,
                'payment_term_id'=>($request->get('payment_term_id'))?$request->get('payment_term_id'):NULL,
                'currency_id'=>($request->get('currency_id'))?$request->get('currency_id'):NULL,
                'commission'=>$request->get('commission'),
                'purchase_contact_no'=>$request->get('purchase_contact_no'),
                'total_value'=>$request->get('total_value'),
                'less_financial_charge'=>$request->get('less_financial_charge'),
                'less_financial_charge_percent'=>$request->get('less_financial_charge_percent'),
                'less_claim_bonus_percent'=>$request->get('less_claim_bonus_percent'),
                'less_claim_bonus'=>$request->get('less_claim_bonus'),
                'less_novi_deduction_percent'=>$request->get('less_novi_deduction_percent'),
                'less_novi_deduction'=>$request->get('less_novi_deduction'),
                'sub_total_value'=>$request->get('sub_total_value'),
                'less_lc_deadline_percent'=>$request->get('less_lc_deadline_percent'),
                'less_lc_deadline'=>$request->get('less_lc_deadline'),
                'final_amount'=>$request->get('final_amount'),
                'remarks'=>$request->get('remarks'),
                'updated_at'=>$updated_at
            );
        }

        $payment_detail = ProductionOrderDetailPayment::where('production_order_id', '=', $production_order_id)->get();
        if(count($payment_detail)){
            ProductionOrderDetailPayment::where('production_order_id','=',$production_order_id)->update($payment_input);
        }
        else{
            ProductionOrderDetailPayment::insert($payment_input);
        }

        Session::flash('message', 'Successfully updated Production Order Payment Detail!');
        return Redirect::to('productionOrder/details/'.$production_order_id);
    }

    public function shippingDetails(Request $request)
    {
        $data = array();
        $order_id = $request->input('id');
        $id = $request->input('id');//$order_id['id'];

        $data['shipping_block_no'] = $request->input('shipping_block_no');
        if($request->input('shipping_id') > 0){
            $shipping_id = Input::get('shipping_id');

            $shipping_details = ProductionOrderDetailShippingBlock::find($shipping_id);

            // create destination array
            $destinations = ProductionOrderDetailShippingBlockDestination::where('production_order_detail_shipping_block_id', '=', $shipping_id)->get();
            $i = 0;
            $shipping_details['destinations'] = array();
            foreach($destinations as $destination){
                $d[$i++] =  $destination['destination_id'];
                $shipping_details['destinations'] = $d;
            }
        }
        $order = ProductionOrder::find($id);

        $order['destination_list'] = Destination::pluck('destination_name','id')->toArray();
        $order['shipping_mode_list'] = array('' => 'Select Shipping Mode') + ShippingMode::pluck('shipping_mode','id')->toArray();

        $buyer_template = Buyer::find($order['buyer_id'])->buyerTemplate->buyer_template;



        if($request->input('shipping_id') > 0) {
            return View::make('production_order.shippingDetails', $data)->with('order', $order)->with('shipping_details', $shipping_details);
        }
        else{
            $shipping_details = array();
            return View::make('production_order.shippingDetails', $data)->with('order', $order)->with('shipping_details', $shipping_details);
        }
    }

    function shippingDetailsStore(Request $request)
    {
        $production_order_id = $request->input('order_id');
        $buyer_template = $request->input('buyer_template');
        $created_at = new DateTime();

        if($buyer_template == "Template 01"){
            $shipment_input = array(
                'production_order_id'=>$production_order_id,
                'consignee_bank_id'=>($request->input('consignee_bank_id'))?$request->input('consignee_bank_id'):NULL,
                'shipper_bank_id'=>($request->input('shipper_bank_id'))?$request->input('shipper_bank_id'):NULL,
                'terms_of_delivery_id'=>($request->input('terms_of_delivery_id'))?$request->input('terms_of_delivery_id'):NULL,
                'forwarder_id'=>($request->input('forwarder_id'))?$request->input('forwarder_id'):NULL
            );
        }
        else if($buyer_template == "Template 02"){
            $shipment_input = array(
                'production_order_id'=>$production_order_id,
                'consignee_bank_id'=>($request->input('consignee_bank_id'))?$request->input('consignee_bank_id'):NULL,
                'shipper_bank_id'=>($request->input('shipper_bank_id'))?$request->input('shipper_bank_id'):NULL,
                'terms_of_delivery_id'=>($request->input('terms_of_delivery_id'))?$request->input('terms_of_delivery_id'):NULL,
                'forwarder_id'=>($request->input('forwarder_id'))?$request->input('forwarder_id'):NULL
            );
        }

        $shipment_detail = ProductionOrderDetailShipment::where('production_order_id', '=', $production_order_id)->get();
        if(count($shipment_detail)){
            $shipment_input['updated_by'] = Auth::user()->id;
            $shipment_input['updated_at'] = $created_at;

            ProductionOrderDetailShipment::where('production_order_id','=',$production_order_id)->update($shipment_input);
        }
        else{
            $shipment_input['created_by'] = Auth::user()->id;
            $shipment_input['created_at'] = $created_at;
            ProductionOrderDetailShipment::insert($shipment_input);
        }

        $total_block = $request->input('total_shipping_block');
        //die;
        for($i=1; $i<=$total_block; $i++){
            if($buyer_template == "Template 01"){
                $shipping_detail_id = $request->input('shipping_detail_id_'.$i);
                if($shipping_detail_id){
                    $shipping_block_input = array(
                        'production_order_id'=>$production_order_id,
                        'shipping_mode_id'=>$request->input('shipping_mode_id_'.$i),
                        'updated_by'=>Auth::user()->id,
                        'updated_at'=>$created_at
                    );
                    ProductionOrderDetailShippingBlock::where('id','=',$shipping_detail_id)->update($shipping_block_input);

                    ProductionOrderDetailShippingBlockDestination::where('production_order_detail_shipping_block_id','=',$shipping_detail_id)->delete();

                    if(count($request->input('destination_id_'.$i))){
                        foreach($request->input('destination_id_'.$i) as $destination){
                            $destination_input = array(array('production_order_detail_shipping_block_id'=>$shipping_detail_id, 'destination_id'=>$destination, 'created_at'=>new DateTime()));
                            ProductionOrderDetailShippingBlockDestination::insert($destination_input);
                        }
                    }
                }
                else{
                    $shipping_block_input = array(
                        'production_order_id'=>$production_order_id,
                        'shipping_mode_id'=>$request->input('shipping_mode_id_'.$i),
                        'created_by'=>Auth::user()->id,
                        'created_at'=>$created_at
                    );
                    $shipping_block_id = ProductionOrderDetailShippingBlock::insertGetId($shipping_block_input);

                    if(count($request->input('destination_id_'.$i))){
                        foreach($request->input('destination_id_'.$i) as $destination){
                            $destination_input = array(array('production_order_detail_shipping_block_id'=>$shipping_block_id, 'destination_id'=>$destination, 'created_at'=>new DateTime()));
                            ProductionOrderDetailShippingBlockDestination::insert($destination_input);
                        }
                    }
                }
            }
            else if($buyer_template == "Template 02"){
                $shipping_detail_id = $request->input('shipping_detail_id_'.$i);
                if($shipping_detail_id){
                    $shipping_block_input = array(
                        'production_order_id'=>$production_order_id,
                        'shipping_mode_id'=>$request->input('shipping_mode_id_'.$i),
                        'updated_by'=>Auth::user()->id,
                        'updated_at'=>$created_at
                    );
                    ProductionOrderDetailShippingBlock::where('id','=',$shipping_detail_id)->update($shipping_block_input);

                    ProductionOrderDetailShippingBlockDestination::where('production_order_detail_shipping_block_id','=',$shipping_detail_id)->delete();

                    if(count($request->input('destination_id_'.$i))){
                        foreach($request->input('destination_id_'.$i) as $destination){
                            $destination_input = array(array('production_order_detail_shipping_block_id'=>$shipping_detail_id, 'destination_id'=>$destination, 'created_at'=>new DateTime()));
                            ProductionOrderDetailShippingBlockDestination::insert($destination_input);
                        }
                    }
                }
                else{
                    $shipping_block_input = array(
                        'production_order_id'=>$production_order_id,
                        'shipping_mode_id'=>$request->input('shipping_mode_id_'.$i),
                        'created_by'=>Auth::user()->id,
                        'created_at'=>$created_at
                    );
                    $shipping_block = ProductionOrderDetailShippingBlock::insert($shipping_block_input);

                    if(count($request->input('destination_id_'.$i))){
                        foreach($request->input('destination_id_'.$i) as $destination){
                            $destination_input = array(array('production_order_detail_shipping_block_id'=>$shipping_block->id, 'destination_id'=>$destination, 'created_at'=>new DateTime()));
                            ProductionOrderDetailShippingBlockDestination::insert($destination_input);
                        }
                    }
                }
            }
        }

        Session::flash('message', 'Successfully updated Production Order Shipment Detail!');
        return Redirect::to('productionOrder/details/'.$production_order_id);
    }


    public function orderTODSizeBreakdownReport()
    {
        $orders = ProductionOrder::all();
        $data['pagetitle'] = "Order TOD Size Breakdown Report";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');


        $data['sizes'] = Size::all()->orderBy('size_no', 'ASC');
        $data['destinations'] = Destination::all();

        foreach($orders as $order){
            $buyer_template = Buyer::find($order['buyer_id'])->buyerTemplate->buyer_template;
            // create article array
            $articles = ProductionOrderArticle::where('production_order_id', '=', $order['id'])->get();
            $i = 0;
            $order['articles'] = array();
            foreach($articles as $article){
                $a[$i++] =  $article['article_id'];
                $order['articles'] = $a;
            }
            // create color array
            $colors = ProductionOrderColor::where('production_order_id', '=', $order['id'])->get();
            $i = 0;
            $order['colors'] = array();
            foreach($colors as $color){
                $c[$i++] =  $color['color_id'];
                $order['colors'] = $c;
            }

            if($buyer_template == "Template 01"){
                // create Destination array
                $destinations = ProductionOrderDestination::where('production_order_id', '=', $order['id'])->get();
                $i = 0;
                $order['destinations'] = array();
                foreach($destinations as $destination){
                    $des[$i++] =  $destination['destination_id'];
                    $order['destinations'] = $des;
                }
            }
            else if($buyer_template == "Template 02"){
                // create Despo array
                $despos = ProductionOrderDespo::where('production_order_id', '=', $order['id'])->get();
                $i = 0;
                $order['despos'] = array();
                foreach($despos as $despo){
                    $dsp[$i++] =  $despo['despo'];
                    $order['despos'] = $dsp;
                }
            }
        }

        return view('production_order.orderTODSizeBreakdownReport', $data)->with('orders', $orders);
    }

    public function orderTODSizeBreakdownReportSearch(Request $request){
        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();
        $data['pagetitle'] = "Order TOD Size Breakdown Report";
        $post_val = $request->all();
        $buyer              = ($request->has('buyer_id'))?$request->get('buyer_id'):'';
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $destination        = ($request->has('destination_id'))?$request->get('destination_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';
        $season             = ($request->has('season_id'))?$request->get('season_id'):'';
        $despo              = ($request->has('despo'))?$request->get('despo'):'';
        $pos                = ($request->has('pos'))?$request->get('pos'):'';
        $order_date_from    = ($request->has('order_date_from'))?$request->get('order_date_from'):'';
        $order_date_to      = ($request->has('order_date_to'))?$request->get('order_date_to'):'';
        $tod_from           = ($request->has('tod_from'))?$request->get('tod_from'):'';
        $tod_to             = ($request->has('tod_to'))?$request->get('tod_to'):'';

        if($buyer || $order_no || $article || $color || $destination || $season || $despo || $pos || $order_date_from || $order_date_to || $tod_from || $tod_to){
            $query = ProductionOrder::query();
            if($buyer){
                $i = 0;
                foreach($buyer as $buyer_id){
                    if($i == 0){
                        $query->where('buyer_id', '=', $buyer_id);
                    }
                    else{
                        $query->orWhere('buyer_id', '=', $buyer_id);
                    }
                    $i++;
                }
            }
            if($order_no){
                $i = 0;
                foreach($order_no as $order){
                    if($i == 0){
                        $query->where('id', '=', $order);
                    }
                    else{
                        $query->orWhere('id', '=', $order);
                    }
                    $i++;
                }
            }
            if($article){
                $i = 0;
                foreach($article as $article_id){
                    $article_order = ProductionOrderArticle::where('article_id', '=', $article_id)->get();
                    if($i == 0){
                        if($article_order){
                            $j = 0;
                            foreach($article_order as $ao){
                                if($j == 0){
                                    $query->where('id', '=', $ao->production_order_id);
                                }
                                else{
                                    $query->orWhere('id', '=', $ao->production_order_id);
                                }
                                $j++;
                            }
                        }
                    }
                    else{
                        if($article_order){
                            foreach($article_order as $ao){
                                $query->orWhere('id', '=', $ao->production_order_id);
                            }
                        }
                    }
                    $i++;
                }
            }
            if($color){
                $i = 0;
                foreach($color as $color_id){
                    $color_order = ProductionOrderColor::where('color_id', '=', $color_id)->get();
                    if($i == 0){
                        if($color_order){
                            $j = 0;
                            foreach($color_order as $co){
                                if($j == 0){
                                    $query->where('id', '=', $co->production_order_id);
                                }
                                else{
                                    $query->orWhere('id', '=', $co->production_order_id);
                                }
                                $j++;
                            }
                        }
                    }
                    else{
                        if($color_order){
                            foreach($color_order as $co){
                                $query->orWhere('id', '=', $co->production_order_id);
                            }
                        }
                    }
                    $i++;
                }
            }
            if($season){
                $i = 0;
                foreach($season as $season_id){
                    if($i == 0){
                        $query->where('season_id', '=', $season_id);
                    }
                    else{
                        $query->orWhere('season_id', '=', $season_id);
                    }
                    $i++;
                }
            }
            if($destination){
                $i = 0;
                foreach($destination as $destination_id){
                    $destination_order = ProductionOrderDestination::where('destination_id', '=', $destination_id)->get();
                    if($i == 0){
                        if($destination_order){
                            $j = 0;
                            foreach($destination_order as $do){
                                if($j == 0){
                                    $query->where('id', '=', $do->production_order_id);
                                }
                                else{
                                    $query->orWhere('id', '=', $do->production_order_id);
                                }
                                $j++;
                            }
                        }
                    }
                    else{
                        if($destination_order){
                            foreach($destination_order as $do){
                                $query->orWhere('id', '=', $do->production_order_id);
                            }
                        }
                    }
                    $i++;
                }
            }
            if($despo){
                $i = 0;
                foreach($despo as $des){
                    $despo_order = ProductionOrderDespo::where('despo', 'LIKE', '%'.$des.'%')->get();
                    if($i == 0){
                        if($despo_order){
                            $j = 0;
                            foreach($despo_order as $dso){
                                if($j == 0){
                                    $query->where('id', '=', $dso->production_order_id);
                                }
                                else{
                                    $query->orWhere('id', '=', $dso->production_order_id);
                                }
                                $j++;
                            }
                        }
                    }
                    else{
                        if($despo_order){
                            foreach($despo_order as $dso){
                                $query->orWhere('id', '=', $dso->production_order_id);
                            }
                        }
                    }
                    $i++;
                }
            }
            if($pos){
                $pos_order = ProductionOrderDetailTod::where('pos', 'LIKE', '%'.$pos.'%')->get();
                if($pos_order){
                    $j = 0;
                    foreach($pos_order as $po){
                        if($j == 0){
                            $query->where('id', '=', $po->production_order_id);
                        }
                        else{
                            $query->orWhere('id', '=', $po->production_order_id);
                        }
                        $j++;
                    }
                }
            }
            if($order_date_from){
                $query->where('order_receive_date', '>=', $order_date_from);
            }
            if($order_date_to){
                $query->where('order_receive_date', '<=', $order_date_to);
            }
            if($tod_from){
                $tod_order = ProductionOrderDetailTod::where('tod', '>=', $tod_from)->get();
                if($tod_order){
                    $j = 0;
                    foreach($tod_order as $to){
                        if($j == 0){
                            $query->where('id', '=', $to->production_order_id);
                        }
                        else{
                            $query->orWhere('id', '=', $to->production_order_id);
                        }
                        $j++;
                    }
                }
            }
            if($tod_to){
                $tod_order = ProductionOrderDetailTod::where('tod', '<=', $tod_to)->get();
                if($tod_order){
                    $j = 0;
                    foreach($tod_order as $to){
                        if($j == 0){
                            $query->where('id', '=', $to->production_order_id);
                        }
                        else{
                            $query->orWhere('id', '=', $to->production_order_id);
                        }
                        $j++;
                    }
                }
            }
            $orders = $query->orderBy('id', 'ASC')->get();
        }
        else{
            $orders = ProductionOrder::all();
        }

        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        foreach($orders as $order){
            $buyer_template = Buyer::find($order['buyer_id'])->buyerTemplate->buyer_template;
            // create article array
            $articles = ProductionOrderArticle::where('production_order_id', '=', $order['id'])->get();
            $i = 0;
            $order['articles'] = array();
            foreach($articles as $article){
                $a[$i++] =  $article['article_id'];
                $order['articles'] = $a;
            }
            // create color array
            $colors = ProductionOrderColor::where('production_order_id', '=', $order['id'])->get();
            $i = 0;
            $order['colors'] = array();
            foreach($colors as $color){
                $c[$i++] =  $color['color_id'];
                $order['colors'] = $c;
            }

            if($buyer_template == "Template 01"){
                // create Destination array
                $destinations = ProductionOrderDestination::where('production_order_id', '=', $order['id'])->get();
                $i = 0;
                $order['destinations'] = array();
                foreach($destinations as $destination){
                    $des[$i++] =  $destination['destination_id'];
                    $order['destinations'] = $des;
                }
            }
            else if($buyer_template == "Template 02"){
                // create Despo array
                $despos = ProductionOrderDespo::where('production_order_id', '=', $order['id'])->get();
                $i = 0;
                $order['despos'] = array();
                foreach($despos as $despo){
                    $dsp[$i++] =  $despo['despo'];
                    $order['despos'] = $dsp;
                }
            }
        }

        return view('production_order.orderTODSizeBreakdownReport', $data)->with('orders', $orders)->with('post_val', $post_val);

    }

    public function sizeBreakdownProductionReport()
    {

        $orders = ProductionOrder::all();
        $data['pagetitle'] = "Size Breakdown Report";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');


        $data['sizes'] = Size::orderBy('size_no', 'ASC')->get();
        $data['destinations'] = Destination::all();

        foreach($orders as $order){
            $buyer_template = Buyer::find($order['buyer_id'])->buyerTemplate->buyer_template;
            // create article array
            $articles = ProductionOrderArticle::where('production_order_id', '=', $order['id'])->get();
            $i = 0;
            $order['articles'] = array();
            foreach($articles as $article){
                $a[$i++] =  $article['article_id'];
                $order['articles'] = $a;
            }
            // create color array
            $colors = ProductionOrderColor::where('production_order_id', '=', $order['id'])->get();
            $i = 0;
            $order['colors'] = array();
            foreach($colors as $color){
                $c[$i++] =  $color['color_id'];
                $order['colors'] = $c;
            }

            if($buyer_template == "Template 01"){
                // create Destination array
                $destinations = ProductionOrderDestination::where('production_order_id', '=', $order['id'])->get();
                $i = 0;
                $order['destinations'] = array();
                foreach($destinations as $destination){
                    $des[$i++] =  $destination['destination_id'];
                    $order['destinations'] = $des;
                }
            }
            else if($buyer_template == "Template 02"){
                // create Despo array
                $despos = ProductionOrderDespo::where('production_order_id', '=', $order['id'])->get();
                $i = 0;
                $order['despos'] = array();
                foreach($despos as $despo){
                    $dsp[$i++] =  $despo['despo'];
                    $order['despos'] = $dsp;
                }
            }
        }

        return view('production_order.sizeBreakdownProductionReport', $data)->with('orders', $orders);
    }


    public function sizeBreakdownProductionReportSearch(Request $request){
        $data['sizes'] = Size::orderBy('size_no', 'ASC')->get();
        $data['destinations'] = Destination::all();
        $data['pagetitle'] = "Size Breakdown Report";
        $post_val = $request->all();
        $buyer              = ($request->has('buyer_id'))?$request->get('buyer_id'):'';
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $destination        = ($request->has('destination_id'))?$request->get('destination_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';
        $season             = ($request->has('season_id'))?$request->get('season_id'):'';
        $despo              = ($request->has('despo'))?$request->get('despo'):'';
        $pos                = ($request->has('pos'))?$request->get('pos'):'';
        $order_date_from    = ($request->has('order_date_from'))?$request->get('order_date_from'):'';
        $order_date_to      = ($request->has('order_date_to'))?$request->get('order_date_to'):'';
        $tod_from           = ($request->has('tod_from'))?$request->get('tod_from'):'';
        $tod_to             = ($request->has('tod_to'))?$request->get('tod_to'):'';

        if($buyer || $order_no || $article || $color || $destination || $season || $despo || $pos || $order_date_from || $order_date_to || $tod_from || $tod_to){
            $query = ProductionOrder::query();
            if($buyer){
                $i = 0;
                foreach($buyer as $buyer_id){
                    if($i == 0){
                        $query->where('buyer_id', '=', $buyer_id);
                    }
                    else{
                        $query->orWhere('buyer_id', '=', $buyer_id);
                    }
                    $i++;
                }
            }
            if($order_no){
                $i = 0;
                foreach($order_no as $order){
                    if($i == 0){
                        $query->where('id', '=', $order);
                    }
                    else{
                        $query->orWhere('id', '=', $order);
                    }
                    $i++;
                }
            }
            if($article){
                $i = 0;
                foreach($article as $article_id){
                    $article_order = ProductionOrderArticle::where('article_id', '=', $article_id)->get();
                    if($i == 0){
                        if($article_order){
                            $j = 0;
                            foreach($article_order as $ao){
                                if($j == 0){
                                    $query->where('id', '=', $ao->production_order_id);
                                }
                                else{
                                    $query->orWhere('id', '=', $ao->production_order_id);
                                }
                                $j++;
                            }
                        }
                    }
                    else{
                        if($article_order){
                            foreach($article_order as $ao){
                                $query->orWhere('id', '=', $ao->production_order_id);
                            }
                        }
                    }
                    $i++;
                }
            }
            if($color){
                $i = 0;
                foreach($color as $color_id){
                    $color_order = ProductionOrderColor::where('color_id', '=', $color_id)->get();
                    if($i == 0){
                        if($color_order){
                            $j = 0;
                            foreach($color_order as $co){
                                if($j == 0){
                                    $query->where('id', '=', $co->production_order_id);
                                }
                                else{
                                    $query->orWhere('id', '=', $co->production_order_id);
                                }
                                $j++;
                            }
                        }
                    }
                    else{
                        if($color_order){
                            foreach($color_order as $co){
                                $query->orWhere('id', '=', $co->production_order_id);
                            }
                        }
                    }
                    $i++;
                }
            }
            if($season){
                $i = 0;
                foreach($season as $season_id){
                    if($i == 0){
                        $query->where('season_id', '=', $season_id);
                    }
                    else{
                        $query->orWhere('season_id', '=', $season_id);
                    }
                    $i++;
                }
            }
            if($destination){
                $i = 0;
                foreach($destination as $destination_id){
                    $destination_order = ProductionOrderDestination::where('destination_id', '=', $destination_id)->get();
                    if($i == 0){
                        if($destination_order){
                            $j = 0;
                            foreach($destination_order as $do){
                                if($j == 0){
                                    $query->where('id', '=', $do->production_order_id);
                                }
                                else{
                                    $query->orWhere('id', '=', $do->production_order_id);
                                }
                                $j++;
                            }
                        }
                    }
                    else{
                        if($destination_order){
                            foreach($destination_order as $do){
                                $query->orWhere('id', '=', $do->production_order_id);
                            }
                        }
                    }
                    $i++;
                }
            }
            if($despo){
                $i = 0;
                foreach($despo as $des){
                    $despo_order = ProductionOrderDespo::where('despo', 'LIKE', '%'.$des.'%')->get();
                    if($i == 0){
                        if($despo_order){
                            $j = 0;
                            foreach($despo_order as $dso){
                                if($j == 0){
                                    $query->where('id', '=', $dso->production_order_id);
                                }
                                else{
                                    $query->orWhere('id', '=', $dso->production_order_id);
                                }
                                $j++;
                            }
                        }
                    }
                    else{
                        if($despo_order){
                            foreach($despo_order as $dso){
                                $query->orWhere('id', '=', $dso->production_order_id);
                            }
                        }
                    }
                    $i++;
                }
            }
            if($pos){
                $pos_order = ProductionOrderDetailTod::where('pos', 'LIKE', '%'.$pos.'%')->get();
                if($pos_order){
                    $j = 0;
                    foreach($pos_order as $po){
                        if($j == 0){
                            $query->where('id', '=', $po->production_order_id);
                        }
                        else{
                            $query->orWhere('id', '=', $po->production_order_id);
                        }
                        $j++;
                    }
                }
            }
            if($order_date_from){
                $query->where('order_receive_date', '>=', $order_date_from);
            }
            if($order_date_to){
                $query->where('order_receive_date', '<=', $order_date_to);
            }
            if($tod_from){
                $tod_order = ProductionOrderDetailTod::where('tod', '>=', $tod_from)->get();
                if($tod_order){
                    $j = 0;
                    foreach($tod_order as $to){
                        if($j == 0){
                            $query->where('id', '=', $to->production_order_id);
                        }
                        else{
                            $query->orWhere('id', '=', $to->production_order_id);
                        }
                        $j++;
                    }
                }
            }
            if($tod_to){
                $tod_order = ProductionOrderDetailTod::where('tod', '<=', $tod_to)->get();
                if($tod_order){
                    $j = 0;
                    foreach($tod_order as $to){
                        if($j == 0){
                            $query->where('id', '=', $to->production_order_id);
                        }
                        else{
                            $query->orWhere('id', '=', $to->production_order_id);
                        }
                        $j++;
                    }
                }
            }
            $orders = $query->orderBy('id', 'ASC')->get();
        }
        else{
            $orders = ProductionOrder::all();
        }

        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        foreach($orders as $order){
            $buyer_template = Buyer::find($order['buyer_id'])->buyerTemplate->buyer_template;
            // create article array
            $articles = ProductionOrderArticle::where('production_order_id', '=', $order['id'])->get();
            $i = 0;
            $order['articles'] = array();
            foreach($articles as $article){
                $a[$i++] =  $article['article_id'];
                $order['articles'] = $a;
            }
            // create color array
            $colors = ProductionOrderColor::where('production_order_id', '=', $order['id'])->get();
            $i = 0;
            $order['colors'] = array();
            foreach($colors as $color){
                $c[$i++] =  $color['color_id'];
                $order['colors'] = $c;
            }

            if($buyer_template == "Template 01"){
                // create Destination array
                $destinations = ProductionOrderDestination::where('production_order_id', '=', $order['id'])->get();
                $i = 0;
                $order['destinations'] = array();
                foreach($destinations as $destination){
                    $des[$i++] =  $destination['destination_id'];
                    $order['destinations'] = $des;
                }
            }
            else if($buyer_template == "Template 02"){
                // create Despo array
                $despos = ProductionOrderDespo::where('production_order_id', '=', $order['id'])->get();
                $i = 0;
                $order['despos'] = array();
                foreach($despos as $despo){
                    $dsp[$i++] =  $despo['despo'];
                    $order['despos'] = $dsp;
                }
            }
        }

        return view('production_order.sizeBreakdownProductionReport', $data)->with('orders', $orders)->with('post_val', $post_val);

    }
    public function seasonalOrderValueReport()
    {
        $data = array();

        $orders = ProductionOrderDetailTod::all();
        $data['pagetitle'] = "Seasonal Order Value Report";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();
        return view('production_order.seasonalOrderValueReport', $data)->with('orders', $orders);

    }
    public function seasonalOrderValueReportGroupbyOrder()
    {
        $data = array();
        $orders = ProductionOrder::all();
        $data['pagetitle'] = "Seasonal Order Value Report";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');
        $data['block_tt_no']        = ProductionOrderDetailPayment::where("block_tt_no","!=",Null)->pluck('block_tt_no','block_tt_no');

        //dd($data);
        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();
        return view('production_order.seasonalOrderValueReportGroupbyOrder', $data)->with('orders', $orders);

    }

    public function seasonalOrderValueReportold()
    {

        $data['orders'] = $orders = ProductionOrder::all();
        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();

        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');
        $data['months']             = array(1 => 'Jan.', 2 => 'Feb.', 3 => 'Mar.', 4 => 'Apr.', 5 => 'May', 6 => 'Jun.', 7 => 'Jul.', 8 => 'Aug.', 9 => 'Sep.', 10 => 'Oct.', 11 => 'Nov.', 12 => 'Dec.');
        $data['years']              = range(1980,2080);;


        foreach($orders as $order){
            $buyer_template = Buyer::find($order['buyer_id'])->buyerTemplate->buyer_template;
            // create article array
            $articles = ProductionOrderArticle::where('production_order_id', '=', $order['id'])->get();
            $i = 0;
            $order['articles'] = array();
            foreach($articles as $article){
                $a[$i++] =  $article['article_id'];
                $order['articles'] = $a;
            }
            // create color array
            $colors = ProductionOrderColor::where('production_order_id', '=', $order['id'])->get();
            $i = 0;
            $order['colors'] = array();
            foreach($colors as $color){
                $c[$i++] =  $color['color_id'];
                $order['colors'] = $c;
            }

            if($buyer_template == "Template 01"){
                // create Destination array
                $destinations = ProductionOrderDestination::where('production_order_id', '=', $order['id'])->get();
                $i = 0;
                $order['destinations'] = array();
                foreach($destinations as $destination){
                    $des[$i++] =  $destination['destination_id'];
                    $order['destinations'] = $des;
                }
            }
            else if($buyer_template == "Template 02"){
                // create Despo array
                $despos = ProductionOrderDespo::where('production_order_id', '=', $order['id'])->get();
                $i = 0;
                $order['despos'] = array();
                foreach($despos as $despo){
                    $dsp[$i++] =  $despo['despo'];
                    $order['despos'] = $dsp;
                }
            }
        }

        return view('production_order.seasonalOrderValueReport', $data);
    }
    public function seasonalTODReport()
    {
        $data['orders'] = ProductionOrder::all();
        $data['tods'] = Helpers::getDistinctTOD();
        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();

        return view('production_order.seasonalTODReport', $data);
    }
    public function seasonalCutOffReport()
    {
        $data['orders'] = ProductionOrder::all();
        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();

        return view('production_order.seasonalCutOffReport', $data);
    }

    public function search(Request $request){
        $post_val = $request->all();
        $buyer              = ($request->has('buyer_id'))?$request->get('buyer_id'):'';
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $destination        = ($request->has('destination_id'))?$request->get('destination_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';
        $season             = ($request->has('season_id'))?$request->get('season_id'):'';
        $despo              = ($request->has('despo'))?$request->get('despo'):'';
        $pos                = ($request->has('pos'))?$request->get('pos'):'';
        $order_date_from    = ($request->has('order_date_from'))?$request->get('order_date_from'):'';
        $order_date_to      = ($request->has('order_date_to'))?$request->get('order_date_to'):'';
        $tod_from           = ($request->has('tod_from'))?$request->get('tod_from'):'';
        $tod_to             = ($request->has('tod_to'))?$request->get('tod_to'):'';

        if($buyer || $order_no || $article || $color || $destination || $season || $despo || $pos || $order_date_from || $order_date_to || $tod_from || $tod_to){
            $query = ProductionOrder::query();
            if($buyer){
                $i = 0;
                foreach($buyer as $buyer_id){
                    if($i == 0){
                        $query->where('buyer_id', '=', $buyer_id);
                    }
                    else{
                        $query->orWhere('buyer_id', '=', $buyer_id);
                    }
                    $i++;
                }
            }
            if($order_no){
                $i = 0;
                foreach($order_no as $order){
                    if($i == 0){
                        $query->where('id', '=', $order);
                    }
                    else{
                        $query->orWhere('id', '=', $order);
                    }
                    $i++;
                }
            }
            if($article){
                $i = 0;
                foreach($article as $article_id){
                    $article_order = ProductionOrderArticle::where('article_id', '=', $article_id)->get();
                    if($i == 0){
                        if($article_order){
                            $j = 0;
                            foreach($article_order as $ao){
                                if($j == 0){
                                    $query->where('id', '=', $ao->production_order_id);
                                }
                                else{
                                    $query->orWhere('id', '=', $ao->production_order_id);
                                }
                                $j++;
                            }
                        }
                    }
                    else{
                        if($article_order){
                            foreach($article_order as $ao){
                                $query->orWhere('id', '=', $ao->production_order_id);
                            }
                        }
                    }
                    $i++;
                }
            }
            if($color){
                $i = 0;
                foreach($color as $color_id){
                    $color_order = ProductionOrderColor::where('color_id', '=', $color_id)->get();
                    if($i == 0){
                        if($color_order){
                            $j = 0;
                            foreach($color_order as $co){
                                if($j == 0){
                                    $query->where('id', '=', $co->production_order_id);
                                }
                                else{
                                    $query->orWhere('id', '=', $co->production_order_id);
                                }
                                $j++;
                            }
                        }
                    }
                    else{
                        if($color_order){
                            foreach($color_order as $co){
                                $query->orWhere('id', '=', $co->production_order_id);
                            }
                        }
                    }
                    $i++;
                }
            }
            if($season){
                $i = 0;
                foreach($season as $season_id){
                    if($i == 0){
                        $query->where('season_id', '=', $season_id);
                    }
                    else{
                        $query->orWhere('season_id', '=', $season_id);
                    }
                    $i++;
                }
            }
            if($destination){
                $i = 0;
                foreach($destination as $destination_id){
                    $destination_order = ProductionOrderDestination::where('destination_id', '=', $destination_id)->get();
                    if($i == 0){
                        if($destination_order){
                            $j = 0;
                            foreach($destination_order as $do){
                                if($j == 0){
                                    $query->where('id', '=', $do->production_order_id);
                                }
                                else{
                                    $query->orWhere('id', '=', $do->production_order_id);
                                }
                                $j++;
                            }
                        }
                    }
                    else{
                        if($destination_order){
                            foreach($destination_order as $do){
                                $query->orWhere('id', '=', $do->production_order_id);
                            }
                        }
                    }
                    $i++;
                }
            }
            if($despo){
                $i = 0;
                foreach($despo as $des){
                    $despo_order = ProductionOrderDespo::where('despo', 'LIKE', '%'.$des.'%')->get();
                    if($i == 0){
                        if($despo_order){
                            $j = 0;
                            foreach($despo_order as $dso){
                                if($j == 0){
                                    $query->where('id', '=', $dso->production_order_id);
                                }
                                else{
                                    $query->orWhere('id', '=', $dso->production_order_id);
                                }
                                $j++;
                            }
                        }
                    }
                    else{
                        if($despo_order){
                            foreach($despo_order as $dso){
                                $query->orWhere('id', '=', $dso->production_order_id);
                            }
                        }
                    }
                    $i++;
                }
            }
            if($pos){
                $pos_order = ProductionOrderDetailTod::where('pos', 'LIKE', '%'.$pos.'%')->get();
                if($pos_order){
                    $j = 0;
                    foreach($pos_order as $po){
                        if($j == 0){
                            $query->where('id', '=', $po->production_order_id);
                        }
                        else{
                            $query->orWhere('id', '=', $po->production_order_id);
                        }
                        $j++;
                    }
                }
            }
            if($order_date_from){
                $query->where('order_receive_date', '>=', $order_date_from);
            }
            if($order_date_to){
                $query->where('order_receive_date', '<=', $order_date_to);
            }
            if($tod_from){
                $tod_order = ProductionOrderDetailTod::where('tod', '>=', $tod_from)->get();
                if($tod_order){
                    $j = 0;
                    foreach($tod_order as $to){
                        if($j == 0){
                            $query->where('id', '=', $to->production_order_id);
                        }
                        else{
                            $query->orWhere('id', '=', $to->production_order_id);
                        }
                        $j++;
                    }
                }
            }
            if($tod_to){
                $tod_order = ProductionOrderDetailTod::where('tod', '<=', $tod_to)->get();
                if($tod_order){
                    $j = 0;
                    foreach($tod_order as $to){
                        if($j == 0){
                            $query->where('id', '=', $to->production_order_id);
                        }
                        else{
                            $query->orWhere('id', '=', $to->production_order_id);
                        }
                        $j++;
                    }
                }
            }
            $orders = $query->orderBy('id', 'ASC')->get();
        }
        else{
            $orders = ProductionOrder::all();
        }

        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        foreach($orders as $order){
            $buyer_template = Buyer::find($order['buyer_id'])->buyerTemplate->buyer_template;
            // create article array
            $articles = ProductionOrderArticle::where('production_order_id', '=', $order['id'])->get();
            $i = 0;
            $order['articles'] = array();
            foreach($articles as $article){
                $a[$i++] =  $article['article_id'];
                $order['articles'] = $a;
            }
            // create color array
            $colors = ProductionOrderColor::where('production_order_id', '=', $order['id'])->get();
            $i = 0;
            $order['colors'] = array();
            foreach($colors as $color){
                $c[$i++] =  $color['color_id'];
                $order['colors'] = $c;
            }

            if($buyer_template == "Template 01"){
                // create Destination array
                $destinations = ProductionOrderDestination::where('production_order_id', '=', $order['id'])->get();
                $i = 0;
                $order['destinations'] = array();
                foreach($destinations as $destination){
                    $des[$i++] =  $destination['destination_id'];
                    $order['destinations'] = $des;
                }
            }
            else if($buyer_template == "Template 02"){
                // create Despo array
                $despos = ProductionOrderDespo::where('production_order_id', '=', $order['id'])->get();
                $i = 0;
                $order['despos'] = array();
                foreach($despos as $despo){
                    $dsp[$i++] =  $despo['despo'];
                    $order['despos'] = $dsp;
                }
            }
        }

        return view('production_order.index', $data)->with('orders', $orders)->with('post_val', $post_val);
    }

    public function getSizeQuantityByTod(Request $request){
        $id = $request->get('tod_detail_id');
        if(isset($id)){
            $data = array();
            $data['error'] = false;
            $sizes = ProductionOrderDetailSize::where('production_order_detail_id', '=', $id)->get();
            $data['sizes'] = array();
            $i = 0;
            foreach($sizes as $size){
                $data['sizes'][$i]['size_id'] = $size->size_id;
                $data['sizes'][$i]['size_no'] = Size::find($size->size_id)->size_no;
                $data['sizes'][$i]['quantity'] = $size->quantity;
                $i++;
            }
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

    public function getProductionOrderByBuyerArray(Request $request)
    {
        $buyer_arr = $request->get('buyer');
        $buyer = explode(',', $buyer_arr);
        if(isset($buyer)){
            $data = array();
            $option = '<option value="">Select Production Order No</option>';
            foreach($buyer as $buyer_id){
                $production_order = ProductionOrder::where('buyer_id', '=', $buyer_id)->get();
                foreach($production_order as $order){
                    $option = $option."<option value='".$order->id."'>".$order->order_no."</option>";
                }
            }
            $data['order_no'] = $option;
            $data['error'] = false;
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

    public function getArticleByOrderArray(Request $request)
    {
        $order_arr = $request->get('order');
        $order = explode(',', $order_arr);
        if(isset($order)){
            $data = array();
            $option = '<option value="">Select Article</option>';
            foreach($order as $order_id){
                $articles = ProductionOrderArticle::where('production_order_id', '=', $order_id)->get();
                foreach($articles as $article){
                    $option = $option."<option value='".$article->id."'>".Helpers::getArticleNoById($article->id)."</option>";
                }
            }
            $data['article'] = $option;
            $data['error'] = false;
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

    public function getColorByOrderArray(Request $request)
    {
        $order_arr = $request->get('order');
        $order = explode(',', $order_arr);
        if(isset($order)){
            $data = array();
            $option = '<option value="">Select Color</option>';
            foreach($order as $order_id){
                $colors = ProductionOrderColor::where('production_order_id', '=', $order_id)->get();
                foreach($colors as $color){
                    $option = $option."<option value='".$color->id."'>".Helpers::getColorNameById($color->id)."</option>";
                }
            }
            $data['color'] = $option;
            $data['error'] = false;
            echo json_encode($data);
            die;
        }
        else{
            $data = array();
            $data['error'] = true;
            $data['message'] = "Something went wrong. Please try again.";
            echo json_encode($data);
            die;
        }
    }

    public function overallTodReport(){

        $data = array();

        $orders = ProductionOrderDetailTod::all();
        $data['pagetitle'] = "TODReport";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();

        //echo "live and die here!";

        return view('production_order.overallTodReportView', $data)->with('orders', $orders);


    }
    public function overallproductionreport(Request $request){

        $data = array();

        $orders = ProductionOrderDetailTod::all();
        //$order = $orders->first();
        //dd($order->where('department_id','=', 6)->get());
        $data['pagetitle'] = "TODReport";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();


        return view('production_order.overallProductionReport', $data)->with('orders', $orders);


    }
    public function overallProductionReportSearch(Request $request){

        $data = array();
        $data['pagetitle'] = "OverallProduction Overview";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        //get division of packeging
        $data['pdivisionid'] = Division::where("name","=","Pakageing")->get();
        $data['pdivisionid'] = Division::where("name","=","Pakageing")->get();
        //get division of warehouse

        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();

        $post_val = $request->all();
        $buyer              = ($request->has('buyer_id'))?$request->get('buyer_id'):'';
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';

        $destination        = ($request->has('destination_id'))?$request->get('destination_id'):'';

        $season             = ($request->has('season_id'))?$request->get('season_id'):'';
        $despo              = ($request->has('despo'))?$request->get('despo'):'';
        $pos                = ($request->has('pos'))?$request->get('pos'):'';
        $order_date_from    = ($request->has('order_date_from'))?$request->get('order_date_from'):'';
        $order_date_to      = ($request->has('order_date_to'))?$request->get('order_date_to'):'';


        $order_date_from = strtotime($order_date_from);
        $order_date_to = strtotime($order_date_to);
        $tod_from           = ($request->has('tod_from'))?$request->get('tod_from'):'';
        $tod_to             = ($request->has('tod_to'))?$request->get('tod_to'):'';
        $tod_from  = strtotime($tod_from);
        $tod_to = strtotime($tod_to);

        if($buyer || $order_no || $article || $color || $destination || $season || $despo || $pos || $order_date_from || $order_date_to || $tod_from || $tod_to){


            $query = ProductionOrder::whereHas('TodDetails', function($q) use ($request)
            {


                $article            = ($request->has('article_id'))?$request->get('article_id'):'';
                $color              = ($request->has('color_id'))?$request->get('color_id'):'';
                $tod_from           = ($request->has('tod_from'))?$request->get('tod_from'):'';
                $tod_to             = ($request->has('tod_to'))?$request->get('tod_to'):'';
                $tod_from  = strtotime($tod_from);
                $tod_to = strtotime($tod_to);


                if($article){

                    $q->whereIn('article_id', $article);

                }

                if($color){

                    $q->whereIn('color_id',  $color );

                }
                if(($tod_from)&&($tod_to)){

                    $q->where('tod', '>=', $tod_from)->where('tod', '<=', $tod_to);

                }

            });


            if($buyer){
                $query->whereIn('buyer_id', $buyer);
            }
            if($order_no){

                $query->whereIn('id',$order_no);
            }

            if($season){

                $query->whereIn('season_id', $season);

            }

            if(($order_date_from)&&($order_date_to)){
                $query->where('order_receive_date', '>=', $order_date_from)->where('order_receive_date', '<=', $order_date_to);
            }

            $orders = $query->orderBy('id', 'ASC')->get();
        }
        else{
            $orders = ProductionOrder::has('TodDetails')->get();
        }

        return view('production_order.overallProductionReport', $data)->with('orders', $orders)->with('post_val', $post_val);


    }


    public function overallTodReportSearch(Request $request){

        $data = array();
        $data['pagetitle'] = "TOD Report";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();

        $post_val = $request->all();
        $buyer              = ($request->has('buyer_id'))?$request->get('buyer_id'):'';
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';

        $destination        = ($request->has('destination_id'))?$request->get('destination_id'):'';

        $season             = ($request->has('season_id'))?$request->get('season_id'):'';
        $despo              = ($request->has('despo'))?$request->get('despo'):'';
        $pos                = ($request->has('pos'))?$request->get('pos'):'';
        $order_date_from    = ($request->has('order_date_from'))?$request->get('order_date_from'):'';
        $order_date_to      = ($request->has('order_date_to'))?$request->get('order_date_to'):'';


        $order_date_from = strtotime($order_date_from);
        $order_date_to = strtotime($order_date_to);
        $tod_from           = ($request->has('tod_from'))?$request->get('tod_from'):'';
        $tod_to             = ($request->has('tod_to'))?$request->get('tod_to'):'';

        if($buyer || $order_no || $article || $color || $destination || $season || $despo || $pos || $order_date_from || $order_date_to || $tod_from || $tod_to){


            $query = ProductionOrderDetailTod::query();

            if($article){

                $query->whereIn('article_id', $article);

            }

            if($color){

                $query->whereIn('color_id',  $color );

            }
            if(($tod_from)&&($tod_to)){
                $tod_from  =  date('Y-m-d',strtotime($tod_from));
                $tod_to =  date('Y-m-d',strtotime($tod_to));
                $query->where('tod', '>=', $tod_from)->where('tod', '<=', $tod_to);

            }

            if($buyer){


                $query->whereHas('ProductionOrder', function($q) use ($buyer){
                    $q->whereIn('buyer_id', $buyer);
                });

            }
            if($order_no){

                $query->whereHas('ProductionOrder', function($q) use ($order_no){
                    $q->whereIn('id',$order_no);
                });

            }

            if($season){
                $query->whereHas('ProductionOrder', function($q) use ($season){
                    $q->whereIn('season_id', $season);
                });
            }

            if(($order_date_from)&&($order_date_to)){
                 $query->whereHas('ProductionOrder', function($q) use ($order_date_from,$order_date_to){
                 $q->where('order_receive_date', '>=', $order_date_from)->where('order_receive_date', '<=', $order_date_to);
                 });

            }

            $orders = $query->orderBy('id', 'ASC')->get();

        }
        else{
            $orders = ProductionOrderDetailTod::all();
        }



        return view::make('production_order.overallTodReportView', $data)->with('orders', $orders)->with('post_val', $post_val);



    }

    public function seasonalReportSearch(Request $request){


        $data = array();
        $data['pagetitle'] = "Seasonal Order Value Report";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');
        $data['block_tt_no']        = ProductionOrderDetailPayment::where("block_tt_no","!=",Null)->pluck('block_tt_no','block_tt_no');

        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();

        $post_val = $request->all();
        $buyer              = ($request->has('buyer_id'))?$request->get('buyer_id'):'';
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $destination        = ($request->has('destination_id'))?$request->get('destination_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';
        $season             = ($request->has('season_id'))?$request->get('season_id'):'';
        $despo              = ($request->has('despo'))?$request->get('despo'):'';
        $pos                = ($request->has('pos'))?$request->get('pos'):'';
        $order_date_from    = ($request->has('order_date_from'))?$request->get('order_date_from'):'';
        $order_date_to      = ($request->has('order_date_to'))?$request->get('order_date_to'):'';
        $tod_from           = ($request->has('tod_from'))?$request->get('tod_from'):'';
        $tod_to             = ($request->has('tod_to'))?$request->get('tod_to'):'';
        $block_tt_no        = ($request->has('bktt_no'))?$request->get('bktt_no'):'';
        //print_r($block_tt_no);die;
        if($buyer || $order_no || $article || $color || $destination || $season || $despo || $pos || $order_date_from || $order_date_to || $tod_from || $tod_to){
            $query = ProductionOrder::query();
            if($buyer){
                $i = 0;
                foreach($buyer as $buyer_id){
                    if($i == 0){
                        $query->where('buyer_id', '=', $buyer_id);
                    }
                    else{
                        $query->orWhere('buyer_id', '=', $buyer_id);
                    }
                    $i++;
                }
            }
            if($order_no){
                $i = 0;
                foreach($order_no as $order){
                    if($i == 0){
                        $query->where('id', '=', $order);
                    }
                    else{
                        $query->orWhere('id', '=', $order);
                    }
                    $i++;
                }
            }
            if($article){

                $query->whereHas('Article', function($q) use( $article)
                {
                    $q->whereIn('article_id',  $article);


                });
            }
            if($color){

                $query->whereHas('Color', function($q) use( $color)
                {
                    $q->whereIn('color_id', $color);


                });
            }
            if($season){

                $query->whereIn('season_id', $season);

            }
            if($destination){

                $query->whereHas('Destination', function($q) use( $destination)
                {
                    $q->where('destination_id', '=', $destination);


                });
            }
            if($despo){
            }
            if($block_tt_no){

                $query->whereHas('PaymentDetails', function($q) use( $block_tt_no)
                {
                    $q->whereIn('block_tt_no', $block_tt_no);

                });

                dd($request);

            }
            if($pos){

                $query->whereHas('TodDetails', function($q) use( $pos)
                {
                    $q->where('pos', 'LIKE', '%'.$pos.'%');


                });
            }
            if($order_date_from){
                $query->where('order_receive_date', '>=', $order_date_from);
            }
            if($order_date_to){
                $query->where('order_receive_date', '<=', $order_date_to);
            }
            if(($tod_from) && ($tod_to)){

                $tod_from= strtotime($tod_from);
                $tod_to= strtotime($tod_to);

                $query->whereHas('TodDetails', function($q) use( $tod_from,$tod_to)
                {
                    $q->where('tod', '>=', $tod_from)->where('tod', '<=', $tod_to);

                });

            }

            $orders = $query->orderBy('id', 'ASC')->get();
            
            //echo $query->orderBy('id', 'ASC')->toSql();die();

        }
        else{
            $orders = ProductionOrder::all();
        }

        return view::make('production_order.seasonalOrderValueReportGroupbyOrder', $data)->with('orders', $orders)->with('post_val', $post_val);



    }

    public function seasonalConcentrationReport(){

        $data = array();
        $data['pagetitle'] = "Seasonal Concentration Report";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();

        $toddata =array();
        $post_val =array();

        $begin = new DateTime();
        $begin = $begin->setDate(date('Y'), 1, 1);
        $end = new DateTime();
        $end = $end->setDate(date('Y'), 6, 1);

        
        $data['begin'] = new DateTime();
        $data['begin']->setDate(date('Y'), 1, 1);
        $data['end'] = new DateTime();
        $data['end']->setDate(date('Y'), 6, 1);
        $data['articlelist'] =  ProductionOrderDetailTod::selectRaw('production_order_detail_tod.article_id')->where("tod",">=",$begin->format('Y-m-d'))->where("tod","<=",$end->format('Y-m-d'))->groupBy('article_id')->get();


        $result = array();
        while ($begin <= $end) {
            $index= $begin->format('F,y');
            $result[$index] =array();
            $toddata[$index] = ProductionOrderDetailTod::selectRaw('sum(total_quantity) as quantity_result,production_order_detail_tod.*')->where("tod",">=",$begin->format('Y-m-d'))->where("tod","<=",$begin->modify('last day of this month'))->groupBy('article_id')->orderBy('tod')->get();
            $begin->modify('+1 day');

            foreach($data['articlelist'] as $art){
                $result[$index][$art->Article->article_no] ="N/A";

                foreach ($toddata[$index] as $tod){
                    if($tod->Article->article_no==$art->Article->article_no){
                        $result[$index][$art->Article->article_no] = $tod->quantity_result;
                    }
                }
            }
            $result[$index]['sum']=$toddata[$index]->sum('quantity_result');

        }

        return view::make('production_order.seasonalConcentrationReport', $data)->with('result', $result)->with('post_val', $post_val);

    }
    public function seasonalConcentrationReportSearch(Request $request){

        //dd($_POST);

        $post_val = $request->all();
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $destination        = ($request->has('destination_id'))?$request->get('destination_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';
        $season             = ($request->has('season_id'))?$request->get('season_id'):'';
        $todfrom           = ($request->has('tod_from'))?$request->get('tod_from'):'';
        $todto             = ($request->has('tod_to'))?$request->get('tod_to'):'';
        $frequency = ($request->has('frequency'))?$request->get('frequency'):1;

        $data = array();
        $toddata =array();

        $data['pagetitle'] = "seasonalConcentrationReport";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();

        $begin  = DateTime::createFromFormat('d-m-yy', $todfrom);
        $end = DateTime::createFromFormat('d-m-yy', $todto);
        $data['begin'] =  DateTime::createFromFormat('d-m-yy', $todfrom);
        $data['end'] = DateTime::createFromFormat('d-m-yy', $todto);

        $articlelist = ProductionOrderDetailTod::selectRaw('production_order_detail_tod.article_id')->where("tod",">=",$begin->format('Y-m-d'))->where("tod","<=",$end->format('Y-m-d'));

        $query =ProductionOrderDetailTod::selectRaw('sum(production_order_detail_tod.total_quantity) as quantity_result,production_order_detail_tod.*')->groupBy('article_id')->orderBy('tod',"asc");
        if($order_no || $article || $color || $destination || $season ) {
            if($order_no){
                $query->whereIn('production_order_id',  $order_no);
            }
            if($article){
                $articlelist->whereIn('article_id',  $article);
                $query->whereIn('article_id',  $article);
            }
            if($color){
                $query->whereIn('color_id',  $color);
            }
            if($season){

                $query->whereIn('season_id', $season);

            }

        }
        $data['articlelist'] = $articlelist->groupBy('article_id')->get();
        $result = array();
        while ($begin <= $end) {


            if($frequency==2){
                $index= $begin->format("W");
                $toddata[$index] = $query->where("tod",">=",$begin->format('Y-m-d'))->where("tod","<=",$begin->modify('+1 week'))->get();
                $begin->modify('+1 day');
            }else if($frequency==1){
                $index= $begin->format('F,y');
                $toddata[$index] = $query->where("tod",">=",$begin->format('Y-m-d'))->where("tod","<=",$begin->modify('first day of next month'))->get();
                $begin->modify('+1 day');
            }else if($frequency==3){
                    $index= $begin->format("d-m-Y");
                    $toddata[$index] = $query->where("tod",">=",$begin->format('Y-m-d'))->where("tod","<=",$begin->modify('+1 day'))->get();
                    //$begin->modify('+1 day');

            }


            foreach($data['articlelist'] as $art){
                $result[$index][$art->Article->article_no] ="N/A";
                foreach ($toddata[$index] as $tod){

                    if($tod->Article->article_no==$art->Article->article_no){
                        $result[$index][$art->Article->article_no] = $tod->quantity_result;
                    }
                }
            }
            $result[$index]['sum']=$toddata[$index]->sum('quantity_result');

        }

        return view::make('production_order.seasonalConcentrationReport', $data)->with('result', $result)->with('post_val', $post_val);

    }

    public function shipmentschedule()
    {

        $data = array();

        $orders = ProductionOrderDetailTod::all()->sortByDesc("tod");//->orderBy('tod', 'DESC');
        $data['pagetitle'] = "shipmentschedule";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();

        return view('production_order.shipmentSchedule', $data)->with('orders', $orders);


    }
    public function shipmentscheduleSearch(Request $request)
    {
        //dd($_POST);

        $data = array();
        $data['pagetitle'] = "shipmentschedule";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();

        $post_val = $request->all();
        $buyer              = ($request->has('buyer_id'))?$request->get('buyer_id'):'';
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';

        $destination        = ($request->has('destination_id'))?$request->get('destination_id'):'';

        $season             = ($request->has('season_id'))?$request->get('season_id'):'';
        $despo              = ($request->has('despo'))?$request->get('despo'):'';
        $pos                = ($request->has('pos'))?$request->get('pos'):'';
        $order_date_from    = ($request->has('order_date_from'))?$request->get('order_date_from'):'';
        $order_date_to      = ($request->has('order_date_to'))?$request->get('order_date_to'):'';


        $order_date_from = strtotime($order_date_from);
        $order_date_to = strtotime($order_date_to);
        $tod_from           = ($request->has('tod_from'))?$request->get('tod_from'):'';
        $tod_to             = ($request->has('tod_to'))?$request->get('tod_to'):'';


        if($buyer || $order_no || $article || $color || $destination || $season || $despo || $pos || $order_date_from || $order_date_to || $tod_from || $tod_to){


            $query = ProductionOrderDetailTod::query();

            if($article){

                $query->whereIn('article_id', $article);

            }

            if($color){

                $query->whereIn('color_id',  $color );

            }
            if(($tod_from)&&($tod_to)){
                $tod_from  =  date('Y-m-d',strtotime($tod_from));
                $tod_to =  date('Y-m-d',strtotime($tod_to));
                $query->where('tod', '>=', $tod_from)->where('tod', '<=', $tod_to);

            }

            if($buyer){


                $query->whereHas('ProductionOrder', function($q) use ($buyer){
                    $q->whereIn('buyer_id', $buyer);
                });
                //print_r($post_val);

            }
            if($order_no){

                $query->whereHas('ProductionOrder', function($q) use ($order_no){
                    $q->whereIn('id',$order_no);
                });

            }

            if($season){
                $query->whereHas('ProductionOrder', function($q) use ($season){
                    $q->whereIn('season_id', $season);
                });
            }

            if(($order_date_from)&&($order_date_to)){
                $query->whereHas('ProductionOrder', function($q) use ($order_date_from,$order_date_to){
                    $q->where('order_receive_date', '>=', $order_date_from)->where('order_receive_date', '<=', $order_date_to);
                });


            }

            $orders = $query->orderBy('tod', 'DESC')->get();
           // echo $query->orderBy('id', 'ASC')->toSql();die;

        }
        else{
            $orders = ProductionOrderDetailTod::all();
        }

        //dd($orders);
        return view::make('production_order.shipmentSchedule', $data)->with('orders',$orders)->with('post_val', $post_val);
    }
    public function salescontract()
    {

        $data = array();
        $data['pagetitle'] = "salescontract";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();
        $orders = ProductionOrder::all();
        return view::make('production_order.salesContract', $data)->with('orders',$orders);

    }
    public function salescontractSearch(Request $request)
    {

        $data = array();
        $data['pagetitle'] = "salescontract";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        $data['sizes'] = Size::all();
        $data['destinations'] = Destination::all();

        $post_val = $request->all();
        $buyer              = ($request->has('buyer_id'))?$request->get('buyer_id'):'';
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $destination        = ($request->has('destination_id'))?$request->get('destination_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';
        $season             = ($request->has('season_id'))?$request->get('season_id'):'';
        $despo              = ($request->has('despo'))?$request->get('despo'):'';
        $pos                = ($request->has('pos'))?$request->get('pos'):'';
        $order_date_from    = ($request->has('order_date_from'))?$request->get('order_date_from'):'';
        $order_date_to      = ($request->has('order_date_to'))?$request->get('order_date_to'):'';
        $tod_from           = ($request->has('tod_from'))?$request->get('tod_from'):'';
        $tod_to             = ($request->has('tod_to'))?$request->get('tod_to'):'';

        if($buyer || $order_no || $article || $color || $destination || $season || $despo || $pos || $order_date_from || $order_date_to || $tod_from || $tod_to){
            $query = ProductionOrder::query();
            if($buyer){
                $i = 0;
                foreach($buyer as $buyer_id){
                    if($i == 0){
                        $query->where('buyer_id', '=', $buyer_id);
                    }
                    else{
                        $query->orWhere('buyer_id', '=', $buyer_id);
                    }
                    $i++;
                }
            }
            if($order_no){
                $i = 0;
                foreach($order_no as $order){
                    if($i == 0){
                        $query->where('id', '=', $order);
                    }
                    else{
                        $query->orWhere('id', '=', $order);
                    }
                    $i++;
                }
            }
            if($article){

                $query->whereHas('Article', function($q) use( $article)
                {
                    $q->whereIn('article_id',  $article);


                });
            }
            if($color){

                $query->whereHas('Color', function($q) use( $color)
                {
                    $q->whereIn('color_id', $color);


                });
            }
            if($season){

                $query->whereIn('season_id', $season);

            }
            if($destination){

                $query->whereHas('Destination', function($q) use( $destination)
                {
                    $q->where('destination_id', '=', $destination);


                });
            }
            if($despo){



            }
            if($pos){

                $query->whereHas('TodDetails', function($q) use( $pos)
                {
                    $q->where('pos', 'LIKE', '%'.$pos.'%');


                });
            }
            if($order_date_from){
                $query->where('order_receive_date', '>=', $order_date_from);
            }
            if($order_date_to){
                $query->where('order_receive_date', '<=', $order_date_to);
            }
            if(($tod_from) && ($tod_to)){

                $tod_from= strtotime($tod_from);
                $tod_to= strtotime($tod_to);

                $query->whereHas('TodDetails', function($q) use( $tod_from,$tod_to)
                {
                    $q->where('tod', '>=', $tod_from)->where('tod', '<=', $tod_to);


                });

            }

            $orders = $query->orderBy('id', 'ASC')->get();
            //echo $query->orderBy('id', 'ASC')->toSql();die;

        }
        else{
            $orders = ProductionOrder::all();
        }
        return view::make('production_order.salesContract', $data)->with('orders',$orders)->with('post_val', $post_val);


    }
    
    public function salescontractpdf(Request $request,$order_id){

        $order = ProductionOrder::find($order_id);

        return view::make('production_order.salescontractpdf')->with("order",$order);
    }

    public function downloadsalescontractpdf($order_id)
    {
        $data=array();
        $data["order"] = ProductionOrder::find($order_id);
        $pdf = PDF::loadView('production_order.salepdf',$data)->setPaper('a4', 'portrait');
        return $pdf->download('salescontract'.date('d-m-Y').'.pdf');
    }

    public function orderbreakdownreport()
    {

        $data['pagetitle'] = "Order TOD Size Breakdown Report";
        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');

        $orders = array();
        $data['sizes'] = Size::orderBy('size_no', 'ASC')->get();
        $data['destinations'] = Destination::all();
        return view::make('production_order.orderbreakdownreport',$data)->with("orders",$orders);
    }
    public function orderbreakdownreportresult(Request $request)
    {

        $data =array();

        $data['buyer']              = Buyer::pluck('buyer_name','id');
        $data['production_order']   = ProductionOrder::pluck('order_no','id');
        $data['article']            = Article::pluck('article_no','id');
        $data['color']              = Color::pluck('color_name_flcl','id');
        $data['season']             = Season::pluck('season','id');
        $data['destination']        = Destination::pluck('destination_name','id');
        $data['despo']              = ProductionOrderDespo::pluck('despo','id');


        $data['sizes'] = Size::orderBy('size_no', 'ASC')->get();
        $data['destinations'] = Destination::all();


        $post_val = $request->all();
        $buyer              = ($request->has('buyer_id'))?$request->get('buyer_id'):'';
        $order_no           = ($request->has('order_no'))?$request->get('order_no'):'';
        $article            = ($request->has('article_id'))?$request->get('article_id'):'';
        $color              = ($request->has('color_id'))?$request->get('color_id'):'';

        $destination        = ($request->has('destination_id'))?$request->get('destination_id'):'';

        $season             = ($request->has('season_id'))?$request->get('season_id'):'';
        $despo              = ($request->has('despo'))?$request->get('despo'):'';
        $pos                = ($request->has('pos'))?$request->get('pos'):'';
        $order_date_from    = ($request->has('order_date_from'))?$request->get('order_date_from'):'';
        $order_date_to      = ($request->has('order_date_to'))?$request->get('order_date_to'):'';

        //print_r($order_no[0] );die();
        $data['pagetitle'] = "Order Breakdown Report for ".json_encode( $order_no)." ".json_encode ( $article);

        $order_date_from = strtotime($order_date_from);
        $order_date_to = strtotime($order_date_to);
        $tod_from           = ($request->has('tod_from'))?$request->get('tod_from'):'';
        $tod_to             = ($request->has('tod_to'))?$request->get('tod_to'):'';

        if($buyer || $order_no || $article || $color || $destination || $season || $despo || $pos || $order_date_from || $order_date_to || $tod_from || $tod_to){


            $query = ProductionOrderDetailTod::query();

            if($article){

                $query->whereIn('article_id', $article);

            }

            if($color){

                $query->whereIn('color_id',  $color );

            }
            if(($tod_from)&&($tod_to)){
                $tod_from  =  date('Y-m-d',strtotime($tod_from));
                $tod_to =  date('Y-m-d',strtotime($tod_to));
                $query->where('tod', '>=', $tod_from)->where('tod', '<=', $tod_to);

            }

            if($buyer){


                $query->whereHas('ProductionOrder', function($q) use ($buyer){
                    $q->whereIn('buyer_id', $buyer);
                });

            }
            if($order_no){

                $query->whereHas('ProductionOrder', function($q) use ($order_no){
                    $q->whereIn('id',$order_no);
                });

            }

            if($season){
                $query->whereHas('ProductionOrder', function($q) use ($season){
                    $q->whereIn('season_id', $season);
                });
            }

            if(($order_date_from)&&($order_date_to)){
                $query->whereHas('ProductionOrder', function($q) use ($order_date_from,$order_date_to){
                    $q->where('order_receive_date', '>=', $order_date_from)->where('order_receive_date', '<=', $order_date_to);
                });

            }

            $orders = $query->orderBy('id', 'ASC')->get();

        }
        else{
            $orders = ProductionOrderDetailTod::all();
        }

       

        return view::make('production_order.orderbreakdownreport',$data)->with("orders",$orders)->with('post_val',$post_val);
    }


}