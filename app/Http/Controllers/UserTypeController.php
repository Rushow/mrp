<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Redirect;
use View;
use HTML;
use Validator;
use Session;
use Form;
use App\UserType;
use DateTime;


class UserTypeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

    public function index()
	{
        $userType = UserType::all();

        return view('user_type.index')->with('userType', $userType);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('user_type.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'user_type'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('userType/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $userType = new UserType;

            $userType->user_type       = $request->get('user_type');

            $userType->save();
            Session::flash('message', 'Successfully created a user type!');
            return Redirect::to('userType');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['userType'] = UserType::find($id);

        return view('user_type.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'user_type'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('userType/edit/'.$id)->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $userType['user_type'] = $request->get('user_type');
            $userType['updated_at'] = new DateTime;

            UserType::where('id','=',$id)->update($userType);

            Session::flash('message', 'Successfully updated a user type!');
            return Redirect::to('userType');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        UserType::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a user type!');
        return Redirect::to('userType');
	}

}