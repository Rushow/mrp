<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use App\Machinelist;
use App\Subdivision;
use Illuminate\Http\Request;
use App\Division;


class MachineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $machine = Machinelist::get();
        return view("machine.index",$data)->with('machine',$machine);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();
        $division = Division::pluck('name','id')->toArray();
        $subdivision = Subdivision::pluck('subdivision_name','id')->toArray();
        return view("machine.create",$data)->with('division',$division)->with('subdivision',$subdivision);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = array();

        $machine_id = $request->input('machine_id');
        $machine_name = $request->input('machine_name');
        $division_id= $request->input('department_id');
        $sub_division_id = $request->input('sub_division_id');

        $machine =new Machinelist;
        $machine->machine_id = $machine_id;
        $machine->machine_name =$machine_name;
        $machine->division_id = $division_id;
        $machine->sub_division_id =$sub_division_id;
        $machine->save();

        $division = Division::pluck('name','id')->toArray();
        $subdivision = Subdivision::pluck('subdivision_name','id')->toArray();
        //return view("machine.create",$data)->with('division',$division)->with('subdivision',$subdivision);
        return Redirect::to('machine');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array();
        $division = Division::pluck('name','id')->toArray();
        $subdivision = Subdivision::pluck('subdivision_name','id')->toArray();
        $data["machine"]=Machinelist::find($id);
        return view("machine.edit",$data)->with('division',$division)->with('subdivision',$subdivision);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $machine_id = $request->input('machine_id');
        $machine_name = $request->input('machine_name');
        $division_id= $request->input('department_id');
        $sub_division_id = $request->input('sub_division_id');

        $machine =Machinelist::find($id);
        $machine->machine_id = $machine_id;
        $machine->machine_name =$machine_name;
        $machine->division_id = $division_id;
        $machine->sub_division_id =$sub_division_id;
        $machine->update();

        return Redirect::to('machine');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
