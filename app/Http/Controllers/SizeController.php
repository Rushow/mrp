<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Http\Request;
use DateTime;
use Session;
use App\Size;
use Validator;
use View;

class SizeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public $layout = 'layouts.master';

	public function index()
	{
        $sizes = Size::orderBy('size_no')->get();
//        print_r($sizes);die;

        return view('size.index')->with('sizes', $sizes);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('size.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $rules = array(
            'size_no'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('size/create')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $size = new Size;

            $size->size_no       = $request->get('size_no');

            $size->save();
            Session::flash('message', 'Successfully created a size no!');
            return Redirect::to('size');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = array();
        $data['size'] = Size::find($id);

        return view('size.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        $rules = array(
            'size_no'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return Redirect::to('size/edit')->with(array('error_messages'=>$validator->errors()->all('<div class="alert alert-error">:message</div>')));
        }else{

            $size['size_no'] = $request->get('size_no');
            $size['updated_at'] = new DateTime;

            Size::where('id','=',$id)->update($size);

            Session::flash('message', 'Successfully updated a size!');
            return Redirect::to('size');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Size::where('id','=',$id)->delete();
        Session::flash('message', 'Successfully deleted a size!');
        return Redirect::to('size');
	}

}