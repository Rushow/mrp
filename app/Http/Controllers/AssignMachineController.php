<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Machinelist;
use App\ProductionOrder;
use App\Buyer;
use App\Destination;
use App\Article;
use App\Color;
use App\Season;
use App\ProductionOrderDespo;
use Validator;



class AssignMachineController extends Controller
{
    public function index()
    {
        $data = array();
        $data['machine'] = Machinelist::pluck('machine_name','id')->toArray();
        $data['article'] = Article::pluck('article_no','id')->toArray();
        $data['order'] =  ProductionOrder::all();

        $data['buyer']              = Buyer::pluck('buyer_name','id')->toArray();
        $data['production_order']   = ProductionOrder::pluck('order_no','id')->toArray();
        $data['article']            = Article::pluck('article_no','id')->toArray();
        $data['color']              = Color::pluck('color_name_flcl','id')->toArray();
        $data['season']             = Season::pluck('season','id')->toArray();
        $data['destination']        = Destination::pluck('destination_name','id')->toArray();
        $data['despo']              = ProductionOrderDespo::pluck('despo','id')->toArray();
        
        
        return view("assign_machine.index",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();
        $data['machine'] = Machinelist::pluck('machine_name','id')->toArray();
        $data['article']            = Article::pluck('article_no','id')->toArray();
        $data['production_order']   = ProductionOrder::pluck('order_no','id')->toArray();
        
        
        
        return view("assign_machine.create",$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array();

        $machine_id = $request->input('machine_id');
        $hours_of_operation = $request->input('hours_of_operation');
        $manpower= $request->input('manpower');
        $product_type = $request->input('product_type');
        $style = $request->input('style');
        $unit = $request->input('unit');

        $capacity =new Capacity;
        $capacity->machine_id = $machine_id;
        $capacity->hours_of_operation =$hours_of_operation;
        $capacity->manpower = $manpower;
        $capacity->product_type =$product_type;
        $capacity->style =$style;
        $capacity->unit =$unit;
        $capacity->save();

        $machine = Machinelist::pluck('machine_name','id')->toArray();
        $data['article']            = Article::pluck('article_no','id')->toArray();
        return view("assign_machine.create",$data)->with('machine',$machine);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array();
        $machine = Machinelist::pluck('machine_name','id')->toArray();
        $data['article'] = Article::pluck('article_no','id')->toArray();
        $data['capacity']  =Capacity::find($id);
        return view("assign_machine.edit",$data)->with('machine',$machine);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array();

        $machine_id = $request->input('machine_id');
        $hours_of_operation = $request->input('hours_of_operation');
        $manpower= $request->input('manpower');
        $product_type = $request->input('product_type');
        $style = $request->input('style');
        $unit = $request->input('unit');

        $capacity =Capacity::find($id);
        $capacity->machine_id = $machine_id;
        $capacity->hours_of_operation =$hours_of_operation;
        $capacity->manpower = $manpower;
        $capacity->product_type =$product_type;
        $capacity->style =$style;
        $capacity->unit =$unit;
        $capacity->update();

        return Redirect::to('assignmachine');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    
}
