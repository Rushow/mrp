<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'makeschedule/*',
        'gantt_data',
        'gantt_data/task',
        'gantt_data/*/task',
        'gantt_data/*/task/*',
        'gantt_data/task/*',
        'gantt_data/link',
        'gantt_data/link/*',
        'gantt_data/*/link',
        'gantt_data/*/link/*',

    ];
}
