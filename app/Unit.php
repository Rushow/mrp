<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'unit';

    public function StoreInDetails()
    {
    	return $this->hasMany('App\StoreInDetail', 'unit_id');
    }
    public function StoreOutDetails()
    {
    	return $this->hasMany('App\StoreOutDetail', 'unit_id');
    }
}