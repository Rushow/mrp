<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class ProductionOrderDetailSize extends Model
{
    protected $table = 'production_order_detail_size';
    protected $primaryKey = 'id';

    function ProductionOrderDetail()
    {
    	return $this->belongsTo('App\ProductionOrderDetail', 'production_order_detail_id');
    }

    function Size()
    {
    	return $this->belongsTo('App\Size', 'size_id');
    }
}