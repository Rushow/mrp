<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class SampleOrderDetail extends Model
{
    protected $table = 'sample_order_detail';
    protected $primaryKey = 'id';

    function SampleOrder()
    {
        return $this->belongsTo('App\SampleOrder', 'sample_order_id');
    }

    function Article()
    {
        return $this->belongsTo('App\Article', 'article_id');
    }

    function SampleType()
    {
        return $this->belongsTo('App\SampleType', 'sample_type_id');
    }

    function Color()
    {
        return $this->belongsTo('App\Color', 'color_id');
    }

    function Sizes(){
        return $this->belongsToMany('App\Size', 'sample_order_details_size');
    }

    function Schedule()
    {
        return $this->hasOne('App\SampleSchedule', 'sample_detail_id');
    }

    function Status()
    {
        return $this->hasOne('App\SampleStatus', 'sample_detail_id');
    }
}