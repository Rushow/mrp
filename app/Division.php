<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Division extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    function machine()
    {
        return $this->belongsTo('App\Machine', 'division_id');
    }
    function subdivision()
    {
        return $this->hasMany('App\Subdivision', 'division_id');
    }
}
