<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class ProductionOrderDestination extends Model
{
    protected $table = 'production_order_destination';
    protected $primaryKey = 'id';

    function ProductionOrderDetail()
    {
    	return $this->belongsTo('App\ProductionOrder', 'production_order_id');
    }

    function Destination()
    {
    	return $this->belongsTo('App\Destination', 'destination_id');
    }
}