<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capacity extends Model
{


    function machine()
    {
        return $this->belongsTo('App\Machinelist', 'machine_id');
    }
    function article(){

        return $this->belongsTo('App\Article', 'product_type');
    }
    function construction(){

        return $this->belongsTo('App\Construction', 'style');
    }
}
