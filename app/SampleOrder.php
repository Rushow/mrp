<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class SampleOrder extends Model
{
    protected $table = 'sample_order';
    protected $primaryKey = 'id';

    function Buyer()
    {
        return $this->belongsTo('App\Buyer', 'buyer_id');
    }
    function Details()
    {
    	return $this->hasMany('App\SampleOrderDetail', 'sample_order_id');
    }
    function StoreIns()
    {
        return $this->hasMany('App\StoreIn', 'order_no_id');
    }
}