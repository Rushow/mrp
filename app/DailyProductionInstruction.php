<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyProductionInstruction extends Model
{
    protected $table = 'daily_production_instruction';
    protected $primaryKey = 'id';

    function DailydetailSize(){

        return $this->hasMany('App\DailyProductionInstructionSize', 'daily_production_instruction_id');
    }
    function Order(){

        return $this->belongsTo('App\ProductionOrder', 'production_order_id');
    }
    function Article()
    {
        return $this->belongsTo('App\Article', 'article_id');
    }
    function Color()
    {
        return $this->belongsTo('App\Color', 'color_id');
    }
    function SizeGroup()
    {
        return $this->belongsTo('App\SizeGroup', 'size_group_id');
    }
    function Component(){
        return $this->belongsTo('App\DailyProductionComponent', 'component_id');
    }

    function Division(){
        return $this->belongsTo('App\Division', 'department_id');
    }

    function SubDivision(){

        return $this->belongsTo('App\Subdivision', 'sub_department_id');
    }
}
