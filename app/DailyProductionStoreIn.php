<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyProductionStoreIn extends Model
{
    protected $table = 'daily_production_store_in';
    protected $primaryKey = 'id';

    function DailydetailSize(){

        return $this->hasMany('App\DailyProductionDetailSize', 'daily_production_detail_id');
    }
    function Order(){

        return $this->belongsTo('App\ProductionOrder', 'production_order_id');
    }
    function Article()
    {
        return $this->belongsTo('App\Article', 'article_id');
    }
    function Color()
    {
        return $this->belongsTo('App\Color', 'color_id');
    }
    function SizeGroup()
    {
        return $this->belongsTo('App\SizeGroup', 'size_group_id');
    }
    function Component(){
        return $this->belongsTo('App\DailyProductionComponent', 'store_unit_component_id');
    }

    function Division(){
        return $this->belongsTo('App\Division', 'department_id');
    }
    
    function SubDivision(){
        
        return $this->belongsTo('App\Subdivision', 'sub_department_id');
    }
}
