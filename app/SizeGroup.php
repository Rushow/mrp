<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class SizeGroup extends Model
{
    protected $table = 'size_group';

    function size(){
        return $this->belongsToMany('App\Size','size_group_size')->orderBy('size_no','ASC');
    }
}