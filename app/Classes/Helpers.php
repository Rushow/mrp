<?php
namespace App\Classes;
use App\ProductionOrder;
use App\Destination;
use App\Article;
use App\Color;
use App\Season;
use App\ProductionOrderDetail;
use App\Size;
use App\ProductionOrderDetailTod;
use App\ProductionOrderDetailSize;


class Helpers {

    public static function getSampleOrderNoById($id){
        return SampleOrder::where('id', $id)->select('sample_order_no')->get()[0]['sample_order_no'];
    }

    public static function getColorNameById($id){
        return Color::where('id', $id)->select('color_name_flcl')->get()[0]['color_name_flcl'];
    }

    public static function getSizeNoById($id){
        return Size::where('id', $id)->select('size_no')->get()[0]['size_no'];
    }

    public static function getSizeGroupNameById($id){
        return SizeGroup::where('id', $id)->select('size_group_name')->get()[0]['size_group_name'];
    }

    public static function getDepartmentNameById($id){
        return Department::where('id', $id)->select('name')->get()[0]['name'];
    }

    public static function getArticleNoById($id){
        return Article::where('id', $id)->select('article_no')->get()[0]['article_no'];
    }

    public static function getArticleTypeById($id){
        return ArticleType::where('id', $id)->select('article_type')->get()[0]['article_type'];
    }

    public static function getCategoryNameById($id){
        return Category::where('id', $id)->select('category_name')->get()[0]['category_name'];
    }

    public static function getSeasonById($id){
        return Season::where('id', $id)->select('season')->get()[0]['season'];
    }

    public static function getBuyerTemplateById($id){
        return BuyerTemplate::where('id', $id)->select('buyer_template')->get()[0]['buyer_template'];
    }

    public static function getPOSizeQuantity($order_id, $size_id){
        $qty = ProductionOrderDetailSize::where('production_order_detail_id', $order_id)->where('size_id', $size_id)->select('quantity')->get()->toArray();
        if($qty){
            return $qty[0]['quantity'];
        }
    }

    public static function getOrdersByDestination($destination_id){
        $order_details = ProductionOrderDetail::where('destination_id', $destination_id)->get();
        $i = 0;
        foreach($order_details as $detail){
            $orders[$i] = ProductionOrder::where('production_order_id', $detail->production_order_id)->get();
            $i++;
        }

        return $orders;
    }

    public static function getDistinctTOD(){
        $tod = ProductionOrderDetailTod::distinct()->select('order_date')->orderBy('order_date', 'ASC')->get();
        return $tod;
    }

    public static function getOrderDetailsByTOD($order_date){
        $order_details = ProductionOrderDetail::where('order_date', $order_date)->get();
        return $order_details;
    }

    public static function getTotalQuantityByDestinationAndSize($destination_id, $size_id){
        $order_details = ProductionOrderDetail::where('destination_id', $destination_id)->get();
        $total_qty = 0;
        foreach($order_details as $detail){
            $detail_sizes = ProductionOrderDetailSize::where('production_order_detail_id', $detail->id)->get();
            foreach($detail_sizes as $size){
                if($size->size_id == $size_id){
                    $total_qty += $size->quantity;
                }
            }
        }
        return $total_qty;
    }

    public static function getDestinationNameById($id){
        return Destination::where('id', $id)->select('destination_name')->get()[0]['destination_name'];
    }

    public static function getDestinationCodeById($id){
        return Destination::where('id', $id)->select('destination_code')->get()[0]['destination_code'];
    }

    public static function getTotalQuantityByOrderAndTOD($order_id, $tod){
        $order_details = ProductionOrderDetail::where('production_order_id', $order_id)->where('order_date', $tod)->get();
        $total = 0;
        foreach($order_details as $detail){
            $total += $detail->total_quantity;
        }
        return $total;
    }

    public static function getTotalQuantityOfOrder($order_id){
        $sizes = Size::all();
        $order_total = 0;
        foreach($sizes as $size){
            $total_qty = 0;
            $order_details = ProductionOrderDetailTod::where('production_order_id', '=', $order_id)->get();
            foreach($order_details as $details){
                $qty = ($details->id && $size->id)?Helpers::getPOSizeQuantity($details->id, $size->id):0;
                $total_qty += $qty;
            }

            $order_total += $total_qty;
        }
        return $order_total;
    }

    public static function testhelper(){
        echo "Say Hi and die!";
    }
}
