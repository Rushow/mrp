<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyProductionComponent extends Model
{
    protected $table = 'daily_production_component';
    protected $primaryKey = 'id';

    function Combination(){

        return $this->hasMany('App\DailyProductionComponentCombination', 'component_id');
    }
}
