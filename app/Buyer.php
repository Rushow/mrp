<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */
namespace App;

use Illuminate\Database\Eloquent\Model;


class Buyer extends Model
{
    protected $table = 'buyer';

    function SampleOrder()
    {
    	return $this->hasMany('App\SampleOrder', 'buyer_id');
    }

    function BuyerTemplate()
    {
    	return $this->belongsTo('App\BuyerTemplate', 'buyer_template_id');
    }

    function Address()
    {
    	return $this->belongsTo('App\Address', 'address_id');
    }

}