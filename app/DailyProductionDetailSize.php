<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyProductionDetailSize extends Model
{
    protected $table = 'daily_production_detail_size';
    protected $primaryKey = 'id';

    function Size(){

        return $this->belongsTo('App\Size', 'size_id');
    }
}
