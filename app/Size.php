<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $table = 'size';


    function sizeGroup(){
        return $this->belongsToMany('App\SizeGroup','size_group_size');
    }

    function SampleOrderDetail(){
    	return $this->belongsToMany('App\SampleOrderDetail', 'sample_order_details_size');
    }

}