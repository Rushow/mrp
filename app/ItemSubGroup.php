<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ 
namespace App;  
use Illuminate\Database\Eloquent\Model;

class ItemSubGroup extends Model
{
    protected $table = 'item_sub_group';

    function group()
    {
        return $this->belongsTo('App\ItemGroup', 'group_id');
    }

}