<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class ProductionOrderDetailShipment extends Model
{
    protected $table = 'production_order_detail_shipment';
    protected $primaryKey = 'id';

    function ProductionOrder()
    {
        return $this->belongsTo('App\ProductionOrder', 'production_order_id');
    }
    function ConsigneeBank()
    {
        return $this->belongsTo('App\Bank', 'consignee_bank_id');
    }
    function ShipperBank()
    {
        return $this->belongsTo('App\Bank', 'shipper_bank_id');
    }
    function TermsOfDelivery()
    {
        return $this->belongsTo('App\TermsOfDelivery', 'terms_of_delivery_id');
    }
    function Forwarder()
    {
        return $this->belongsTo('App\Forwarder', 'forwarder_id');
    }
    function Address(){
        return $this->belongsTo('App\Address', 'address_id');
    }

}