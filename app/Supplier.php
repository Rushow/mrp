<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'supplier';

    function Address()
    {
    	return $this->belongsTo('App\Address', 'address_id');
    }

}