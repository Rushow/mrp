<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class ProductionOrderDetailTod extends Model
{
    protected $table = 'production_order_detail_tod';
    protected $primaryKey = 'id';

    function ProductionOrder()
    {
        return $this->belongsTo('App\ProductionOrder', 'production_order_id');
    }
    
    function Article()
    {
        return $this->belongsTo('App\Article', 'article_id');
    }
    function Destination()
    {
        return $this->belongsTo('App\Destination', 'destination_id');
    }
    
    function Color()
    {
        return $this->belongsTo('App\Color', 'color_id');
    }
    function SizeGroup()
    {
        return $this->belongsTo('App\SizeGroup', 'size_group_id');
    }
    function Sizes(){
        return $this->hasMany('App\ProductionOrderDetailSize', 'production_order_detail_id');
    }
    function PaymentDetails()
    {
        return $this->hasOne('App\ProductionOrderDetailPayment', 'production_order_id');
    }
    function ShipmentDetails()
    {
        return $this->hasOne('App\ProductionOrderDetailShipment', 'production_order_id');
    }
    function ShippingMode(){

        return $this->belongsTo('App\ShippingMode', 'shipping_mode_id');
    }
    function ShippingBlocks()
    {
        return $this->hasMany('App\ProductionOrderDetailShippingBlock', 'production_order_id');
    }

//    function DailyProductionStoreOut(){
//
//        return $this->hasMany('App\DailyProductionStoreOut', 'production_order_detail_tod_id');
//
//    }
  

    function DailyProductionStoreOut($article='',$color=''){

        $q=$this->hasMany('App\DailyProductionStoreOut', 'production_order_id')->where('department_id','=', 6);
        if($article){

            $q->whereIn("article","=",$article);
        }
        if($color){
            $q->whereIn("article","=",$color);
        }

        return $q->sum('quantity');
    }

}