<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class ProductionOrderDetailPayment extends Model
{
    protected $table = 'production_order_detail_payment';
    protected $primaryKey = 'id';

    function ProductionOrder()
    {
        return $this->belongsTo('App\ProductionOrder', 'production_order_id');
    }
    function PaymentTerm()
    {
        return $this->belongsTo('App\PaymentTerm', 'payment_term_id');
    }
    function Currency()
    {
        return $this->belongsTo('App\Currency', 'currency_id');
    }
    function ProductionOrderTod()
    {
        return $this->belongsTo('App\ProductionOrderDetailTod', 'production_order_id');
    }


}