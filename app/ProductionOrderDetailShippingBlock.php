<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class ProductionOrderDetailShippingBlock extends Model
{
    protected $table = 'production_order_detail_shipping_block';
    protected $primaryKey = 'id';

    function ProductionOrder()
    {
        return $this->belongsTo('App\ProductionOrder', 'production_order_id');
    }
    function ShippingMode()
    {
        return $this->belongsTo('App\ShippingMode', 'shipping_mode_id');
    }
//    function Destinations()
//    {
//        return $this->hasMany('App\Destination', 'destination_id');
//    }
    function ShippingBlockDestination()
    {
        return $this->belongsTo('App\ProductionOrderDetailShippingBlockDestination','id', 'production_order_detail_shipping_block_id');
    }

}