<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ 
namespace App;  

use Illuminate\Database\Eloquent\Model;



class Article extends Model
{
    protected $table = 'article';

    function ArticleType()
    {
        return $this->belongsTo('App\ArticleType');
    }

    function Construction()
    {
        return $this->belongsTo('App\Construction');
    }

    function CustomerGroup()
    {
        return $this->belongsTo('App\CustomerGroup','customer_group_id','id');
    }

}