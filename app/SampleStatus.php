<?php

class SampleStatus extends Model
{
    protected $table = 'sample_statuses';
    protected $fillable = array('sample_detail_id');

    function SampleOrderDetail()
    {
        return $this->belongsTo('App\SampleOrderDetail', 'sample_detail_id');
    }
    function DevelopmentStatus()
    {
    	return $this->belongsTo('App\DevelopmentStatus', 'sample_status_id');
    }

}