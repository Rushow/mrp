<?php
namespace App;  use Illuminate\Database\Eloquent\Model;
class StoreIn extends Model
{
    protected $table = 'store_ins';
    protected $primaryKey = 'id';
    protected $fillable = array('warehouse_id', 'supplier_id', 'challan_no', 'in_type', 'order_no_id', 'store_in_date', 'remarks');

    function Details()
    {
    	return $this->hasMany('App\StoreInDetail', 'store_in_id');
    }
    function Warehouse()
    {
    	return $this->belongsTo('App\Warehouse', 'warehouse_id');
    }
    function Supplier()
    {
    	return $this->belongsTo('App\Supplier', 'supplier_id');
    }
    function SampleOrder()
    {
        return $this->belongsTo('App\SampleOrder', 'order_no_id');
    }

    function ProductionOrder()
    {
        return $this->belongsTo('App\ProductionOrder', 'production_order_no');
    }
}