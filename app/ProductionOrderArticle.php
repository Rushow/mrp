<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class ProductionOrderArticle extends Model
{
    protected $table = 'production_order_article';
    protected $primaryKey = 'id';

    function ProductionOrderDetail()
    {
    	return $this->belongsTo('App\ProductionOrder', 'production_order_id');
    }

    function Article()
    {
    	return $this->belongsTo('App\Article', 'article_id');
    }
}