<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;


class Color extends Model
{
    protected $table = 'color';

    public function getColorDetailForListAttribute(){
        return $this->attributes['color_name_flcl'].' '.$this->attributes['hm_code'];
    }

}