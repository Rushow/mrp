<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ 

namespace App; 


use Illuminate\Database\Eloquent\Model;

class ProductionOrder extends Model
{
    protected $table = 'production_order';
    protected $primaryKey = 'id';

    function Department()
    {
        return $this->belongsTo('App\Department', 'department_id');
    }
    function Buyer()
    {
        return $this->belongsTo('App\Buyer', 'buyer_id');
    }
    
    function BlockTtNo()
    {
        return $this->belongsTo('App\BlockTtNo', 'block_tt_no_id');
    }
    function Article()
    {
        return $this->hasMany('App\ProductionOrderArticle', 'production_order_id');
    }
    function ArticleType()
    {
        return $this->belongsTo('App\ArticleType', 'article_type_id');
    }
    function Category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }
    function Color()
    {
        return $this->hasMany('App\ProductionOrderColor', 'production_order_id');
    }
    function SizeGroup()
    {
        return $this->belongsTo('App\SizeGroup', 'size_group_id');
    }
    function Season()
    {
        return $this->belongsTo('App\Season', 'season_id');
    }
    function Status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }
    function Destination()
    {
        return $this->hasMany('App\ProductionOrderDestination', 'production_order_id');
    }
    function Despo()
    {
        return $this->hasMany('App\ProductionOrderDespo', 'production_order_id');
    }
    function TodDetails()
    {
        return $this->hasMany('App\ProductionOrderDetailTod', 'production_order_id','id');
    }
    function PaymentDetails()
    {
        return $this->hasOne('App\ProductionOrderDetailPayment', 'production_order_id');
    }
    function ShipmentDetails()
    {
        return $this->hasOne('App\ProductionOrderDetailShipment', 'production_order_id');
    }
    function ShippingBlocks()
    {
        return $this->hasMany('App\ProductionOrderDetailShippingBlock', 'production_order_id');
    }
    
    /*
    function PoColor(){

        return $this->hasMany('ProductionOrderColor', 'production_order_id');
    }
    function PoArticle(){

        return $this->hasMany('ProductionOrderArticle', 'production_order_id');
    }
    */

    function TodDetailsSearch($color,$article)
    {

        //dd($color);
        //echo  $this->hasMany('ProductionOrderDetailTod')->whereIn('article_id', $color)->whereIn('color_id',  $article)->toSql();die;
        return $this->hasMany('App\ProductionOrderDetailTod')->whereIn('article_id', $article)->whereIn('color_id',  $color);
    }



}