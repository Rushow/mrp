<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class SizeGroupSize extends Model
{
    protected $table = 'size_group_size';

    function Size()
    {
        return $this->belongsTo('App\Size', 'size_id');
    }

}