<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class GanttTask extends Model
{
    protected $table = "gantt_tasks";
    public $primaryKey = "id";
    public $timestamps = true;

    function link()
    {
        return $this->belongsTo('App\GanttLink', 'source');
    }
    function subdivision(){

        return $this->belongsTo('App\Subdivision', 'sub_division_id');
    }
    function line(){

        return $this->belongsTo('App\Line', 'id');
    }
    function linename(){

        return $this->belongsTo('App\Line', 'id','line_id');
    }
}
