<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyProductionInstructionSize extends Model
{
    protected $table = 'daily_production_instruction_size';
    protected $primaryKey = 'id';
}
