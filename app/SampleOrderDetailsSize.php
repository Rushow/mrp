<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class SampleOrderDetailsSize extends Model
{
    protected $table = 'sample_order_details_size';
    protected $primaryKey = 'id';

    function SampleOrderDetail()
    {
    	return $this->belongsTo('App\SampleOrderDetail', 'sample_order_detail_id');
    }

    function Size()
    {
    	return $this->belongsTo('App\Size', 'size_id');
    }
}