<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rusho
 * Date: 1/29/14
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */ namespace App;  use Illuminate\Database\Eloquent\Model;

class ProductionOrderDetailShippingBlockDestination extends Model
{
    protected $table = 'production_order_detail_shipping_block_destination';
    protected $primaryKey = 'id';

    function ProductionOrderDetailShippingBlock()
    {
        return $this->belongsTo('App\ProductionOrderDetailShippingBlock', 'production_order_detail_shipping_block_id');
    }
    function Destination()
    {
        return $this->belongsTo('App\Destination', 'destination_id');
    }

}