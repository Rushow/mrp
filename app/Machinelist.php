<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Machinelist extends Model
{
    function capacity()
    {
        return $this->hasMany('App\Machine', 'machine_id');
    }
    function division(){
        return $this->belongsTo('App\Division', 'division_id');
    }
    function subdivision(){

        return $this->belongsTo('App\Subdivision', 'sub_division_id');
    }
    
}
