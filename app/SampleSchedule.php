<?php

class SampleSchedule extends Model
{
    protected $table = 'sample_schedule';
    protected $fillable = array('schedule_date', 'sample_detail_id');

    function SampleOrderDetail()
    {
        return $this->belongsTo('App\SampleOrderDetail', 'sample_detail_id');
    }

}