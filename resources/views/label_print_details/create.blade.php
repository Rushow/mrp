@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-6 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Add Last No</h2>
            <?= Form::open(array('url' => 'labelPrintDetails', 'method' => 'post', 'id' => 'labelPrintDetailsForm')); ?>
            <div class="form-group">
                <?= Form::label('print_code', 'Tag/Print Code', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::text('print_code', null, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <?= Form::label('type', 'Type', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::text('type', null, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <?= Form::label('remarks', 'Remarks', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::textarea('remarks', null, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Submit', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#labelPrintDetailsForm").validate({
            rules: {
                print_code: "required",
                type: "required"
            },
            messages: {
                print_code: "Please enter Tag/Print Code",
                type: "Please enter Type"
            }
        });
    });
</script>

@stop