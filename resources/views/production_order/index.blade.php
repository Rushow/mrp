@extends('layouts.master')
@section('head')

@parent
{{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')

<?php
$buyer_val = (isset($post_val['buyer_id'])) ? $post_val['buyer_id'] : null;
$order_no_val = (isset($post_val['order_no'])) ? $post_val['order_no'] : null;
$article_val = (isset($post_val['article_id'])) ? $post_val['article_id'] : null;
$color_val = (isset($post_val['color_id'])) ? $post_val['color_id'] : null;
$season_val = (isset($post_val['season_id'])) ? $post_val['season_id'] : null;
$destination_val = (isset($post_val['destination_id'])) ? $post_val['destination_id'] : null;
$despo_val = (isset($post_val['despo'])) ? $post_val['despo'] : null;
$pos_val = (isset($post_val['pos'])) ? $post_val['pos'] : null;
$order_date_from_val = (isset($post_val['order_date_from'])) ? $post_val['order_date_from'] : null;
$order_date_to_val = (isset($post_val['order_date_to'])) ? $post_val['order_date_to'] : null;
$tod_from_val = (isset($post_val['tod_from'])) ? $post_val['tod_from'] : null;
$tod_to_val = (isset($post_val['tod_to'])) ? $post_val['tod_to'] : null;
?>

<div class="row">
    <div class="col-md-12 sidebar">
        <?php if(Session::has('message')){ ?>
        <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
        <script>
            $('#alert').delay(2000).fadeOut(400)
        </script>
        <?php } ?>
        <data class="row">
            <div class="col-sm-10">
                @if (!Auth::user()->isViewer())
                    <a href="<?= URL::to('productionOrder/create')?>" class="btn btn-warning">Create PO</a>
                @endif
            </div>
            <div class="col-sm-2">
            </div>
        </data>
        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <!-- form start -->
                        <?= Form::open(array('url' => 'productionOrder/search', 'method' => 'post')); ?>
                        <div class="box-body">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= Form::label('buyer_id', 'Buyer', array('class' => 'control-label')); ?>
                                    {{ Form::select('buyer_id', $buyer, $buyer_val, $attributes = array('id'=>'buyer_id', 'name'=>'buyer_id[]', 'data-placeholder' => 'Select an Buyer', 'multiple'=>'', 'class'=>'form-control select2', 'onchange' => 'buyerChange();')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('order_no', 'Select Order No', array('class' => 'control-label')); ?>
                                    {{ Form::select('order_no', $production_order, $order_no_val, $attributes = array('id'=>'order_no', 'name'=>'order_no[]', 'data-rel'=>'chosen', 'data-placeholder'=>'Select Order No', 'multiple'=>'', 'class'=>'form-control select2')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('article_id', 'Article No', array('class' => 'control-label')); ?>
                                    {{ Form::select('article_id', $article, $article_val, $attributes = array('id'=>'article_id', 'name'=>'article_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Article', 'multiple'=>'')) }}
                                </div>
                                <div class="form-group template01">
                                    <?= Form::label('destination_id', 'Destination', array('class' => 'control-label')); ?>
                                    {{ Form::select('destination_id', $destination, $destination_val, $attributes = array('id'=>'destination_id', 'name'=>'destination_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Destination', 'multiple'=>'')) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= Form::label('color_id', 'Color', array('class' => 'control-label')); ?>
                                    {{ Form::select('color_id', $color, $color_val, $attributes = array('id'=>'color_id', 'name'=>'color_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Color', 'multiple'=>'')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('season_id', 'Season', array('class' => 'control-label')); ?>
                                    {{ Form::select('season_id', $season, $season_val, $attributes = array('id'=>'season_id', 'name'=>'season_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Season', 'multiple'=>'')) }}
                                </div>
                                <div class="form-group template02">
                                    <?= Form::label('despo', 'Dispo', array('class' => 'control-label')); ?>
                                    {{ Form::select('despo', $despo, $despo_val, $attributes = array('id'=>'despo', 'name'=>'despo[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Dispo', 'multiple'=>'')) }}
                                </div>
                                <div class="form-group template02">
                                    <?= Form::label('pos', 'Pos', array('class' => 'control-label')); ?>
                                    {{ Form::text('pos', $pos_val,array('class'=>'form-control','id'=>'pos')) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-daterange form-group">
                                    <?= Form::label('order_date', 'Order Date', array('class' => 'control-label')); ?>
                                    <div class="input-daterange input-group" id="datepicker">
                                        {{ Form::text('order_date_from', $order_date_from_val,array('class'=>'input-sm form-control datepicker','id'=>'order_date_from')) }}
                                        <span class="input-group-addon">To</span>
                                        {{ Form::text('order_date_to', $order_date_to_val,array('class'=>'input-sm form-control datepicker','id'=>'order_date_to')) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= Form::label('tod', 'TOD', array('class' => 'control-label')); ?>
                                    <div class="input-daterange input-group" id="datepicker">
                                        {{ Form::text('tod_from', $tod_from_val,array('class'=>'input-sm form-control datepicker','id'=>'tod_from')) }}
                                        <span class="input-group-addon">To</span>
                                        {{ Form::text('tod_to', $tod_to_val,array('class'=>'input-sm form-control datepicker','id'=>'tod_to')) }}
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-2 col-md-offset-11">
                                <button type="submit" class="btn btn-success">Search</button>
                            </div>

                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                PO List
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">PO List</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Order No<span class="sort-icon"><span></th>
                                    <th>Buyer Name<span class="sort-icon"><span></th>
                                    <th>Article No<span class="sort-icon"><span></th>
                                    <th>Season<span class="sort-icon"><span></th>
                                    <th>Color<span class="sort-icon"><span></th>
                                    <th>Destination/Dispo<span class="sort-icon"><span></th>
                                    <th>Quantity<span class="sort-icon"><span></th>
                                    <th>TOD<span class="sort-icon"><span></th>
                                    <th>Status<span class="sort-icon"><span></th>
                                    @if (!Auth::user()->isViewer())
                                        <th style="width: 270px !important;">Action</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($orders as $key => $value){
                                $buyer_template = $value->buyer->buyerTemplate->buyer_template;//Buyer::find($value->buyer_id)->buyerTemplate->buyer_template;
                                ?>
                                <tr>
                                    <td><?= $value->order_no ?>&nbsp;</td>
                                    <td><?= ($value->buyer_id)?$value->buyer->buyer_name:'' ?>&nbsp;</td>
                                    <td>
                                        <?php

                                        foreach($value->Article as $k=>$article){
                                            if($k==0){
                                                echo $article->Article->article_no;
                                            }else{
                                                echo ", ".$article->Article->article_no;
                                            }

                                        } ?>&nbsp;
                                    </td>
                                    <td><?= ($value->season_id)?$value->season->season:'' ?>&nbsp;</td>
                                    <td>
                                        <?php
                                        foreach($value->Color as $k=>$color){
                                            if($k==0){
                                                echo $color->Color->color_name_flcl;
                                            }else{
                                                echo ", ".$color->Color->color_name_flcl;
                                            }

                                        }
                                         ?>&nbsp;
                                    </td>
                                    <td>

                                       <? if($buyer_template == "Template 01"){
                                                foreach($value->Destination as $k=>$des){
                                                    if($k==0){
                                                        echo  $des->Destination->destination_code;
                                                    }else{
                                                        echo  ", ".$des->Destination->destination_code;
                                                    }

                                                }
                                            }else{

                                                foreach($value->Despo as $k=>$despo){
                                                    if($k==0){
                                                        echo  $des->despo;
                                                    }else{
                                                        echo  ",".$des->despo;
                                                    }
                                                }
                                            }
                                       ?>

                                    </td>
                                    <td><?= ($value->id)?App\Classes\Helpers::getTotalQuantityOfOrder($value->id):'' ?>&nbsp;</td>

                                    <td style="width:100px;">

                                    {{date('Md,Y',strtotime($value->TodDetails->min('tod')))}} -
                                    {{date('Md,Y',strtotime($value->TodDetails->max('tod')))}}


                                    </td>
                                    <td><?= ($value->status)?$value->status->status:'' ?>&nbsp;</td>
                                    @if (!Auth::user()->isViewer())
                                        <td>
                                            <a class="btn btn-primary btn-sm" href="<?= URL::to('productionOrder/details/'.$value->id);?>">Details</a>
                                            <a class="btn btn-info btn-sm" href="<?= URL::to('productionOrder/'.$value->id).'/edit';?>">Edit</a>
                                            {{ Form::open(array('class' => 'btn btn-danger btn-xs','url' => 'productionOrder/'.$value->id, 'method' => 'delete', 'style' => 'padding: 3px 0; border-color: transparent !important;')) }}
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs', 'style' => 'display:block;')) }}
                                            {{ Form::close() }}
                                        </td>
                                    @endif
                                </tr>
                                <?php } ?>

                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
        </section>


    </div>
</div>


@stop

@section('page-script')
    <!-- Select2 -->
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js') }}

    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}

    {{HTML::script('https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js')}}
    {{HTML::script('https://cdn.datatables.net/buttons/1.3.1/js/buttons.bootstrap.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')}}
    {{HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js')}}
    {{HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js')}}
@stop

@section('plugin-script')
    <script type="text/javascript">
        $(function () {
            $(".select2").select2();

            $("#order_date_from").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#order_date_to").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#tod_from").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#tod_to").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $(".template01").hide();
            $(".template02").hide();

            <?php if($destination_val){ ?>
                $("#destination_id").parent('.form-group').show();
            <?php } ?>
            <?php if($despo_val){ ?>
                $("#despo").parent('.form-group').show();
            <?php } ?>
            <?php if($pos_val){ ?>
                $("#pos").parent('.form-group').show();
            <?php } ?>

            var table=$('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "scrollX": true,
                "lengthChange": true,
                "buttons": [  {
                    extend: 'excel',
                    message: 'This print was produced using the Print button for DataTables',
                    exportOptions: {
                        columns: ':visible',
                    }
                }, 'pdf', 'colvis' ]
            });
            table.buttons().container()
                    .appendTo( '#example1_wrapper .col-sm-6:eq(0)' );
        });

        function buyerChange(){
            var buyer = $('#buyer_id').val();
            var buyer_template = '';
            if(buyer){
                $(".template01").hide();
                $(".template02").hide();
                $.each( buyer, function( key, value ) {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: '<?= Url("buyer/getBuyerInfo") ?>',
                        data: 'id='+value,
                        async: true,
                        success: function(data){
                            buyer_template = data.buyer_template;
                            if(buyer_template == "Template 01"){
                                $(".template01").show();
                            }
                            if(buyer_template == "Template 02"){
                                $(".template02").show();
                            }
                        }
                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getProductionOrderByBuyerArray") ?>',
                    data: 'buyer='+buyer,
                    async: true,
                    success: function(data){
                        $("#order_no").removeAttr('readonly');
                        $("#order_no").removeAttr('disabled');
                        $("#order_no").html(data.order_no);
                    }
                });
            }
            else{
                $(".template01").hide();
                $(".template02").hide();
                $("#order_no").html('<option value="">Select</option>');
                $("#order_no").attr('readonly', 'readonly');
                $("#order_no").attr('disabled', 'disabled');
            }
        }

        function orderChange(){
            var order = $('#order_no').val();
            if(order){
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getArticleByOrderArray") ?>',
                    data: 'order='+order,
                    async: true,
                    success: function(data){
                        $("#article_id").removeAttr('readonly');
                        $("#article_id").removeAttr('disabled');
                        $("#article_id").html(data.article);
                    }
                });

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getColorByOrderArray") ?>',
                    data: 'order='+order,
                    async: true,
                    success: function(data){
                        $("#color_id").removeAttr('readonly');
                        $("#color_id").removeAttr('disabled');
                        $("#color_id").html(data.color);
                    }
                });
            }
            else{
                $("#article_id").html('<option value="">Select</option>');
                $("#article_id").attr('readonly', 'readonly');
                $("#article_id").attr('disabled', 'disabled');

                $("#color_id").html('<option value="">Select</option>');
                $("#color_id").attr('readonly', 'readonly');
                $("#color_id").attr('disabled', 'disabled');
            }
        }
    </script>
@stop
