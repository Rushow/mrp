@extends('layouts.master')
@section('head')

@parent
@stop
@section('header')
    {{ HTML::style('admin-lte/plugins/select2/select2.min.css') }}

@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('messages');

        if(isset($error_messages)){
            foreach($error_messages as $message){
                echo $message;
            }
        }
        ?>

        <?php
        $buyer_template = App\Buyer::find($order->buyer_id)->buyerTemplate->buyer_template;
        ?>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <div class="box box-primary" style="text-align: center;">
                        <div class="box-body">
                            <h3 class="box-title">Order No <?=$order->order_no?></h3>
                            <h4>Buyer Name:  <?=$order->buyer->buyer_name?></h4>
                        </div>
                        <div class="btn_btn_poka pull-right">
                            <a href="{{url('pdf/productionOrder/details')}}/{{$id}}" class="btn btn-primary">Export PDF</a>
                        </div>
                    </div><!-- /.box -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Custom Tabs -->
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs tabs">
                                    <li class="active"><a href="#summery" data-toggle="tab">Summery</a></li>
                                    <li><a href="#todDetails" data-toggle="tab">TOD Details</a></li>
                                    <li><a href="#quantityDetails" data-toggle="tab">Quantity Details</a></li>
                                    <li><a href="#paymentDetails" data-toggle="tab">Payment Details</a></li>
                                    <li><a href="#material" data-toggle="tab">Material Details</a></li>
                                    <li><a href="#packing" data-toggle="tab">Packing Details</a></li>
                                    <li><a href="#shippingDetails" data-toggle="tab">Shipping Details</a></li>
                                    <li><a href="#others" data-toggle="tab">Others</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="summery">
                                        <div id="box-paymentDetails">
                                            <div class="box box-info">
                                                <div class="box-header">
                                                    <i class="fa fa-th"></i>
                                                    <h3 class="box-title">Summery</h3>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h3 class="box-title"><b>Order No: </b><?=$order->order_no?></h3>
                                                            <h4><b>Buyer Name: </b> <?=$order->buyer->buyer_name?></h4>
                                                            <br>
                                                            <h5>
                                                                <b>Articles: </b>
                                                                <?php
                                                                $total = count($order->articles);
                                                                $i = 1;
                                                                foreach($order->articles as $article){
                                                                    echo App\Classes\Helpers::getArticleNoById($article);
                                                                    echo ($total > $i)?', ':'';
                                                                    $i++;
                                                                }
                                                                ?>
                                                            </h5>
                                                            <h5>
                                                                <?php
                                                                if($order->season_id){
                                                                    echo "<b>Season: </b>".$order->season->season;
                                                                }
                                                                ?>
                                                            </h5>
                                                            <h5>
                                                                <b>Colors: </b>
                                                                <?php
                                                                $total = count($order->colors);
                                                                $i = 1;
                                                                foreach($order->colors as $color){
                                                                    echo App\Classes\Helpers::getColorNameById($color);
                                                                    echo ($total > $i)?', ':'';
                                                                    $i++;
                                                                }
                                                                ?>
                                                            </h5>
                                                            <h5>
                                                                <?php
                                                                if($buyer_template == "Template 01"){
                                                                    echo "<b>Destinations: </b>";
                                                                    $total = count($order->destinations);
                                                                    $i = 1;
                                                                    foreach($order->destinations as $destination){
                                                                        echo App\Classes\Helpers::getDestinationNameById($destination);
                                                                        echo ($total > $i)?', ':'';
                                                                        $i++;
                                                                    }
                                                                }
                                                                else if($buyer_template == "Template 02"){
                                                                    echo "<b>Despo: </b>";
                                                                    $total = count($order->despos);
                                                                    $i = 1;
                                                                    foreach($order->despos as $despo){
                                                                        echo $despo;
                                                                        echo ($total > $i)?', ':'';
                                                                        $i++;
                                                                    }
                                                                }
                                                                ?>
                                                            </h5>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    if(count($order_payment_details) > 0){
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <h3 class="box-title">Payment Details</h3>
                                                            <h5><b>Payment Term: </b><?=$order_payment_details->PaymentTerm->term?></h5>
                                                            <h5><b>Currency: </b><?=$order_payment_details->Currency->name?></h5>
                                                            <h5><b>Commission: </b><?=$order_payment_details->commission?>%</h5>
                                                            <h5><b>Total Value: </b>$<?=$order_payment_details->total_value?></h5>
                                                            <h5><b>Commission Value: </b>$<?=$order_payment_details->commission_value?></h5>
                                                            <h5><b>Invoice Value: </b>$<?=$order_payment_details->invoice_value?></h5>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php if($order_payment_details->block_tt_no){ ?>
                                                                <h5><b>Block TT No: </b><?=$order_payment_details->block_tt_no?></h5>
                                                            <?php }?>
                                                            <?php if($order_payment_details->block_tt_order_from){ ?>
                                                                <h5><b>Block TT From: </b><?=date('m/d/Y', $order_payment_details->block_tt_order_from)?></h5>
                                                            <?php }?>
                                                            <?php if($order_payment_details->block_tt_order_to){ ?>
                                                                <h5><b>Block TT No: </b><?=date('m/d/Y', $order_payment_details->block_tt_order_to)?></h5>
                                                            <?php }?>
                                                            <?php if($order_payment_details->sales_contact_no){ ?>
                                                                <h5><b>Sales Contact No: </b>$<?=$order_payment_details->sales_contact_no?></h5>
                                                            <?php }?>
                                                            <?php if($order_payment_details->remarks){ ?>
                                                                <h5><b>Remarks: </b>$<?=$order_payment_details->remarks?></h5>
                                                            <?php }?>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                    <?php
                                                    if(count($order_tod_details) > 0){
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h3 class="box-title">TOD Details</h3>

                                                            <div class="box">
                                                                <div class="box-header">
                                                                    {{--<h3 class="box-title">Condensed Full Width Table</h3>--}}
                                                                </div><!-- /.box-header -->
                                                                <div class="box-body no-padding">
                                                                    <table class="table table-condensed">
                                                                        <tr>
                                                                            <th style="width: 15px">#</th>
                                                                            <th>Article</th>
                                                                            <th>Color</th>
                                                                            <th>Destination</th>
                                                                            <th>TOD</th>
                                                                            <th>No. of Days Need Ship Advance</th>
                                                                            <th>Cut Off</th>
                                                                            <th>ETD</th>
                                                                        </tr>
                                                            <?php
                                                            if(count($order_tod_details) > 0){
                                                                $i = 1;
                                                                foreach($order_tod_details as $key => $value){
                                                            ?>
                                                                        <tr>
                                                                            <td><?=$i++?></td>
                                                                            <td><?= $value->article->article_no ?></td>
                                                                            <td><?= $value->color->color_name_flcl ?></td>
                                                                            <td><?= $value->destination->destination_name ?></td>
                                                                            <td><?= $value->tod ?></td>
                                                                            <td><?= $value->days_ship_advance ?></td>
                                                                            <td><?= $value->destination->cut_off_day ?></td>
                                                                            <td><?= $value->destination->etd_day ?></td>
                                                                        </tr>
                                                            <?php
                                                                }
                                                            }
                                                            ?>

                                                                    </table>
                                                                </div><!-- /.box-body -->
                                                            </div><!-- /.box -->
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                    <?php
                                                    if(count($order_tod_details) > 0){
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h3 class="box-title">Quantity Details</h3>

                                                            <div class="box">
                                                                <div class="box-header">
                                                                    {{--<h3 class="box-title">Condensed Full Width Table</h3>--}}
                                                                </div><!-- /.box-header -->
                                                                <div class="box-body no-padding">
                                                                    <table class="table table-condensed">
                                                                        <tr>
                                                                            <th style="width: 15px">#</th>
                                                                            <th>Article</th>
                                                                            <th>Color</th>
                                                                            <th>Destination</th>
                                                                            <th>Total Quantity</th>
                                                                            <th>Unit Value</th>
                                                                            <th>Invoice Value</th>
                                                                        </tr>
                                                            <?php
                                                            if(count($order_tod_details) > 0){
                                                                $i = 1;
                                                                foreach($order_tod_details as $key => $value){
                                                            ?>
                                                                        <tr>
                                                                            <td><?=$i++?></td>
                                                                            <td><?= $value->article->article_no ?></td>
                                                                            <td><?= $value->color->color_name_flcl ?></td>
                                                                            <td><?= $value->destination->destination_name ?></td>
                                                                            <td><span class="badge bg-green"><?= $value->total_quantity ?></span></td>
                                                                            <td><span class="badge bg-yellow"><?= '$ '.$value->unit_value ?></span></td>
                                                                            <td><span class="badge bg-light-blue"><?= '$ '.($value->unit_value * $value->total_quantity) ?></span></td>
                                                                        </tr>
                                                            <?php
                                                                }
                                                            }
                                                            ?>

                                                                    </table>
                                                                </div><!-- /.box-body -->
                                                            </div><!-- /.box -->
                                                        </div>
                                                    </div>
                                                    <?php } ?><?php
                                                    if(count($order_shipment_details) > 0){
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h3 class="box-title">Shipment Details</h3>
                                                            <h5><b>Consignee Bank: </b><?=$order_shipment_details->ConsigneeBank->name?></h5>
                                                            <h5><b>Shipper Bank: </b><?=$order_shipment_details->ShipperBank->name?></h5>
                                                            <h5><b>Terms of Delivery: </b><?=$order_shipment_details->TermsOfDelivery->terms_of_delivery?></h5>
                                                            <h5><b>Forwarder: </b><?=$order_shipment_details->Forwarder->forwarder_name?></h5>
                                                        </div>
                                                    </div>
                                                    <?php } ?><?php
                                                    if(count($order_tod_details) > 0){
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h3 class="box-title">Shipment Mode Details</h3>

                                                            <div class="box">
                                                                <div class="box-header">
                                                                </div><!-- /.box-header -->
                                                                <div class="box-body no-padding">
                                                                    <table class="table table-condensed">
                                                                        <tr>
                                                                            <th>SL.</th>
                                                                            <th>Shipping Mode</th>
                                                                            <th>Destination</th>
                                                                        </tr>
                                                            <?php
                                                             foreach($order_tod_details as $key => $value){
                                                            ?>
                                                                        <tr>
                                                                            <td><?=$key+1?></td>
                                                                            <td><? if($value->ShippingMode){echo $value->ShippingMode->shipping_mode;} ?></td>
                                                                            <td><? if($value->Destination){echo $value->Destination->destination_name;} ?></td>
                                                                        </tr>
                                                            <?php

                                                            }
                                                            ?>

                                                                    </table>
                                                                </div><!-- /.box-body -->
                                                            </div><!-- /.box -->
                                                        </div>
                                                    </div>
                                                    <?php } ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.tab-pane -->
                                    <div class="tab-pane" id="todDetails">
                                        <?= Form::open(array('url' => 'productionOrder/todDetailsStore', 'method' => 'post', 'id' => 'todDetailsStoreForm')); ?>
                                        {{ Form::hidden('order_id', $order->id, array('id'=> 'order_id')) }}
                                        {{ Form::hidden('buyer_template', $buyer_template, array('id'=> 'buyer_template')) }}
                                        {{ Form::hidden('total_tod_block', count($order_tod_details), array('id'=> 'total_tod_block')) }}
                                        <div id="box-todDetails">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-offset-10 col-md-1" style="margin-bottom: 20px;">
                                                <a class="btn btn-info" style="color: #fff;" onclick="addMoreTodDetailBlock();">Add More +</a>
                                            </div>
                                            <div class="col-md-offset-10 col-md-6">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                                <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                                            </div>
                                        </div>
                                        <?= Form::close(); ?>
                                    </div><!-- /.tab-pane -->
                                    <div class="tab-pane" id="quantityDetails">
                                        <?= Form::open(array('url' => 'productionOrder/quantityDetailsStore', 'method' => 'post', 'id' => 'quantityDetailsStoreForm')); ?>
                                        {{ Form::hidden('order_id', $order->id, array('id'=> 'order_id')) }}
                                        {{ Form::hidden('buyer_template', $buyer_template, array('id'=> 'buyer_template')) }}
                                        {{ Form::hidden('total_quantity_block', count($order_tod_details), array('id'=> 'total_quantity_block')) }}
                                        <div id="box-quantityDetails">
                                            {{--<div class="row" style="margin-bottom: 20px;">--}}
                                                {{--<a class="btn btn-info col-md-offset-11 col-md-1" style="color: #fff;" onclick="addQuantityDetailBlock(0);">Add More +</a>--}}
                                            {{--</div>--}}


                                        </div>
                                        <div class="row">
                                            <div class="col-md-offset-10 col-md-6">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                                <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                                            </div>
                                        </div>
                                        <?= Form::close(); ?>
                                    </div><!-- /.tab-pane -->
                                    <div class="tab-pane" id="paymentDetails">
                                        <?= Form::open(array('url' => 'productionOrder/paymentDetailsStore', 'method' => 'post', 'id' => 'paymentDetailsStoreForm')); ?>
                                        {{ Form::hidden('order_id', $order->id, array('id'=> 'order_id')) }}
                                        {{ Form::hidden('buyer_id', $order->buyer_id, array('id'=> 'buyer_id')) }}
                                        {{ Form::hidden('buyer_template', $buyer_template, array('id'=> 'buyer_template')) }}
                                        <div id="box-paymentDetails">
                                            <div class="box box-info">
                                                <div class="box-header">
                                                    <i class="fa fa-th"></i>
                                                    <h3 class="box-title">Payment Details</h3>
                                                </div>
                                                <div class="box-body">

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <?= Form::label('payment_term_id', 'Payment Term', array('class' => 'control-label')); ?>
                                                                    {{ Form::select('payment_term_id', $payment_term, ($order_payment_details)?$order_payment_details->payment_term_id:'', $attributes = array('id'=>'payment_term_id', 'name'=>'payment_term_id', 'class'=>'form-control')) }}
                                                                </div>
                                                                <div class="form-group">
                                                                    <?= Form::label('currency_id', 'Currency', array('class' => 'control-label')); ?>
                                                                    {{ Form::select('currency_id', $currency, ($order_payment_details)?$order_payment_details->currency_id:'', $attributes = array('id'=>'currency_id','name'=>'currency_id', 'class'=>'form-control')) }}
                                                                </div>
                                                                <div class="form-group template02">
                                                                    <?= Form::label('purchase_contact_no', 'Purchase Contact No', array('class' => 'control-label')); ?>
                                                                    {{ Form::text('purchase_contact_no', ($order_payment_details)?$order_payment_details->purchase_contact_no:'', array('class'=>'form-control','id'=>'purchase_contact_no')) }}
                                                                </div>
                                                                <div class="form-group template01">
                                                                    <?= Form::label('commission', 'Commission', array('class' => 'control-label')); ?>
                                                                    <div class="input-group">
                                                                        {{ Form::text('commission', ($order_payment_details)?$order_payment_details->commission:'', array('class'=>'form-control','id'=>'commission', 'onkeyup' => 'calculateCommission();')) }}
                                                                        <span class="input-group-addon">%</span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <?= Form::label('total_value', 'Total Value', array('class' => 'control-label')); ?>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::text('total_value', ($order_payment_details)?$order_payment_details->total_value:'', array('class'=>'form-control','id'=>'total_value', 'readonly'=>'readonly')) }}
                                                                    </div>
                                                                </div>
                                                                <div class="form-group template01">
                                                                    <?= Form::label('commission_value', 'Commission Value', array('class' => 'control-label')); ?>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::text('commission_value', ($order_payment_details)?$order_payment_details->commission_value:'', array('class'=>'form-control','id'=>'commission_value', 'readonly'=>'readonly')) }}
                                                                    </div>
                                                                </div>
                                                                <div class="form-group template01">
                                                                    <?= Form::label('invoice_value', 'Invoice Value', array('class' => 'control-label')); ?>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::text('invoice_value', $order->invoice_value, array('class'=>'form-control','id'=>'invoice_value', 'readonly'=>'readonly')) }}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group template01">
                                                                    <?= Form::label('block_tt', 'Block TT', array('class' => 'control-label')); ?>
                                                                    <div class="input-group">
                                                                        {{ Form::radio('block_tt', 'Yes', '', array('class'=>'','id'=>'block_tt')) }}&nbsp;&nbsp;<?= Form::label('block_tt', 'Yes', array('class' => 'control-label')); ?><br>
                                                                        {{ Form::radio('block_tt', 'No', '', array('class'=>'','id'=>'block_tt', 'checked' => 'checked')) }}&nbsp;&nbsp;<?= Form::label('block_tt', 'No', array('class' => 'control-label')); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group template01">
                                                                    <?= Form::label('block_tt_no', 'Block TT No', array('class' => 'control-label')); ?>
                                                                    {{ Form::text('block_tt_no', ($order_payment_details)?$order_payment_details->block_tt_no:'', array('class'=>'form-control','id'=>'block_tt_no', 'onchange' => 'setSalesContactNo();')) }}
                                                                </div>
                                                                <div class="form-group template01">
                                                                    <?= Form::label('block_tt_order_from', 'Block TT From', array('class' => 'control-label')); ?>
                                                                    {{ Form::text('block_tt_order_from', ($order_payment_details)?date('m/d/Y', $order_payment_details->block_tt_order_from):'',array('class'=>'form-control datepicker','id'=>'block_tt_order_from', 'onchange' => 'setSalesContactNo();')) }}
                                                                </div>
                                                                <div class="form-group template01">
                                                                    <?= Form::label('block_tt_order_to', 'Block TT To', array('class' => 'control-label')); ?>
                                                                    {{ Form::text('block_tt_order_to', ($order_payment_details)?date('m/d/Y', $order_payment_details->block_tt_order_to):'',array('class'=>'form-control datepicker','id'=>'block_tt_order_to', 'onchange' => 'setSalesContactNo();')) }}
                                                                </div>
                                                                <div class="form-group template01">
                                                                    <?= Form::label('sales_contact_no', 'Sales Contact No', array('class' => 'control-label')); ?>
                                                                    {{ Form::text('sales_contact_no', ($order_payment_details)?$order_payment_details->sales_contact_no:'',array('class'=>'form-control','id'=>'sales_contact_no')) }}
                                                                </div>
                                                                <div class="form-group template02">
                                                                    <?= Form::label('less_financial_charge', 'Less Financial Charge', array('class' => 'control-label')); ?>
                                                                    <div class="row">
                                                                        <div class="col-md-5">
                                                                            {{ Form::text('less_financial_charge_percent', ($order_payment_details)?$order_payment_details->less_financial_charge_percent:'',array('class'=>'form-control col-md-4','id'=>'less_financial_charge_percent', 'onkeyup' => 'calculateLessFinancialCharge();')) }}
                                                                        </div>
                                                                        <div class="col-md-1">&nbsp;&nbsp;%</div>
                                                                        <div class="col-md-5">
                                                                            {{ Form::text('less_financial_charge', ($order_payment_details)?$order_payment_details->less_financial_charge:'',array('class'=>'form-control col-md-4','id'=>'less_financial_charge', 'readonly'=>'readonly')) }}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group template02">
                                                                    <?= Form::label('less_claim_bonus', 'Less Claim Bonus', array('class' => 'control-label')); ?>
                                                                    <div class="row">
                                                                        <div class="col-md-5">
                                                                            {{ Form::text('less_claim_bonus_percent', ($order_payment_details)?$order_payment_details->less_claim_bonus_percent:'',array('class'=>'form-control','id'=>'less_claim_bonus_percent', 'onkeyup' => 'calculateLessClaimBonus();')) }}
                                                                        </div>
                                                                        <div class="col-md-1">&nbsp;&nbsp;%</div>
                                                                        <div class="col-md-5">
                                                                            {{ Form::text('less_claim_bonus', ($order_payment_details)?$order_payment_details->less_claim_bonus:'',array('class'=>'form-control','id'=>'less_claim_bonus', 'readonly'=>'readonly')) }}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group template02">
                                                                    <?= Form::label('less_novi_deduction', 'Less Novi Deduction', array('class' => 'control-label')); ?>
                                                                    <div class="row">
                                                                        <div class="col-md-5">
                                                                            {{ Form::text('less_novi_deduction_percent', ($order_payment_details)?$order_payment_details->less_novi_deduction_percent:'',array('class'=>'form-control','id'=>'less_novi_deduction_percent', 'onkeyup' => 'calculateLessNoviDeduction();')) }}
                                                                        </div>
                                                                        <div class="col-md-1">&nbsp;&nbsp;%</div>
                                                                        <div class="col-md-5">
                                                                            {{ Form::text('less_novi_deduction', ($order_payment_details)?$order_payment_details->less_novi_deduction:'',array('class'=>'form-control','id'=>'less_novi_deduction', 'readonly'=>'readonly')) }}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group template02">
                                                                    <?= Form::label('sub_total_value', 'Sub Total Value', array('class' => 'control-label')); ?>
                                                                    {{ Form::text('sub_total_value', ($order_payment_details)?$order_payment_details->sub_total_value:'',array('class'=>'form-control','id'=>'sub_total_value', 'readonly'=>'readonly')) }}
                                                                </div>
                                                                <div class="form-group template02">
                                                                    <?= Form::label('less_lc_deadline', 'Less L/C Deadline', array('class' => 'control-label')); ?>
                                                                    <div class="row">
                                                                        <div class="col-md-5">
                                                                            {{ Form::text('less_lc_deadline_percent', ($order_payment_details)?$order_payment_details->less_lc_deadline_percent:'',array('class'=>'form-control','id'=>'less_lc_deadline_percent', 'onkeyup' => 'calculateLessLcDeadline();')) }}
                                                                        </div>
                                                                        <div class="col-md-1">&nbsp;&nbsp;%</div>
                                                                        <div class="col-md-5">
                                                                            {{ Form::text('less_lc_deadline', ($order_payment_details)?$order_payment_details->less_lc_deadline:'',array('class'=>'form-control','id'=>'less_lc_deadline', 'readonly'=>'readonly')) }}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group template02">
                                                                    <?= Form::label('final_amount', 'Final Amount', array('class' => 'control-label')); ?>
                                                                    {{ Form::text('final_amount', ($order_payment_details)?$order_payment_details->final_amount:'',array('class'=>'form-control','id'=>'final_amount', 'readonly'=>'readonly')) }}
                                                                </div>
                                                                <div class="form-group">
                                                                    <?= Form::label('remarks', 'Remarks', array('class' => 'control-label')); ?>
                                                                    {{ Form::textarea('remarks', ($order_payment_details)?$order_payment_details->remarks:'',array('class'=>'form-control','id'=>'remarks')) }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-offset-10 col-md-6">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                                <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                                            </div>
                                        </div>
                                        <?= Form::close(); ?>
                                    </div><!-- /.tab-pane -->
                                    <div class="tab-pane" id="material">
                                    </div><!-- /.tab-pane -->
                                    <div class="tab-pane" id="packing">
                                    </div><!-- /.tab-pane -->
                                    <div class="tab-pane" id="shippingDetails">
                                        <?= Form::open(array('url' => 'productionOrder/shippingDetailsStore', 'method' => 'post', 'id' => 'shippingDetailsStoreForm')); ?>
                                        {{ Form::hidden('order_id', $order->id, array('id'=> 'order_id')) }}
                                        {{ Form::hidden('buyer_template', $buyer_template, array('id'=> 'buyer_template')) }}
                                        {{ Form::hidden('total_shipping_block', 0, array('id'=> 'total_shipping_block')) }}

                                        <div id="box-allShipmentDetails">
                                            <div class="box box-info">
                                                <div class="box-header">
                                                    <i class="fa fa-th"></i>
                                                    <h3 class="box-title">Shipment Details</h3>
                                                </div>
                                                <div class="box-body">

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <?= Form::label('consignee_bank_id', 'Consignee Bank', array('class' => 'control-label')); ?>
                                                                {{ Form::select('consignee_bank_id', $consignee_bank, ($order_shipment_details)?$order_shipment_details->consignee_bank_id:'', $attributes = array('id'=>'consignee_bank_id', 'name'=>'consignee_bank_id', 'class'=>'form-control'))}}
                                                            </div>
                                                            <div class="form-group">
                                                                <?= Form::label('shipper_bank_id', 'Shipper Bank', array('class' => 'control-label')); ?>
                                                                {{ Form::select('shipper_bank_id', $shipper_bank, ($order_shipment_details)?$order_shipment_details->shipper_bank_id:'', $attributes = array('id'=>'shipper_bank_id', 'name'=>'shipper_bank_id', 'class'=>'form-control')) }}
                                                            </div>
                                                            <div class="form-group">
                                                                <?= Form::label('terms_of_delivery_id', 'Terms of Delivery', array('class' => 'control-label')); ?>
                                                                {{ Form::select('terms_of_delivery_id', $terms_of_delivery, ($order_shipment_details)?$order_shipment_details->terms_of_delivery_id:'', $attributes = array('id'=>'terms_of_delivery_id', 'name'=>'terms_of_delivery_id', 'class'=>'form-control')) }}
                                                            </div>
                                                            <div class="form-group">
                                                                <?= Form::label('forwarder_id', 'Forwarder', array('class' => 'control-label')); ?>
                                                                {{ Form::select('forwarder_id', $forwarder, ($order_shipment_details)?$order_shipment_details->forwarder_id:'', $attributes = array('id'=>'forwarder_id', 'name'=>'forwarder_id', 'class'=>'form-control')) }}
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-offset-10 col-md-6">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                                <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                                            </div>
                                        </div>
                                        <?= Form::close(); ?>
                                    </div><!-- /.tab-pane -->
                                    <div class="tab-pane" id="others">
                                    </div><!-- /.tab-pane -->
                                </div><!-- /.tab-content -->
                            </div><!-- nav-tabs-custom -->
                        </div><!-- /.col -->
                    </div>
                </div>

                <img src="{{ asset('images/loading.gif') }}" class="loading" alt="loading..."  />
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>


@stop

@section('page-script')
    <!-- Select2 -->
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js') }}
@stop

@section('plugin-script')
<script>
    $(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    });
    $(function() {
        $(".loading").hide();
        $("#tod").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format:  'yyyy-mm-dd'
        });
        $("#block_tt_order_from").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
        $("#block_tt_order_to").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
        $('#despo').tagsInput({width:'auto', defaultText: 'Add a Dispo'});

        <?php
        if($buyer_template == "Template 01"){
        ?>
        $(".template02").hide();
        <?php
        }else if($buyer_template == "Template 02"){
        ?>
        $(".template01").hide();
        <?php } ?>

        $(".select2").select2();


        $('#block_tt_no').parents('.form-group').hide();
        $('#block_tt_order_from').parents('.form-group').hide();
        $('#block_tt_order_to').parents('.form-group').hide();

        $('input:radio[name="block_tt"]').change(function(){
            if ($(this).val() == 'Yes') {
                $('#block_tt_no').parents('.form-group').show();
                $('#block_tt_order_from').parents('.form-group').show();
                $('#block_tt_order_to').parents('.form-group').show();
                $('#sales_contact_no').attr('readonly', true);
            }
            else {
                $('#block_tt_no').parents('.form-group').hide();
                $('#block_tt_order_from').parents('.form-group').hide();
                $('#block_tt_order_to').parents('.form-group').hide();
                $('#sales_contact_no').removeAttr('readonly');
            }
        });

    });

    $(function() {
        $('.nav-tabs a[href="#todDetails"]').click(function(event){
            event.preventDefault();
            $("#todDetails").find(".box-info").remove();
            $("#todDetails").append($(".loading"));
            $("#todDetails .loading").show();
            $(this).show();
            <?php
            if(count($order_tod_details) > 0){
                foreach($order_tod_details as $key => $value){
            ?>
                addTodDetailBlock(<?=$value->id?>,<?=$key+1?>);
            <?php
            }}else{
            ?>
                //addTodDetailBlock(0,1);
            <?php
            }
            ?>
        });
        $('.nav-tabs a[href="#quantityDetails"]').click(function(event){
            event.preventDefault();
            $("#quantityDetails").find(".box-info").remove();
            $("#quantityDetails").append($(".loading"));
            $("#quantityDetails .loading").show();
            $(this).show();

            <?php
            if(count($order_tod_details) > 0){
                foreach($order_tod_details as $key => $value){
            ?>
                addQuantityDetailBlock(<?=$value->id?>,<?=$key+1?>);
            <?php

            }}else{
            ?>
                addQuantityDetailBlock(0,1);
            <?php
            }
            ?>
        });
        //old shipping
    });
    $(document).ready(function() {
        $("#productionOrderForm").validate({
            rules: {
                buyer_id: "required",
                order_no: "required",
                season_id: "required",
                order_receive_date: "required"
            }
        });

        $.validator.addClassRules("Required", {
            required: true
        });

    });
    function addMoreTodDetailBlock(){
        var tod_block_no = $("#total_tod_block").val();
        tod_block_no = parseInt(tod_block_no) + 1;

        $.ajax({
            type:'POST',
            url: '<?= URL::to('productionOrder/todDetails'); ?>',
            data: 'id='+<?=$order->id?>+'&tod_block_no='+tod_block_no+'&tod_id=0',
            dataType: "html",
            async: true,
            success: function(response)
            {
                $("#todDetails .loading").hide();
                $('#box-todDetails').append(response);
                $("#total_tod_block").val(tod_block_no);

                // activate box-widget
                $.AdminLTE.boxWidget.activate();

                $("#tod_"+tod_block_no).datepicker({format:  'yyyy-mm-dd'});
                <?php
                if($buyer_template == "Template 01"){
                ?>
                $(".template02").hide();
                <?php
                }else if($buyer_template == "Template 02"){
                ?>
                $(".template01").hide();
                <?php } ?>
            }
        });
    }
    function addTodDetailBlock(tod_id,tod_block_no){
//        var tod_block_no = $("#total_tod_block").val();
//        tod_block_no = parseInt(tod_block_no) + 1;
        //alert(tod_block_no);
        $.ajax({
            type:'POST',
            url: '<?= URL::to('productionOrder/todDetails'); ?>',
            data: 'id='+<?=$order->id?>+'&tod_block_no='+tod_block_no+'&tod_id='+tod_id,
            dataType: "html",
            async: true,
            success: function(response)
            {
                $("#todDetails .loading").hide();
                $('#box-todDetails').append(response);
                $("#total_tod_block").val(tod_block_no);

                // activate box-widget
                $.AdminLTE.boxWidget.activate();

                $("#tod_"+tod_block_no).datepicker({format:  'yyyy-mm-dd'});
                <?php
                if($buyer_template == "Template 01"){
                ?>
                $(".template02").hide();
                <?php
                }else if($buyer_template == "Template 02"){
                ?>
                $(".template01").hide();
                <?php } ?>
            }
        });
    }

    function addQuantityDetailBlock(quantity_id,quantity_block_no){
        //var quantity_block_no = $("#total_quantity_block").val();
        //quantity_block_no = parseInt(quantity_block_no) + 1;//alert(quantity_block_no);
        $.ajax({
            type:'POST',
            url: '<?= URL::to('productionOrder/quantityDetails'); ?>',
            data: 'id='+<?=$order->id?>+'&quantity_block_no='+quantity_block_no+'&quantity_id='+quantity_id,
            dataType: "html",
            async: true,
            success: function(response)
            {
                $("#quantityDetails .loading").hide();
                $('#box-quantityDetails').append(response);
                $("#total_quantity_block").val(quantity_block_no);

                // activate box-widget
                $.AdminLTE.boxWidget.activate();

                <?php
                if($buyer_template == "Template 01"){
                ?>
                $(".template02").hide();
                <?php
                }else if($buyer_template == "Template 02"){
                ?>
                $(".template01").hide();
                <?php } ?>
            }
        });
    }

    function addShippingDetailBlock(shipping_id){
        var shipping_block_no = $("#total_shipping_block").val();
        shipping_block_no = parseInt(shipping_block_no) + 1;
        $.ajax({
            type:'POST',
            url: '<?= URL::to('productionOrder/shippingDetails'); ?>',
            data: 'id='+<?=$order->id?>+'&shipping_block_no='+shipping_block_no+'&shipping_id='+shipping_id,
            dataType: "html",
            async: true,
            success: function(response)
            {
                console.log(response);
                $("#shippingDetails .loading").hide();
                $('#box-shippingDetails').append(response);
                $("#total_shipping_block").val(shipping_block_no);

                // activate box-widget
                $.AdminLTE.boxWidget.activate();

                <?php
                if($buyer_template == "Template 01"){
                ?>
                $(".template02").hide();
                <?php
                }else if($buyer_template == "Template 02"){
                ?>
                $(".template01").hide();
                <?php } ?>
                $(".select2").select2();
            }
        });
    }

    function changeBuyer(){
        var buyer = $("#buyer_id").val();
        var buyer_template = '';
        if(buyer!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("buyer/getBuyerInfo") ?>',
                data: 'id='+buyer,
                async: true,
                success: function(data){
                    buyer_template = data.buyer_template;
                    if(buyer_template == "Template 01"){
                        $(".template01").show();
                        $(".template02").hide();
                    }
                    else if(buyer_template == "Template 02"){
                        $(".template02").show();
                        $(".template01").hide();
                    }
                }
            });
        }
    }

    function removeTodBlock(item){
        var tod_detail_id = $(item).parents('.box-info').find(".tod_detail_id").val();
//        console.log(tod_detail_id);
        $.ajax({
            type:'POST',
            url: '<?= URL::to('productionOrder/removeTodDetails'); ?>',
            data: 'tod_detail_id='+tod_detail_id,
            dataType: "html",
            async: true,
            success: function(response)
            {
                $(item).parents('.box-info').remove();
            }
        });
    }

    function removeShippingBlock(item){
        var shipping_block_id = $(item).parents('.box-info').find(".shipping_detail_id").val();
        $.ajax({
            type:'POST',
            url: '<?= URL::to('productionOrder/removeShippingBlock'); ?>',
            data: 'shipping_block_id='+shipping_block_id,
            dataType: "html",
            async: true,
            success: function(response)
            {
                $(item).closest('.box-info').remove();
                $(".select2").select2();
            }
        });
    }

    /*function setColorHMCode(item){
        var val = $(item).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("color/getColorHMCode") ?>',
                data: 'id='+val,
                success: function(data){
                    $("#color_code").val(data.hm_code);

                }
            });
        }
    }*/

    function setSalesContactNo(){
        var sales_val = 'FLCL';
        var buyer = $("#buyer_id").val();
        var buyer_name = '';
        if(buyer!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("buyer/getBuyerInfo") ?>',
                data: 'id='+buyer,
                async: true,
                success: function(data){
                    buyer_name = data.buyer_name;
                }
            });
        }
        if(buyer_name){
            sales_val += '/' + buyer_name;
        }
        if($('#block_tt_order_from').val()){
            sales_val += '/' + $('#block_tt_order_from').val();
        }
        if($("#block_tt_order_to").val()){
            sales_val += '-' + $("#block_tt_order_to").val();
        }
        if($("#block_tt_no").val()){
            sales_val += '/' + $("#block_tt_no").val();
        }
        $("#sales_contact_no").val(sales_val);
    }

    function getSizesQuantityModal(item){
        var size_group_id = $(item).parents('.box-body').find('.size_group_id').val();
        var quantity_block_no = $(item).parents('.box-body').find('.quantity_block_no').val();
        var tod_detail_id = $(item).parents('.box-body').find('.tod_detail_id').val();
//        console.log(size_group_id);
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '<?= Url("productionOrder/getSizeQuantityByTod") ?>',
            data: 'tod_detail_id='+tod_detail_id,
            async: true,
            success: function(data){
                var j = 1;
                if(data.sizes.length){

                    $(item).parents('.box-body').find('.size-error').hide();
                    $(item).parents('.box-body').find('.size-box').show();
                    $(item).parents('.box-body').find('.size-box').html('');

                    $(item).parents('.box-body').find('.size-box').append("<div class='form-group'>");
                    $(item).parents('.box-body').find('.size-box').append("<label class='col-sm-2 control-label' for=''>Size</label>");
                    $(item).parents('.box-body').find('.size-box').append("<label class='col-sm-10 control-label' for=''>Quantity</label>");
                    $.each(data.sizes, function(i, val){
                        $(item).parents('.box-body').find('.size-box').append("<div class='form-group'>");
                        $(item).parents('.box-body').find('.size-box').append("<label class='col-sm-2 control-label' for='size_"+quantity_block_no+"_"+j+"'>"+val.size_no+"</label><input id='size_"+quantity_block_no+"_"+j+"' type='hidden' value='"+val.size_id+"' name='size_"+quantity_block_no+"_"+j+"' />");
                        $(item).parents('.box-body').find('.size-box').append("<div class='col-sm-10'><input id='quantity_"+quantity_block_no+"_"+j+"' class='form-control' type='text' value='"+val.quantity+"' name='quantity_"+quantity_block_no+"_"+j+"' /> </div><div class='clearfix'></div></div>");
                        j++;
                    });
                }
                else{
                    if(size_group_id!=""){
                        $(item).parents('.box-body').find('.size-error').hide();
                        $(item).parents('.box-body').find('.size-box').show();
                        $(item).parents('.box-body').find('.size-box').html('');

                        $(item).parents('.box-body').find('.size-box').append("<div class='form-group'>");
                        $(item).parents('.box-body').find('.size-box').append("<label class='col-sm-2 control-label' for=''>Size</label>");
                        $(item).parents('.box-body').find('.size-box').append("<label class='col-sm-10 control-label' for=''>Quantity</label>");
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            url: '<?= Url("sizeGroup/getSizesBySizeGroup") ?>',
                            data: 'size_group_id='+size_group_id,
                            async: true,
                            success: function(data){

                                //console.log(data);
                                var j = 1;
                                $.each(data.sizes, function(i, val){
                                    //alert(val.size_no);
                                    $(item).parents('.box-body').find('.size-box').append("<div class='form-group'>");
                                    $(item).parents('.box-body').find('.size-box').append("<label class='col-sm-2 control-label' for='size_"+quantity_block_no+"_"+j+"'>"+val.size_no+"</label><input id='size_"+quantity_block_no+"_"+j+"' type='hidden' value='"+val.id+"' name='size_"+quantity_block_no+"_"+j+"' />");
                                    $(item).parents('.box-body').find('.size-box').append("<div class='col-sm-10'><input id='quantity_"+quantity_block_no+"_"+j+"' class='form-control' type='text' value='' name='quantity_"+quantity_block_no+"_"+j+"' /> </div><div class='clearfix'></div></div>");
                                    j++;
                                });
                            }
                        });
                    }
                    else{
                        $(item).parents('.box-body').find('.size-error').show();
                        $(item).parents('.box-body').find('.size-box').hide();
                    }
                }
            }
        });
    }

    function calculateTotalQuantity(item){
        var size_group_id = $(item).parents('.box-body').find('.size_group_id').val();
        var quantity_block_no = $(item).parents('.box-body').find('.quantity_block_no').val();
        var $i = 1;
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '<?= Url("sizeGroup/getSizesBySizeGroup") ?>',
            data: 'size_group_id='+size_group_id,
            async: true,
            success: function(data){
                var j = 1;
                var total_quantity = 0;
                $.each(data.sizes, function(i, val){
                    var quantity = (parseInt($(item).parents('.box-body').find('#quantity_'+quantity_block_no+'_'+j).val()))?parseInt($(item).parents('.box-body').find('#quantity_'+quantity_block_no+'_'+j).val()):0;
                    total_quantity = total_quantity + quantity;
                    j++;
                });
                $(item).parents('.box-body').find('#total_quantity_'+quantity_block_no).val(total_quantity);
            }
        });
    }

    function calculateCommission(){

        var commission  = ($('#commission').val())?parseFloat($('#commission').val()):0;
        var invoice_value = ($('#invoice_value').val())?parseFloat($('#invoice_value').val()):0;

        <?php
        if($buyer_template == "Template 01"){
        ?>
            $('#commission_value').val((commission * invoice_value) / 100);
            $('#total_value').val(invoice_value - ((commission * invoice_value) / 100));

        <?php } ?>
    }

    function calculateLessFinancialCharge(){
        var less_financial_charge_percent   = ($('#less_financial_charge_percent').val())?parseFloat($('#less_financial_charge_percent').val()):0;
        var total_value                     = ($('#total_value').val())?parseFloat($('#total_value').val()):0;

        <?php
        if($buyer_template == "Template 02"){
        ?>
            $('#less_financial_charge').val((less_financial_charge_percent * total_value) / 100);
            calculateSubTotalValue();
        <?php } ?>
    }

    function calculateLessClaimBonus(){
        var less_claim_bonus_percent    = ($('#less_claim_bonus_percent').val())?parseFloat($('#less_claim_bonus_percent').val()):0;
        var total_value                 = ($('#total_value').val())?parseFloat($('#total_value').val()):0;

        <?php
        if($buyer_template == "Template 02"){
        ?>
            $('#less_claim_bonus').val((less_claim_bonus_percent * total_value) / 100);
            calculateSubTotalValue();
        <?php } ?>
    }

    function calculateLessNoviDeduction(){
        var less_novi_deduction_percent = ($('#less_novi_deduction_percent').val())?parseFloat($('#less_novi_deduction_percent').val()):0;
        var total_value                 = ($('#total_value').val())?parseFloat($('#total_value').val()):0;

        <?php
        if($buyer_template == "Template 02"){
        ?>
            $('#less_novi_deduction').val((less_novi_deduction_percent * total_value) / 100);
            calculateSubTotalValue();
        <?php } ?>
    }

    function calculateSubTotalValue(){
        var less_financial_charge   = ($('#less_financial_charge').val())?parseFloat($('#less_financial_charge').val()):0;
        var less_claim_bonus        = ($('#less_claim_bonus').val())?parseFloat($('#less_claim_bonus').val()):0;
        var less_novi_deduction     = ($('#less_novi_deduction').val())?parseFloat($('#less_novi_deduction').val()):0;
        var total_value             = ($('#total_value').val())?parseFloat($('#total_value').val()):0;

        <?php
        if($buyer_template == "Template 02"){
        ?>
            $('#sub_total_value').val(total_value - (less_financial_charge + less_claim_bonus + less_novi_deduction));
        <?php } ?>
    }

    function calculateLessLcDeadline(){
        var less_lc_deadline_percent    = ($('#less_lc_deadline_percent').val())?parseFloat($('#less_lc_deadline_percent').val()):0;
        var sub_total_value             = ($('#sub_total_value').val())?parseFloat($('#sub_total_value').val()):0;

        <?php
        if($buyer_template == "Template 02"){
        ?>
        $('#less_lc_deadline').val((less_lc_deadline_percent * sub_total_value) / 100);
        $('#final_amount').val(sub_total_value - ((less_lc_deadline_percent * sub_total_value) / 100));
        <?php } ?>
    }
</script>

@stop