@extends('layouts.master')
@section('head')
    @parent
    {{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}

@stop


@section('content')


    <div class="row">
        <div class="downloadpdf"><a class="btn btn-primary" href="{{url('productionOrder/downloadsalescontractpdf/'.$order->id)}}">Download PDF</a></div>
        <div id="editor"></div>
        <h3 class="leftpad"> ORDER NO :<?if(isset($order->order_no)){echo $order->order_no;}?></h3>
        <div id="a4" class="a4">
            <h3 class="contract">CONTRACT</h3>
            <div>
                <div class="ref">Block Contract Ref:<?if(isset($order->PaymentDetails)){echo $order->PaymentDetails->sales_contact_no;}?></div>
                <div class="dat">Date :<?=date('d-m-Y')?></div>
            </div>
            <div class="htext">
                This irrevocable cortract made between H&M Hennes & Mauritz GBC AB, Master Samuelsgatan 46A 106 38 Stockholm, Sweden  & Fortuna Leather Craft Ltd. Fortuna Park, Kunia, Gazipur, Bangladesh under the following terms and condition -
            </div>
            <div class="consigneetext">
                <div class="leftdiv">Name and Address of Consignee:</div>
                <div class="rightdiv">
                    <p><?if(isset($order->Destination)){

                         foreach ($order->Destination as $ks=>$consignee){
                             //print_r($consignee->Destination);
                             if($ks==0){
                                 echo $consignee->Destination->consignee_address;
                             }else{
                                 echo ",".$consignee->Destination->consignee_address;
                             }

                         }
                        }?>
                    </p>
                </div>
            </div>
            <div class="consigneebank">
                <div class="leftdiv">Name and Address of Consignee’s Bank</div>
                <div class="rightdiv">
                    <p><? if(isset($order->ShipmentDetails)){
                            echo $order->ShipmentDetails->ConsigneeBank->name;
                        }
                        if(isset($order->ShipmentDetails->ConsigneeBank)){
                            echo $order->ShipmentDetails->ConsigneeBank->Address->street_address;
                        }
                        ?>
                    </p>


                </div>
            </div>
            <div class="common">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL<span class="sort-icon"><span></th>
                    <th>Description<span class="sort-icon"><span></th>
                    <th>Quantity<span class="sort-icon"><span></th>
                    <th>Unit Price US$<span class="sort-icon"><span></th>
                    <th>Total Value<span class="sort-icon"><span></th>
                    <th>Shipment Date<span class="sort-icon"><span></th>
                    <th>Destination<span class="sort-icon"><span></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i=1;
                $total  = 0;
                foreach($order->TodDetails as $key => $po){

                ?>
                <tr>
                    <td> <?=$i++;?></td>
                    <td> <?if(isset($po->Article->article_no)){echo $po->Article->article_no;}?></td>
                    <td><?= $po->total_quantity ?></td>
                    <td><?= $po->unit_value ?></td>
                    <td><?$total =$total+ $po->total_quantity*$po->unit_value;echo $po->total_quantity*$po->unit_value; ?></td>
                    <td><?=date('d-m-Y',strtotime($po->tod));?></td>
                    <td> <?php if(isset($po->Destination->destination_name)){echo $po->Destination->destination_name;}?></td>
                </tr>
                <?php
                }
                ?>
                </tbody>
                <tfoot>
                <tr>
                    <td></td>
                    <td> Total Unit:</td>
                    <td><?=$order->TodDetails->sum("total_quantity")?></td>
                    <td>Amount</td>
                    <td>$<?=$total?></td>
                    <td></td>
                    <td> </td>
                </tr>
                </tfoot>
            </table>
            </div>
            <div class="common">
                <div class="leftdiv">Tolerance:</div>
                <div class="rightdiv">
                    2% +/- in Quantity and Value Acceptable. Footwear Description, Footwear  Quantity, Unit Price and Total Value can be amended as per order sheet.
                </div>

            </div>
            <div class="common">
                <div class="leftdiv">Quota Category:</div>
                <div class="rightdiv">
                    Non Quota/Quota.
                </div>

            </div>
            <div class="common">
                <div class="leftdiv">Transport Documents:</div>
                <div class="rightdiv">
                    <p>For Sea shipments - Transport documents will be issued by Damco to the order of negotiating Bank marked freight collect.</p>
                    <p>For Air shipments – House Air Way Bill will be issued by ITSL PANALPINA marked freight collect/freight prepaid .</p>
                    <p>For Sea-Air shipments made by Damco, transport documents will be issued marked freight Prepaid / collect. </p>
                </div>

            </div>
            <div class="common">
                <div class="leftdiv">Mode of Shipment:</div>
                <div class="rightdiv">
                    <? foreach($order->TodDetails as $ks=>$shipmode){?>
                        <? //print_r($shipmode->ShippingBlockDestination);?>
                        <? if($ks>0){ echo ",";}?>
                        <? if($shipmode->ShippingMode) {?>
                            <? echo $shipmode->Destination->destination_name;?> by <? echo $shipmode->ShippingMode->shipping_mode;?>
                        <? } ?>
                    <? }?>

                </div>

            </div>
            <div class="common">
                <div class="leftdiv">Insurance:</div>
                <div class="rightdiv">
                    To be covered by buyer upon received the cargo by the nominated forwarders.
                </div>
            </div>
            <div class="common">
                <div class="leftdiv">Latest Date of Shipment:</div>
                <div class="rightdiv">
                    <?=date('d-m-Y',strtotime($order->TodDetails->max("tod")));?>
                </div>
            </div>

            <div class="common">
                <div class="leftdiv">Expiry of the Contract:</div>
                <div class="rightdiv">
                    After 30 days from the date of shipment.
                </div>
            </div>
            <div class="common">
                <div class="leftdiv">Payment Terms:</div>
                <div class="rightdiv">
                    <p>By TT within 20 days from the required  documents submission to Damco / Puls Trading Far East Ltd, Bangladesh Liaison Office  mentioning the Contract,  H&M order, and the invoice number, by showing  2%  discount  from the total invoice value.</p>
                    <p>In case of absence/irregularities or discrepancies in below required documents H&M Hennes & Mauritz GBC AB  will withhold payments until documents are corrected.</p>
                </div>
            </div>
            <div class="common">
                <div class="leftdiv">Name of Supplier’s Bank:</div>
                <div class="rightdiv">
                    {{--Mercantile Bank Ltd., Main Branch, 61 Dilkusha C/A, Dhaka-1000, Bangladesh, Swift Code: MBLBBDDH002--}}
                    {{--(note:shipper bank)--}}
                    <? if($order->ShipmentDetails){echo $order->ShipmentDetails->ShipperBank->name;}?>
                </div>
            </div>
            <div class="common">
                <div class="leftdiv">Account Number:</div>
                <div class="rightdiv">
                    <? if($order->ShipmentDetails){echo $order->ShipmentDetails->account_no;}?>
                </div>
            </div>
            <div class="common">
                <div class="leftdiv">Authentication:</div>
                <div class="rightdiv">
                    Counter signed by Puls Trading Far East Ltd, Bangladesh Liaison Office bank HSBC in Dhaka.
                </div>
            </div>
            <div class="common">
                <div class="leftdiv">All Details:</div>
                <div class="rightdiv">
                    As per Order Sheet & Terms and Conditions of Purchase – As per H&M Standard Purchase Conditions
                </div>
            </div>
            <div class="common">
                <div class="leftdiv">Required documents to be submitted to Puls Trading Far East Ltd, Bangladesh Liaison Office:</div>
                <div class="rightdiv">
                    <p>1.  Original signed invoice & packing list covering gross weight/net weight & summary sheet.</p>
                    <p>2.  Original Certificate of Origin/GSP Form-A/APTA/ B255E/CCI (if applicable)</p>
                    <p>3.  Documents for payments: Shipment/Feeder vessel wise combined Payment Invoice.</p>
                    <p>4.  Copy of Original FCR (Sea-air) and Original HAWB (Air) with in 7 days after vessel sailing/flight departure.</p>
                </div>
            </div>

            <div class="foot">
                <div class="leftdivfoot">
                    <p class="signature">For and on Behalf of :</p>
                    <p>Puls Trading Far East Ltd, Bangladesh Liaison Office</p>
                    <p>House # NW(K) 8/A, Road # 50, Gulshan - 2, Dhaka - 1212, Bangladesh.</p>
                </div>
                <div class="rightdivfoot">
                    <p class="signature">For and on Behalf of :</p>
                    <p>Fortuna Leather Craft Ltd.</p>
                </div>
            </div>

        </div>
    </div>
@stop

    @section('page-script')
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js')}}
    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js')}}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}
    {{HTML::script('https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js')}}
    {{HTML::script('https://cdn.datatables.net/buttons/1.3.1/js/buttons.bootstrap.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')}}
    {{HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js')}}
    {{HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js')}}
@stop

@section('plugin-script')

    <style>
        .a4{width: 595px; margin:auto;}
        .contract{text-align:center;}
        .ref{
            float: left;
            font-weight: bold;
        }
        .dat{
            float:right;
            font-weight: bold;
        }
        .htext{
            clear: both;
            padding-top: 40px;
        }
        .common{
            clear: both;
            padding-top: 40px;
        }
        .foot{
            clear: both;
            padding-top: 100px;
        }
        .leftdivfoot{
            float: left;
            width:300px;
        }
        .rightdivfoot{
            float: right;

        }
        .consigneetext{
            clear: both;
            padding-top: 40px;
        }
         .consigneebank{
             padding-top: 40px;
             clear: both;
         }
        .rightdiv{
            float: left;
            width:300px;
        }
        .leftdiv{
            font-weight: bold;
            float: left;
            width:295px;
        }
        .leftpad{text-align: center;}
        .signature{
            text-decoration: underline;
            font-weight: bold;
        }
        .downloadpdf{
            padding: 10px 100px 10px 0px;
            float:right;
        }

    </style>

@stop