@extends('layouts.master')
@section('head')
    @parent
    {{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
    <script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['annotationchart']}]}"></script>

@stop

@section('content')

    <?php
    $buyer_val = (isset($post_val['buyer_id'])) ? $post_val['buyer_id'] : null;
    $order_no_val = (isset($post_val['order_no'])) ? $post_val['order_no'] : null;
    $article_val = (isset($post_val['article_id'])) ? $post_val['article_id'] : null;
    $color_val = (isset($post_val['color_id'])) ? $post_val['color_id'] : null;
    $season_val = (isset($post_val['season_id'])) ? $post_val['season_id'] : null;
    $destination_val = (isset($post_val['destination_id'])) ? $post_val['destination_id'] : null;
    $despo_val = (isset($post_val['despo'])) ? $post_val['despo'] : null;
    $pos_val = (isset($post_val['pos'])) ? $post_val['pos'] : null;
    $order_date_from_val = (isset($post_val['order_date_from'])) ? $post_val['order_date_from'] : null;
    $order_date_to_val = (isset($post_val['order_date_to'])) ? $post_val['order_date_to'] : null;
    $tod_from_val = (isset($post_val['tod_from'])) ? $post_val['tod_from'] : null;
    $tod_to_val = (isset($post_val['tod_to'])) ? $post_val['tod_to'] : null;
    $frequency_val = (isset($post_val['frequency'])) ? $post_val['frequency'] : null;
    ?>

    <div class="row">
        <div class="col-md-12 sidebar">
            <?php if(Session::has('message')){ ?>
            <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
            <script>
                $('#alert').delay(2000).fadeOut(400)
            </script>
            <?php } ?>
            <data class="row">
                <div class="col-sm-10"><h3>Seasonal Concentration Report</h3></div>
                <div class="col-sm-2">
                 </div>
            </data>
            <!-- Content Header (Page header) -->
            <section class="content-header">
            </section>
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <!-- form start -->
                            <?= Form::open(array('url' => 'productionOrder/seasonalConcentrationReport', 'method' => 'post')); ?>
                            <div class="box-body">
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <?= Form::label('tod', 'Select Range', array('class' => 'control-label')); ?>
                                        <div class="input-daterange input-group" id="datepicker">
                                            {{ Form::text('tod_from', $begin->format('d-m-Y'),array('class'=>'input-sm form-control datepicker','id'=>'tod_from')) }}
                                            <span class="input-group-addon">To</span>
                                            {{ Form::text('tod_to', $end->format('d-m-Y'),array('class'=>'input-sm form-control datepicker','id'=>'tod_to')) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('frequency', 'Fequency', array('class' => 'control-label')); ?>
                                            {{ Form::select('frequency',  [1=>'Monthly',2=>'Weekly',3=>'Daily'], $frequency_val, $attributes = array('id'=>'frequency', 'name'=>'frequency', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Frequency')) }}

                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('sum_type', 'Sum Type', array('class' => 'control-label')); ?>
                                            {{ Form::select('sum_type', [1=>'Quantity',2=>'Value'], 1, $attributes = array('id'=>'sum_type', 'name'=>'sum_type', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Frequency')) }}

                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('order_no', 'Select Order No', array('class' => 'control-label')); ?>
                                        {{ Form::select('order_no', $production_order, $order_no_val, $attributes = array('id'=>'order_no', 'name'=>'order_no[]', 'data-rel'=>'chosen', 'data-placeholder'=>'Select Order No', 'multiple'=>'', 'class'=>'form-control select2')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <?= Form::label('article_id', 'Article No', array('class' => 'control-label')); ?>
                                        {{ Form::select('article_id', $article, $article_val, $attributes = array('id'=>'article_id', 'name'=>'article_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Article', 'multiple'=>'')) }}
                                    </div>
                                    <div class="form-group template01">
                                        <?= Form::label('destination_id', 'Destination', array('class' => 'control-label')); ?>
                                        {{ Form::select('destination_id', $destination, $destination_val, $attributes = array('id'=>'destination_id', 'name'=>'destination_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Destination', 'multiple'=>'')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('color_id', 'Color', array('class' => 'control-label')); ?>
                                        {{ Form::select('color_id', $color, $color_val, $attributes = array('id'=>'color_id', 'name'=>'color_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Color', 'multiple'=>'')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('season_id', 'Season', array('class' => 'control-label')); ?>
                                        {{ Form::select('season_id', $season, $season_val, $attributes = array('id'=>'season_id', 'name'=>'season_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Season', 'multiple'=>'')) }}
                                    </div>

                                </div>


                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <div class="col-md-2 col-md-offset-11">
                                    <button type="submit" class="btn btn-success">Search</button>
                                </div>

                            </div>
                            <?= Form::close(); ?>
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Seasonal Concentration Report
                </h1>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Seasonal Order Value Report</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>SL<span class="sort-icon"><span></th>
                                        <th><?if($frequency_val==2){echo "Week";}else if($frequency_val==1){echo "Month";}else{echo "Day";}?><span class="sort-icon"><span></th>
                                        <? foreach($articlelist as $art){ ?>
                                            <th> Article No:<?=$art->Article->article_no?><span class="sort-icon"><span></th>
                                        <? } ?>
                                        <th>Total<span class="sort-icon"><span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                            <? $i=1;foreach ($result as $k=>$tod){ ?>
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$k}}<span class="sort-icon"><span></td>
                                                    <? foreach ($tod as $k=>$res){ ?>

                                                        <td>{{$res}}</td>

                                                    <? } ?>
                                                </tr>
                                            <? }?>



                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </div>


    @stop


    @section('page-script')
            <!-- Select2 -->
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js')}}

    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js')}}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}

    {{HTML::script('https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js')}}
    {{HTML::script('https://cdn.datatables.net/buttons/1.3.1/js/buttons.bootstrap.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')}}
    {{HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js')}}
    {{HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js')}}
@stop

@section('plugin-script')
    <script type="text/javascript">
        $(function () {
            $(".select2").select2();
            $("#yearpicker").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'yyyy'
            });
            $("#order_date_from").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#order_date_to").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#tod_from").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#tod_to").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $(".template01").hide();
            $(".template02").hide();

            <?php if($destination_val){ ?>
                $("#destination_id").parent('.form-group').show();
            <?php } ?>
        <?php if($despo_val){ ?>
            $("#despo").parent('.form-group').show();
            <?php } ?>
        <?php if($pos_val){ ?>
            $("#pos").parent('.form-group').show();
            <?php } ?>

            var table=$('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "scrollX": true,
                "lengthChange": true,
                "buttons": [  {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                }, 'pdf', 'colvis' ]
            });
            table.buttons().container()
                    .appendTo( '#example1_wrapper .col-sm-6:eq(0)' );
        });

        function buyerChange(){
            var buyer = $('#buyer_id').val();
            var buyer_template = '';
            if(buyer){
                $(".template01").hide();
                $(".template02").hide();
                $.each( buyer, function( key, value ) {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: '<?= Url("buyer/getBuyerInfo") ?>',
                        data: 'id='+value,
                        async: true,
                        success: function(data){
                            buyer_template = data.buyer_template;
                            if(buyer_template == "Template 01"){
                                $(".template01").show();
                            }
                            if(buyer_template == "Template 02"){
                                $(".template02").show();
                            }
                        }
                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getProductionOrderByBuyerArray") ?>',
                    data: 'buyer='+buyer,
                    async: true,
                    success: function(data){
                        $("#order_no").removeAttr('readonly');
                        $("#order_no").removeAttr('disabled');
                        $("#order_no").html(data.order_no);
                    }
                });
            }
            else{
                $(".template01").hide();
                $(".template02").hide();
                $("#order_no").html('<option value="">Select</option>');
                $("#order_no").attr('readonly', 'readonly');
                $("#order_no").attr('disabled', 'disabled');
            }
        }

        function orderChange(){
            var order = $('#order_no').val();
            if(order){
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getArticleByOrderArray") ?>',
                    data: 'order='+order,
                    async: true,
                    success: function(data){
                        $("#article_id").removeAttr('readonly');
                        $("#article_id").removeAttr('disabled');
                        $("#article_id").html(data.article);
                    }
                });

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getColorByOrderArray") ?>',
                    data: 'order='+order,
                    async: true,
                    success: function(data){
                        $("#color_id").removeAttr('readonly');
                        $("#color_id").removeAttr('disabled');
                        $("#color_id").html(data.color);
                    }
                });
            }
            else{
                $("#article_id").html('<option value="">Select</option>');
                $("#article_id").attr('readonly', 'readonly');
                $("#article_id").attr('disabled', 'disabled');

                $("#color_id").html('<option value="">Select</option>');
                $("#color_id").attr('readonly', 'readonly');
                $("#color_id").attr('disabled', 'disabled');
            }
        }

    </script>

@stop
