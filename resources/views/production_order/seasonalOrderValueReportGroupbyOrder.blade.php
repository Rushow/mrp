@extends('layouts.master')
@section('head')
    @parent
    {{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
    <script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['annotationchart']}]}"></script>

@stop

@section('content')

    <?php
    $buyer_val = (isset($post_val['buyer_id'])) ? $post_val['buyer_id'] : null;
    $order_no_val = (isset($post_val['order_no'])) ? $post_val['order_no'] : null;
    $article_val = (isset($post_val['article_id'])) ? $post_val['article_id'] : null;
    $color_val = (isset($post_val['color_id'])) ? $post_val['color_id'] : null;
    $season_val = (isset($post_val['season_id'])) ? $post_val['season_id'] : null;
    $destination_val = (isset($post_val['destination_id'])) ? $post_val['destination_id'] : null;
    $despo_val = (isset($post_val['despo'])) ? $post_val['despo'] : null;
    $pos_val = (isset($post_val['pos'])) ? $post_val['pos'] : null;
    $order_date_from_val = (isset($post_val['order_date_from'])) ? $post_val['order_date_from'] : null;
    $order_date_to_val = (isset($post_val['order_date_to'])) ? $post_val['order_date_to'] : null;
    $tod_from_val = (isset($post_val['tod_from'])) ? $post_val['tod_from'] : null;
    $tod_to_val = (isset($post_val['tod_to'])) ? $post_val['tod_to'] : null;
    $block_tt_no_val = (isset($post_val['block_tt_no'])) ? $post_val['block_tt_no'] : null;
    ?>

    <div class="row">
        <div class="col-md-12 sidebar">
            <?php if(Session::has('message')){ ?>
            <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
            <script>
                $('#alert').delay(2000).fadeOut(400)
            </script>
            <?php } ?>
            <data class="row">
                <div class="col-sm-10"><h3>Seasonal Order Value Report</h3></div>
                <div class="col-sm-2">
                 </div>
            </data>
            <!-- Content Header (Page header) -->
            <section class="content-header">
            </section>
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <!-- form start -->
                            <?= Form::open(array('url' => 'productionOrder/seasonalOrderValueReportGroupbyOrder', 'method' => 'post')); ?>
                            <div class="box-body">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= Form::label('buyer_id', 'Buyer', array('class' => 'control-label')); ?>
                                        {{ Form::select('buyer_id', $buyer, $buyer_val, $attributes = array('id'=>'buyer_id', 'name'=>'buyer_id[]', 'data-placeholder' => 'Select an Buyer', 'multiple'=>'', 'class'=>'form-control select2', 'onchange' => 'buyerChange();')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('order_no', 'Select Order No', array('class' => 'control-label')); ?>
                                        {{ Form::select('order_no', $production_order, $order_no_val, $attributes = array('id'=>'order_no', 'name'=>'order_no[]', 'data-rel'=>'chosen', 'data-placeholder'=>'Select Order No', 'multiple'=>'', 'class'=>'form-control select2')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('article_id', 'Article No', array('class' => 'control-label')); ?>
                                        {{ Form::select('article_id', $article, $article_val, $attributes = array('id'=>'article_id', 'name'=>'article_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Article', 'multiple'=>'')) }}
                                    </div>
                                    <div class="form-group template01">
                                        <?= Form::label('destination_id', 'Destination', array('class' => 'control-label')); ?>
                                        {{ Form::select('destination_id', $destination, $destination_val, $attributes = array('id'=>'destination_id', 'name'=>'destination_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Destination', 'multiple'=>'')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= Form::label('color_id', 'Color', array('class' => 'control-label')); ?>
                                        {{ Form::select('color_id', $color, $color_val, $attributes = array('id'=>'color_id', 'name'=>'color_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Color', 'multiple'=>'')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('season_id', 'Season', array('class' => 'control-label')); ?>
                                        {{ Form::select('season_id', $season, $season_val, $attributes = array('id'=>'season_id', 'name'=>'season_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Season', 'multiple'=>'')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('bktt_no', 'Select Block TT No', array('class' => 'control-label')); ?>
                                        {{ Form::select('bktt_no', $block_tt_no, $block_tt_no_val, $attributes = array('id'=>'bktt_no', 'name'=>'bktt_no[]', 'data-rel'=>'chosen', 'data-placeholder'=>'Select Block TT No', 'multiple'=>'', 'class'=>'form-control select2')) }}

                                    </div>
                                    <div class="form-group template02">
                                        <?= Form::label('despo', 'Dispo', array('class' => 'control-label')); ?>
                                        {{ Form::select('despo', $despo, $despo_val, $attributes = array('id'=>'despo', 'name'=>'despo[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Dispo', 'multiple'=>'')) }}
                                    </div>
                                    <div class="form-group template02">
                                        <?= Form::label('pos', 'Pos', array('class' => 'control-label')); ?>
                                        {{ Form::text('pos', $pos_val,array('class'=>'form-control','id'=>'pos')) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-daterange form-group">
                                        <?= Form::label('order_date', 'Order Date', array('class' => 'control-label')); ?>
                                        <div class="input-daterange input-group" id="datepicker">
                                            {{ Form::text('order_date_from', $order_date_from_val,array('class'=>'input-sm form-control datepicker','id'=>'order_date_from')) }}
                                            <span class="input-group-addon">To</span>
                                            {{ Form::text('order_date_to', $order_date_to_val,array('class'=>'input-sm form-control datepicker','id'=>'order_date_to')) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('tod', 'TOD', array('class' => 'control-label')); ?>
                                        <div class="input-daterange input-group" id="datepicker">
                                            {{ Form::text('tod_from', $tod_from_val,array('class'=>'input-sm form-control datepicker','id'=>'tod_from')) }}
                                            <span class="input-group-addon">To</span>
                                            {{ Form::text('tod_to', $tod_to_val,array('class'=>'input-sm form-control datepicker','id'=>'tod_to')) }}
                                        </div>
                                    </div>


                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <div class="col-md-2 col-md-offset-11">
                                    <button type="submit" class="btn btn-success">Search</button>
                                </div>

                            </div>
                            <?= Form::close(); ?>
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Seasonal Order Value Report
                </h1>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Seasonal Order Value Report</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Buyer<span class="sort-icon"><span></th>
                                        <th>Order No<span class="sort-icon"><span></th>
                                        <th>Article<span class="sort-icon"><span></th>
                                        <th>Color<span class="sort-icon"><span></th>
                                        <th>Destination/Despo<span class="sort-icon"><span></th>
                                        <th>Season<span class="sort-icon"><span></th>
                                        <th>TOD WK<span class="sort-icon"><span></th>
                                        <th>TOD<span class="sort-icon"><span></th>
                                        {{--<th>Cut Off<span class="sort-icon"><span></th>--}}
                                        <th>Block TT No<span class="sort-icon"><span></th>
                                        <th>Order Quantity<span class="sort-icon"><span></th>
                                        <th>Avg Unit Price<span class="sort-icon"><span></th>
                                        <th>Invoice Value<span class="sort-icon"><span></th>
                                        <th>Commision<span class="sort-icon"><span></th>
                                        <th>Total Value<span class="sort-icon"><span></th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($orders as  $po){

                                    ?>
                                                <tr>
                                                    <td> <?if(isset($po->Buyer)){echo $po->Buyer->buyer_name;}?></td>
                                                    <td> <?if(isset($po->order_no)){echo $po->order_no;}?></td>
                                                    <td>
                                                        <? foreach($po->Article as $k=>$poa){
                                                            if($k==0){
                                                                echo $poa->Article->article_no;
                                                            }else{
                                                                echo ",".$poa->Article->article_no;
                                                            }

                                                        }?>

                                                    </td>
                                                    <td>
                                                        <?foreach($po->Color as $k=>$col){
                                                            if($k==0){
                                                                echo $col->Color->color_name_flcl;
                                                            }else{
                                                                echo ",".$col->Color->color_name_flcl;
                                                            }
                                                        }?>

                                                    </td>
                                                    <td style="width: 20%">
                                                     All

                                                    </td>
                                                    <td>
                                                        <?=$po->Season->season;?>

                                                    </td>
                                                    <td>
                                                            W{{date("W",strtotime($po->TodDetails->min('tod')))}} to W{{date("W",strtotime($po->TodDetails->max('tod')))}}


                                                    </td>
                                                    <td>

                                                        {{date("Md,Y",strtotime($po->TodDetails->min('tod')))}} to {{date("Md,Y",strtotime($po->TodDetails->max('tod')))}}

                                                    </td>



                                                    <td>
                                                        <? if(isset($po->PaymentDetails)){echo $po->PaymentDetails->block_tt_no;}?>

                                                    </td>
                                                    <td style="width: 20%">
                                                         <?=$po->TodDetails->sum('total_quantity');?>
                                                    </td>
                                                    <td style="width: 20%">


                                                        <? echo ($po->TodDetails->sum('unit_value')/$po->TodDetails->count('unit_value'));

                                                        ?>

                                                    </td>
                                                    <td style="width: 20%">
                                                         {{--breakdown right now not necessary --}}
                                                        <?

                                                        if(($po->PaymentDetails)){
                                                            echo $po->PaymentDetails->invoice_value;
                                                        }?>

                                                    </td>
                                                    <td>

                                                        <?
                                                        if(($po->PaymentDetails)){
                                                            echo $po->PaymentDetails->commission;
                                                        }else{
                                                            echo "Nil";
                                                        }?>

                                                    </td>
                                                    <td>

                                                        <?
                                                        if(($po->PaymentDetails)){
                                                            echo $po->PaymentDetails->total_value;
                                                        }else{
                                                            echo "Nil";
                                                        }?>

                                                    </td>
                                                </tr>
                                    <?php

                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </div>


    @stop


    @section('page-script')
            <!-- Select2 -->
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js')}}

    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js')}}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}

    {{HTML::script('https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js')}}
    {{HTML::script('https://cdn.datatables.net/buttons/1.3.1/js/buttons.bootstrap.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')}}
    {{HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js')}}
    {{HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js')}}
@stop

@section('plugin-script')
    <script type="text/javascript">
        $(function () {
            $(".select2").select2();

            $("#order_date_from").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#order_date_to").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#tod_from").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#tod_to").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $(".template01").hide();
            $(".template02").hide();

            <?php if($destination_val){ ?>
                $("#destination_id").parent('.form-group').show();
            <?php } ?>
        <?php if($despo_val){ ?>
            $("#despo").parent('.form-group').show();
            <?php } ?>
        <?php if($pos_val){ ?>
            $("#pos").parent('.form-group').show();
            <?php } ?>

            var table=$('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "scrollX": true,
                "lengthChange": true,
                "buttons": [  {
                    extend: 'excel',
                    message: 'This print was produced using the Print button for DataTables',
                    exportOptions: {
                        columns: ':visible',
                    }
                }, 'pdf', 'colvis' ]
            });
            table.buttons().container()
                    .appendTo( '#example1_wrapper .col-sm-6:eq(0)' );
        });

        function buyerChange(){
            var buyer = $('#buyer_id').val();
            var buyer_template = '';
            if(buyer){
                $(".template01").hide();
                $(".template02").hide();
                $.each( buyer, function( key, value ) {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: '<?= Url("buyer/getBuyerInfo") ?>',
                        data: 'id='+value,
                        async: true,
                        success: function(data){
                            buyer_template = data.buyer_template;
                            if(buyer_template == "Template 01"){
                                $(".template01").show();
                            }
                            if(buyer_template == "Template 02"){
                                $(".template02").show();
                            }
                        }
                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getProductionOrderByBuyerArray") ?>',
                    data: 'buyer='+buyer,
                    async: true,
                    success: function(data){
                        $("#order_no").removeAttr('readonly');
                        $("#order_no").removeAttr('disabled');
                        $("#order_no").html(data.order_no);
                    }
                });
            }
            else{
                $(".template01").hide();
                $(".template02").hide();
                $("#order_no").html('<option value="">Select</option>');
                $("#order_no").attr('readonly', 'readonly');
                $("#order_no").attr('disabled', 'disabled');
            }
        }

        function orderChange(){
            var order = $('#order_no').val();
            if(order){
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getArticleByOrderArray") ?>',
                    data: 'order='+order,
                    async: true,
                    success: function(data){
                        $("#article_id").removeAttr('readonly');
                        $("#article_id").removeAttr('disabled');
                        $("#article_id").html(data.article);
                    }
                });

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getColorByOrderArray") ?>',
                    data: 'order='+order,
                    async: true,
                    success: function(data){
                        $("#color_id").removeAttr('readonly');
                        $("#color_id").removeAttr('disabled');
                        $("#color_id").html(data.color);
                    }
                });
            }
            else{
                $("#article_id").html('<option value="">Select</option>');
                $("#article_id").attr('readonly', 'readonly');
                $("#article_id").attr('disabled', 'disabled');

                $("#color_id").html('<option value="">Select</option>');
                $("#color_id").attr('readonly', 'readonly');
                $("#color_id").attr('disabled', 'disabled');
            }
        }

    </script>

@stop
