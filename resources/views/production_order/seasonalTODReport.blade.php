@extends('layouts.master')
@section('head')

@parent
    {{--{{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css'); }}--}}
    <script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}"></script>
@stop
@section('header')
@stop
@section('content')

    <div class="row">
        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">

                <? if(Session::has('message')){ ?>
                <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
                <script>
                    $('#alert').delay(2000).fadeOut(400)
                </script>
                <? } ?>

                <h1>
                    Seasonal Order Quantity and TOD Status Report
                </h1>
                <br>
                <data class="row">
                    <div class="col-sm-10">
                    </div>
                    <div class="col-sm-2">
                     </div>
                </data>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Seasonal Order Quantity and TOD Status Report</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Order No<span class="sort-icon"><span></th>
                                        <th>Article<span class="sort-icon"><span></th>
                                        <th>Color<span class="sort-icon"><span></th>
                                        <th>Destination<span class="sort-icon"><span></th>
                                        <th>Season<span class="sort-icon"><span></th>
                                        <? foreach($sizes as $size){ ?>
                                        <th><?= $size->size_no ?><span class="sort-icon"><span></th>
                                        <? } ?>
                                        <th>Total<span class="sort-icon"><span></th>
                                        <th>TOD<span class="sort-icon"><span></th>
                                        <th>Cut Off<span class="sort-icon"><span></th>
                                        <th>Remarks</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?
                                    $wk_array = array();
                                    $prev_month = $crnt_month = '';
                                    $prev_week = $crnt_week = '';
                                    $month_cum = 0;
                                    $cum_total = 0;
                                    $i = 0;
                                    $wki = 0;
                                    foreach($orders as $order){
                                        ${'order_'.$order->id.'_qty'} = 0;
                                    }
                                    foreach($tods as $tod){
                                    if($i == 0){
                                        $prev_month = date('M', $tod->order_date);
                                        $crnt_month = date('M', $tod->order_date);

                                        $prev_week = date('W', $tod->order_date);
                                        $crnt_week = date('W', $tod->order_date);
                                    }
                                    else if($i > 0){
                                        $crnt_month = date('M', $tod->order_date);

                                        $crnt_week = date('W', $tod->order_date);
                                    }
                                    ?>
                                    <tr>
                                        <td><?= date('M d, Y', $tod->order_date) ?>&nbsp;</td>
                                        <td><?= date('M, Y', $tod->order_date) ?>&nbsp;</td>
                                        <td>
                                            <?
                                            echo 'WK '.date('W', $tod->order_date);
                                            ?>
                                        </td>
                                        <td>
                                            <?
                                            $details = Helpers::getOrderDetailsByTOD($tod->order_date);
                                            $i = 1;
                                            foreach($details as $detail){
                                                echo Helpers::getDestinationCodeById($detail->destination_id);
                                                echo ($i < count($details))?", ":"&nbsp;";
                                                $i++;
                                            }
                                            ?>
                                        </td>
                                        <?
                                        $total = 0;
                                        $odi = 0;
                                        foreach($orders as $order){
                                        ?>
                                        <td>
                                            <?
                                            $qty = Helpers::getTotalQuantityByOrderAndTOD($order->id, $tod->order_date);
                                            echo $qty;
                                            ${'order_'.$order->id.'_qty'} += $qty;
                                            $total += $qty;
                                            $wk_array[$wki][$odi][0] = date('W', $tod->order_date);
                                            $wk_array[$wki][$odi][1] = $order->order_no;
                                            $wk_array[$wki][$odi][2] = $qty;
                                            ?>
                                        </td>

                                        <?
                                        $odi++;
                                        }
                                        ?>
                                        <td><?= $total ?>&nbsp;</td>
                                        <td>
                                            <?
                                            if($prev_month == $crnt_month){
                                                $month_cum += $total;
                                            }
                                            else{
                                                $month_cum = 0;
                                                $month_cum += $total;
                                            }
                                            echo $month_cum;
                                            ?>
                                        </td>
                                        <td>
                                            <?=  $cum_total += $total; ?>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <?
                                    if($prev_month != $crnt_month){
                                        $prev_month = $crnt_month;
                                    }
                                    $wki++;
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="4"><b>Total</b></td>
                                        <? foreach($orders as $order){ ?>
                                        <td><?= ${'order_'.$order->id.'_qty'} ?></td>
                                        <? } ?>
                                        <td><b><?= $cum_total ?></b></td>
                                        <td><b><?= $cum_total ?></b></td>
                                        <td><b><?= $cum_total ?></b></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>


        </div>
    </div>


    <div class="row">
        <div class="col-sm-12"><!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Graphical Comparison
                </h1>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-success">
                            <div class="box-header">
                                <h3 class="box-title">Graphical Comparison</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div id="linechart_material"></div>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </div>


@stop

@section('page-script')
    {{--{{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js'); }}--}}
    {{--{{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js'); }}--}}
@stop

@section('plugin-script')
    {{--<script type="text/javascript">--}}
    {{--$(function () {--}}
    {{--$('#example1').DataTable({--}}
    {{--"paging": true,--}}
    {{--"lengthChange": true,--}}
    {{--"searching": true,--}}
    {{--"ordering": true,--}}
    {{--"info": true,--}}
    {{--"autoWidth": false--}}
    {{--});--}}
    {{--});--}}
    {{--</script>--}}
    <script type='text/javascript'>
        google.load('visualization', '1.1', {packages: ['line']});
        google.setOnLoadCallback(drawChart);



        function drawChart() {

            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Weeks');
            <?php
                foreach($orders as $order){
            ?>

            data.addColumn('number', '<?= $order->order_no ?>');
            <?php } ?>
            data.addRows([
                <?php
                for($i = 0; $i < count($wk_array); $i++){
                    $j = 0;
                ?>

                [
                    '<?= $wk_array[$i][$j][0] ?>',
                    <?
                    foreach($orders as $order){
                    ?>
                    <?= $wk_array[$i][$j][2] ?>,
                    <? $j++; } ?>
                ],
                <?php } ?>
            ]);

            var options = {
                title: 'Graphical Comparison',
                curveType: 'function',
                legend: { position: 'bottom' }
            };

            var chart = new google.charts.Line(document.getElementById('linechart_material'));

            chart.draw(data, options);
        }


    </script>

@stop