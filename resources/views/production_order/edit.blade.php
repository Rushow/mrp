@extends('layouts.master')
@section('head')

@parent
<!-- Select2 -->
{{ HTML::style('admin-lte/plugins/select2/select2.min.css') }}

@stop
@section('header')
@stop
@section('content')


    <div class="row">

        <div class="col-md-12 sidebar">
            <?php
            $error_messages = Session::get('error_messages');

            if(isset($error_messages)){
                foreach($error_messages as $message){
                    echo $message;
                }
            }
            ?>

            <?php
                $buyer_template = App\Buyer::find($order->buyer_id)->buyerTemplate->buyer_template;
            ?>
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Edit PO
                </h1>
            </section>
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Edit PO</h3>
                            </div><!-- /.box-header -->
                            <!-- form start -->
                            <?= Form::open(array('url' => 'productionOrder/'.$order->id, 'method' => 'put', 'id' => 'productionOrderForm')); ?>
                            <div class="box-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= Form::label('buyer_id', 'Buyer', array('class' => 'control-label')); ?>
                                        {{ Form::select('buyer_id', $buyer, $order->buyer_id, $attributes = array('id'=>'buyer_id','data-rel'=>'chosen', 'class'=>'validate[required] form-control', 'onChange'=>'setSalesContactNo();')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('order_no', 'Order No', array('class' => 'control-label')); ?>
                                        {{ Form::text('order_no', $order->order_no, array('class'=>'validate[required] form-control','id'=>'order_no')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('article_id', 'Article', array('class' => 'control-label')); ?>
                                            {{ Form::select('article_id', $article, $order->articles, $attributes = array('id'=>'article_id','name'=>'article_id[]', 'multiple'=>'', 'data-rel'=>'chosen', 'class'=>'form-control select2')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('color_id', 'Color', array('class' => 'control-label')); ?>
                                        {{ Form::select('color_id', $color, $order->colors, $attributes = array('id'=>'color_id','name'=>'color_id[]', 'multiple'=>'', 'data-rel'=>'chosen', 'class'=>'form-control select2')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('season_id', 'Season', array('class' => 'control-label')); ?>
                                        {{ Form::select('season_id', $season, $order->season_id, $attributes = array('id'=>'season_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                    </div>
                                    <div class="form-group template01" style="height:198px;">
                                        <?= Form::label('destination_id', 'Destination', array('class' => 'control-label')); ?>
                                        {{ Form::select('destination_id', $destination, $order->destinations, $attributes = array('id'=>'destination_id','name'=>'destination_id[]', 'multiple'=>'', 'class'=>'form-control select2', 'data-placeholder' => 'Select a Destination')) }}
                                    </div>
                                    <div class="form-group template02">
                                        <?= Form::label('despo', 'Dispo', array('class' => 'control-label')); ?>
                                        {{ Form::text('despo', $order->despos,array('class'=>'form-control tags','id'=>'despo', 'placeholder' => 'Add a Dispo')) }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= Form::label('order_receive_date', 'Order Receive Date', array('class' => 'control-label')); ?>
                                        {{ Form::text('order_receive_date', ($order->order_receive_date)?$order->order_receive_date:'',array('class'=>'validate[required] form-control datepicker','id'=>'order_receive_date')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('status_id', 'Status', array('class' => 'control-label')); ?>
                                        {{ Form::select('status_id', $status, $order->status_id, $attributes = array('id'=>'status_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('remarks', 'Remarks', array('class' => 'control-label')); ?>
                                        {{ Form::textarea('remarks', $order->remarks, array('class'=>'validate[required] form-control','id'=>'remarks')) }}
                                    </div>
                                </div>
                            </div><!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                            </div>
                            <?= Form::close(); ?>
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </div>



    <style>
        .sidebar{
            padding-bottom: 20px;
        }
        form{
            margin: 15px;
        }
    </style>


@stop

@section('page-script')
    <!-- Select2 -->
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js') }}
@stop

@section('plugin-script')


<script>

    $(function() {
        $(".select2").select2();

        $("#order_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
        $("#order_receive_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'yyyy-mm-dd'
        });

        <?php
        if($buyer_template == "Template 01"){
        ?>
        $(".template02").hide();
        <?php
        }else if($buyer_template == "Template 02"){
        ?>
        $(".template01").hide();
        <?php } ?>
        $('#despo').tagsInput({width:'auto', height: '25', defaultText: 'Add a Dispo'});
    });
    $(document).ready(function() {
        $("#productionOrderForm").validate({
            rules: {
                buyer_id: "required",
                order_no: "required",
                order_date: "required",
                order_receive_date: "required",
                block_tt_no: "required",
                block_tt_order_from: "required",
                block_tt_order_to: "required",
                remarks: "required"
            },
            messages: {
                buyer_id: "Please select a Buyer",
                order_no: "Please enter a Order No",
                order_date: "Please enter Order Date",
                order_receive_date: "Please enter Order Receive Date",
                block_tt_no: "Please select Block TT No",
                block_tt_order_from: "Please select Block TT Ordering Year From",
                block_tt_order_to: "Please select Block TT Ordering Year To",
                remarks: "Please enter remarks"
            }
        });

//        $.validator.addClassRules("Required", {
//            required: true
//        });
//
//        setSalesContactNo();
    });

    $(document).ready(function() {
        $("#productionOrderForm").validate({
            rules: {
                buyer_id: "required",
                order_no: "required",
                season_id: "required",
                order_receive_date: "required"
            }
        });

        $.validator.addClassRules("Required", {
            required: true
        });

    });

    function changeBuyer(){
        var buyer = $("#buyer_id").val();
        var buyer_template = '';
        if(buyer!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("buyer/getBuyerInfo") ?>',
                data: 'id='+buyer,
                async: true,
                success: function(data){
                    buyer_template = data.buyer_template;
                    if(buyer_template == "Template 01"){
                        $(".template01").show();
                        $(".template02").hide();
                    }
                    else if(buyer_template == "Template 02"){
                        $(".template02").show();
                        $(".template01").hide();
                    }
                }
            });
        }
    }

    /*function setColorHMCode(item){
        var val = $(item).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("color/getColorHMCode") ?>',
                data: 'id='+val,
                success: function(data){
                    $("#color_code").val(data.hm_code);

                }
            });
        }
    }*/

    /*function setSalesContactNo(){
        var sales_val = 'FLCL';
        var buyer = $("#buyer_id").val();
        var buyer_name = '';
        if(buyer!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("buyer/getBuyerName") ?>',
                data: 'id='+buyer,
                async: true,
                success: function(data){
                    buyer_name = data.buyer_name;
                }
            });
        }
        if(buyer_name){
            sales_val += '/' + buyer_name;
        }
        if($('#block_tt_order_from').val()){
            sales_val += '/' + $('#block_tt_order_from').val();
        }
        if($("#block_tt_order_to").val()){
            sales_val += '-' + $("#block_tt_order_to").val();
        }
        if($("#block_tt_no").val()){
            sales_val += '/' + $("#block_tt_no").val();
        }
        $("#sales_contact_no").val(sales_val);
    }*/
</script>

@stop