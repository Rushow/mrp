@extends('layouts.master')
@section('head')

@parent
@stop
@section('header')
    {{ HTML::style('admin-lte/plugins/select2/select2.min.css') }}
@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message){
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Create PO
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create PO</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'productionOrder', 'method' => 'post', 'id' => 'productionOrderForm')); ?>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('buyer_id', 'Buyer', array('class' => 'control-label')); ?>
                                    {{ Form::select('buyer_id', $buyer, '', $attributes = array('id'=>'buyer_id','data-rel'=>'chosen', 'class'=>'validate[required] form-control', 'onChange'=>'changeBuyer();')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('order_no', 'Order No', array('class' => 'control-label')); ?>
                                    {{ Form::text('order_no','',array('class'=>'validate[required] form-control','id'=>'order_no')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('article_id', 'Article', array('class' => 'control-label')); ?>
                                    {{ Form::select('article_id', $article, '', $attributes = array('id'=>'article_id', 'name'=>'article_id[]', 'multiple'=>'', 'class'=>'form-control select2', 'data-placeholder' => 'Select an Article')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('color_id', 'Color', array('class' => 'control-label')); ?>
                                    {{ Form::select('color_id', $color, '', $attributes = array('id'=>'color_id','name'=>'color_id[]', 'multiple'=>'', 'class'=>'form-control select2', 'data-placeholder' => 'Select a Color')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('season_id', 'Season', array('class' => 'control-label')); ?>
                                    {{ Form::select('season_id', $season, '', $attributes = array('id'=>'season_id','data-rel'=>'chosen', 'class'=>'form-control'))}}
                                </div>
                                <div class="form-group template01" style="height:198px;">
                                    <?= Form::label('destination_id', 'Destination', array('class' => 'control-label')); ?>
                                    {{ Form::select('destination_id', $destination, '', $attributes = array('id'=>'destination_id','name'=>'destination_id[]', 'multiple'=>'', 'class'=>'form-control select2', 'data-placeholder' => 'Select a Destination'))}}
                                </div>
                                <div class="form-group template02">
                                    <?= Form::label('despo', 'Dispo', array('class' => 'control-label')); ?>
                                    {{ Form::text('despo', '',array('class'=>'form-control tags','id'=>'despo', 'placeholder' => 'Add a Dispo')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('order_receive_date', 'Order Receive Date', array('class' => 'control-label')); ?>
                                    {{ Form::text('order_receive_date', '',array('class'=>'validate[required] form-control datepicker','id'=>'order_receive_date')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('status_id', 'Status', array('class' => 'control-label')); ?>
                                    {{ Form::select('status_id', $status, '', $attributes = array('id'=>'status_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('remarks', 'Remarks', array('class' => 'control-label')); ?>
                                    {{ Form::textarea('remarks','',array('class'=>'validate[required] form-control','id'=>'remarks')) }}
                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>


@stop

@section('page-script')
    <!-- Select2 -->
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js') }}
@stop

@section('plugin-script')
<script>
    $(function() {
        $(".select2").select2();

        $("#order_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
        $("#order_receive_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'yyyy-mm-dd'
        });
        $(".template02").hide();
        $('#despo').tagsInput({width:'auto', height: '25', defaultText: 'Add a Dispo'});
    });
    $(document).ready(function() {
        $("#productionOrderForm").validate({
            rules: {
                buyer_id: "required",
                order_no: "required",
                season_id: "required",
                status_id: "required",
                order_receive_date: "required",
                remarks: "required"
            }
        });

        $.validator.addClassRules("Required", {
            required: true
        });

    });

    function changeBuyer(){
        var buyer = $("#buyer_id").val();
        var buyer_template = '';
        if(buyer!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("buyer/getBuyerInfo") ?>',
                data: 'id='+buyer,
                async: true,
                success: function(data){
                    buyer_template = data.buyer_template;
                    if(buyer_template == "Template 01"){
                        $(".template01").show();
                        $(".template02").hide();
                    }
                    else if(buyer_template == "Template 02"){
                        $(".template02").show();
                        $(".template01").hide();
                    }
                }
            });
        }
    }

    /*function setColorHMCode(item){
        var val = $(item).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("color/getColorHMCode") ?>',
                data: 'id='+val,
                success: function(data){
                    $("#color_code").val(data.hm_code);

                }
            });
        }
    }*/
</script>

@stop