<?php
$buyer_template = App\Buyer::find($order->buyer_id)->buyerTemplate->buyer_template;
?>

<div class="box box-info">
    <div class="box-header">
        <i class="fa fa-th"></i>
        <h3 class="box-title">Shipping Detail Block</h3>
        <!-- tools box -->
        <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove" onclick="removeShippingBlock(this);"><i class="fa fa-times"></i></button>
        </div><!-- /. tools -->
    </div>
    <div class="box-body">

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    {{ Form::hidden('shipping_detail_id_'.$shipping_block_no, ($shipping_details)?$shipping_details->id:'', array('id'=> 'shipping_detail_id_'.$shipping_block_no, 'class'=>'shipping_detail_id')) }}
                    <div class="form-group">
                        <?= Form::label('shipping_mode_id_'.$shipping_block_no, 'Shipping Mode', array('class' => 'control-label')); ?>
                        {{ Form::select('shipping_mode_id_'.$shipping_block_no, $order->shipping_mode_list, ($shipping_details)?$shipping_details->shipping_mode_id:'', $attributes = array('id'=>'shipping_mode_id_'.$shipping_block_no, 'name'=>'shipping_mode_id_'.$shipping_block_no, 'class'=>'form-control')) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= Form::label('destination_id_'.$shipping_block_no, 'Destination', array('class' => 'control-label')); ?><br>
                        {{ Form::select('destination_id_'.$shipping_block_no, $order->destination_list, ($shipping_details)?$shipping_details->destinations:'', $attributes = array('id'=>'destination_id_'.$shipping_block_no,'name'=>'destination_id_'.$shipping_block_no.'[]', 'multiple'=>'', 'class'=>'form-control select2', 'style'=>'width: 100%;', 'data-placeholder' => 'Select a Destination')) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>