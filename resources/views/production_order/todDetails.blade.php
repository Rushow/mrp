<?php
$buyer_template = App\Buyer::find($order->buyer_id)->buyerTemplate->buyer_template;
?>

<div class="box box-info">
    <div class="box-header">
        <i class="fa fa-th"></i>
        <h3 class="box-title">TOD Detail Block</h3>
        <!-- tools box -->
        <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove" onclick="removeTodBlock(this);"><i class="fa fa-times"></i></button>
        </div><!-- /. tools -->
    </div>
    <div class="box-body">

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::hidden('tod_detail_id_'.$tod_block_no, ($tod_details)?$tod_details->id:'', array('id'=> 'tod_detail_id_'.$tod_block_no, 'class'=>'tod_detail_id')) }}
                    <div class="form-group">
                        <?= Form::label('article_id_'.$tod_block_no, 'Article', array('class' => 'control-label')); ?>
                        {{ Form::select('article_id_'.$tod_block_no, $order->article_list, ($tod_details)?$tod_details->article_id:'', $attributes = array('id'=>'article_id_'.$tod_block_no, 'name'=>'article_id_'.$tod_block_no, 'class'=>'form-control')) }}
                    </div>
                    <div class="form-group">
                        <?= Form::label('color_id_'.$tod_block_no, 'Color', array('class' => 'control-label')); ?>
                        {{ Form::select('color_id_'.$tod_block_no, $order->color_list, ($tod_details)?$tod_details->color_id:'', $attributes = array('id'=>'color_id','name'=>'color_id_'.$tod_block_no, 'class'=>'form-control')) }}
                    </div>
                    <div class="form-group template01">
                        <?= Form::label('destination_id_'.$tod_block_no, 'Destination', array('class' => 'control-label')); ?>
                        {{ Form::select('destination_id_'.$tod_block_no, $order->destination_list, ($tod_details)?$tod_details->destination_id:'', $attributes = array('id'=>'destination_id_'.$tod_block_no,'name'=>'destination_id_'.$tod_block_no, 'class'=>'form-control'))}}
                    </div>
                    <div class="form-group template02">
                        <?= Form::label('despo_'.$tod_block_no, 'Dispo', array('class' => 'control-label')); ?>
                        {{ Form::select('despo_'.$tod_block_no, $order->despo_list, ($tod_details)?$tod_details->despo:'', $attributes = array('id'=>'despo_'.$tod_block_no,'name'=>'despo_'.$tod_block_no, 'class'=>'form-control')) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group template02">
                        <?= Form::label('pos_'.$tod_block_no, 'Pos', array('class' => 'control-label')); ?>
                        {{ Form::text('pos_'.$tod_block_no, ($tod_details)?$tod_details->pos:'', array('class'=>'form-control','id'=>'pos_'.$tod_block_no)) }}
                    </div>
                    <div class="form-group">
                        <?= Form::label('tod_'.$tod_block_no, 'TOD', array('class' => 'control-label')); ?>
                        {{ Form::text('tod_'.$tod_block_no, ($tod_details)?$tod_details->tod:'',array('class'=>'form-control datepicker tod','id'=>'tod_'.$tod_block_no)) }}
                    </div>
                    <div class="form-group">
                        <?= Form::label('days_ship_advance_'.$tod_block_no, 'No. of Days Need Ship Advance', array('class' => 'control-label')); ?>
                        {{ Form::text('days_ship_advance_'.$tod_block_no, ($tod_details)?$tod_details->days_ship_advance:'',array('class'=>'form-control','id'=>'days_ship_advance_'.$tod_block_no)) }}
                    </div>
                    <div class="form-group">
                        <?= Form::label('shipping_mode_id_'.$tod_block_no, 'Shipping Mode', array('class' => 'control-label')); ?>
                        {{ Form::select('shipping_mode_id_'.$tod_block_no, $order->shipping_mode_list, ($tod_details)?$tod_details->shipping_mode_id:'', $attributes = array('id'=>'shipping_mode_id_'.$tod_block_no, 'name'=>'shipping_mode_id_'.$tod_block_no, 'class'=>'form-control')) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= Form::label('cut_off_'.$tod_block_no, 'Cut Off', array('class' => 'control-label')); ?>
                        <?php if($buyer_template == 'Template 02'){ ?>
                        {{ Form::text('cut_off_'.$tod_block_no, ($tod_details)?$tod_details->destination->cut_off_day:'', array('class'=>'form-control','id'=>'cut_off_'.$tod_block_no, 'disabled'=>'disabled')) }}
                        <?php } else{ ?>
                        {{ Form::text('cut_off_'.$tod_block_no, ($tod_details)?$tod_details->destination->cut_off_day:'',array('class'=>'form-control','id'=>'cut_off_'.$tod_block_no, 'disabled'=>'disabled')) }}
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <?= Form::label('etd_'.$tod_block_no, 'ETD', array('class' => 'control-label')); ?>
                        <?php if($buyer_template == 'Template 02'){ ?>
                        {{ Form::text('etd_'.$tod_block_no, ($tod_details)?$tod_details->destination->etd_day:'', array('class'=>'form-control','id'=>'etd_'.$tod_block_no, 'disabled'=>'disabled')) }}
                        <?php } else{ ?>
                        {{ Form::text('etd_'.$tod_block_no, ($tod_details)?$tod_details->destination->etd_day:'', array('class'=>'form-control','id'=>'etd_'.$tod_block_no, 'disabled'=>'disabled')) }}
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= Form::label('remarks_'.$tod_block_no, 'Remarks', array('class' => 'control-label')); ?>
                        {{ Form::textarea('remarks_'.$tod_block_no, ($tod_details)?$tod_details->remarks:'',array('class'=>'form-control','id'=>'remarks_'.$tod_block_no)) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
