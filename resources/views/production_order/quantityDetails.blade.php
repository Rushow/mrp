<?php
$buyer_template = App\Buyer::find($order->buyer_id)->buyerTemplate->buyer_template;
?>

<div class="box box-info">
    <div class="box-header">
        <i class="fa fa-th"></i>
        <h3 class="box-title">Quantity Detail Block </h3>
        <!-- tools box -->
        <div class="pull-right box-tools">
            {{--<button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove" onclick="removeQuantityBlock(this);"><i class="fa fa-times"></i></button>--}}
        </div><!-- /. tools -->
    </div>
    <div class="box-body">

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4">
                    {{ Form::hidden('tod_detail_id_'.$quantity_block_no, ($quantity_details)?$quantity_details->id:'', array('id'=> 'tod_detail_id_'.$quantity_block_no, 'class'=>'tod_detail_id')) }}
                    {{ Form::hidden('quantity_block_no', $quantity_block_no, array('class'=> 'quantity_block_no')) }}
                    <div class="form-group">
                        <?= Form::label('article_id_'.$quantity_block_no, 'Article', array('class' => 'control-label')); ?>
                        {{ Form::select('article_id_'.$quantity_block_no, $order->article_list, ($quantity_details)?$quantity_details->article_id:'', $attributes = array('id'=>'article_id_'.$quantity_block_no, 'name'=>'article_id_'.$quantity_block_no, 'class'=>'form-control', 'disabled'=>'disabled')) }}
                    </div>
                    <div class="form-group">
                        <?= Form::label('color_id_'.$quantity_block_no, 'Color', array('class' => 'control-label')); ?>
                        {{ Form::select('color_id_'.$quantity_block_no, $order->color_list, ($quantity_details)?$quantity_details->color_id:'', $attributes = array('id'=>'color_id','name'=>'color_id_'.$quantity_block_no, 'class'=>'form-control', 'disabled'=>'disabled')) }}
                    </div>
                    <div class="form-group template01">
                        <?= Form::label('destination_id_'.$quantity_block_no, 'Destination', array('class' => 'control-label')); ?>
                        {{ Form::select('destination_id_'.$quantity_block_no, $order->destination_list, ($quantity_details)?$quantity_details->destination_id:'', $attributes = array('id'=>'destination_id_'.$quantity_block_no,'name'=>'destination_id_'.$quantity_block_no, 'class'=>'form-control', 'disabled'=>'disabled')) }}
                    </div>
                    <div class="form-group template02">
                        <?= Form::label('despo_'.$quantity_block_no, 'Dispo', array('class' => 'control-label')); ?>
                        {{ Form::select('despo_'.$quantity_block_no, $order->despo_list, ($quantity_details)?$quantity_details->despo:'', $attributes = array('id'=>'despo_'.$quantity_block_no,'name'=>'despo_'.$quantity_block_no, 'class'=>'form-control', 'disabled'=>'disabled')) }}
                    </div>
                </div>
                <div class="col-md-4 qty-col">
                    <div class="form-group template02">
                        <?= Form::label('pos_'.$quantity_block_no, 'Pos', array('class' => 'control-label')); ?>
                        {{ Form::text('pos_'.$quantity_block_no, ($quantity_details)?$quantity_details->pos:'', array('class'=>'form-control','id'=>'pos_'.$quantity_block_no, 'disabled'=>'disabled')) }}
                    </div>
                    <div class="form-group">
                        <?= Form::label('size_group_id_'.$quantity_block_no, 'Size Group', array('class' => 'control-label')); ?>
                        {{ Form::select('size_group_id_'.$quantity_block_no, $size_group, ($quantity_details)?$quantity_details->size_group_id:'', $attributes = array('id'=>'size_group_id_'.$quantity_block_no,'name'=>'size_group_id_'.$quantity_block_no, 'class'=>'form-control size_group_id')) }}
                    </div>
                    <div class="form-group">
                        <?= Form::label('total_quantity_'.$quantity_block_no, 'Total Quantity', array('class' => 'control-label')); ?>
                        {{ Form::text('total_quantity_'.$quantity_block_no, ($quantity_details)?$quantity_details->total_quantity:'', array('class'=>'form-control','id'=>'total_quantity_'.$quantity_block_no, 'data-toggle'=>"modal", 'data-target'=>"#myModal_".$quantity_block_no, 'onclick'=>'getSizesQuantityModal(this);', 'readonly'=>'readonly')) }}
                    </div>
                    <div class="form-group">
                        <?= Form::label('unit_value_'.$quantity_block_no, 'Unit Value', array('class' => 'control-label')); ?>
                        {{ Form::text('unit_value_'.$quantity_block_no, ($quantity_details)?$quantity_details->unit_value:'', array('class'=>'form-control','id'=>'unit_value_'.$quantity_block_no)) }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= Form::label('remarks_'.$quantity_block_no, 'Remarks', array('class' => 'control-label')); ?>
                        {{ Form::textarea('remarks_'.$quantity_block_no, ($quantity_details)?$quantity_details->remarks:'',array('class'=>'form-control','id'=>'remarks_'.$quantity_block_no)) }}
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal_<?=$quantity_block_no?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Size Quantity</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 size-error">
                                Please select Size Group First
                            </div>
                            <div class="col-md-12 size-box">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="calculateTotalQuantity(this);">Close</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="calculateTotalQuantity(this);">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


