@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Additional Print
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Additional Print</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'additionalPrint', 'method' => 'post', 'id' => 'additionalPrintForm', 'role' => 'form')); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <?= Form::label('additional_print', 'Additional Print', array('class' => 'control-label')); ?>
                                    {{ Form::text('additional_print','',array('class'=>'validate[required] form-control','id'=>'additional_print','placeholder'=>'Additional Print')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('status', 'Status', array('class' => 'control-label')); ?>
                                    {{ Form::select('status', $status,null,$attributes = array('id'=>'status','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'changeStatus(this)')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('enable_date', 'Enable Date', array('class' => 'control-label')); ?>
                                        {{ Form::text('enable_date', '',array('class'=>'validate[required] form-control datepicker','id'=>'enable_date')) }}
                                </div>

                            </div><!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                            </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(function() {
        $("#enable_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
    });
</script>



<script>
    $(document).ready(function() {
        changeStatus();

        $("#additionalPrintForm").validate({
            rules: {
                additional_print: "required",
                status: "required"
            },
            messages: {
                additional_print: "Please enter Additional Print",
                status: "Please select Status"
            }
        });
    });

    function changeStatus(){
        var status = $('#status').val();

        if(status == 1){
            $('#enable_date').removeAttr("disabled");
        }
        else{
            $('#enable_date').val('');
            $('#enable_date').attr("disabled", true);
        }
    }
</script>
@stop