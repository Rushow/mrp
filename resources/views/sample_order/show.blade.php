@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row" style="margin-bottom: 20px;">
    <div class="col-md-10 col-lg-10 col-sm-10">
        <h2>Sample Making Order</h2>
        <div class="col-md-4 col-lg-4 col-sm-4">
            <b>Sample Order No :</b> {{ $order->sample_order_no }} <br>
            <b>Buyer :</b> {{ $order->buyer_name }}
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4">
            <b>Order Date :</b> {{ date('d-m-Y', strtotime($order->order_date)) }}<br>
            <b>Delivery Date :</b> {{ date('d-m-Y', strtotime($order->delivery_date)) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">

        <table class="table table-first-column-number data-table display" style="margin-bottom: 20px;">
            <thead>
            <tr>
                <th>#<span class="sort-icon"><span></th>
                <th>Article<span class="sort-icon"><span></th>
                <th>Sample Type<span class="sort-icon"><span></th>
                <th>Size<span class="sort-icon"><span></th>
                <th>Color<span class="sort-icon"><span></th>
                <th>Quantity<span class="sort-icon"><span></th>
                <th>Note<span class="sort-icon"><span></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <? $total = 0 ?>
            @foreach($order_details as $key => $value)
                <tr>
                    <td>{{  $key + 1  }}&nbsp;</td>
                    <td>{{  $value->article_name  }}&nbsp;</td>
                    <td>{{  $value->sample_type  }}&nbsp;</td>
                    <td>
                        <? $t = count($value->Sizes) ?>
                        <? $i = 0 ?>
                        @foreach($value->Sizes as $s)
                            {{ $s->size_no }}
                            <? $i++ ?>
                            {{ ($i != $t)?', ':'' }}
                        @endforeach
                        &nbsp;
                    </td>
                    <td>{{  $value->color_name  }}&nbsp;</td>
                    <td>{{  $value->quantity  }} Pair&nbsp;</td>
                    <td>{{  $value->note  }}&nbsp;</td>
                    <td>
                        <? $spec = SampleOrderSpec::where('sample_order_detail_id', '=', $value->id)->get() ?>
                        @if(count($spec))
                            <a class="btn btn-default btn-md" href="{{ URL::to('sampleOrderSpec/'.$value->id) }}">Show Sample Spec for this Order</a>
                        @elseif (Auth::user()->isAdmin() || Auth::user()->isSuperAdmin() || Auth::user()->isSuperUser())
                            <a class="btn btn-default btn-md" href="{{ URL::to('sampleOrderSpec/create/'.$value->id) }}">Create Sample Spec for this Order</a>
                        @else
                            {{ "No Spec Available." }}
                        @endif
                        &nbsp;
                    </td>
                    <td>
                        
                        <? $spec = SampleOrderMds::where('sample_order_detail_id', '=', $value->id)->get() ?>
                        @if(count($spec))
                            <a class="btn btn-default btn-md" href="{{ URL::to('sampleOrderMds/'.$value->id) }}">Show MDS & TDS for this Order</a>
                        @elseif (Auth::user()->isAdmin() || Auth::user()->isSuperAdmin() || Auth::user()->isSuperUser())
                            <a class="btn btn-default btn-md" href="{{ URL::to('sampleOrderMds/create/'.$value->id) }}">Create MDS & TDS for this Order</a>
                        @else
                            {{ "No MDS & TDS Available." }}
                        @endif
                        &nbsp;
                    </td>
                </tr>
                <? $total += $value->quantity ?>
            @endforeach
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><b>Total&nbsp;</b></td>
                <td><b>{{ $total }} Pair&nbsp;</b></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        @if (!\Auth::user()->isViewer())
            <a class="btn btn-default btn-md" href="{{  URL::to('sampleOrder/'.$order->id.'/edit'); }}">Edit Order</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        @endif
        <a class="btn btn-default btn-md" href="{{ URL::to('excel/sampleOrder/'.$order->id) }}">Download Excel</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-default btn-md" href="{{ URL::to('pdf/sampleOrder/'.$order->id) }}">Download Pdf</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-default btn-md" href="{{ URL::to('sampleOrder') }}">Close</a>
    </div>
</div>

@stop