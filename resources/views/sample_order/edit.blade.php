@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<script>
    $(function() {
        $("#order_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });

        $("#delivery_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });

    });

</script>

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message){
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Edit Sample Order</h2>
            <?= Form::open(array('url' => 'sampleOrder/'.$order->id, 'method' => 'put', 'id' => 'sampleOrderForm')); ?>
            <div class="col-md-4 sidebar" style="margin-right: 150px;">
                <div class="form-group">
                    <?= Form::label('sample_order_no', 'Sample Order No', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('sample_order_no', $order->sample_order_no, array('class'=>'validate[required] form-control', 'id'=>'sample_order_no', 'readonly'=>'readonly')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('buyer_id', 'Buyer', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::select('buyer_id', $buyer, $order->buyer_id, $attributes = array('id'=>'buyer_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                    </div>
                </div>
            </div>
            <div class="col-md-6 sidebar">

                <div class="control-group">
                    <?= Form::label('order_date', 'Order Date', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('order_date', $order->order_date,array('class'=>'validate[required] form-control datepicker','id'=>'order_date')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('delivery_date', 'Delivery Date', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('delivery_date', $order->delivery_date, array('class'=>'validate[required] form-control datepicker','id'=>'delivery_date')) }}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <table>
                    <tr>Sample List</tr>
                    <tr>
                        <td>SL No.</td>
                        <td class="col-md-2">Article No.</td>
                        <td class="col-md-2">Type of Sample</td>
                        <td class="col-md-1">Sizes</td>
                        <td class="col-md-2">Color</td>
                        <td class="col-md-2">Quantity</td>
                        <td class="col-md-2">Note</td>
                    </tr>
                    {{ Form::hidden('total_row', $order_details->count(), array('id'=> 'total_row')) }}
                    <?
                    $i = 1;
                    foreach($order_details as $key => $value){ ?>
                    <tr>

                        <td><?=$i?></td>
                        <td>
                            {{ Form::select('article_id_'.$i, $article, $value->article_id, array('id'=>'article_id_'.$i,'data-rel'=>'chosen', 'class'=>'form-control Required')) }}
                        </td>
                        <td>
                            {{ Form::select('sample_type_id_'.$i, $sample_type, $value->sample_type_id, array('id'=>'sample_type_id_'.$i,'data-rel'=>'chosen', 'class'=>'form-control Required')) }}
                        </td>
                        <td>
                            {{ Form::select('size_id_'.$i, $size, array(1,2), array('id'=>'size_id_'.$i, 'name'=>'size_id_'.$i.'[]', 'multiple'=>'',  'class'=>'form-control Required multiselect')) }}
                        </td>
                        <td>
                            {{ Form::select('color_id_'.$i, $color, $value->color_id, array('id'=>'color_id_'.$i,'data-rel'=>'chosen', 'class'=>'form-control Required')) }}
                        </td>
                        <td>
                            {{ Form::text('quantity_'.$i, $value->quantity,array('class'=>'form-control Required','id'=>'quantity_'.$i)) }}
                        </td>
                        <td>
                            {{ Form::text('note_'.$i, $value->note,array('class'=>'form-control','id'=>'note_'.$i)) }}
                        </td>
                        <td>
                            <a href="#" onclick="return checkDelete(this);">
                                <span class="badge badge-important">X</span>
                            </a>
                        </td>
                    </tr>
                    <?
                    $i++;
                    }
                    ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><label onclick="addOrderRow(this);">+Add More</label> </td>
                    </tr>
                </table>
            </div>

            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Save', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    function addOrderRow(item){
        var last_id = $("#total_row").val();
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '<?= Url("sampleOrder/addOrderRow") ?>',
            data: 'last_id='+last_id,
            success: function(data){
                if(data.error == false){
                    $(item).parents('tr').before('<tr>'+data.html+'</tr>');
                    last_id = parseInt(last_id) + 1;
                    $('#total_row').val(last_id);
                    $('#size_id_'+last_id).multiselect();
                }
                else{
                    alert(data.message);
                }
            }
        });
    }


    $(document).ready(function() {
        $("#sampleOrderForm").validate({
            rules: {
                buyer_id: "required",
                order_date: "required",
                delivery_date: "required"
            },
            messages: {
                buyer_id: "Please Select a Buyer",
                order_date: "Please enter Order Date",
                delivery_date: "Please enter Delivery Date"
            }
        });

        $.validator.addClassRules("Required", {
            required: true
        });
    });
</script>

<style>
    .widget{
        overflow: visible !important;
    }
</style>

@stop