@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<?
    $sample_order_no_val = (isset($sample_order_no)) ? $sample_order_no : null;
    $buyer_val = (isset($buyer_id)) ? $buyer_id : null;
    $article_val = (isset($article_no)) ? $article_no : null;
    $sample_type_val = (isset($sample_type_id)) ? $sample_type_id : null;
    $order_date_val = (isset($order_date)) ? $order_date : null;
    $delivery_date_val = (isset($delivery_date)) ? $delivery_date : null;
?>
<div class="row">
    <?php if(Session::has('message')){ ?>
    <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
    <script>
        $('#alert').delay(2000).fadeOut(400);
    </script>
    <?php } ?>
    <div class="col-sm-10">
        @if (!Auth::user()->isViewer())
            <a href="<?= URL::to('sampleOrder/create')?>" class="grid btn">Create Sample Order</a>
        @endif
        <a href="<?= URL::to('excel/sampleOrder')?>" class="grid btn">Download excel file</a>
        <a href="<?= URL::to('pdf/sampleOrder')?>" target="_blank" class="grid btn">Download pdf file</a>
        <div class="row" style="margin-top: 20px;">
            <div class="col-sm-10">
                <div class="widget">
                    <?= Form::open(array('url' => 'sampleOrder/search', 'method' => 'post')); ?>
                    <div class="col-md-4" style="margin-right: 150px;">
                        <div class="form-group">
                            <?= Form::label('sample_order_no', 'Select Sample Order No', array('class' => 'control-label')); ?>
                            <div class="controls">
                                {{ Form::select('sample_order_no', $sample_order_no, $sample_order_no_val, $attributes = array('id'=>'sample_order_no','data-rel'=>'chosen', 'class'=>'form-control')) }}
                            </div>
                        </div>
                        <div class="control-group">
                            <?= Form::label('buyer_id', 'Buyer', array('class' => 'control-label')); ?>
                            <div class="controls">
                                {{ Form::select('buyer_id', $buyer, $buyer_val, $attributes = array('id'=>'buyer_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                            </div>
                        </div>
                        <div class="control-group">
                            <?= Form::label('article_id', 'Article No', array('class' => 'control-label')); ?>
                            <div class="controls">
                                {{ Form::select('article_id', $article, $article_val, $attributes = array('id'=>'article_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="control-group">
                            <?= Form::label('sample_type_id', 'Type of Sample', array('class' => 'control-label')); ?>
                            <div class="controls">
                                {{ Form::select('sample_type_id', $sample_type, $sample_type_val, $attributes = array('id'=>'sample_type_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                            </div>
                        </div>
                        <div class="control-group">
                            <?= Form::label('order_date', 'Order Date', array('class' => 'control-label')); ?>
                            <div class="controls">
                                {{ Form::text('order_date', $order_date_val,array('class'=>'validate[required] form-control datepicker','id'=>'order_date')) }}
                            </div>
                        </div>
                        <div class="control-group">
                            <?= Form::label('delivery_date', 'Delivery Date', array('class' => 'control-label')); ?>
                            <div class="controls">
                                {{ Form::text('delivery_date', $delivery_date_val,array('class'=>'validate[required] form-control datepicker','id'=>'delivery_date')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <?= Form::submit('Search', array('class' => 'btn btn-success')); ?>
                            </div>
                        </div>
                    </div>
                    <?= Form::close(); ?>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-10">
        <h2>Sample Order List</h2>
        <table class="table data-table display full" style="margin-bottom: 20px;">
            <thead>
            <tr>
                <th colspan="2">Sample Order No<span class="sort-icon"><span></th>
                <th>Buyer<span class="sort-icon"><span></th>
                <th>Order Date<span class="sort-icon"><span></th>
                <th>Delivery Date<span class="sort-icon"><span></th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($orders as $key => $value){ ?>
            <tr>
                <td colspan="2"><?= $value->sample_order_no ?>&nbsp;</td>
                <td><?= $value->Buyer->buyer_name ?>&nbsp;</td>
                <td><?= date('d.m.Y' ,strtotime($value->order_date)) ?>&nbsp;</td>
                <td><?= date('d.m.Y' ,strtotime($value->delivery_date)) ?>&nbsp;</td>
                <td>
                    <a class="" href="<?= URL::to('sampleOrder/'.$value->id);?>"><span class="label label-warning">Details</span></a>
                    @if (!Auth::user()->isViewer())
                        {{ Form::open(array('url' => 'sampleOrder/'.$value->id, 'method' => 'delete', 'style' => 'float:right; width:65px; margin-top:-5px; margin-bottom:-10px;')) }}
                        {{ Form::submit('Delete', array('class' => 'label label-warning', 'style' => 'display:block;')) }}
                        {{ Form::close() }}
                    @endif
                </td>
            </tr>
                <?php } ?>
        </table>
    </div>

</div>


<style>
    .btn{
        width: 200px!important;
        margin-top: 15px;
    }
</style>

@stop