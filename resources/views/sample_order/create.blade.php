@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<script>
    $(function() {
        $("#order_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });

        $("#delivery_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });

    });

</script>

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message){
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Create Sample Order</h2>
            <?= Form::open(array('url' => 'sampleOrder', 'method' => 'post', 'id' => 'sampleOrderForm')); ?>
            <div class="col-md-4 sidebar" style="margin-right: 150px;">
                <div class="form-group">
                    <?= Form::label('sample_order_no', 'Sample Order No', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('sample_order_no',$sample_order_no,array('class'=>'validate[required] form-control','id'=>'sample_order_no', 'readonly'=>'readonly')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('buyer_id', 'Buyer', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::select('buyer_id', $buyer, '', $attributes = array('id'=>'buyer_id','data-rel'=>'chosen', 'class'=>'form-control'))}}
                    </div>
                </div>
            </div>
            <div class="col-md-6 sidebar">

                <div class="control-group">
                    <?= Form::label('order_date', 'Order Date', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('order_date', '',array('class'=>'validate[required] form-control datepicker','id'=>'order_date')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('delivery_date', 'Delivery Date', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('delivery_date', '',array('class'=>'validate[required] form-control datepicker','id'=>'delivery_date')) }}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <table>
                    <tr>Sample List</tr>
                    <tr>
                        <td>SL No.</td>
                        <td class="col-md-2">Article No.</td>
                        <td class="col-md-2">Type of Sample</td>
                        <td class="col-md-1">Sizes</td>
                        <td class="col-md-2">Color</td>
                        <td class="col-md-2">Quantity</td>
                        <td class="col-md-2">Note</td>
                        <td></td>
                    </tr>
                    <tr>
                        {{ Form::hidden('total_row', 1, array('id'=> 'total_row')) }}
                        <td>1</td>
                        <td>
                            {{ Form::select('article_id_1', $article, '', $attributes = array('id'=>'article_id_1','data-rel'=>'chosen', 'class'=>'form-control Required')) }}
                        </td>
                        <td>
                            {{ Form::select('sample_type_id_1', $sample_type, '', $attributes = array('id'=>'sample_type_id_1','data-rel'=>'chosen', 'class'=>'form-control Required')) }}
                        </td>
                        <td >
                            {{ Form::select('size_id_1', $size, '', $attributes = array('id'=>'size_id_1', 'name'=>'size_id_1[]', 'multiple'=>'', 'class'=>'form-control Required multiselect')) }}
                        </td>
                        <td>
                            {{ Form::select('color_id_1', $color, '', $attributes = array('id'=>'color_id_1','data-rel'=>'chosen', 'class'=>'form-control Required')) }}
                        </td>
                        <td>
                            {{ Form::text('quantity_1', '',array('class'=>'form-control Required','id'=>'quantity_1')) }}
                        </td>
                        <td>
                            {{ Form::text('note_1', '',array('class'=>'form-control','id'=>'note_1')) }}
                        </td>
                        <td>
                            <!-- <a href="#" onclick="return checkDelete(this);">
                                <span class="badge badge-important">X</span>
                            </a> -->
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><label onclick="addOrderRow(this);" style="cursor: pointer;">+Add More</label> </td>
                    </tr>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Save', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    function addOrderRow(item){
        var last_id = $("#total_row").val();
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '<?= Url("sampleOrder/addOrderRow") ?>',
            data: 'last_id='+last_id,
            success: function(data){
                if(data.error == false){
                    $(item).parents('tr').before('<tr>'+data.html+'</tr>');
                    last_id = parseInt(last_id) + 1;
                    $('#total_row').val(last_id);
                    $('#size_id_'+last_id).multiselect();
                }
                else{
                    alert(data.message);
                }
            }
        });
    }


    $(document).ready(function() {
        $("#sampleOrderForm").validate({
            rules: {
                buyer_id: "required",
                order_date: "required",
                delivery_date: "required"
            },
            messages: {
                buyer_id: "Please Select a Buyer",
                order_date: "Please enter Order Date",
                delivery_date: "Please enter Delivery Date"
            }
        });

        $.validator.addClassRules("Required", {
            required: true
        });
    });
</script>

<style>
    .widget{
        overflow: visible !important;
    }
</style>

@stop