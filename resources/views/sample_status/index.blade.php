@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<script>
    $(function() {
        $("#order_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });

        $("#delivery_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });

    });

</script>
<?
    $sample_order_no_val = (isset($sample_order_no)) ? $sample_order_no : null;
    $buyer_val = (isset($buyer_id)) ? $buyer_id : null;
    $article_val = (isset($article_no)) ? $article_no : null;
    $sample_type_val = (isset($sample_type_id)) ? $sample_type_id : null;
    $order_date_val = (isset($order_date)) ? $order_date : null;
    $delivery_date_val = (isset($delivery_date)) ? $delivery_date : null;
?>
<div class="row">
    <?php if(Session::has('message')){ ?>
    <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
    <script>
        $('#alert').delay(2000).fadeOut(400)
    </script>
    <?php } ?>
    <div class="col-sm-10">
        <div class="row" style="margin-top: 10px;">
            <div class="col-sm-10">
                <a href="<?= URL::to('excel/sampleStatus')?>" class="grid btn">Download excel file</a>
                <a href="<?= URL::to('pdf/sampleStatus')?>" target="_blank" class="grid btn">Download pdf file</a>
            <div class="widget" style="margin-top: 15px;">
                <?= Form::open(array('url' => 'sampleStatus/search', 'method' => 'post')); ?>
                <div class="col-md-4" style="margin-right: 150px;">
                    <div class="form-group">
                        <?= Form::label('sample_order_no', 'Select Sample Order No', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('sample_order_no', $sample_order_no, $sample_order_no_val, $attributes = array('id'=>'sample_order_no','data-rel'=>'chosen', 'class'=>'form-control')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('buyer_id', 'Buyer', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('buyer_id', $buyer, $buyer_val, $attributes = array('id'=>'buyer_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('article_id', 'Article No', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('article_id', $article, $article_val, $attributes = array('id'=>'article_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="control-group">
                        <?= Form::label('sample_type_id', 'Type of Sample', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('sample_type_id', $sample_type, $sample_type_val, $attributes = array('id'=>'sample_type_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('order_date', 'Order Date', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('order_date', $order_date_val,array('class'=>'validate[required] form-control datepicker','id'=>'order_date')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('delivery_date', 'Delivery Date', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('delivery_date', $delivery_date_val,array('class'=>'validate[required] form-control datepicker','id'=>'delivery_date')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="controls">
                            <?= Form::submit('Search', array('class' => 'btn btn-success')); ?>
                        </div>
                    </div>
                </div>
                <?= Form::close(); ?>
                <div class="clear"></div>
            </div>
            </div>
        </div>
    </div>


    <div class="col-sm-10">
        <h2>Sample Status List</h2>
        <table class="table table-first-column-number data-table display full" style="margin-bottom: 20px;">
            <thead>
            <tr>
                <th>Sl. No<span class="sort-icon"><span></th>
                <th>Sample Order No<span class="sort-icon"><span></th>
                <th>Article No<span class="sort-icon"><span></th>
                <th>Order Date<span class="sort-icon"><span></th>
                <th>Delivery Date<span class="sort-icon"><span></th>
                <th>Pattern<span class="sort-icon"><span></th>
                <th>Test Sample<span class="sort-icon"><span></th>
                <th>Grading<span class="sort-icon"><span></th>
                <th>Material Status<span class="sort-icon"><span></th>

                            <th>Cutting<span class="sort-icon"><span></th>
                            <th>Closing<span class="sort-icon"><span></th>
                            <th>Assembly<span class="sort-icon"><span></th>
                            <th>QC Report<span class="sort-icon"><span></th>

                <th>Actual Delivery<span class="sort-icon"><span></th>
                <th>Note<span class="sort-icon"><span></th>
                @if (!Auth::user()->isViewer())
                    <th>Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
            <?php foreach ($details as $detail_count => $detail) { ?>
            <tr>
                <td><?= $detail->id ?>&nbsp;</td>
                <td><?= $detail->SampleOrder->sample_order_no ?>&nbsp;</td>
                <td><?= $detail->Article->article_no ?>&nbsp;</td>
                <td><?= $detail->SampleOrder->order_date ?>&nbsp;</td>
                <td><?= $detail->SampleOrder->delivery_date ?>&nbsp;</td>

                <td><?= ($detail->status->pattern != null)? $detail->status->pattern : 'None' ?>&nbsp;</td>
                <td><?= ($detail->status->test_sample != null)? $detail->status->test_sample : 'None' ?>&nbsp;</td>
                <td><?= ($detail->status->grading != null)? $detail->status->grading : 'None' ?>&nbsp;</td>
                <td><?= ($detail->status->material_status != null)? $detail->status->material_status : 'None' ?>&nbsp;</td>
                <td><?= ($detail->status->DevelopmentStatus != null)? $detail->status->DevelopmentStatus->cutting : 'None' ?>&nbsp;</td>
                <td><?= ($detail->status->DevelopmentStatus != null)? $detail->status->DevelopmentStatus->closing : 'None' ?>&nbsp;</td>
                <td><?= ($detail->status->DevelopmentStatus != null)? $detail->status->DevelopmentStatus->assembly : 'None' ?>&nbsp;</td>
                <td><?= ($detail->status->DevelopmentStatus != null)? $detail->status->DevelopmentStatus->qc_report : 'None' ?>&nbsp;</td>

                <td><?= ($detail->status != null)? $detail->status->actual_delivery : 'None' ?>&nbsp;</td>
                <td style="max-width:200px; word-wrap:break-word;"><?= ($detail->status != null)? $detail->status->note : 'No Notes' ?>&nbsp;</td>
                @if (!Auth::user()->isViewer())
                    <td>
                            <?= ($detail->status != null)? '<a class="" href="' . URL::to('sampleStatus/'.$detail->schedule->id.'/edit') . '"><span class="label label-warning">Edit</span></a>':'<a class="" href="' . URL::to('sampleStatus/create/') . '"><span class="label label-warning">Create</span></a>' ?>
                    </td>
                @endif
            </tr> 
            <?php } ?>
        </table>
    </div>

</div>


<style>
    .btn{
        width: 200px!important;
        margin-top: 15px;
    }
</style>

@stop