@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<script>
    $(function() {

        $("#actual_delivery").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });

    });

</script>

<div class="row">
    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Update Sample Status Info</h2>            
        </div>
        <div>
            <?= Form::open(array('url' => 'sampleStatus/'.$sample_status->id, 'method' => 'put', 'id' => 'sampleScheduleForm')); ?>
            <table class="table table-first-column-number data-table display sort" style="margin-bottom: 20px;">
                <thead>
                <tr>
                    <th>Sample Order No<span class="sort-icon"><span></th>
                    <th>Article No<span class="sort-icon"><span></th>
                    <th>Order Date<span class="sort-icon"><span></th>
                    <th>Delivery Date<span class="sort-icon"><span></th>
                    <th>Pattern<span class="sort-icon"><span></th>
                    <th>Test Sample<span class="sort-icon"><span></th>
                    <th>Grading<span class="sort-icon"><span></th>
                    <th>Material Status<span class="sort-icon"><span></th>
                    <th>Cutting<span class="sort-icon"><span></th>
                    <th>Closing<span class="sort-icon"><span></th>
                    <th>Assembly<span class="sort-icon"><span></th>
                    <th>QC Report<span class="sort-icon"><span></th>
                    <th>Actual Delivery<span class="sort-icon"><span></th>
                    <th>Note<span class="sort-icon"><span></th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $sample_status->SampleOrderDetail->SampleOrder->sample_order_no ?></td>
                        <td><?= $sample_status->SampleOrderDetail->Article->article_no ?></td>
                        <td><?= $sample_status->SampleOrderDetail->SampleOrder->order_date ?></td>
                        <td><?= $sample_status->SampleOrderDetail->SampleOrder->delivery_date ?></td>
                        <td>
                            <div class="control-group">
                                <div class="controls">
                                    {{ Form::select('pattern', array('ok' => 'OK', 'running' => 'Running', 'n/a' => 'N/A'), $sample_status->pattern) }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="control-group">
                                <div class="controls">
                                    {{ Form::select('test_sample', array('ok' => 'OK', 'running' => 'Running', 'n/a' => 'N/A'), $sample_status->test_sample) }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="control-group">
                                <div class="controls">
                                    {{ Form::select('grading', array('ok' => 'OK', 'running' => 'Running', 'n/a' => 'N/A'), $sample_status->grading) }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="control-group">
                                <div class="controls">
                                    {{ Form::select('material_status', array('ok' => 'OK', 'running' => 'Running', 'n/a' => 'N/A'), $sample_status->material_status) }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="control-group">
                                <div class="controls">
                                    {{ Form::select('cutting', array('ok' => 'OK', 'running' => 'Running', 'n/a' => 'N/A'),  $cutting) }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="control-group">
                                <div class="controls">
                                    {{ Form::select('closing', array('ok' => 'OK', 'running' => 'Running', 'n/a' => 'N/A'),  $closing) }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="control-group">
                                <div class="controls">
                                    {{ Form::select('assembly', array('ok' => 'OK', 'running' => 'Running', 'n/a' => 'N/A'),  $assembly) }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="control-group">
                                <div class="controls">
                                    {{ Form::select('qc_report', array('ok' => 'OK', 'running' => 'Running', 'n/a' => 'N/A'),  $qc_report) }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="control-group">

                                <div class="controls">
                                    {{ Form::text('actual_delivery', $sample_status->actual_delivery,array('class'=>'validate[required] form-control datepicker','id'=>'actual_delivery')) }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <div class="controls">
                                    {{ Form::textarea('note', $sample_status->note,array('class'=>'form-control','id'=>'note')) }}
                                </div>
                            </div>
                        </td>
                    </tr> 
                </tbody>
            </table>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Update Status Info', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#sampleScheduleForm").validate({
            rules: {
                pattern: "required",
                test_sample: "required",
                grading: "required",
                material_status: "required",
                cutting: "required",
                closing: "required",
                assembly: "required",
                qc_report: "required",
                actual_delivery: "required"
            },
            messages: {
                pattern: "Please enter a pattern",
                test_sample: "Please enter a test sample",
                grading: "Please enter a grading",
                material_status: "Please enter a material status",
                cutting: "Please enter a cutting",
                closing: "Please enter a closing",
                assembly: "Please enter a assembly",
                qc_report: "Please enter a QC report",
                actual_delivery: "Please enter actual delivery date"
            }
        });
    });
</script>

@stop