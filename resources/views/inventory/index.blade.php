@extends('layouts.master')
@section('head')

@parent
{{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')

<?
    $item_val = (isset($item)) ? $item : null;
    $from_date_val = (isset($from_date)) ? $from_date : null;
    $to_date_val = (isset($to_date)) ? $to_date : null;
?>


<div class="row">
    <div class="col-md-12 sidebar">

        <?php if(Session::has('message')){ ?>
        <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
        <script>
            $('#alert').delay(2000).fadeOut(400)
        </script>
        <?php } ?>
        <data class="row">
            <div class="col-sm-8">
            </div>
            <div class="col-sm-4">
                <a href="<?= URL::to('excel/inventory')?>" class="btn btn-primary" style="max-width: 200px">Download excel file</a>
                <a href="<?= URL::to('pdf/inventory')?>" target="_blank" class="btn btn-info" style="max-width: 200px">Download pdf file</a>
            </div>
        </data>
        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <!-- form start -->
                        <?= Form::open(array('url' => 'inventory/search', 'method' => 'post')); ?>
                        <div class="box-body">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= Form::label('item_id', 'Item Name', array('class' => 'control-label')); ?>
                                    {{ Form::select('item_id', $item, $item_val, $attributes = array('id'=>'item_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= Form::label('from_date', 'From', array('class' => 'control-label')); ?>
                                    {{ Form::text('from_date', $from_date_val,array('class'=>'validate[required] form-control datepicker','id'=>'from_date')) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= Form::label('to_date', 'To', array('class' => 'control-label')); ?>
                                    {{ Form::text('to_date', $to_date_val,array('class'=>'validate[required] form-control datepicker','id'=>'to_date')) }}
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-2 col-md-offset-11">
                                <button type="submit" class="btn btn-success">Search</button>
                            </div>

                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Inventory List
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Inventory List</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Item Name<span class="sort-icon"><span></th>
                                    <th>Group Name<span class="sort-icon"><span></th>
                                    <th>Sub Group Name<span class="sort-icon"><span></th>
                                    <th>Store Unit<span class="sort-icon"><span></th>
                                    <th>Store Ins<span class="sort-icon"><span></th>
                                    <th>Store Outs<span class="sort-icon"><span></th>
                                    <th>Remainder<span class="sort-icon"><span></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($allItems as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->item_name }}</td>
                                        <td>{{ $item->Group->group_name }}</td>
                                        <td>{{ $item->SubGroup->sub_group_name }}</td>
                                        <td>{{ $item->StoreUnit->unit_name }}</td>
                                        <td>{{ $store_in_items['item_'.$item->id] }}</td>
                                        <td>{{ $store_out_items['item_'.$item->id] }}</td>
                                        <td>{{ $remainder_items['item_'.$item->id] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
        </section>


    </div>
</div>

@stop

@section('page-script')
    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js')}}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js')}}
@stop

@section('plugin-script')
    <script type="text/javascript">

        function generateSubGroup(item){
            var val = $(item).val();
            if(val!=""){
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("item/subGroup") ?>',
                    data: 'group_id='+val,
                    success: function(data){
                        $("#sub_group_id").removeAttr('readonly');
                        $("#sub_group_id").removeAttr('disabled');
                        $("#sub_group_id").html(data.sub_group);
                    }
                });
            }
            else{
                $("#sub_group_id").html('<option value="">Select</option>');
                $("#sub_group_id").attr('readonly', 'readonly');
                $("#sub_group_id").attr('disabled', 'disabled');
            }
        }
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });

            $("#from_date").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#to_date").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
        });
    </script>

    <style>
        .sidebar{
            padding-bottom: 20px;
        }
        form{
            margin: 15px;
        }
    </style>
@stop