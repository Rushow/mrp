@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add User Type
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add User Type</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'userType', 'method' => 'post', 'id' => 'userTypeForm')); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <?= Form::label('user_type', 'User Type', array('class' => 'control-label', 'placeholder' => 'User Type')); ?>
                                <?= Form::text('user_type', null, array('class' => 'form-control')); ?>
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#userTypeForm").validate({
            rules: {
                user_type: "required"
            },
            messages: {
                user_type: "Please enter User Type"
            }
        });
    });
</script>

@stop