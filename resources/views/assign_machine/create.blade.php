@extends('layouts.master')
@section('head')

    @parent

@stop
@section('header')
@stop
@section('content')


    <div class="row">

        <div class="col-md-12 sidebar">
            <?php
            $error_messages = Session::get('error_messages');

            if(isset($error_messages)){
                foreach($error_messages as $message){
                    echo $message;
                }
            }


            ?>
            <div class="widget">
                <h2 class="topheadertext"> Assign Machine :</h2>
                <?= Form::open(array('url' => 'capacity', 'method' => 'post', 'id' => 'addAssignForm')); ?>
                <div class="col-md-4 sidebar" style="margin-right: 150px;">
                    <div class="form-group">
                        <?= Form::label('machine_id', 'Machine ID', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('machine_id',$machine,'',$attributes=array('class'=>'validate[required] form-control','id'=>'machine_id'))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('order_id', 'Order Id', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('order_id',$production_order,'',$attributes=array('class'=>'validate[required] form-control','id'=>'order_id')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('style', 'Style/Design', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('style',array('Ballerina','Converse','Something'),'',$attributes=array('class'=>'validate[required] form-control','id'=>'style')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('complexity', 'Style Complexity', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('complexity',array('Easy','Hard','Advance'),'',$attributes=array('class'=>'validate[required] form-control','id'=>'complexity')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('task', 'Task', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('task','',array('class'=>'validate[required] form-control','id'=>'task'))}}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('start_time', 'Start DateTime', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('start_time','',array('class'=>'validate[required] form-control','id'=>'start_time'))}}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('end_time', 'End DateTime', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('end_time','',array('class'=>'validate[required] form-control','id'=>'end_time'))}}

                        </div>
                    </div>

                    <br>
                    <div class="form-group">
                        <div class="controls">
                            <?= Form::submit('Save', array('class' => 'btn btn-success')); ?>
                        </div>
                    </div>
                </div>


                <?= Form::close(); ?>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function() {
            $("#addAssignForm").validate({
                rules: {
                    machine_id:"required",
                    hours_of_operation:"required",
                    manpower: "required",
                    product_type: "required",
                    style:"required",
                    unit:"required"


                }
            });

            $.validator.addClassRules("Required", {
                required: true
            });

        });
        $(function() {


            $("#start_time").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#end_time").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy',
                timeFormat: 'hh:mm tt'
            });

        });
    </script>
    <style>
        .sidebar{
            padding-bottom: 20px;
        }
        form{
            margin: 15px;
        }

        .widget{
            overflow: visible !important;
        }
    </style>

@stop