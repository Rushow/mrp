@extends('layouts.master')
@section('head')

    @parent
    {{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')

    <div class="row">

        <div class="col-md-12 sidebar">
            <?php
            $error_messages = Session::get('error_messages');

            if(isset($error_messages)){
                foreach($error_messages as $message){
                    echo $message;
                }
            }

            ?>
                    <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Machine List
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Machine List</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">

                                <div class="col-md-12">
                                    <table class="table table-first-column-number data-table display" id="example1">
                                        <thead>
                                        <tr>
                                            <th>SL No.</th>
                                            <th class="col-md-2">Machine ID</th>
                                            <th class="col-md-1">Machine Name</th>
                                            <th class="col-md-1">Division </th>
                                            <th class="col-md-1">Sub Division </th>
                                            <th class="col-md-1">Current Order</th>
                                            <th class="col-md-1">Article </th>
                                            <th class="col-md-1">Start Date</th>
                                            <th class="col-md-1">End Date</th>
                                            <th class="col-md-2">Action</th>


                                            <td></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?
                                        $total = 0;
                                        foreach($machine as $key => $value){
                                        ?>
                                        <tr>
                                            <td><?=$value->id?></td>
                                            <td><?=$value->machine_id?></td>
                                            <td><?=$value->machine_name?></td>
                                            <td><?=$value->division->name?></td>
                                            <td><?=$value->subdivision->subdivision_name?></td>

                                            <th class="col-md-1">Order ID </th>
                                            <th class="col-md-1">Article </th>
                                            <th class="col-md-1">Start Date</th>
                                            <th class="col-md-1">End Date</th>
                                            <td>

                                                <a class="btn btn-info btn-sm" href="<?=URL::to('machine/'.$value->id.'/edit')?>">Edit</a>
                                                {{ Form::open(['route' => ['machine.destroy', $value->id], 'method' => 'delete']) }}
                                                {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                                {{ Form::close() }}


                                            </td>


                                        </tr>
                                        <? } ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><a href="<?=URL::to('machine/create')?>" class="btn btn-warning btn-sm">Add More</a>  </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>




        </div>







        @stop
        @section('page-script')
                <!-- Select2 -->
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js') }}

    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}
@stop