@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Register User
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Register User</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'registration', 'method' => 'post', 'id' => 'registrationForm')); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <?= Form::label('first_name', 'First Name', array('class' => 'control-label')); ?>
                                {{ Form::text('first_name','',array('class'=>'validate[required] form-control','id'=>'first_name')) }}
                            </div>
                            <div class="form-group">
                                <?= Form::label('last_name', 'Last Name', array('class' => 'control-label')); ?>
                                {{ Form::text('last_name', '', array('class'=>'validate[required] form-control','id'=>'last_name')) }}
                            </div>
                            <div class="form-group">
                                <?= Form::label('email', 'Email', array('class' => 'control-label')); ?>
                                {{ Form::text('email', '', array('class'=>'validate[required] email form-control','id'=>'email')) }}
                            </div>
                            <div class="form-group">
                                <?= Form::label('name', 'Username', array('class' => 'control-label')); ?>
                                {{ Form::text('name', '', array('class'=>'validate[required] form-control','id'=>'name')) }}
                            </div>
                            <div class="form-group">
                                <?= Form::label('user_type_id', 'User Type', array('class' => 'control-label')); ?>
                                {{ Form::select('user_type_id', $roles, null, array('id'=>'role','data-rel'=>'chosen', 'class'=>'validate[required] form-control')) }}
                            </div>
                            <div class="form-group">
                                <?= Form::label('password', 'Password', array('class' => 'control-label')); ?>
                                {{ Form::password('password',array('class'=>'validate[required,minSize[6]] form-control','id'=>'password','placeholder'=>'xxxxxxxxx')) }}
                            </div>
                            <div class="form-group">
                                <?= Form::label('re_password', 'Re-enter Password', array('class' => 'control-label')); ?>
                                {{ Form::password('re_password',array('class'=>'validate[required,minSize[6],equals[password]] form-control','id'=>'re_password','placeholder'=>'xxxxxxxxx')) }}
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>


<script>
    $(document).ready(function() {
        $("#registrationForm").validate({
            rules: {
                first_name: "required",
                last_name: "required",
                email: {
                    required: true,
                    email: true
                },
                name: "required",
                password: {
                    required: true,
                    minlength: 6
                },
                confirm_password: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                }
            },
            messages: {
                first_name: "Please enter First Name",
                last_name: "Please enter Last Name",
                email: "Please enter a valid email address",
                name: "Please enter User Name",
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                }
            }
        });
    });
</script>

@stop