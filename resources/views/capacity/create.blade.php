@extends('layouts.master')
@section('head')

    @parent

@stop
@section('header')
@stop
@section('content')


    <div class="row">

        <div class="col-md-12 sidebar">
            <?php
            $error_messages = Session::get('error_messages');

            if(isset($error_messages)){
                foreach($error_messages as $message){
                    echo $message;
                }
            }


            ?>
            <div class="widget">
                <h2 class="topheadertext"> Add Capacity :</h2>
                <?= Form::open(array('url' => 'capacity', 'method' => 'post', 'id' => 'addCapacityForm')); ?>
                <div class="col-md-4 sidebar" style="margin-right: 150px;">
                    <div class="form-group">
                        <?= Form::label('machine_id', 'Machine ID', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('machine_id',$machine,'',$attributes=array('class'=>'validate[required] form-control','id'=>'machine_id'))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('hours_of_operation', 'Hour of operation ', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('hours_of_operation',array( '1' => 1, '2' => 2,'3' => 3,'4' => 4,'5' => 5, '6' => 6,'7' => 7,'8' => 8),'',$attributes=array('class'=>'validate[required] form-control','id'=>'hours_of_operation')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('manpower', 'Man Power', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('manpower',array( '50' => 50, '100' => 100,'200' => 200,'500' => 500),'',$attributes=array('class'=>'validate[required] form-control','id'=>'manpower'))}}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('product_type', 'Product Type/Article', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('product_type',$article, '', $attributes=array('class'=>'validate[required] form-control','id'=>'product_type'))}}

                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('Style', 'Style/Construction', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('style',$construction, '',$attributes=array('class'=>'validate[required] form-control','id'=>'style'))}}

                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('unit', 'Unit', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('unit', '',array('class'=>'validate[required] form-control','id'=>'unit'))}}

                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="controls">
                            <?= Form::submit('Save', array('class' => 'btn btn-success')); ?>
                        </div>
                    </div>
                </div>


                <?= Form::close(); ?>
            </div>
        </div>
    </div>


<script>
    $(document).ready(function() {
        $("#addCapacityForm").validate({
            rules: {
                machine_id:"required",
                hours_of_operation:"required",
                manpower: "required",
                product_type: "required",
                style:"required",
                unit:"required"


            }
        });

        $.validator.addClassRules("Required", {
            required: true
        });

    });
</script>
    <style>
        .sidebar{
            padding-bottom: 20px;
        }
        form{
            margin: 15px;
        }

        .widget{
            overflow: visible !important;
        }
    </style>

@stop