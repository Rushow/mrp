@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">
    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Update Color
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Update Color</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'color/'.$color->id, 'method' => 'put', 'id' => 'colorForm')); ?>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('pantone_code', 'Pantone Code', array('class' => 'control-label')); ?>
                                    {{ Form::text('pantone_code',$color->pantone_code,array('class'=>'validate[required] form-control','id'=>'pantone_code')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('hm_code', 'Buyer Code', array('class' => 'control-label')); ?>
                                    {{ Form::text('hm_code',$color->hm_code,array('class'=>'validate[required] form-control','id'=>'hm_code')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('color_group_code', 'Color Group Code', array('class' => 'control-label')); ?>
                                    {{ Form::text('color_group_code',$color->color_group_code,array('class'=>'form-control','id'=>'color_group_code')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('color_name_flcl', 'Color Name(FLCL)', array('class' => 'control-label')); ?>
                                    {{ Form::text('color_name_flcl',$color->color_name_flcl,array('class'=>'form-control','id'=>'color_name_flcl')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('color_name_hm', 'Color Name(Buyer)', array('class' => 'control-label')); ?>
                                    {{ Form::text('color_name_hm',$color->color_name_hm,array('class'=>'form-control','id'=>'color_name_hm')) }}
                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <div class="col-md-4 col-md-offset-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                            </div>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#colorForm").validate({
            rules: {
                pantone_code: "required",
                hm_code: "required"
            },
            messages: {
                pantone_code: "Please enter Pantone Code",
                hm_code: "Please enter HM Code"
            }
        });
    });
</script>
@stop