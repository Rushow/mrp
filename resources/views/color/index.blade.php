@extends('layouts.master')
@section('head')

@parent
{{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')


    <div class="row">
        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">

                <?php if(Session::has('message')){ ?>
                <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
                <script>
                    $('#alert').delay(2000).fadeOut(400)
                </script>
                <?php } ?>

                <h1>
                    Color List
                </h1>
                <br>
                <data class="row">
                    <div class="col-sm-8">
                        @if (!Auth::user()->isViewer())
                            <a href="<?= URL::to('color/create')?>" class="btn btn-warning">Create Color</a>
                        @endif
                    </div>
                    <div class="col-sm-4">
                        <a href="<?= URL::to('excel/color')?>" class="btn btn-primary">Download excel file</a>
                        <a href="<?= URL::to('pdf/color')?>" target="_blank" class="btn btn-info">Download pdf file</a>
                    </div>
                </data>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Color List</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Pantone Code<span class="sort-icon"><span></th>
                                        <th>Buyer Code<span class="sort-icon"><span></th>
                                        <th>Color Group Code<span class="sort-icon"><span></th>
                                        <th>Color Name (FLCL)<span class="sort-icon"><span></th>
                                        <th>Color Name (Buyer)<span class="sort-icon"><span></th>
                                        <th>Create Date<span class="sort-icon"><span></th>
                                        @if (!Auth::user()->isViewer())
                                            <th>Action</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($colors as $key => $value){ ?>
                                    <tr>
                                        <td><?= $value->id ?>&nbsp;</td>
                                        <td><?= $value->pantone_code ?>&nbsp;</td>
                                        <td><?= $value->hm_code ?>&nbsp;</td>
                                        <td><?= $value->color_group_code ?>&nbsp;</td>
                                        <td><?= $value->color_name_flcl ?>&nbsp;</td>
                                        <td><?= $value->color_name_hm ?>&nbsp;</td>
                                        <td><?= $value->created_at->format('d.m.Y') ?>&nbsp;</td>
                                        @if (!Auth::user()->isViewer())
                                            <td>
                                                <a class="btn btn-info btn-sm" href="<?= URL::to('color/'.$value->id).'/edit';?>">Edit</a>
                                                {{ Form::open(array('class' => 'btn btn-danger btn-xs','url' => 'color/'.$value->id, 'method' => 'delete', 'style' => 'padding: 3px 0; border-color: transparent !important;')) }}
                                                {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs', 'style' => 'display:block;')) }}
                                                {{ Form::close() }}
                                            </td>
                                        @endif
                                    </tr>
                                    <?php } ?>

                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>


        </div>
    </div>

@stop

@section('page-script')
    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js')}}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js')}}
@stop

@section('plugin-script')
    <script type="text/javascript">
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@stop
