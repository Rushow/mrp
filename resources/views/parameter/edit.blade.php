@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">
    <div class="col-md-6 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Update Parameter</h2>
            <?= Form::open(array('url' => 'parameter/'.$parameter->id, 'method' => 'put', 'id' => 'parameterForm')); ?>
            <div class="form-group">
                <?= Form::label('parameter_name', 'Parameter Name', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::text('parameter_name', $parameter->parameter_name, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Submit', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#parameterForm").validate({
            rules: {
                parameter_name: "required"
            },
            messages: {
                parameter_name: "Please enter Parameter Name"
            }
        });
    });
</script>
@stop