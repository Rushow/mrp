@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">
    <div class="col-md-6 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Update Heel No</h2>
            <?= Form::open(array('url' => 'heelNo/'.$heel_no->id, 'method' => 'put', 'id' => 'heelNoForm')); ?>
            <div class="form-group">
                <?= Form::label('heel_no', 'Heel No', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::text('heel_no', $heel_no->heel_no, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Submit', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#heelNoForm").validate({
            rules: {
                heel_no: "required"
            },
            messages: {
                heel_no: "Please enter Heel No"
            }
        });
    });
</script>
@stop