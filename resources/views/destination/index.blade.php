@extends('layouts.master')
@section('head')

@parent
{{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')

    <div class="row">
        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">

                <?php if(Session::has('message')){ ?>
                <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
                <script>
                    $('#alert').delay(2000).fadeOut(400)
                </script>
                <?php } ?>

                <h1>
                    Country List
                </h1>
                <br>
                <data class="row">
                    <div class="col-sm-8">
                        @if (!Auth::user()->isViewer())
                            <a href="<?= URL::to('destination/create')?>" class="btn btn-warning">Create Country</a>
                        @endif
                    </div>
                    <div class="col-sm-4">
                        <a href="<?= URL::to('excel/destination')?>" class="btn btn-primary">Download excel file</a>
                        <a href="<?= URL::to('pdf/destination')?>" target="_blank" class="btn btn-info">Download pdf file</a>
                    </div>
                </data>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Country List</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Country Code<span class="sort-icon"><span></th>
                                        <th>Country<span class="sort-icon"><span></th>
                                        <th>Cut Off Day Difference<span class="sort-icon"><span></th>
                                        <th>Cut Off Day<span class="sort-icon"><span></th>
                                        <th>ETD Difference<span class="sort-icon"><span></th>
                                        <th>ETD Day<span class="sort-icon"><span></th>
                                        <th>Assortment Packing<span class="sort-icon"><span></th>
                                        <th>Carton Details Assort<span class="sort-icon"><span></th>
                                        <th>Carton Details Primary<span class="sort-icon"><span></th>
                                        <th>Carton Details Secondary<span class="sort-icon"><span></th>
                                        <th>Consignee Address<span class="sort-icon"><span></th>
                                        <th>Socks Print<span class="sort-icon"><span></th>
                                        <th>Lining Print<span class="sort-icon"><span></th>
                                        <th>Additional Print<span class="sort-icon"><span></th>
                                        <th>Poly Sticker<span class="sort-icon"><span></th>
                                        <th>Pictogram<span class="sort-icon"><span></th>
                                        <th>Price Tag<span class="sort-icon"><span></th>
                                        <th>PM Tag<span class="sort-icon"><span></th>
                                        <th>Additional Information Sticker<span class="sort-icon"><span></th>
                                        <th>Remarks<span class="sort-icon"><span></th>
                                        @if (!Auth::user()->isViewer())
                                            <th>Action</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($destinations as $key => $value){ ?>
                                    <tr>
                                        <td><?= $value->destination_code ?></td>
                                        <td><?= $value->destination_name ?></td>
                                        <td><?= $value->cut_off_diff ?></td>
                                        <td><?= $value->cut_off_day ?></td>
                                        <td><?= $value->etd_diff ?></td>
                                        <td><?= $value->etd_day ?></td>
                                        <td><?= $value->assort_packing ?></td>
                                        <td><?= $value->carton_details_assort ?></td>
                                        <td><?= $value->carton_details_primary ?></td>
                                        <td><?= $value->carton_details_secondary ?></td>
                                        <td><?= $value->consignee_address ?></td>
                                        <td><?= $value->sock_print ?></td>
                                        <td><?= $value->lining_print ?></td>
                                        <td><?= $value->additional_print ?></td>
                                        <td><?= $value->poly_sticker ?></td>
                                        <td><?= $value->pictogram ?></td>
                                        <td><?= $value->price_tag ?></td>
                                        <td><?= $value->pm_tag ?></td>
                                        <td><?= $value->additional_sticker_info ?></td>
                                        <td><?= $value->remarks ?></td>
                                        @if (!Auth::user()->isViewer())
                                            <td>
                                                <a class="btn btn-info btn-sm" href="<?= URL::to('destination/'.$value->id).'/edit';?>">Edit</a>
                                                {{ Form::open(array('class' => 'btn btn-danger btn-xs','url' => 'destination/'.$value->id, 'method' => 'delete', 'style' => 'padding: 3px 0; border-color: transparent !important;')) }}
                                                {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs', 'style' => 'display:block;')) }}
                                                {{ Form::close() }}
                                            </td>
                                        @endif
                                    </tr>
                                    <?php } ?>

                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>


        </div>
    </div>

@stop

@section('page-script')
    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}
@stop

@section('plugin-script')
    <script type="text/javascript">
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                'scrollX': true
            });
        });
    </script>
@stop
