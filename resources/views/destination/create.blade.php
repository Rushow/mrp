@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Country
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Country</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'destination', 'method' => 'post', 'id' => 'destinationForm')); ?>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('destination_name', 'Country Name', array('class' => 'control-label')); ?>
                                    {{ Form::text('destination_name','',array('class'=>'validate[required] form-control','id'=>'destination_name')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('destination_code', 'Country Code', array('class' => 'control-label')); ?>
                                    {{ Form::text('destination_code','',array('class'=>'validate[required] form-control','id'=>'destination_code')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('cut_off_diff', 'Cut Off Day Difference', array('class' => 'control-label')); ?>
                                    {{ Form::select('cut_off_diff', $cut_off_diff, null, array('id'=>'cut_off_diff','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'cutOffChange(this);')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('cut_off_day', 'Cut Off Day', array('class' => 'control-label')); ?>
                                    {{ Form::text('cut_off_day','',array('class'=>'validate form-control','id'=>'cut_off_day','readonly'=>'readonly')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('etd_diff', 'ETD Difference', array('class' => 'control-label')); ?>
                                    {{ Form::select('etd_diff', $etd_diff, null, array('id'=>'etd_diff','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'etdChange();')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('etd_day', 'ETD Day', array('class' => 'control-label')); ?>
                                    {{ Form::text('etd_day','',array('class'=>'validate form-control','id'=>'etd_day','readonly'=>'readonly')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('assort_packing', 'Assortment Packing', array('class' => 'control-label')); ?>
                                    {{ Form::select('assort_packing', $assort_packing, null, array('id'=>'assort_packing','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'assortChange(this);')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('carton_details_assort', 'Carton Details Assort', array('class' => 'control-label')); ?>
                                    {{ Form::select('carton_details_assort', $carton_details, null, array('id'=>'carton_details_assort','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('carton_details_primary', 'Carton Details Primary', array('class' => 'control-label')); ?>
                                    {{ Form::select('carton_details_primary', $carton_details, null, array('id'=>'carton_details_primary','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('carton_details_secondary', 'Carton Details Secondary', array('class' => 'control-label')); ?>
                                    {{ Form::select('carton_details_secondary', $carton_details, null, array('id'=>'carton_details_secondary','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('consignee_address', 'Consignee Address', array('class' => 'control-label')); ?>
                                    {{ Form::textarea('consignee_address','',array('class'=>'form-control','id'=>'consignee_address')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('sock_print', 'Socks Print', array('class' => 'control-label')); ?>
                                    {{ Form::select('sock_print_id', $sock_print, null, array('id'=>'sock_print_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('lining_print', 'Lining Print', array('class' => 'control-label')); ?>
                                    {{ Form::select('lining_print_id', $lining_print, null, array('id'=>'lining_print_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('additional_print', 'Additional Print', array('class' => 'control-label')); ?>
                                    {{ Form::select('additional_print_id', $additional_print, null, array('id'=>'additional_print_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('poly_sticker', 'Poly Sticker', array('class' => 'control-label')); ?>
                                    {{ Form::select('poly_sticker_id', $poly_sticker, null, array('id'=>'poly_sticker_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('pictogram', 'Pictogram', array('class' => 'control-label')); ?>
                                    {{ Form::select('pictogram_id', $pictogram, null, array('id'=>'pictogram_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('price_tag', 'Price Tag', array('class' => 'control-label')); ?>
                                    {{ Form::select('price_tag_id', $price_tag, null, array('id'=>'price_tag_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('pm_tag', 'PM Tag', array('class' => 'control-label')); ?>
                                    {{ Form::select('pm_tag_id', $pm_tag, null, array('id'=>'pm_tag_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('additional_sticker_info', 'Additional Sticker Info', array('class' => 'control-label')); ?>
                                    {{ Form::select('additional_sticker_info_id', $additional_sticker_info, null, array('id'=>'additional_sticker_info_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('remarks', 'Remarks', array('class' => 'control-label')); ?>
                                    {{ Form::textarea('remarks','',array('class'=>'form-control','id'=>'remarks')) }}
                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <div class="col-md-4 col-md-offset-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                            </div>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#destinationForm").validate({
            rules: {
                destination_name: "required",
                destination_code: "required",
//                etd_diff: {
//                    depends: function(ele){
//                        return true;
//                    }
//                }
            },
            messages: {
                destination_name: "Please enter Destination Name",
                destination_code: "Please enter Destination Code"
            }
        });
    });

    function etdChange(){
        var cut_off_diff = $('#cut_off_diff').val();

        var etd_day = '';
        var etd_diff = $('#etd_diff').val();
        if(etd_diff){
            if(!cut_off_diff){
                alert('Please Select Cut Off Difference First');
                $("#etd_diff").val('');
            }
            else{
                var diff = cut_off_diff - etd_diff;
                console.log(diff);
                switch(diff){
                    case 0:
                        etd_day = 'Monday';
                        break;
                    case 1:
                    case 7:
                        etd_day = 'Tuesday';
                        break;
                    case 2:
                    case 8:
                        etd_day = 'Wednesday';
                        break;
                    case 3:
                    case 9:
                        etd_day = 'Thursday';
                        break;
                    case 4:
                    case 10:
                        etd_day = 'Friday';
                        break;
                    case 5:
                    case 11:
                        etd_day = 'Saturday';
                        break;
                    case 6:
                    case 12:
                        etd_day = 'Sunday';
                        break;
                }
                console.log(etd_day);
                $('#etd_day').val(etd_day);
            }
        }
    }

    function cutOffChange(item){
        var cut_off_diff = $('#cut_off_diff').val();
        var cut_off_day = '';
        switch(cut_off_diff){
            case '0':
                cut_off_day = 'Monday';
                break;
            case '1':
                cut_off_day = 'Tuesday';
                break;
            case '2':
                cut_off_day = 'Wednesday';
                break;
            case '3':
                cut_off_day = 'Thursday';
                break;
            case '4':
                cut_off_day = 'Friday';
                break;
            case '5':
                cut_off_day = 'Saturday';
                break;
            case '6':
                cut_off_day = 'Sunday';
                break;
        }

        $('#cut_off_day').val(cut_off_day);
        etdChange();
    }

    function assortChange(item){
        var assort_packing = $('#assort_packing').val();
        if(assort_packing == 1){
            $('#carton_details_assort').val('N/A');
            $('#carton_details_assort').attr("disabled", true);
        }
        else if(assort_packing == 0){
            $('#carton_details_assort').removeAttr("disabled");
        }
    }
</script>
@stop