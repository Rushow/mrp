@extends('layouts.master')
@section('head')
@parent

{{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css')}}
@stop
@section('header')
@stop
@section('content')

<div class="row">
    <div class="col-sm-12">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <? if(Session::has('message')){ ?>
            <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
            <script>
                $('#alert').delay(2000).fadeOut(400)
            </script>
            <? } ?>

            <h1>
                Buyer Template List
            </h1>
            <br>
            <data class="row">
                <div class="col-sm-8">
                    @if (!Auth::user()->isViewer())
                        <a href="<?= URL::to('buyerTemplate/create')?>" class="btn btn-warning">Create Buyer Template</a>
                    @endif
                </div>
                <div class="col-sm-4">
                    <a href="<?= URL::to('excel/buyerTemplate')?>" class="btn btn-primary">Download excel file</a>
                    <a href="<?= URL::to('pdf/buyerTemplate')?>" target="_blank" class="btn btn-info">Download pdf file</a>

                </div>
            </data>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Buyer Template List</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Buyer Template<span class="sort-icon"><span></th>
                                    @if (!Auth::user()->isViewer())
                                        <th>Action</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($templates as $key => $value){ ?>
                                <tr>
                                    <td><?= $value->id ?></td>
                                    <td><?= $value->buyer_template ?></td>
                                    @if (!Auth::user()->isViewer())
                                        <td>
                                            <a class="btn btn-info btn-sm" href="<?= URL::to('buyerTemplate/'.$value->id).'/edit';?>">Edit</a>
                                            {{ Form::open(array('class' => 'btn btn-danger btn-xs','url' => 'buyerTemplate/'.$value->id, 'method' => 'delete', 'style' => 'padding: 3px 0; border-color: transparent !important;')) }}
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs', 'style' => 'display:block;')) }}
                                            {{ Form::close() }}
                                        </td>
                                    @endif
                                </tr>
                                <?php } ?>

                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
        </section>


    </div>
</div>

@stop

@section('page-script')
    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js')}}
@stop

@section('plugin-script')
    <script type="text/javascript">
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@stop


