@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Buyer
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Buyer</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'buyer', 'method' => 'post', 'files'=>true, 'id' => 'buyerForm')); ?>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('buyer_name', 'Buyer', array('class' => 'control-label')); ?>
                                    <?= Form::text('buyer_name', null, array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?= Form::label('full_name', 'Name', array('class' => 'control-label')); ?>
                                    <?= Form::text('full_name', null, array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?= Form::label('week_start_day', 'Week Start Day', array('class' => 'control-label')); ?>
                                    {{ Form::select('week_start_day', $week_days, null, array('id'=>'week_start_day','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('buyer_template', 'Buyer Template', array('class' => 'control-label')); ?>
                                    {{ Form::select('buyer_template_id', $buyer_template, null, array('id'=>'buyer_template_id','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'changeBuyerTemplate();')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('logo', 'Logo', array('class' => 'control-label')); ?>
                                    {{ Form::file('logo', array('class'=>'', 'id'=>'logo')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('buyer_email', 'Email', array('class' => 'control-label')); ?>
                                    <?= Form::text('buyer_email', null, array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?= Form::label('buyer_phn', 'Phone', array('class' => 'control-label')); ?>
                                    <?= Form::text('buyer_phn', null, array('class' => 'form-control')); ?>
                                </div>
                            </div>

                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <div class="box box-primary template02">
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('cut_off_diff', 'Cut Off Day Difference', array('class' => 'control-label')); ?>
                                    {{ Form::select('cut_off_diff', $cut_off_diff, null, array('id'=>'cut_off_diff','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'cutOffChange(this);')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('cut_off_day', 'Cut Off Day', array('class' => 'control-label')); ?>
                                    {{ Form::text('cut_off_day','',array('class'=>'validate form-control','id'=>'cut_off_day','readonly'=>'readonly')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('etd_diff', 'ETD Difference', array('class' => 'control-label')); ?>
                                    {{ Form::select('etd_diff', $etd_diff, null, array('id'=>'etd_diff','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'etdChange();')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('etd_day', 'ETD Day', array('class' => 'control-label')); ?>
                                    {{ Form::text('etd_day','',array('class'=>'validate form-control','id'=>'etd_day','readonly'=>'readonly')) }}
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Address</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('floor', 'Floor', array('class' => 'control-label')); ?>
                                    <?= Form::text('floor', null, array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?= Form::label('street_address', 'Street Address', array('class' => 'control-label')); ?>
                                    <?= Form::textarea('street_address', null, array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('city', 'City', array('class' => 'control-label')); ?>
                                    <?= Form::text('city', null, array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?= Form::label('state', 'State', array('class' => 'control-label')); ?>
                                    <?= Form::text('state', null, array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?= Form::label('zip', 'Zip', array('class' => 'control-label')); ?>
                                    <?= Form::text('zip', null, array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?= Form::label('country', 'Country', array('class' => 'control-label')); ?>
                                    <?= Form::text('country', null, array('class' => 'form-control')); ?>
                                </div>
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <div class="col-md-4 col-md-offset-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                            </div>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#buyerForm").validate({
            rules: {
                buyer_name: "required",
                buyer_template_id: "required",

                street_address:"required",
                city:"required",
                zip:"required",
                country:"required"

            },
            messages: {
                buyer_name: "Please enter Buyer Name",
                buyer_template_id: "Please select Buyer Template",
                street_address:"Enter address",
                city:"Enter City",
                zip:"Enter Zip",
                country:"Enter Country"
            }
        });
        $(".template02").hide();
    });



    function changeBuyerTemplate(){
        var buyer_template = $("#buyer_template_id").val();
        if(buyer_template == "1"){
            $(".template01").show();
            $(".template02").hide();
        }
        else if(buyer_template == "2"){
            $(".template02").show();
            $(".template01").hide();
        }
    }

    function etdChange(){
        var cut_off_diff = $('#cut_off_diff').val();

        var etd_day = '';
        var etd_diff = $('#etd_diff').val();
        if(etd_diff){
            if(!cut_off_diff){
                alert('Please Select Cut Off Difference First');
                $("#etd_diff").val('');
            }
            else{
                var diff = cut_off_diff - etd_diff;
                console.log(diff);
                switch(diff){
                    case 0:
                        etd_day = 'Monday';
                        break;
                    case 1:
                    case 7:
                        etd_day = 'Tuesday';
                        break;
                    case 2:
                    case 8:
                        etd_day = 'Wednesday';
                        break;
                    case 3:
                    case 9:
                        etd_day = 'Thursday';
                        break;
                    case 4:
                    case 10:
                        etd_day = 'Friday';
                        break;
                    case 5:
                    case 11:
                        etd_day = 'Saturday';
                        break;
                    case 6:
                    case 12:
                        etd_day = 'Sunday';
                        break;
                }
                console.log(etd_day);
                $('#etd_day').val(etd_day);
            }
        }
    }

    function cutOffChange(item){
        var cut_off_diff = $('#cut_off_diff').val();
        var cut_off_day = '';
        switch(cut_off_diff){
            case '0':
                cut_off_day = 'Monday';
                break;
            case '1':
                cut_off_day = 'Tuesday';
                break;
            case '2':
                cut_off_day = 'Wednesday';
                break;
            case '3':
                cut_off_day = 'Thursday';
                break;
            case '4':
                cut_off_day = 'Friday';
                break;
            case '5':
                cut_off_day = 'Saturday';
                break;
            case '6':
                cut_off_day = 'Sunday';
                break;
        }

        $('#cut_off_day').val(cut_off_day);
        etdChange();
    }
</script>
@stop