@extends('layouts.master')
@section('head')

@parent
{{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')



    <div class="row">
        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">

                <?php if(Session::has('message')){ ?>
                <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
                <script>
                    $('#alert').delay(2000).fadeOut(400)
                </script>
                <?php } ?>

                <h1>
                    Buyer List
                </h1>
                <br>
                <data class="row">
                    <div class="col-sm-8">
                        @if (!Auth::user()->isViewer())
                            <a href="<?= URL::to('buyer/create')?>" class="btn btn-warning">Create Buyer</a>
                        @endif
                    </div>
                    <div class="col-sm-4">
                        <a href="<?= URL::to('excel/buyer')?>" class="btn btn-primary">Download excel file</a>
                        <a href="<?= URL::to('pdf/buyer')?>" target="_blank" class="btn btn-info">Download pdf file</a>
                    </div>
                </data>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Buyer List</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Buyer<span class="sort-icon"><span></th>
                                        <th>Name<span class="sort-icon"><span></th>
                                        <th>Logo<span class="sort-icon"><span></th>
                                        <th>Email<span class="sort-icon"><span></th>
                                        <th>Phone<span class="sort-icon"><span></th>
                                        <th>Week Start Day<span class="sort-icon"><span></th>
                                        <th>Buyer Template<span class="sort-icon"><span></th>
                                        <th>Address<span class="sort-icon"><span></th>
                                        @if (!Auth::user()->isViewer())
                                            <th>Action</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($buyers as $key => $value){ ?>
                                    <tr>
                                        <td><?= $value->id ?></td>
                                        <td><?= $value->buyer_name ?></td>
                                        <td><?= $value->full_name ?></td>
                                        <td><?= HTML::image('images/buyer/160X160/'.$value->logo) ?></td>
                                        <td><?= $value->buyer_email ?></td>
                                        <td><?= $value->buyer_phn ?></td>
                                        <td><?= $value->week_start_day ?></td>
                                        <td><?= $value->BuyerTemplate->buyer_template ?></td>
                                        <td>
                                            @if ($value->address_id && $value->Address->floor)
                                                Floor {{ $value->Address->floor }}, {{ $value->Address->street_address }} {{ $value->Address->city }} {{ $value->Address->state }} {{ $value->Address->zip }} {{ $value->Address->country }}
                                            @else
                                                No Available Address.
                                            @endif
                                        </td>
                                        @if (!Auth::user()->isViewer())
                                            <td>
                                                <a class="btn btn-info btn-sm" href="<?= URL::to('buyer/'.$value->id).'/edit';?>">Edit</a>
                                                {{ Form::open(array('class' => 'btn btn-danger btn-xs','url' => 'buyer/'.$value->id, 'method' => 'delete', 'style' => 'padding: 3px 0; border-color: transparent !important;')) }}
                                                {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs', 'style' => 'display:block;')) }}
                                                {{ Form::close() }}
                                            </td>
                                        @endif
                                    </tr>
                                    <?php } ?>

                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>


        </div>
    </div>

@stop

@section('page-script')
    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}
@stop

@section('plugin-script')
    <script type="text/javascript">
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@stop
