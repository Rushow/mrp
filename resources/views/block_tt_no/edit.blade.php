@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">
    <div class="col-md-6 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Update Block TT No</h2>
            <?= Form::open(array('url' => 'blockTtNo/'.$block_tt_no->id, 'method' => 'put', 'id' => 'blockTtNoForm')); ?>
            <div class="form-group">
                <?= Form::label('block_tt_no', 'Block TT No', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::text('block_tt_no', $block_tt_no->block_tt_no, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Submit', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#blockTtNoForm").validate({
            rules: {
                block_tt_no: "required"
            },
            messages: {
                block_tt_no: "Please enter Block TT No"
            }
        });
    });
</script>
@stop