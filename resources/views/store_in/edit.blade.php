@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<script type="text/javascript">
    $(function() {
        $("#store_in_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
    });
</script>
<div class="row">

    <div class="col-md-12 sidebar">
        <?
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message){
                echo $message;
            }
        }
        ?>
        <div class="clearfix"></div>
        <div class="col-md-12">            
            <div class="widget" style="margin-bottom:25px;">
                <h2>Edit Store In</h2>
            </div>        
            {{ Form::open(array('url' => 'storeIn/'.$store_in->id, 'method' => 'put', 'id' => 'storeInForm')) }}
            <div class="widget" style="margin-bottom:25px;">
                <h2>Store In Detail</h2>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-5">
                <div class="control-group">
                    {{ Form::label('warehouse_id', 'Warehouse', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::select('warehouse_id', $warehouse, $store_in->warehouse_id, $attributes = array('id'=>'warehouse_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('challan_no', 'Challan No', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::text('challan_no', $store_in->challan_no, array('class'=>'validate[required] form-control', 'id'=>'challan_no')) }}
                    </div>
                </div>                
            </div>
            <div class="col-md-2">
                <div class="control-group">
                    <?= Form::label('order_type_id', 'Order Type', array('class' => 'control-label')) ?>
                    <div class="controls">
                        {{ Form::hidden('in_type', $store_in->in_type, array('id'=> 'in_type')) }}
                        {{ Form::select('order_type_id', $order_type, $store_in->in_type, $attributes = array('id'=>'order_type_id','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'generateOrders(this);')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('order_no_id', 'Order No', array('class' => 'control-label')) ?>
                    <div class="controls">
                        {{ Form::select('order_no_id', $order_no, $store_in->SampleOrder->id, $attributes = array('id'=>'order_no_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                    </div>
                </div>  
            </div>
            <div class="col-md-5">
                <div class="control-group">
                    {{ Form::label('supplier_id', 'Supplier', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::select('supplier_id', $supplier, $store_in->supplier_id, $attributes = array('id'=>'supplier_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                    {{ Form::label('store_in_date', 'Store In Date', array('class' => 'control-label')) }}
                        <div class="controls">
                            {{ Form::text('store_in_date', $store_in->store_in_date, array('class'=>'validate[required] form-control datepicker','id'=>'store_in_date')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="control-group">
                {{ Form::label('remarks', 'Remarks', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::text('remarks', $store_in->remarks, array('class'=>'form-control', 'id'=>'remarks')) }}
                </div>
            </div>
            <div class="widget" style="margin-bottom:25px;">
                <h2>Individual Store In Detail</h2>
            </div>
            <table>
                <tr>
                    <td>SL No.</td>
                    <td class="col-md-1">Item Group</td>
                    <td class="col-md-1">Sub Group</td>
                    <td class="col-md-2">Parameter</td>
                    <td class="col-md-2">Color</td>
                    <td class="col-md-2">Item</td>
                    <!-- <td class="col-md-2">Rate</td> -->
                    <!-- <td>Currency</td> -->
                    <td class="col-md-2">Quantity</td>
                    <td>Unit</td>
                    <td></td>
                </tr>            
                    {{ Form::hidden('total_row', $store_in_details->count(), array('id'=> 'total_row')) }}
                    <? $count = 1; ?>
                    @foreach ($store_in_details as $store_in_detail)
                        <tr>
                            <td><?= $count ?></td>
                            <td>
                                {{ Form::select('item_group_id_'.$count, $item_group, $store_in_detail->ItemGroup->id, $attributes = array('id'=>'item_group_id_'.$count,'data-rel'=>'chosen', 'class'=>'form-control Required', 'onChange'=>'generateSubGroup(this, '.$count.');')) }}
                            </td>
                            <td>
                                {{ Form::select('sub_group_id_'.$count, $item_sub_group, $store_in_detail->ItemSubGroup->id, $attributes = array('id'=>'sub_group_id_'.$count,'data-rel'=>'chosen', 'class'=>'form-control Required', 'disabled'=>'disabled')) }}
                            </td>
                            <td >
                                {{ Form::select('parameter_id_'.$count, $parameter, $store_in_detail->parameter_id, $attributes = array('id'=>'parameter_id_'.$count,'data-rel'=>'chosen', 'class'=>'form-control Required')) }}
                            </td>
                            <td>
                                {{ Form::select('color_id_'.$count, $color, $store_in_detail->color_id, $attributes = array('id'=>'color_id_'.$count,'data-rel'=>'chosen', 'class'=>'form-control Required')) }}
                            </td>
                            <td>
                                {{ Form::select('item_id_'.$count, $item, $store_in_detail->item_id, $attributes = array('id'=>'item_id_'.$count,'data-rel'=>'chosen', 'class'=>'form-control Required', 'onChange'=>'generateUnit(this, '.$count.');')) }}
                            </td>
                            <td>
                                {{ Form::text('quantity_'.$count, $store_in_detail->quantity,array('class'=>'form-control Required','id'=>'quantity_'.$count)) }}
                            </td>
                            <td>
                                {{ Form::hidden('unit_id_'.$count, '', array('id'=> 'unit_id_'.$count)) }}
                                {{ Form::text('unit_'.$count, $store_in_detail->Unit->unit_name, array('id'=>'unit_'.$count, 'data-rel'=>'chosen', 'class'=>'form-control Required', 'readonly'=>'readonly')) }}
                            </td>
                            <td>
                                <a href="#" onclick="return checkDelete(this);">
                                    <span class="badge badge-important">X</span>
                                </a>
                            </td>
                        </tr>
                        <? $count++; ?>
                    @endforeach            
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <!-- <td></td>
                    <td></td> -->
                    <td></td>
                    <td><label onclick="addStoreInRow(this);" class="pull-right" style="min-width:50px; cursor: pointer;">+Add More</label> </td>
                </tr>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <div class="controls">
                <?= Form::submit('Update Store In', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
        <?= Form::close(); ?>
    </div>
</div>

<script type="text/javascript">
    function generateSubGroup(item, count){
        var val = $(item).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                async: true,
                dataType: 'json',
                url: '<?= Url("item/subGroup") ?>',
                data: 'group_id='+val,
                success: function(data){
                    $("#sub_group_id_" + count).removeAttr('readonly');
                    $("#sub_group_id_" + count).removeAttr('disabled');
                    $("#sub_group_id_" + count).html(data.sub_group);
                }
            });
        }
        else{
            $("#sub_group_id_" + count).html('<option value="">Select</option>');   
            $("#sub_group_id_" + count).attr('readonly', 'readonly');
            $("#sub_group_id_" + count).attr('disabled', 'disabled');
        }
    }

    function generateUnit (item, count) {
        var val = $(item).val();
        if(val!=""){
            $.ajax({
                type: "POST",                
                dataType: 'json',
                url: '<?= Url("item/purchaseUnit") ?>',
                data: 'item_id='+val,
                success: function(data){
                    $("#unit_id_" + count).val(data.id);
                    $("#unit_" + count).val(data.unit);
                }
            });
        }else{

        }
    }

    function generateOrders(type){
        var val = $(type).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("storeIn/orderType") ?>',
                data: 'order_type='+val,
                success: function(data){
                    $("#order_no_id").html(data.orders);
                }
            });
        }
        else{
        }
    }

    function addStoreInRow(item){
        var last_id = $("#total_row").val();
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '<?= Url("storeIn/addStoreInRow") ?>',
            data: 'last_id='+last_id,
            success: function(data){
                if(data.error == false){
                    $(item).parents('tr').before('<tr>'+data.html+'</tr>');
                    last_id = parseInt(last_id) + 1;
                    $('#total_row').val(last_id);
                    $('#size_id_'+last_id).multiselect();
                }
                else{
                    alert(data.message);
                }
            }
        });
    }
    $(document).ready(function() {
        @for ($i = 1; $i <= $groups; $i++)
            {{ 'generateSubGroup($("#item_group_id_'.$i.'"), '.$i.');' }}
            {{ '$("#sub_group_id_'.$i.'").val('. $store_in_details[$i-1]->ItemSubGroup->id .');' }}
        @endfor
        
        $("#storeInForm").validate({
            rules: {
                warehouse_id: "required",
                challan_no: "required",
                store_in_date: "required",
                supplier_id: "required"
            },
            messages: {
                warehouse_id: "Please Select a Warehouse",
                challan_no: "Please Enter a Challan No",
                store_in_date: "Please Select a Store In Date",
                supplier_id: "Please Select a Supplier"
            }
        });
        $.validator.addClassRules("Required", {
            required: true
        });
    });
</script>

<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<style>
    .widget{
        overflow: visible !important;
    }
</style>

@stop