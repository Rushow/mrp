@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-6 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Add Group</h2>
            <?= Form::open(array('url' => 'itemGroup', 'method' => 'post', 'id' => 'itemGroupForm')); ?>
            <div class="form-group">
                <?= Form::label('group_name', 'Item Group Name', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::text('group_name', null, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Submit', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#itemGroupForm").validate({
            rules: {
                group_name: "required"
            },
            messages: {
                group_name: "Please enter Group Name"
            }
        });
    });
</script>
@stop