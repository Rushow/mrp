@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Additional Sticker Info
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Additional Sticker Info</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'additionalStickerInfo', 'method' => 'post', 'id' => 'additionalStickerInfoForm')); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <?= Form::label('sticker_info', 'Additional Sticker Info', array('class' => 'control-label')); ?>
                                {{ Form::text('sticker_info','',array('class'=>'validate[required] form-control','id'=>'sticker_info', 'placeholder'=>'Additional Sticker Info')) }}
                            </div>
                            <div class="form-group">
                                <?= Form::label('status', 'Status', array('class' => 'control-label')); ?>
                                    {{ Form::select('status', $status,null,$attributes = array('id'=>'status','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'changeStatus(this)')) }}
                            </div>
                            <div class="form-group">
                                <?= Form::label('enable_date', 'Enable Date', array('class' => 'control-label')); ?>
                                    {{ Form::text('enable_date', '',array('class'=>'validate[required] form-control datepicker','id'=>'enable_date', 'placeholder'=>'Enable Date')) }}
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="submit" class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</button>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>

    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>


<script>
    $(function() {
        $("#enable_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
    });
</script>

<script>
    $(document).ready(function() {
        changeStatus();

        $("#additionalStickerInfoForm").validate({
            rules: {
                sticker_info: "required",
                status: "required"
            },
            messages: {
                sticker_info: "Please enter Additional Sticker Info",
                status: "Please select Status"
            }
        });
    });

    function changeStatus(){
        var status = $('#status').val();

        if(status == 1){
            $('#enable_date').removeAttr("disabled");
        }
        else{
            $('#enable_date').val('');
            $('#enable_date').attr("disabled", true);
        }
    }
</script>
@stop