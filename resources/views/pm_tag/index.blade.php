@extends('layouts.master')
@section('head')

@parent
{{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css')}}
@stop
@section('header')
@stop
@section('content')

    <div class="row">
        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">

                <? if(Session::has('message')){ ?>
                <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
                <script>
                    $('#alert').delay(2000).fadeOut(400)
                </script>
                <? } ?>

                <h1>
                    PM Tag List
                </h1>
                <br>
                <data class="row">
                    <div class="col-sm-8">
                        @if (!Auth::user()->isViewer())
                            <a href="<?= URL::to('pmTag/create')?>" class="btn btn-warning">Create PM Tag</a>
                        @endif
                    </div>
                    <div class="col-sm-4">
                        <a href="<?= URL::to('excel/pmTag')?>" class="btn btn-primary">Download excel file</a>
                        <a href="<?= URL::to('pdf/pmTag')?>" target="_blank" class="btn btn-info">Download pdf file</a>
                    </div>
                </data>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-success">
                            <div class="box-header">
                                <h3 class="box-title">PM Tag List</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>PM Tag<span class="sort-icon"><span></th>
                                        <th>Status<span class="sort-icon"><span></th>
                                        <th>Enable Date<span class="sort-icon"><span></th>
                                        @if (!Auth::user()->isViewer())
                                            <th>Action</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <? foreach($linings as $key => $value){ ?>
                                    <tr>
                                        <td><?= $value->id ?></td>
                                        <td><?= $value->pm_tag ?></td>
                                        <td><?= ($value->status)?"Enable":"Disable" ?></td>
                                        <td><?= ($value->status)?date('d-m-Y', $value->enable_date):"" ?></td>
                                        @if (!Auth::user()->isViewer())
                                            <td>
                                                <a class="btn btn-info btn-sm" href="<?= URL::to('pmTag/'.$value->id).'/edit';?>">Edit</a>
                                                {{ Form::open(array('class' => 'btn btn-danger btn-xs','url' => 'pmTag/'.$value->id, 'method' => 'delete', 'style' => 'padding: 3px 0; border-color: transparent !important;')) }}
                                                {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs', 'style' => 'display:block;')) }}
                                                {{ Form::close() }}
                                            </td>
                                        @endif
                                    </tr>
                                    <? } ?>

                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>


        </div>
    </div>

@stop

@section('page-script')
    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js')}}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}
@stop

@section('plugin-script')
    <script type="text/javascript">
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@stop
