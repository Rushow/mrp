@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')
<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Season
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Season</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'season', 'method' => 'post', 'id' => 'seasonForm')); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <?= Form::label('season', 'Season', array('class' => 'control-label')); ?>
                                <?= Form::text('season', null, array('class' => 'form-control', 'placeholder' => 'Season')); ?>
                            </div>
                            <div class="form-group">
                                <?= Form::label('start_date', 'Start Date', array('class' => 'control-label')); ?>
                                {{ Form::text('start_date', '',array('class'=>'validate[required] form-control datepicker','id'=>'start_date', 'placeholder' => 'Start Date')) }}
                            </div>
                            <div class="form-group">
                                <?= Form::label('end_date', 'End Date', array('class' => 'control-label')); ?>
                                {{ Form::text('end_date', '',array('class'=>'validate[required] form-control datepicker','id'=>'end_date', 'placeholder' => 'End Date')) }}
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="submit" class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</button>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(function() {
        $("#start_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
        $("#end_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
    });
    $(document).ready(function() {
        $("#seasonForm").validate({
            rules: {
                season: "required",
                start_date: "required",
                end_date: "required"
            },
            messages: {
                season: "Please enter season",
                start_date: "Please enter Start Date",
                end_date: "Please enter End Date"
            }
        });
    });
</script>

@stop