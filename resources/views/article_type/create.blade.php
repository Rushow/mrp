@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Appearance
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Appearance</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'articleType', 'method' => 'post', 'id' => 'articleTypeForm')); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <?= Form::label('article_type', 'Appearance', array('class' => 'control-label')); ?>
                                    {{ Form::text('article_type','',array('class'=>'validate[required] form-control','id'=>'article_type', 'placeholder'=>'Appearance')) }}
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="submit" class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</button>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>



<script>
    $(document).ready(function() {
        $("#articleTypeForm").validate({
            rules: {
                article_type: "required"
            },
            messages: {
                article_type: "Please enter Appearance"
            }
        });
    });
</script>
@stop