@extends('layouts.master')
@section('head')

    @parent

@stop
@section('header')
@stop
@section('content')
    <?php

    $order_no_val = (isset($post_val['order_no'])) ? $post_val['order_no'] : null;
    $article_val = (isset($post_val['article_id'])) ? $post_val['article_id'] : null;
    $color_val = (isset($post_val['color_id'])) ? $post_val['color_id'] : null;

    ?>
    <div class="container">

    <div class="row">

        <div class="col-md-10 sidebar">
            <?php
            $error_messages = Session::get('error_messages');

            if(isset($error_messages)){
                foreach($error_messages as $message){
                    echo $message;
                }
            }
            ?>
            <div class="widget">
                <h2> Cutting : Stock In</h2>
                <?= Form::open(array('url' => 'item', 'method' => 'post','files'=>true)); ?>
                <div class="col-md-4 sidebar" style="margin-right: 150px;">
                    <div class="form-group">
                        <?= Form::label('order_no', 'Order No', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('order_no',$production_order,$order_no_val,array('class'=>'form-control','id'=>'order_no')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('article_no', 'Article No', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('article_no',$article,$article_val,array('class'=>'validate[required] form-control','id'=>'article_no')) }}
                        </div>
                    </div>

                    <div class="control-group">
                        <?= Form::label('color_id', 'Color', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('color_id', $color, null, $attributes = array('id'=>'color_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('item_group_id', 'Item Group ID (Optional right now)', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('material_code','',array('class'=>'form-control','id'=>'material_code_code')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('subdivision', 'Sub Division', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('subdivision',$sub_division,'',array('class'=>'form-control','id'=>'subdivision')) }}
                        </div>
                    </div>

                </div>
                <div class="col-md-6 sidebar">
                    <div class="form-group">
                        <?= Form::label('size', 'Size', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('size',$size,'',array('class'=>'form-control','id'=>'size')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('quantity', 'Quantity', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('quantity','',array('class'=>'form-control','id'=>'material_code_code')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('store_unit', 'Store Unit', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('store_unit', $unit, '', array('id'=>'store_unit','data-rel'=>'chosen', 'class'=>'validate[required] form-control', 'onChange'=>'storeUnitChange();')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('store_in_date', 'Store In Date', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('store_in_date','', array('id'=>'store_in_date','data-rel'=>'chosen', 'class'=>'datepicker validate[required] form-control')) }}
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="controls">
                            <?= Form::submit('Add Item', array('class' => 'btn btn-success')); ?>
                        </div>
                    </div>
                </div>
                <?= Form::close(); ?>
            </div>
        </div>
    </div>

    </div>

    <style>
        .sidebar{
            padding-bottom: 20px;
        }
        form{
            margin: 15px;
        }
    </style>

    <script>

        $(document).ready(function(){

            $("#store_in_date").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });

            $('#cutability_yes').prop('checked', true);
            $('#cutability_yes').prop('checked', true);
            $("#store_unit").val("2");
            $("#purchase_unit").val("3");
            storeUnitChange();
            purchaseUnitChange();

            $("#articleForm").validate({
                rules: {
                    group_id: "required",
                    sub_group_id: "required",
                    cutability_factor: "required",
                    store_unit: "required",
                    purchase_unit: "required"
                },
                messages: {
                    group_id: "Please select a group!",
                    sub_group_id: "Please select a sub-group!",
                    cutability_factor: "Please provide the cutability factor!",
                    store_unit: "Please select a store unit!",
                    purchase_unit: "Please select a purchase unit!",
                }
            });
        });

        function cutabilityChange(item){
            var item_id = $(item).attr('id');
            if(item_id == 'cutability_no'){
                $('#cutable_width').attr("disabled", "disabled");
                $('#cutable_width_unit').attr("disabled", "disabled");
            }
            else if(item_id == 'cutability_yes'){
                $('#cutable_width').removeAttr("disabled");
                $('#cutable_width_unit').removeAttr("disabled");
            }
        }

        function storeUnitChange(){
            var val = $( "#store_unit option:selected" ).text();
            $('#unit_conversion_store_label').html(val);
        }

        function purchaseUnitChange(){
            var val = $( "#purchase_unit option:selected" ).text();
            $('#unit_conversion_purchase_label').html(val);
        }
        function generateSubGroup(item){
            var val = $(item).val();
            if(val!=""){
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("item/subGroup") ?>',
                    data: 'group_id='+val,
                    success: function(data){
                        $("#sub_group_id").removeAttr('readonly');
                        $("#sub_group_id").removeAttr('disabled');
                        $("#sub_group_id").html(data.sub_group);
                    }
                });
            }
            else{
                $("#sub_group_id").html('<option value="">Select</option>');
                $("#sub_group_id").attr('readonly', 'readonly');
                $("#sub_group_id").attr('disabled', 'disabled');
            }
        }
    </script>
@stop