<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Size Quantity</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 size-error">
                        Please select Size Group First
                    </div>
                    <div class="col-md-12 size-box">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="calculateTotalQuantity(this);">Close</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="calculateTotalQuantity(this);">Save changes</button>
            </div>
        </div>
    </div>
</div>

else {
if (size_group_id != "") {
$('#myModal').find('.size-error').hide();
$('#myModal').find('.size-box').show();
$('#myModal').find('.size-box').html('');

$('#myModal').find('.size-box').append("<div class='form-group'>");
    $('#myModal').find('.size-box').append("<label class='col-sm-2 control-label' for=''>Size</label>");
    $('#myModal').find('.size-box').append("<label class='col-sm-10 control-label' for=''>Quantity</label>");
    $.ajax({
    type: "POST",
    dataType: 'json',
    url: '<?= Url("sizeGroup/getSizesBySizeGroup") ?>',
    data: 'size_group_id=' + size_group_id,
    async: true,
    success: function (data) {
    var j = 1;
    $.each(data.sizes, function (i, val) {
    $('#myModal').find('.size-box').append("<div class='form-group'>");
        $('#myModal').find('.size-box').append("<label class='col-sm-2 control-label' for='size_" + val.size_no +  "'>" + val.size_no + "</label><input id='size_" + val.size_no +  "' type='hidden' value='" + val.id + "' name='size_" + val.size_no + "' />");
        $('#myModal').find('.size-box').append("<div class='col-sm-10'><input id='quantity_" + val.size_no + "' class='form-control' type='text' value='' name='quantity_" + val.size_no +  "' /> </div><div class='clearfix'></div></div>");
    j++;
    });
    }
    });
    }
    else {
    $('#myModal').find('.size-error').show();
    $('#myModal').find('.size-box').hide();
    }
    }