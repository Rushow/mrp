@extends('layouts.master')
@section('head')
    @parent
    {{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}

@stop

@section('content')

    <?php

    $order_no_val = (isset($post_val['order_no'])) ? $post_val['order_no'] : null;
    $article_val = (isset($post_val['article_id'])) ? $post_val['article_id'] : null;
    $color_val = (isset($post_val['color_id'])) ? $post_val['color_id'] : null;
    $division_val = (isset($post_val['division'])) ? $post_val['division'] : null;
    $sub_division_val = (isset($post_val['sub_division'])) ? $post_val['sub_division'] : null;
    $production_date_from_val = (isset($post_val['production_date_from'])) ? $post_val['production_date_from'] : null;
    $production_date_to_val = (isset($post_val['production_date_to'])) ? $post_val['production_date_to'] : null;

    ?>

    <div class="row">
        <div class="col-md-12 sidebar">
            <?php if(Session::has('message')){ ?>
            <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
            <script>
                $('#alert').delay(2000).fadeOut(400)
            </script>
            <?php } ?>
            <data class="row">
                <div class="col-sm-10"><h3>Instruction</h3></div>
                <div class="col-sm-2">
                </div>
            </data>
            <!-- Content Header (Page header) -->
            <section class="content-header">
            </section>
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <!-- form start -->
                            <?= Form::open(array('url' => 'dailyproduction/instruction', 'method' => 'get')); ?>
                            <div class="box-body">
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <?= Form::label('division', 'Select Division', array('class' => 'control-label')); ?>
                                        {{ Form::select('division', $division, $division_val, $attributes = array('id'=>'division', 'name'=>'division[]', 'data-rel'=>'chosen', 'data-placeholder'=>'Select Division', 'multiple'=>'', 'class'=>'form-control select2')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('sub_division', 'Select Sub Division', array('class' => 'control-label')); ?>
                                        {{ Form::select('sub_division', $sub_division, $sub_division_val, $attributes = array('id'=>'sub_division', 'name'=>'sub_division[]', 'data-rel'=>'chosen', 'data-placeholder'=>'Select Sub Division', 'multiple'=>'', 'class'=>'form-control select2')) }}
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= Form::label('order_no', 'Select Order No', array('class' => 'control-label')); ?>
                                        {{ Form::select('order_no', $production_order, $order_no_val, $attributes = array('id'=>'order_no', 'name'=>'order_no[]', 'data-rel'=>'chosen', 'data-placeholder'=>'Select Order No', 'multiple'=>'', 'class'=>'form-control select2','onchange="orderChange()')) }}
                                    </div>
                                    <div class="form-group">
                                        <?= Form::label('article_id', 'Article No', array('class' => 'control-label')); ?>
                                        {{ Form::select('article_id', $article, $article_val, $attributes = array('id'=>'article_id', 'name'=>'article_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Article', 'multiple'=>'')) }}
                                    </div>


                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= Form::label('color_id', 'Color', array('class' => 'control-label')); ?>
                                        {{ Form::select('color_id', $color, $color_val, $attributes = array('id'=>'color_id', 'name'=>'color_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Color', 'multiple'=>'')) }}
                                    </div>
                                    <div class="input-daterange form-group">
                                        <?= Form::label('production_date', 'Date', array('class' => 'control-label')); ?>
                                        <div class="input-daterange input-group" id="datepicker">
                                            {{ Form::text('production_date_from', $production_date_from_val,array('class'=>'input-sm form-control datepicker','id'=>'production_date_from')) }}
                                            <span class="input-group-addon">To</span>
                                            {{ Form::text('production_date_to',  $production_date_to_val,array('class'=>'input-sm form-control datepicker','id'=>'production_date_to')) }}
                                        </div>
                                    </div>

                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <div class="col-md-2 col-md-offset-11">
                                    <button type="submit" class="btn btn-success">Search</button>
                                </div>

                            </div>
                            <?= Form::close(); ?>
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Instruction List
                </h1>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">

                                <h3 class="box-title"></h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Division<span class="sort-icon"><span></th>
                                        <th>Sub Department<span class="sort-icon"><span></th>
                                        <th>Date<span class="sort-icon"><span></th>
                                        <th>Order No<span class="sort-icon"><span></th>
                                        <th>Article<span class="sort-icon"><span></th>
                                        <th>Color<span class="sort-icon"><span></th>
                                        <th>SizeGroup<span class="sort-icon"><span></th>
                                        @foreach($size as $s)
                                            <th>{{$s->size_no}}<span class="sort-icon"><span></th>
                                        @endforeach
                                        <th>Quanity<span class="sort-icon"><span></th>
                                        <th>Component Unit<span class="sort-icon"><span></th>


                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    foreach($instruction as $key => $stk){

                                    ?>
                                    <tr>
                                        <td><?=$stk->Division->name?></td>
                                        <td><? if(isset($stk->SubDivision)){ echo $stk->SubDivision->subdivision_name;}?></td>
                                        <td>{{$stk->production_in_date}}</td>
                                        <td>{{$stk->Order->order_no}}</td>
                                        <td> <?if(isset($stk->Article->article_no)){echo $stk->Article->article_no;}?></td>
                                        <td> <?if(isset($stk->Color->color_name_flcl)){ echo $stk->Color->color_name_flcl;}?></td>
                                        <td> <? if(isset($stk->SizeGroup->size_group_name)){echo $stk->SizeGroup->size_group_name;}?></td>
                                        @foreach($size as $s)
                                            <? $szexist=0;?>
                                            @foreach($stk->DailydetailSize as $sizedetail)
                                                <? if($s->id == $sizedetail->size_id){?>
                                                <td>{{$sizedetail->quantity}}<span class="sort-icon"><span></td>
                                                <? $szexist=1; }?>
                                            @endforeach
                                            <? if($szexist==0){?>
                                            <td></td>
                                            <? } ?>
                                        @endforeach

                                        <td> <?=$stk->quantity?></td>
                                        <td><?=$stk->Component->component_name?></td>


                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->

                            <?=$instruction->appends(request()->all())->render();?>
                         </div><!-- /.box -->
                    </div>

                </div>
            </section>
        </div>
    </div>


    @stop


    @section('page-script')
            <!-- Select2 -->
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js')}}

    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js')}}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}

    {{HTML::script('https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js')}}
    {{HTML::script('https://cdn.datatables.net/buttons/1.3.1/js/buttons.bootstrap.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')}}
    {{HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js')}}
    {{HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js')}}
@stop

@section('plugin-script')
    <script type="text/javascript">
        $(function () {
            $(".select2").select2();

            $("#production_date_from").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#production_date_to").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });

            var table=$('#example1').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "scrollX": true,
                "lengthChange": true,
                "buttons": [  {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                }, 'pdf', 'colvis' ],


            });

            table.buttons().container()
                    .appendTo( '#example1_wrapper .col-sm-6:eq(0)' );

            $('#example1_wrapper #example1_info').html("");
        });



        $("#order_no").change(function () {
            var order = $('#order_no').val();
            if(order){
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getArticleByOrderArray") ?>',
                    data: 'order='+order,
                    async: true,
                    success: function(data){
                        $("#article_id").removeAttr('readonly');
                        $("#article_id").removeAttr('disabled');
                        $("#article_id").html(data.article);

                    }
                });

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '<?= Url("productionOrder/getColorByOrderArray") ?>',
                    data: 'order='+order,
                    async: true,
                    success: function(data){
                        $("#color_id").removeAttr('readonly');
                        $("#color_id").removeAttr('disabled');
                        $("#color_id").html(data.color);
                    }
                });

            }
            else{
                $("#article_id").html('<option value="">Select</option>');
                $("#article_id").attr('readonly', 'readonly');
                $("#article_id").attr('disabled', 'disabled');

                $("#color_id").html('<option value="">Select</option>');
                $("#color_id").attr('readonly', 'readonly');
                $("#color_id").attr('disabled', 'disabled');
            }
        });



    </script>

@stop
