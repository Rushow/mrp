@extends('layouts.master')
@section('head')
    @parent
    {{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}

@stop

@section('content')
    <div class="container">
        <?php

        $order_no_val = (isset($post_val['order_no'])) ? $post_val['order_no'] : null;
        $article_val = (isset($post_val['article_id'])) ? $post_val['article_id'] : null;
        $color_val = (isset($post_val['color_id'])) ? $post_val['color_id'] : null;
        $sub_division_val = (isset($post_val['sub_division'])) ? $post_val['sub_division'] : null;
        $production_date_from_val = (isset($post_val['production_date_from'])) ? $post_val['production_date_from'] : null;
        $production_date_to_val = (isset($post_val['production_date_to'])) ? $post_val['production_date_to'] : null;

        ?>

        <div class="row  searchbox">
            <div class="col-md-12 sidebar">
                <?php if(Session::has('message')){ ?>
                <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
                <script>
                    $('#alert').delay(2000).fadeOut(400)
                </script>
                <?php } ?>
                <data class="row">
                    <div class="col-sm-10"><h3>{{$division->name}} Division</h3></div>
                    <div class="col-sm-2">
                    </div>
                </data>
                <!-- Content Header (Page header) -->
                <section class="content-header">
                </section>
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <!-- form start -->
                                <?= Form::open(array('url' => 'dailyproduction/balance/'.$division->id, 'method' => 'post')); ?>
                                <div class="box-body">
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <?= Form::label('order_no', 'Select Order No', array('class' => 'control-label')); ?>
                                            {{ Form::select('order_no', $production_order, $order_no_val, $attributes = array('id'=>'order_no', 'name'=>'order_no[]', 'data-rel'=>'chosen', 'data-placeholder'=>'Select Order No', 'multiple'=>'', 'class'=>'form-control select2')) }}
                                        </div>
                                        <div class="form-group">
                                            <?= Form::label('sub_division', 'Select Sub Division', array('class' => 'control-label')); ?>
                                            {{ Form::select('order_no', $sub_division, $sub_division_val, $attributes = array('id'=>'order_no', 'name'=>'sub_division[]', 'data-rel'=>'chosen', 'data-placeholder'=>'Select Sub Division', 'multiple'=>'', 'class'=>'form-control select2')) }}
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= Form::label('article_id', 'Article No', array('class' => 'control-label')); ?>
                                            {{ Form::select('article_id', $article, $article_val, $attributes = array('id'=>'article_id', 'name'=>'article_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Article', 'multiple'=>'')) }}
                                        </div>
                                        <div class="form-group">
                                            <?= Form::label('color_id', 'Color', array('class' => 'control-label')); ?>
                                            {{ Form::select('color_id', $color, $color_val, $attributes = array('id'=>'color_id', 'name'=>'color_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Color', 'multiple'=>'')) }}
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-daterange form-group">
                                            <?= Form::label('production_date', 'Date', array('class' => 'control-label')); ?>
                                            <div class="input-daterange input-group" id="datepicker">
                                                {{ Form::text('production_date_from', $production_date_from_val,array('class'=>'input-sm form-control datepicker','id'=>'production_date_from')) }}
                                                <span class="input-group-addon">To</span>
                                                {{ Form::text('production_date_to',  $production_date_to_val,array('class'=>'input-sm form-control datepicker','id'=>'production_date_to')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?= Form::label('Filter Result', 'Filter Result', array('class' => 'control-label')); ?>
                                            <div><button type="submit" class="btn btn-success">Search</button></div>
                                        </div>
                                    </div>
                                </div><!-- /.box-body -->
                                <div class="box-footer">


                                </div>
                                <?= Form::close(); ?>
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section>
            </div>
        </div>



    </div>

@stop
@section('page-script')
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js') }}
@stop

@section('plugin-script')

    <script type="text/javascript">
        $(function () {
            $(".select2").select2();

            $("#production_date_from").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#production_date_to").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });


        });



        function orderChange(){
            var order = $('#order_no').val();
            if(order){
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getArticleByOrderArray") ?>',
                    data: 'order='+order,
                    async: true,
                    success: function(data){
                        $("#article_id").removeAttr('readonly');
                        $("#article_id").removeAttr('disabled');
                        $("#article_id").html(data.article);
                    }
                });

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getColorByOrderArray") ?>',
                    data: 'order='+order,
                    async: true,
                    success: function(data){
                        $("#color_id").removeAttr('readonly');
                        $("#color_id").removeAttr('disabled');
                        $("#color_id").html(data.color);
                    }
                });
            }
            else{
                $("#article_id").html('<option value="">Select</option>');
                $("#article_id").attr('readonly', 'readonly');
                $("#article_id").attr('disabled', 'disabled');

                $("#color_id").html('<option value="">Select</option>');
                $("#color_id").attr('readonly', 'readonly');
                $("#color_id").attr('disabled', 'disabled');
            }
        }

    </script>
@stop