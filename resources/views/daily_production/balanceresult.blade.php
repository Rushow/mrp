@extends('layouts.master')
@section('head')
    @parent
    {{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}

@stop

@section('content')
    <div class="container">
        <?php

        $order_no_val = (isset($post_val['order_no'])) ? $post_val['order_no'] : null;
        $article_val = (isset($post_val['article_id'])) ? $post_val['article_id'] : null;
        $color_val = (isset($post_val['color_id'])) ? $post_val['color_id'] : null;
        $sub_division_val = (isset($post_val['sub_division'])) ? $post_val['sub_division'] : null;
        $production_date_from_val = (isset($post_val['production_date_from'])) ? $post_val['production_date_from'] : null;
        $production_date_to_val = (isset($post_val['production_date_to'])) ? $post_val['production_date_to'] : null;

        ?>

        <div class="row  searchbox">
            <div class="col-md-12 sidebar">
                <?php if(Session::has('message')){ ?>
                <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
                <script>
                    $('#alert').delay(2000).fadeOut(400)
                </script>
                <?php } ?>
                <data class="row">
                    <div class="col-sm-10"><h3>{{$division->name}} Division</h3></div>
                    <div class="col-sm-2">
                    </div>
                </data>
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <button id="downloadpdf" href="#" class="btn-primary">Download-Pdf</button>
                </section>
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <!-- form start -->
                                <?= Form::open(array('url' => 'dailyproduction/balance/'.$division->id, 'method' => 'post')); ?>
                                <div class="box-body">
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <?= Form::label('order_no', 'Select Order No', array('class' => 'control-label')); ?>
                                            {{ Form::select('order_no', $production_order, $order_no_val, $attributes = array('id'=>'order_no', 'name'=>'order_no[]', 'data-rel'=>'chosen', 'data-placeholder'=>'Select Order No', 'multiple'=>'', 'class'=>'form-control select2')) }}
                                        </div>
                                        <div class="form-group">
                                            <?= Form::label('sub_division', 'Select Sub Division', array('class' => 'control-label')); ?>
                                            {{ Form::select('order_no', $sub_division, $sub_division_val, $attributes = array('id'=>'order_no', 'name'=>'sub_division[]', 'data-rel'=>'chosen', 'data-placeholder'=>'Select Sub Division', 'multiple'=>'', 'class'=>'form-control select2')) }}
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= Form::label('article_id', 'Article No', array('class' => 'control-label')); ?>
                                            {{ Form::select('article_id', $article, $article_val, $attributes = array('id'=>'article_id', 'name'=>'article_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Article', 'multiple'=>'')) }}
                                        </div>
                                        <div class="form-group">
                                            <?= Form::label('color_id', 'Color', array('class' => 'control-label')); ?>
                                            {{ Form::select('color_id', $color, $color_val, $attributes = array('id'=>'color_id', 'name'=>'color_id[]', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'data-placeholder'=>'Select Color', 'multiple'=>'')) }}
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-daterange form-group">
                                            <?= Form::label('production_date', 'Date', array('class' => 'control-label')); ?>
                                            <div class="input-daterange input-group" id="datepicker">
                                                {{ Form::text('production_date_from', $production_date_from_val,array('class'=>'input-sm form-control datepicker','id'=>'production_date_from')) }}
                                                <span class="input-group-addon">To</span>
                                                {{ Form::text('production_date_to',  $production_date_to_val,array('class'=>'input-sm form-control datepicker','id'=>'production_date_to')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?= Form::label('Filter Result', 'Filter Result', array('class' => 'control-label')); ?>
                                            <div><button type="submit" class="btn btn-success">Search</button></div>
                                        </div>
                                    </div>
                                </div><!-- /.box-body -->
                                <div class="box-footer">


                                </div>
                                <?= Form::close(); ?>
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 ">
                <div class="box box-primary">
                    <div class="box-body">
                        <h3>Component Recieved :</h3>
                        <table id="component" class="btable" style="width:60%">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Sub Division</th>
                                <th>Component Name</th>
                                <th>Date</th>
                                <th>Quantity</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($component as $key => $stk){
                            ?>
                            <tr>
                                <td>{{++$key}}</td>
                                <td><? if(isset($stk->SubDivision)){ echo $stk->SubDivision->subdivision_name;}?></td>
                                <td>{{$stk->Component->component_name}}</td>
                                <td>{{$stk->store_out_date}}</td>
                                <td>{{$stk->quantity}}</td>
                            </tr>
                            <? } ?>
                            </tbody>
                            <tfoot>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Total</th>
                            <th>{{$component->sum('quantity')}}</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 sidebar">
                <div class="box box-primary">
                    <div class="box-body">
                        <h3>Produced :</h3>
                        <table id="production" class="btable" style="width:60%">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Sub Division</th>
                                <th>Component Name</th>
                                <th>Date</th>
                                <th>Quantity</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($stockin as $key => $stk){
                            ?>
                            <tr>
                                <td>{{++$key}}</td>
                                <td><? if(isset($stk->SubDivision)){ echo $stk->SubDivision->subdivision_name;}?></td>
                                <td>{{$stk->Component->component_name}}</td>
                                <td>{{$stk->store_in_date}}</td>
                                <td>{{$stk->quantity}}</td>
                            </tr>
                            <? } ?>
                            </tbody>
                            <tfoot>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Total</th>
                            <th>{{$stockin->sum('quantity')}}</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 sidebar">
                <div class="box box-primary">
                    <div class="box-body">
                        <h3>Transfered :</h3>
                        <table id="transfered" class="btable" style="width:60%">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>From Sub Division</th>
                                <th>Component Name</th>
                                <th>Transfer To Division</th>
                                <th>Date</th>
                                <th>Quantity</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($stockout as $key =>$stk){
                            ?>
                            <tr>
                                <td>{{++$key}}</td>
                                <td><? if(isset($stk->SubDivision)){ echo $stk->SubDivision->subdivision_name;}?></td>
                                <td>{{$stk->Component->component_name}}</td>
                                <td>{{$stk->StoreOutDivision->name}}</td>
                                <td>{{$stk->store_out_date}}</td>
                                <td>{{$stk->quantity}}</td>
                            </tr>
                            <? } ?>
                            </tbody>
                            <tfoot>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Total</th>
                            <th>{{$stockout->sum('quantity')}}</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>

@stop
@section('page-script')
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js') }}
    {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js') }}
    {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.2/jspdf.plugin.autotable.js') }}

@stop

@section('plugin-script')
    <style>
        /*.searchbox{position: fixed;z-index: 999;background-color: white;}*/
        .btable, .btable th, .btable td {
            border: 1px solid #e0e0e0;
            border-collapse: collapse;
        }
        .btable th, .btable td {
            padding: 15px;
        }
        .btable th {
            text-align: left;
        }
        .btable  {
            border-spacing: 5px;
        }
        .btable tr:nth-child(even) {
            background-color: #eee;
        }
        .btable tr:nth-child(odd) {
            background-color: #fff;
        }
        .btable th {
            color: white;
            background-color:#3c8dbc;
        }
        .btable tfoot th{
            background-color: #3c8dbc;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $(".select2").select2();

            $("#production_date_from").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
            $("#production_date_to").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });


        });

        $(function () {


            $('#downloadpdf').click(function () {
                var pdf = new jsPDF('p', 'pt');//new jsPDF('l', 'mm', [300, 300]);


                var elem1 = document.getElementById('component');
                var res = pdf.autoTableHtmlToJson(elem1);

                var elem2 = document.getElementById('production');
                var data2 = pdf.autoTableHtmlToJson(elem2);

                var elem3 = document.getElementById('transfered');
                var data3 = pdf.autoTableHtmlToJson(elem3);

                var order = '<? foreach ( $order_no_val as $k=>$ord){ if($k==0){echo $ord;}else{echo ",".$ord;}}?>';


                var header = function(data) {
                    pdf.setFontSize(16);
                    pdf.setTextColor(40);
                    pdf.setFontStyle('normal');
                    //  doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
                    pdf.text(120,40,"<?=$division->name?> Division:Production-Summery-Report"+" Order:"+order);

                };


//var endPos = pdf.autoTableEndPosY();

                pdf.autoTable(res.columns, res.data, {
                    startY: pdf.autoTableEndPosY() + 80,
                    pageBreak: 'auto',
                    theme: 'grid',
                    beforePageContent: header,
                    margin: {horizontal: 20, top: 20, bottom: 20},
                    headerStyles: {fillColor: [204, 204, 204],textColor: 20},
                    styles: {
                        overflow: 'linebreak',
                        fontSize: 10,
                        tableWidth: 280,
                        columnWidth: 'auto',
                        valign: 'middle',
                        rowHeight: 10
                    },
                    drawHeaderRow: function(row, data) {
                        row.height = 25;
                    }

                });
                // pdf.addPage();
                pdf.autoTable(data2.columns, data2.data, {
                    startY : pdf.autoTableEndPosY() + 20,
                    pageBreak: 'auto',
                    theme: 'grid',
                    beforePageContent: header,
                    headerStyles: {fillColor: [204, 204, 204],textColor: 20},
                    styles: {
                        overflow: 'linebreak',
                        fontSize: 10,
                        tableWidth: 280,
                        columnWidth: 'auto',
                        valign: 'middle',
                        rowHeight: 10
                    },
                    drawHeaderRow: function(row, data) {
                        row.height = 25;
                    },
                    margin: {horizontal: 20, top: 20, bottom: 20}
                });
//pdf.addPage();
                pdf.autoTable(data3.columns, data3.data, {
                    startY : pdf.autoTableEndPosY() + 20,
                    pageBreak: 'auto',
                    theme: 'grid',
                    beforePageContent: header,
                    headerStyles: {fillColor: [204, 204, 204],textColor: 20},
                    styles: {
                        overflow: 'linebreak',
                        fontSize: 10,
                        tableWidth: 280,
                        columnWidth: 'auto',
                        valign: 'middle',
                        rowHeight: 10
                    },
                    drawHeaderRow: function(row, data) {
                        row.height = 25;
                    },
                    margin: {horizontal: 20, top: 20, bottom: 20}
                });



                pdf.save('{{$division->name}} division: Production-Summery-Report-order-'+order+'- <?=date('d-m-y')?>.pdf');


            });

        });

        function orderChange(){
            var order = $('#order_no').val();
            if(order){
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getArticleByOrderArray") ?>',
                    data: 'order='+order,
                    async: true,
                    success: function(data){
                        $("#article_id").removeAttr('readonly');
                        $("#article_id").removeAttr('disabled');
                        $("#article_id").html(data.article);
                    }
                });

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '<?= Url("productionOrder/getColorByOrderArray") ?>',
                    data: 'order='+order,
                    async: true,
                    success: function(data){
                        $("#color_id").removeAttr('readonly');
                        $("#color_id").removeAttr('disabled');
                        $("#color_id").html(data.color);
                    }
                });
            }
            else{
                $("#article_id").html('<option value="">Select</option>');
                $("#article_id").attr('readonly', 'readonly');
                $("#article_id").attr('disabled', 'disabled');

                $("#color_id").html('<option value="">Select</option>');
                $("#color_id").attr('readonly', 'readonly');
                $("#color_id").attr('disabled', 'disabled');
            }
        }

    </script>
@stop