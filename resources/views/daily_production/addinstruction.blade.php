@extends('layouts.master')
@section('head')
    @parent
    {{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')
    <?php

    $order_no_val = (isset($post_val['order_no'])) ? $post_val['order_no'] : null;
    $article_val = (isset($post_val['article_id'])) ? $post_val['article_id'] : null;
    $color_val = (isset($post_val['color_id'])) ? $post_val['color_id'] : null;

    ?>
    <div class="container">

        <div class="row">
            <?= Form::open(array('url' => 'dailyproduction/addinstruction', 'method' => 'post')); ?>
            <div class="col-md-10 sidebar">
                <?php
                $error_messages = Session::get('error_messages');

                if(isset($error_messages)){
                    foreach($error_messages as $message){
                        echo $message;
                    }
                }
                ?>
                <div class="widget">
                    <h2>Add Instruction</h2>

                    <div class="col-md-4 sidebar" style="margin-right: 150px;">
                        <div class="form-group">
                            <?= Form::label('department_id', 'Division', array('class' => 'control-label')); ?>
                            <div class="controls">
                                {{ Form::select('department_id',$division,'',array('class'=>'form-control','id'=>'department_id')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <?= Form::label('sub_department_id', 'Sub Division', array('class' => 'control-label')); ?>
                            <div class="controls">
                                {{ Form::select('sub_department_id',$sub_division,'',array('class'=>'form-control','id'=>'sub_department_id')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <?= Form::label('order_no', 'Order No', array('class' => 'control-label')); ?>
                            <div class="controls">
                                {{ Form::select('order_no',$production_order,$order_no_val,array('class'=>'form-control','id'=>'order_no')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <?= Form::label('article_no', 'Article No', array('class' => 'control-label')); ?>
                            <div class="controls">
                                {{ Form::select('article_no',$article,$article_val,array('class'=>'validate[required] form-control','id'=>'article_no')) }}
                            </div>
                        </div>

                        <div class="control-group">
                            <?= Form::label('color_id', 'Color', array('class' => 'control-label')); ?>
                            <div class="controls">
                                {{ Form::select('color_id', $color, null, $attributes = array('id'=>'color_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                            </div>
                        </div>



                    </div>
                    <div class="col-md-6 sidebar">
                        <div class="form-group">
                            <?= Form::label('size_group_id', 'Select Size Group', array('class' => 'control-label','onChange'=>"getSizesQuantityModal(this);")); ?>
                            <div class="controls">
                                {{ Form::select('size_group_id',$sizegroup,'',array('class'=>'form-control','id'=>'size_group_id',"onchange"=>'getSizesQuantityModal(this);')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <?= Form::label('quantity', 'Quantity', array('class' => 'control-label')); ?>
                            <div class="controls">
                                {{ Form::text('quantity','',array('class'=>'form-control','id'=>'quantity')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <?= Form::label('component_id', 'Component', array('class' => 'control-label')); ?>
                            {{ Form::select('component_id', $unit,null, $attributes =array('id'=>'component_id[]','name'=>'component_id[]','data-rel'=>'chosen','data-placeholder'=>'Component Unit', 'class'=>'validate[required] form-control select2', 'multiple'=>'true')) }}

                        </div>
                        <div class="form-group">
                            <?= Form::label('production_in_date', 'Production Date', array('class' => 'control-label')); ?>
                            <div class="controls">
                                {{ Form::text('production_in_date','', array('id'=>'production_in_date','data-rel'=>'chosen', 'class'=>'datepicker validate[required] form-control')) }}
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="controls">

                                <?= Form::submit('Add', array('class' => 'btn btn-success')); ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Size Quantity</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 size-error">
                                    Please select Size Group First
                                </div>
                                <div class="col-md-12 size-box">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancelModal()">Close</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="calculateTotalQuantity(this);">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>

    </div>



    <style>
        .sidebar{
            padding-bottom: 20px;
        }
        form{
            margin: 15px;
        }
    </style>
@stop
@section('page-script')
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js') }}
@stop

@section('plugin-script')
    <script type="text/javascript">

        $(document).ready(function(){

            $("#production_in_date").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });

            $(".select2").select2();
        });

        function cancelModal() {
            $('#myModal').modal('hide');
        }
        function getSizesQuantityModal(item){

            size_group_id=item.value;
            if(!item.value){
                $('#myModal').modal('show');return;
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                dataType: 'json',
                url: '<?= Url("dailyproduction/getSizesBySizeGroup") ?>',
                data: 'size_group_id='+size_group_id,
                async: true,
                success: function(data) {

                    var j = 1;
                    if (data.sizes.length) {

                        $('#myModal').find('.size-error').hide();
                        $('#myModal').find('.size-box').show();
                        $('#myModal').find('.size-box').html('');

                        $('#myModal').find('.size-box').append("<div class='form-group'>");
                        $('#myModal').find('.size-box').append("<label class='col-sm-2 control-label' for=''>Size</label>");
                        $('#myModal').find('.size-box').append("<label class='col-sm-10 control-label' for=''>Quantity</label>");
                        $.each(data.sizes, function (i, val) {
                            if(!val.quantity){
                                val.quantity=0;
                            }
                            $('#myModal').find('.size-box').append("<div class='form-group'>");
                            $('#myModal').find('.size-box').append("<label class='col-sm-2 control-label' for='size_" + val.size_no +  "'>" + val.size_no + "</label><input id='size_" + val.size_no + "' type='hidden' value='" + val.size_id + "' name='size_" + val.size_no + "' />");
                            $('#myModal').find('.size-box').append("<div class='col-sm-10'><input id='quantity_" + val.size_no + "' class='form-control' type='text' value='" + val.quantity + "' name='quantity_" + val.size_no + "' /> </div><div class='clearfix'></div></div>");
                            j++;
                        });
                        $('#myModal').modal('show');
                    }

                }
            });


        }

        function calculateTotalQuantity(item){
            var size_group_id = $('#size_group_id').val();
            var $i = 1;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                dataType: 'json',
                url: '<?= Url("dailyproduction/getSizesBySizeGroup") ?>',
                data: 'size_group_id='+size_group_id,
                async: true,
                success: function(data){

                    var total_quantity = 0;
                    $.each(data.sizes, function(i, val){
                        var quantity = (parseInt( $('#myModal').find('#quantity_'+val.size_no).val()))?parseInt( $('#myModal').find('#quantity_'+val.size_no).val()):0;
                        total_quantity = total_quantity + quantity;
                    });
                    $('#quantity').val(total_quantity);
                }
            });
        }

        function storeUnitChange(){
            var val = $( "#store_unit option:selected" ).text();
            $('#unit_conversion_store_label').html(val);

            //if combo
            alert("change if it is combo!");
        }

    </script>
@stop