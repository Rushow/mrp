@extends('layouts.master')
@section('head')
    @parent
    {{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}

@stop

@section('content')
    <div class="container">


        <div class="row  searchbox">
            <div class="col-md-12 sidebar">
                <?php if(Session::has('message')){ ?>
                <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
                <script>
                    $('#alert').delay(2000).fadeOut(400)
                </script>
                <?php } ?>
                <data class="row">
                    <div class="col-sm-10"><h2>{{$division->name}} Division : <h3>Daily Instruction {{date('d-m-yy')}}</h3></h2></div>
                    <div class="col-sm-2">
                    </div>
                </data>
                <!-- Content Header (Page header) -->
                <section class="content-header">
                </section>
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        @foreach ($dailyinstructions as $subname=>$dailyinstruction)
                        <div class="col-sm-12 ">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <h3>{{$subname}}</h3>
                                    <table class="btable" style="width:60%">
                                        <thead>
                                        <tr>
                                            <th>Order:</th>
                                            <th>Article:</th>
                                            <th>Color:</th>
                                            @foreach($size as $s)
                                                <th>{{$s->size_no}}<span class="sort-icon"><span></th>
                                            @endforeach
                                            <th>Quantity</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($dailyinstruction as $ks=>$ins)
                                            <tr>
                                                <td>{{$ins->Order->order_no}}</td>
                                                <td> <?if(isset($ins->Article->article_no)){echo $ins->Article->article_no;}?></td>
                                                <td> <?if(isset($ins->Color->color_name_flcl)){ echo $ins->Color->color_name_flcl;}?></td>

                                                @foreach($size as $s)
                                                    <? $szexist=0;?>
                                                    @foreach($ins->DailydetailSize as $sizedetail)
                                                        <? if($s->id == $sizedetail->size_id){?>
                                                        <td>{{$sizedetail->quantity}}<span class="sort-icon"><span></td>
                                                        <? $szexist=1; }?>
                                                    @endforeach
                                                    <? if($szexist==0){?>
                                                    <td></td>
                                                    <? } ?>
                                                @endforeach

                                                <td> <?=$ins->quantity?></td>

                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        @foreach($size as $s)
                                            <th></th>
                                        @endforeach
                                        <th></th>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </section>
            </div>
        </div>



    </div>

@stop
@section('page-script')
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js') }}
@stop

@section('plugin-script')
    <style>
        /*.searchbox{position: fixed;z-index: 999;background-color: white;}*/
        .btable, .btable th, .btable td {
            border: 1px solid #e0e0e0;
            border-collapse: collapse;
        }
        .btable th, .btable td {
            padding: 15px;
        }
        .btable th {
            text-align: left;
        }
        .btable  {
            border-spacing: 5px;
        }
        .btable tr:nth-child(even) {
            background-color: #eee;
        }
        .btable tr:nth-child(odd) {
            background-color: #fff;
        }
        .btable th {
            color: white;
            background-color:#3c8dbc;
        }
        .btable tfoot th{
            background-color: #3c8dbc;
        }
    </style>
@stop