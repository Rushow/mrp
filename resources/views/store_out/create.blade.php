@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<script>
    $(function() {
        $("#store_out_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
    });
</script>

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message){
                echo $message;
            }
        }
        ?>
        <div class="clearfix"></div>
        <div class="col-md-12">            
            <div class="widget" style="margin-bottom:25px;">
                <h2>Create Store Out</h2>
            </div>
            <?= Form::open(array('url' => 'storeOut', 'method' => 'post', 'id' => 'storeOutForm')) ?>
            <div class="widget" style="margin-bottom:25px;">
                <h2>Store Out Detail</h2>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-5">
                <div class="control-group">
                    <?= Form::label('warehouse_id', 'Warehouse', array('class' => 'control-label')) ?>
                    <div class="controls">
                        {{ Form::select('warehouse_id', $warehouse, '', $attributes = array('id'=>'warehouse_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group">
                    <?= Form::label('challan_no', 'Challan No', array('class' => 'control-label')) ?>
                    <div class="controls">
                        {{ Form::text('challan_no', '', array('class'=>'validate[required] form-control', 'id'=>'challan_no')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('department_id', 'Department', array('class' => 'control-label')) ?>
                    <div class="controls">
                        {{ Form::select('department_id', $department, '', $attributes = array('id'=>'department_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('order_type_id', 'Order Type', array('class' => 'control-label')) ?>
                    <div class="controls">
                        {{ Form::hidden('out_type', 'sample', array('id'=> 'out_type')) }}
                        {{ Form::select('order_type_id', $order_type, '', $attributes = array('id'=>'order_type_id','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'generateOrders(this);')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('order_no_id', 'Order No', array('class' => 'control-label')) ?>
                    <div class="controls">
                        {{ Form::select('order_no_id', $order_no, '', $attributes = array('id'=>'order_no_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                    </div>
                </div>                
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <div class="control-group">
                    <?= Form::label('store_out_date', 'Store Out Date', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('store_out_date', '',array('class'=>'validate[required] form-control datepicker','id'=>'store_out_date')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('remarks', 'Remarks', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('remarks', '', array('class'=>'form-control', 'style' => 'min-height: 225px', 'id'=>'remarks')) }}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>            
            <div class="widget" style="margin-bottom:25px;">
                <h2>Individual Store Out Detail</h2>
            </div>
            <table>
                <tr>
                    <td>SL No.</td>
                    <td class="col-md-1">Item Group</td>
                    <td class="col-md-1">Sub Group</td>
                    <td class="col-md-2">Parameter</td>
                    <td class="col-md-2">Color</td>
                    <td class="col-md-2">Item</td>
                    <td class="col-md-2">Quantity</td>
                    <td>Unit</td>
                    <td></td>
                </tr>
                <tr>
                    {{ Form::hidden('total_row', 1, array('id'=> 'total_row')) }}
                    <td>1</td>
                    <td>
                        {{ Form::select('item_group_id_1', $item_group, '', $attributes = array('id'=>'item_group_id_1','data-rel'=>'chosen', 'class'=>'form-control Required', 'onChange'=>'generateSubGroup(this, 1);')) }}
                    </td>
                    <td>
                        {{ Form::select('sub_group_id_1', $item_sub_group, null, $attributes = array('id'=>'sub_group_id_1','data-rel'=>'chosen', 'class'=>'form-control Required', 'disabled'=>'disabled', 'onChange'=>'generateItems(this, 1);')) }}
                    </td>
                    <td >
                        {{ Form::select('parameter_id_1', $parameter, '', $attributes = array('id'=>'parameter_id_1','data-rel'=>'chosen', 'class'=>'form-control Required')) }}
                    </td>
                    <td>
                        {{ Form::select('color_id_1', $color, '', $attributes = array('id'=>'color_id_1','data-rel'=>'chosen', 'class'=>'form-control Required')) }}
                    </td>
                    <td>
                        {{ Form::select('item_id_1', $item, '', $attributes = array('id'=>'item_id_1','data-rel'=>'chosen', 'class'=>'form-control Required', 'disabled' => 'disabled', 'onChange'=>'generateUnit(this, 1);')) }}
                    </td>
                    <td>
                        {{ Form::text('quantity_1', '',array('class'=>'form-control Required','id'=>'quantity_1')) }}
                    </td>
                    <td>
                        {{ Form::hidden('unit_id_1', '', array('id'=> 'unit_id_1')) }}
                        {{ Form::text('unit_1', $unit, array('id'=>'unit_1','data-rel'=>'chosen', 'class'=>'form-control Required', 'readonly'=>'readonly')) }}
                    </td>
                    <td>
                        <!-- <a href="#" onclick="return checkDelete(this);">
                            <span class="badge badge-important">X</span>
                        </a> -->
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><label onclick="addStoreOutRow(this);" class="pull-right" style="min-width:50px; cursor: pointer;">+Add More</label> </td>
                </tr>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <div class="controls">
                <?= Form::submit('Save Store Out', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
        <?= Form::close(); ?>
    </div>
</div>

<script type="text/javascript">

    function generateItems(item, count){
        var val = $(item).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("item/items") ?>',
                data: 'sub_group_id='+val,
                success: function(data){
                    $("#item_id_" + count).removeAttr('readonly');
                    $("#item_id_" + count).removeAttr('disabled');
                    $("#item_id_" + count).html(data.items);
                }
            });
        }
        else{
            $("#item_id_" + count).html('<option value="">Select</option>');   
            $("#item_id_" + count).attr('readonly', 'readonly');
            $("#item_id_" + count).attr('disabled', 'disabled');
        }
    }

    function generateSubGroup(item, count){
        var val = $(item).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("item/subGroup") ?>',
                data: 'group_id='+val,
                success: function(data){
                    $("#sub_group_id_" + count).removeAttr('readonly');
                    $("#sub_group_id_" + count).removeAttr('disabled');
                    $("#sub_group_id_" + count).html(data.sub_group);
                }
            });
        }
        else{
            $("#sub_group_id_" + count).html('<option value="">Select</option>');   
            $("#sub_group_id_" + count).attr('readonly', 'readonly');
            $("#sub_group_id_" + count).attr('disabled', 'disabled');
        }
    }

    function generateUnit (item, count) {
        var val = $(item).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("item/storeUnit") ?>',
                data: 'item_id='+val,
                success: function(data){
                    $("#unit_id_" + count).val(data.id);
                    $("#unit_" + count).val(data.unit);
                }
            });
        }else{

        }
    }

    function generateOrders(type){
        var val = $(type).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("storeIn/orderType") ?>',
                data: 'order_type='+val,
                success: function(data){
                    $("#order_no_id").html(data.orders);
                }
            });
        }
        else{
        }
    }

    function addStoreOutRow(item){
        var last_id = $("#total_row").val();
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '<?= Url("storeOut/addStoreOutRow") ?>',
            data: 'last_id='+last_id,
            success: function(data){
                if(data.error == false){
                    $(item).parents('tr').before('<tr>'+data.html+'</tr>');
                    last_id = parseInt(last_id) + 1;
                    $('#total_row').val(last_id);
                    $('#size_id_'+last_id).multiselect();
                }
                else{
                    alert(data.message);
                }
            }
        });
    }
    $(document).ready(function() {
        $("#storeOutForm").validate({
            rules: {
                warehouse_id: "required",
                challan_no: "required",
                store_out_date: "required",
                // remarks: "required",
                department_id: "required"
            },
            messages: {
                warehouse_id: "Please Select a Warehouse",
                challan_no: "Please Enter a Challan No",
                store_out_date: "Please Select a Store In Date",
                // remarks: "Please enter your Remarks",
                department_id: "Please Select a Supplier"
            }
        });
        $.validator.addClassRules("Required", {
            required: true
        });
    });
</script>

<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<style>
    .widget{
        overflow: visible !important;
    }
</style>

@stop