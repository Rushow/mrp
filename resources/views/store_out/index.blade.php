@extends('layouts.master')
@section('head')

@parent
{{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')

<?
    $sample_order_no_val = (isset($sample_order_no)) ? $sample_order_no : null;
    $buyer_val = (isset($buyer_id)) ? $buyer_id : null;
    $article_val = (isset($article_no)) ? $article_no : null;
    $sample_type_val = (isset($sample_type_id)) ? $sample_type_id : null;
    $order_date_val = (isset($order_date)) ? $order_date : null;
    $delivery_date_val = (isset($delivery_date)) ? $delivery_date : null;
?>


<div class="row">
    <div class="col-md-12 sidebar">

        <?php if(Session::has('message')){ ?>
        <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
        <script>
            $('#alert').delay(2000).fadeOut(400)
        </script>
        <?php } ?>
        <data class="row">
            <div class="col-sm-8">
                @if (!Auth::user()->isViewer())
                    <a href="<?= URL::to('storeOut/create')?>" class="btn btn-warning" style="max-width: 200px;" >Create Store Out</a>
                @endif
            </div>
            <div class="col-sm-4">
                <a href="<?= URL::to('excel/storeOut')?>" class="btn btn-primary" style="max-width: 200px;">Download excel file</a>
                <a href="<?= URL::to('pdf/storeOut')?>" target="_blank" class="btn btn-info" style="max-width: 200px;">Download pdf file</a>
            </div>
        </data>
        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <!-- form start -->
                        <?= Form::open(array('url' => 'storeOut/search', 'method' => 'post')); ?>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('sample_order_no', 'Select Sample Order No', array('class' => 'control-label')); ?>
                                    {{ Form::select('sample_order_no', $sample_order_no, $sample_order_no_val, $attributes = array('id'=>'sample_order_no','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('buyer_id', 'Buyer', array('class' => 'control-label')); ?>
                                    {{ Form::select('buyer_id', $buyer, $buyer_val, $attributes = array('id'=>'buyer_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('article_id', 'Article No', array('class' => 'control-label')); ?>
                                    {{ Form::select('article_id', $article, $article_val, $attributes = array('id'=>'article_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('sample_type_id', 'Type of Sample', array('class' => 'control-label')); ?>
                                    {{ Form::select('sample_type_id', $sample_type, $sample_type_val, $attributes = array('id'=>'sample_type_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('order_date', 'Order Date', array('class' => 'control-label')); ?>
                                    {{ Form::text('order_date', $order_date_val,array('class'=>'validate[required] form-control datepicker','id'=>'order_date')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('delivery_date', 'Delivery Date', array('class' => 'control-label')); ?>
                                    {{ Form::text('delivery_date', $delivery_date_val,array('class'=>'validate[required] form-control datepicker','id'=>'delivery_date')) }}
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-2 col-md-offset-11">
                                <button type="submit" class="btn btn-success">Search</button>
                            </div>

                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Store Out List
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Store Out List</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>SL No.<span class="sort-icon"><span></th>
                                    <th>Challan No.<span class="sort-icon"><span></th>
                                    <th>Warehouse<span class="sort-icon"><span></th>
                                    <th>Department<span class="sort-icon"><span></th>
                                    <th>Store Out Date<span class="sort-icon"><span></th>
                                    <th>Remarks<span class="sort-icon"><span></th>
                                    @if (!Auth::user()->isViewer())
                                        <th>Action</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($store_outs as $store_out)
                                    <tr>
                                        <td>{{ $store_out->id }}</td>
                                        <td>{{ $store_out->challan_no }}</td>
                                        <td>{{ $store_out->Warehouse->name }}</td>
                                        <td>{{ $store_out->Department->name }}</td>
                                        <td>{{ date('d.m.Y' ,strtotime($store_out->store_out_date)) }}</td>
                                        <td>{{ $store_out->remarks }}</td>
                                        <td>
                                            <a class="" href="<?= URL::to('storeOut/'.$store_out->id) ;?>"><span class="btn btn-primary btn-sm">Details</span></a>
                                            @if (!Auth::user()->isViewer())
                                                <a class="btn btn-info btn-sm" href="<?= URL::to('storeOut/'.$value->id).'/edit';?>">Edit</a>
                                                {{ Form::open(array('class' => 'btn btn-danger btn-xs','url' => 'storeOut/'.$value->id, 'method' => 'delete', 'style' => 'padding: 3px 0; border-color: transparent !important;')) }}
                                                {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs', 'style' => 'display:block;')) }}
                                                {{ Form::close() }}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
        </section>


    </div>
</div>

@stop

@section('page-script')
    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js')}}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js')}}
@stop

@section('plugin-script')
    <script type="text/javascript">
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });

            $("#order_date").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });

            $("#delivery_date").datepicker({
                changeMonth: true,
                changeYear: true,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                format: 'dd-mm-yyyy'
            });
        });
    </script>

<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<style>
    .widget{
        overflow: visible !important;
    }
</style>

@stop