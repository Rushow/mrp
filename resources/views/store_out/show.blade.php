@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message){
                echo $message;
            }
        }
        ?>

    <div class="col-md-12" style="margin-top:15px;">
        <a href="<?= URL::to('storeOut')?>" class="grid btn" style="max-width: 200px;" >Close</a>
        <a href="<?= URL::to('excel/storeOut/'.$store_out->id)?>" class="grid btn" style="max-width: 200px;">Download excel file</a>
        <a href="<?= URL::to('pdf/storeOut/'.$store_out->id)?>" target="_blank" class="grid btn" style="max-width: 200px;">Download pdf file</a>
    </div>
        <div class="col-md-12">            
            <div class="widget" style="margin-bottom:25px;">
                <h2>Store Out No : <strong>{{ $store_out->challan_no }}</strong></h2>
            </div>
            <table class="table table-first-column-number data-table display sort" style="margin-bottom: 20px;">
                <thead>
                    <tr>
                        <th class="col-md-1">#<span class="sort-icon"><span></th>
                        <th class="col-md-2">Item Group<span class="sort-icon"><span></th>
                        <th class="col-md-2">Sub Group<span class="sort-icon"><span></th>
                        <th class="col-md-1">Parameter<span class="sort-icon"><span></th>
                        <th class="col-md-1">Color<span class="sort-icon"><span></th>
                        <th class="col-md-3">Item<span class="sort-icon"><span></th>
                        <th class="col-md-1">Quantity<span class="sort-icon"><span></th>
                        <th class="col-md-1">Unit<span class="sort-icon"><span></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($store_out_details as $key => $store_out_detail)
                        <tr>
                        <td>
                            {{ $key + 1 }}
                        </td>
                        <td>
                            {{ $store_out_detail->ItemGroup->group_name }}
                        </td>
                        <td>
                            {{ $store_out_detail->ItemSubGroup->sub_group_name }}
                        </td>
                        <td>
                            {{ $store_out_detail->Parameter->parameter_name }}
                        </td>
                        <td>
                            {{ $store_out_detail->Color->color_name }}
                        </td>
                        <td>
                            {{ $store_out_detail->Item->item_name }}
                        </td>
                        <td>
                            {{ $store_out_detail->quantity }}
                        </td>
                        <td>
                            {{ $store_out_detail->Unit->unit_name }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        @if (!Auth::user()->isViewer())
            <div class="col-md-12">
                <a class="" href="<?= URL::to('storeOut/'.$store_out->id.'/edit');?>"><span class="label label-warning">Edit</span></a>
                {{ Form::open(array('url' => 'storeOut/'.$store_out->id, 'method' => 'delete', 'style' => 'float:right; width:65px; margin-top:-5px; margin-bottom:-10px;')) }}
                {{ Form::submit('Delete', array('class' => 'label label-warning', 'style' => 'display:block;')) }}
                {{ Form::close() }}
            </div>
        @endif
    </div>
</div>

<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<style>
    .widget{
        overflow: visible !important;
    }
</style>

@stop