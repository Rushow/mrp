@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<script type="text/javascript">
    $(function() {
        $("#store_out_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
    });
</script>
<div class="row">

    <div class="col-md-12 sidebar">
        <?
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message){
                echo $message;
            }
        }
        ?>
        <div class="clearfix"></div>
        <div class="col-md-12">            
            <div class="widget" style="margin-bottom:25px;">
                <h2>Edit Store Out</h2>
            </div>        
            {{ Form::open(array('url' => 'storeOut/'.$store_out->id, 'method' => 'put', 'id' => 'storeOutForm')) }}
            <div class="widget" style="margin-bottom:25px;">
                <h2>Store Out Detail</h2>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-5">
                <div class="control-group">
                    {{ Form::label('warehouse_id', 'Warehouse', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::select('warehouse_id', $warehouse, $store_out->warehouse_id, $attributes = array('id'=>'warehouse_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('challan_no', 'Challan No', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::text('challan_no', $store_out->challan_no, array('class'=>'validate[required] form-control', 'id'=>'challan_no')) }}
                    </div>
                </div>                
            </div>
            <div class="col-md-2">
                <div class="control-group">
                    <?= Form::label('order_type_id', 'Order Type', array('class' => 'control-label')) ?>
                    <div class="controls">
                        {{ Form::hidden('out_type', $store_out->out_type, array('id'=> 'out_type')) }}
                        {{ Form::select('order_type_id', $order_type, $store_out->out_type, $attributes = array('id'=>'order_type_id','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'generateOrders(this);')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('order_no_id', 'Order No', array('class' => 'control-label')) ?>
                    <div class="controls">
                        {{ Form::select('order_no_id', $order_no, $store_out->SampleOrder->id, $attributes = array('id'=>'order_no_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="control-group">
                    {{ Form::label('department_id', 'Department', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::select('department_id', $department, $store_out->department_id, $attributes = array('id'=>'department_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                    {{ Form::label('store_out_date', 'Store Out Date', array('class' => 'control-label')) }}
                        <div class="controls">
                            {{ Form::text('store_out_date', $store_out->store_out_date, array('class'=>'validate[required] form-control datepicker','id'=>'store_out_date')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="control-group">
                {{ Form::label('remarks', 'Remarks', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::text('remarks', $store_out->remarks, array('class'=>'validate[required] form-control', 'id'=>'remarks')) }}
                </div>
            </div>
            <div class="widget" style="margin-bottom:25px;">
                <h2>Individual Store Out Detail</h2>
            </div>
            <table>
                <tr>
                    <td>SL No.</td>
                    <td class="col-md-1">Item Group</td>
                    <td class="col-md-1">Sub Group</td>
                    <td class="col-md-2">Parameter</td>
                    <td class="col-md-2">Color</td>
                    <td class="col-md-2">Item</td>
                    <td class="col-md-2">Quantity</td>
                    <td>Unit</td>
                    <td></td>
                </tr>            
                    {{ Form::hidden('total_row', $store_out_details->count(), array('id'=> 'total_row')) }}
                    <? $count = 1; ?>
                    @foreach ($store_out_details as $store_out_detail)
                        <tr>
                            <td><?= $count ?></td>
                            <td>
                                {{ Form::select('item_group_id_'.$count, $item_group, $store_out_detail->item_group_id, $attributes = array('id'=>'item_group_id_'.$count,'data-rel'=>'chosen', 'class'=>'form-control Required', 'onChange'=>'generateSubGroup(this, 1);')) }}
                            </td>
                            <td>
                                {{ Form::select('sub_group_id_'.$count, $item_sub_group, $store_out_detail->sub_group_id, $attributes = array('id'=>'sub_group_id_'.$count,'data-rel'=>'chosen', 'class'=>'form-control Required')) }}
                            </td>
                            <td >
                                {{ Form::select('parameter_id_'.$count, $parameter, $store_out_detail->parameter_id, $attributes = array('id'=>'parameter_id_'.$count,'data-rel'=>'chosen', 'class'=>'form-control Required')) }}
                            </td>
                            <td>
                                {{ Form::select('color_id_'.$count, $color, $store_out_detail->color_id, $attributes = array('id'=>'color_id_'.$count,'data-rel'=>'chosen', 'class'=>'form-control Required')) }}
                            </td>
                            <td>
                                {{ Form::select('item_id_'.$count, $item, $store_out_detail->item_id, $attributes = array('id'=>'item_id_'.$count,'data-rel'=>'chosen', 'class'=>'form-control Required', 'onChange'=>'generateUnit(this, '.$count.');')) }}
                            </td>
                            <td>
                                {{ Form::text('quantity_'.$count, $store_out_detail->quantity,array('class'=>'form-control Required','id'=>'quantity_'.$count)) }}
                            </td>
                            <td>
                                {{ Form::hidden('unit_id_'.$count, '', array('id'=> 'unit_id_'.$count)) }}
                                {{ Form::text('unit_'.$count, $store_out_detail->Unit->unit_name, array('id'=>'unit_'.$count, 'data-rel'=>'chosen', 'class'=>'form-control Required', 'readonly'=>'readonly')) }}
                            </td>
                            <td>
                                <a href="#" onclick="return checkDelete(this);">
                                    <span class="badge badge-important">X</span>
                                </a>
                            </td>
                        </tr>
                        <? $count++; ?>
                    @endforeach            
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><label onclick="addStoreOutRow(this);" class="pull-right" style="min-width:50px; cursor: pointer;">+Add More</label> </td>
                </tr>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <div class="controls">
                <?= Form::submit('Update Store Out', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
        <?= Form::close(); ?>
    </div>
</div>

<script type="text/javascript">
    function generateSubGroup(item, count){
        var val = $(item).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                async: true,
                url: '<?= Url("item/subGroup") ?>',
                data: 'group_id='+val,
                success: function(data){
                    console.log(count);
                    $("#sub_group_id_" + count).removeAttr('readonly');
                    $("#sub_group_id_" + count).removeAttr('disabled');
                    $("#sub_group_id_" + count).html(data.sub_group);
                }
            });
        }
        else{
            $("#sub_group_id_" + count).html('<option value="">Select</option>');   
            $("#sub_group_id_" + count).attr('readonly', 'readonly');
            $("#sub_group_id_" + count).attr('disabled', 'disabled');
        }
    }

    function generateOrders(type){
        var val = $(type).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("storeIn/orderType") ?>',
                data: 'order_type='+val,
                success: function(data){
                    $("#order_no_id").html(data.orders);
                }
            });
        }
        else{
        }
    }

    function generateUnit (item, count) {
        var val = $(item).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("item/purchaseUnit") ?>',
                data: 'item_id='+val,
                success: function(data){
                    $("#unit_id_" + count).val(data.id);
                    $("#unit_" + count).val(data.unit);
                }
            });
        }else{

        }
    }

    function addStoreOutRow(item){
        var last_id = $("#total_row").val();
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '<?= Url("storeOut/addStoreOutRow") ?>',
            data: 'last_id='+last_id,
            success: function(data){
                if(data.error == false){
                    $(item).parents('tr').before('<tr>'+data.html+'</tr>');
                    last_id = parseInt(last_id) + 1;
                    $('#total_row').val(last_id);
                    $('#size_id_'+last_id).multiselect();
                }
                else{
                    alert(data.message);
                }
            }
        });
    }
    $(document).ready(function() {
        @for ($i = 1; $i <= $groups; $i++)
            {{ 'generateSubGroup($("#item_group_id_'.$i.'"), '.$i.');' }}
            {{ '$("#sub_group_id_'.$i.'").val('. $store_out_details[$i-1]->ItemSubGroup->id .');' }}
        @endfor

        $("#storeOutForm").validate({
            rules: {
                warehouse_id: "required",
                challan_no: "required",
                store_out_date: "required",
                department_id: "required"
            },
            messages: {
                warehouse_id: "Please Select a Warehouse",
                challan_no: "Please Enter a Challan No",
                store_out_date: "Please Select a Store In Date",
                department_id: "Please Select a Department"
            }
        });
        $.validator.addClassRules("Required", {
            required: true
        });
    });
</script>

<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<style>
    .widget{
        overflow: visible !important;
    }
</style>

@stop