@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-6 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Add Sub Group</h2>
            <?= Form::open(array('url' => 'itemSubGroup', 'method' => 'post', 'id' => 'itemSubGroupForm')); ?>
            <div class="control-group">
                <label class="control-label" for="group_id">Group</label>
                <div class="controls">
                    {{ Form::select('group_id', $item_group,null,$attributes = array('id'=>'group_id','data-rel'=>'chosen', 'class'=>'form-control')); }}
                </div>
            </div>
            <div class="form-group">
                <?= Form::label('sub_group_name', 'Item Sub Group Name', array('class' => 'control-label')); ?>
                <div class="controls">
                    {{ Form::text('sub_group_name','',array('class'=>'validate[required] form-control','id'=>'sub_group_name')) }}
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Submit', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#itemSubGroupForm").validate({
            rules: {
                group_id: "required",
                sub_group_name: "required"
            },
            messages: {
                group_id: "Please select a Group",
                sub_group_name: "Please enter Sub Group Name"
            }
        });
    });
</script>
@stop