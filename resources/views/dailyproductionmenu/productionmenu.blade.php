@foreach($divisonmenu as $divy)

    <li><a href="<?= url('dailyproduction')?>">
            <i class="fa fa-circle-o"></i>{{$divy->name}}</a>
        <?php if($divy->name=='PPIC'){$varname = "Match Up";}else{$varname = $divy->name;} ?>
        <ul class="treeview-menu">

            <li><a href="<?= url('dailyproduction/storein/'.$divy->id)?>"><i class="fa fa-hdd-o"></i>Add {{$varname }}</a></li>
            <li><a href="<?= url('dailyproduction/storeout/'.$divy->id)?>"><i class="fa fa-eject"></i><? if($divy->name =='Warehouse'){echo "Shipped Out";}else{echo "Transfered";}?> {{$varname}}</a></li>

            <li><a href="<?= url('dailyproduction/summeryin/'.$divy->id)?>"><i class="fa fa-list-alt"></i><? if($divy->name =='Warehouse'){echo "Stock In";}else{echo $varname;}?> List</a></li>
            <li><a href="<?= url('dailyproduction/summeryout/'.$divy->id)?>"><i class="fa fa-list-alt"></i><? if($divy->name =='Warehouse'){echo "Shipped Out";}else{echo "Transfered";}?> List</a></li>

            <li><a href="<?= url('dailyproduction/balance/'.$divy->id)?>"><i class="fa fa-list-alt"></i>Summery Report</a></li>
            <li><a href="<?= url('dailyproduction/totalbalance/'.$divy->id)?>"><i class="fa fa-list-alt"></i>Total Balance(Order)</a></li>
            <li><a href="<?= url('dailyproduction/componentrecieved/'.$divy->id)?>"><i class="fa fa-list-alt"></i>Component Recieved</a></li>
            <li><a href="<?= url('dailyproduction/dailyinstruction/'.$divy->id)?>"><i class="fa fa-list-alt"></i>Today's Instruction</a></li>

        </ul>
    </li>

@endforeach