@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">
    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Update Shipping Mode
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Update Shipping Mode</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'shippingMode/'.$shipping_mode->id, 'method' => 'put', 'id' => 'shippingModeForm')); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <?= Form::label('shipping_mode', 'Shipping Mode', array('class' => 'control-label')); ?>
                                <?= Form::text('shipping_mode', $shipping_mode->shipping_mode, array('class' => 'form-control')); ?>
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</button>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#shippingModeForm").validate({
            rules: {
                shipping_mode: "required"
            },
            messages: {
                shipping_mode: "Please enter Shipping Mode"
            }
        });
    });
</script>
@stop