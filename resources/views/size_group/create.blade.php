@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Create Size Group
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create Size Group</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'sizeGroup', 'method' => 'post')); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <?= Form::label('size_group_name', 'Size Group Name', array('class' => 'control-label')); ?>
                                {{ Form::text('size_group_name','',array('class'=>'validate[required] form-control','id'=>'size_group_name','placeholder'=>'Size Group Name')) }}
                            </div>
                            <div class="form-group">
                                <?= Form::label('sizes', 'Select Sizes', array('class' => 'control-label')); ?>
                                <? foreach($sizes as $size){ ?>
                                    {{ Form::checkbox('size[]', $size['id'],false,array('id'=>'size'.$size['id'])) }}
                                    {{ $size['size_no'] }}
                                <? } ?>
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>
@stop