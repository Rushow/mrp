@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')


<div class="row">
    <div class="col-sm-6">
        <a href="javascript:history.back()" class="grid btn">Back</a>
        <h2>Size Group Detail</h2>
        <table class="table table-first-column-number data-table display sort">
            <thead>
            <tr>
                <th>Size Group Name</th>
                <th>Size<span class="sort-icon"><span></th>
            </tr>
            </thead>
            <tbody>
            <? foreach($size_group_size as $key => $value){ ?>
            <tr>
                <td><?= $size_group->size_group_name ?></td>
                <td><?= $value->size_no ?></td>
            </tr>
                <? } ?>
        </table>
    </div>
</div>


<style>
    .btn{
        width: 150px!important;
        margin-top: 15px;
    }
</style>

@stop