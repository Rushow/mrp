@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">
    <div class="col-md-6 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Update Customer Group</h2>
            <?= Form::open(array('url' => 'customerGroup/'.$customer_group->id, 'method' => 'put', 'id' => 'customerGroupForm')); ?>
            <div class="form-group">
                <?= Form::label('customer_group', 'Customer Group', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::text('customer_group', $customer_group->customer_group, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Submit', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#customerGroupForm").validate({
            rules: {
                customer_group: "required"
            },
            messages: {
                customer_group: "Please enter Customer Group"
            }
        });
    });
</script>
@stop