@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">
    <div class="col-md-6 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Update Outsole No</h2>
            <?= Form::open(array('url' => 'outsoleNo/'.$outsole_no->id, 'method' => 'put', 'id' => 'outsoleNoForm')); ?>
            <div class="form-group">
                <?= Form::label('outsole_no', 'Heel No', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::text('outsole_no', $outsole_no->outsole_no, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Submit', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#outsoleNoForm").validate({
            rules: {
                outsole_no: "required"
            },
            messages: {
                outsole_no: "Please enter Outsole No"
            }
        });
    });
</script>

@stop