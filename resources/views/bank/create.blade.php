@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Bank
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Bank</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'bank', 'method' => 'post', 'files'=>true, 'id' => 'bankForm')); ?>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('name', 'Bank Name', array('class' => 'control-label')); ?>
                                    <?= Form::text('name', null, array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?= Form::label('account_no', 'Account No', array('class' => 'control-label')); ?>
                                    <?= Form::text('account_no', null, array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('swift_code', 'Swift Code', array('class' => 'control-label')); ?>
                                    <?= Form::text('swift_code', null, array('class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Address</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-md-6">
                                {{--<div class="form-group">--}}
                                    {{--<?= Form::label('floor', 'Floor', array('class' => 'control-label')); ?>--}}
                                    {{--<?= Form::text('floor', null, array('class' => 'form-control')); ?>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <?= Form::label('street_address', 'Street Address', array('class' => 'control-label')); ?>
                                    <?= Form::textarea('street_address', null, array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('city', 'City', array('class' => 'control-label')); ?>
                                    <?= Form::text('city', null, array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?= Form::label('state', 'State', array('class' => 'control-label')); ?>
                                    <?= Form::text('state', null, array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?= Form::label('zip', 'Zip', array('class' => 'control-label')); ?>
                                    <?= Form::text('zip', null, array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?= Form::label('country', 'Country', array('class' => 'control-label')); ?>
                                    <?= Form::text('country', null, array('class' => 'form-control')); ?>
                                </div>
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <div class="col-md-4 col-md-offset-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                            </div>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#bankForm").validate({
            rules: {
                name: "required"
            },
            messages: {
                name: "Please enter Bank Name"
            }
        });
    });
</script>
@stop