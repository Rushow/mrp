@extends('layouts.master')
@section('head')

@parent
{{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')

<?
$order_no_val = (isset($post_val['order_no'])) ? $post_val['order_no'] : null;
$destination_val = (isset($post_val['destination_id'])) ? $post_val['destination_id'] : null;
$buyer_val = (isset($post_val['buyer_id'])) ? $post_val['buyer_id'] : null;
$article_val = (isset($post_val['article_no'])) ? $post_val['article_no'] : null;
$season_val = (isset($post_val['season_id'])) ? $post_val['season_id'] : null;
$order_date_val = (isset($post_val['order_date'])) ? $post_val['order_date'] : null;
$delivery_date_val = (isset($post_val['delivery_date'])) ? $post_val['delivery_date'] : null;
?>

<div class="row">
    <div class="col-md-12 sidebar">
        <?php if(Session::has('message')){ ?>
        <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
        <script>
            $('#alert').delay(2000).fadeOut(400)
        </script>
        <?php } ?>
        <data class="row">
            <div class="col-sm-8">
                @if (!Auth::user()->isViewer())
                    <a href="<?= URL::to('productionOrderDetail/create')?>" class="btn btn-warning">Define Order Details</a>
                @endif
            </div>
            <div class="col-sm-4">
                <a href="<?= URL::to('pdf/productionOrderDetail')?>" target="_blank" class="btn btn-info">Download pdf file</a>
            </div>
        </data>
        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <!-- form start -->
                        <?= Form::open(array('url' => 'productionOrderDetail/index', 'method' => 'post')); ?>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('order_no', 'Select Production Order No', array('class' => 'control-label')); ?>
                                    {{ Form::select('order_no', $production_order, $order_no_val, $attributes = array('id'=>'order_no','data-rel'=>'chosen', 'class'=>'form-control')); }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('destination_id', 'Shopping Destination', array('class' => 'control-label')); ?>
                                    {{ Form::select('destination_id', $destination, $destination_val, $attributes = array('id'=>'destination_id','data-rel'=>'chosen', 'class'=>'form-control')); }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('season_id', 'Season', array('class' => 'control-label')); ?>
                                    {{ Form::select('season_id', $season, $season_val, $attributes = array('id'=>'season_id','data-rel'=>'chosen', 'class'=>'form-control')); }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('order_date', 'Order Date', array('class' => 'control-label')); ?>
                                    {{ Form::text('order_date', $order_date_val,array('class'=>'validate[required] form-control datepicker','id'=>'order_date')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('delivery_date', 'Delivery Date', array('class' => 'control-label')); ?>
                                    {{ Form::text('delivery_date', $delivery_date_val,array('class'=>'validate[required] form-control datepicker','id'=>'delivery_date')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('article_id', 'Article No', array('class' => 'control-label')); ?>
                                    {{ Form::select('article_id', $article, $article_val, $attributes = array('id'=>'article_id','data-rel'=>'chosen', 'class'=>'form-control')); }}
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-2 col-md-offset-11">
                                <button type="submit" class="btn btn-success">Search</button>
                            </div>

                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Production Order Detail List
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Production Order Detail List</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Order No<span class="sort-icon"><span></th>
                                    <th>Buying Dept.<span class="sort-icon"><span></th>
                                    <th>Article No<span class="sort-icon"><span></th>
                                    <th>Article Type<span class="sort-icon"><span></th>
                                    <th>Category<span class="sort-icon"><span></th>
                                    <th>Color<span class="sort-icon"><span></th>
                                    <th>Color Code<span class="sort-icon"><span></th>
                                    <th>Size Group<span class="sort-icon"><span></th>
                                    <th>Season<span class="sort-icon"><span></th>
                                    <th>Order Date<span class="sort-icon"><span></th>
                                    <th>Order Receive Date<span class="sort-icon"><span></th>
                                    @if (!Auth::user()->isViewer())
                                        <th style="width: 270px !important;">Action</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($orders as $key => $value){ ?>
                                <tr>
                                    <td><?= ($value->production_order_id)?$value->ProductionOrder->order_no:'' ?></td>
                                    <td><?= ($value->production_order_id)?Helpers::getDepartmentNameById($value->ProductionOrder->department_id):'' ?></td>
                                    <td><?= ($value->production_order_id)?Helpers::getArticleNoById($value->ProductionOrder->article_id):'' ?></td>
                                    <td><?= ($value->production_order_id)?Helpers::getArticleTypeById($value->ProductionOrder->article_type_id):'' ?></td>
                                    <td><?= ($value->production_order_id)?Helpers::getCategoryNameById($value->ProductionOrder->category_id):'' ?></td>
                                    <td><?= ($value->color_id)?$value->color->color_name_flcl:'' ?></td>
                                    <td><?= ($value->color_id)?$value->color->hm_code:'' ?></td>
                                    <td><?= ($value->production_order_id)?Helpers::getSizeGroupNameById($value->ProductionOrder->size_group_id):'' ?></td>
                                    <td><?= ($value->production_order_id)?Helpers::getSeasonById($value->ProductionOrder->size_group_id):'' ?></td>
                                    <td><?= ($value->order_date)?date('Y-m-d', $value->order_date):'' ?></td>
                                    <td><?= ($value->order_date)?date('Y-m-d', $value->order_receive_date):'' ?></td>
                                    @if (!Auth::user()->isViewer())
                                        <td>
                                            <a class="btn btn-info btn-sm" href="<?= URL::to('productionOrderDetail/'.$value->id).'/edit';?>">Edit</a>
                                            {{ Form::open(array('class' => 'btn btn-danger btn-xs','url' => 'productionOrderDetail/'.$value->id, 'method' => 'delete', 'style' => 'padding: 3px 0; border-color: transparent !important;')) }}
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs', 'style' => 'display:block;')) }}
                                            {{ Form::close() }}
                                        </td>
                                    @endif
                                </tr>
                                <?php } ?>

                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
        </section>


    </div>
</div>

<style>
    a.btn{
        color: #FFFFFF !important;
    }
</style>

@stop

@section('page-script')
    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}
@stop

@section('plugin-script')
    <script type="text/javascript">
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "scrollX": true
            });
        });
    </script>
@stop