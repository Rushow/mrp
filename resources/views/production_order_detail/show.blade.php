@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row" style="margin-bottom: 20px;">
    <a href="<?= URL::to('pdf/productionOrderDetail')?>" target="_blank" class="grid btn">Download pdf file</a>
    <div class="col-md-10 col-lg-10 col-sm-10">
        <h2>Production Order</h2>
        <div class="col-md-4 col-lg-4 col-sm-4">
            <b>Production Order No :</b> <?=$order->order_no?> <br>
            <b>Buyer :</b> <?=$order->buyer_name?>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4">
            <b>Order Date :</b> <?=date('d-m-Y', strtotime($order->order_date))?><br>
            <b>Delivery Date :</b> <?=date('d-m-Y', strtotime($order->delivery_date))?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">

        <table class="table table-first-column-number data-table display" style="margin-bottom: 20px;">
            <thead>
            <tr>
                <th>Production Order No<span class="sort-icon"><span></th>
                <th>Article Name<span class="sort-icon"><span></th>
                <th>Destination<span class="sort-icon"><span></th>
                <th>Size<span class="sort-icon"><span></th>
                <th>Color<span class="sort-icon"><span></th>
                <th>Quantity<span class="sort-icon"><span></th>
                <th>Note<span class="sort-icon"><span></th>
            </tr>
            </thead>
            <tbody>
            <?
            $total = 0;
            foreach($order_details as $key => $value){
            ?>
            <tr>
                <td><?= $value->production_order_no ?></td>
                <td><?= $value->article_no ?></td>
                <td><?= $value->destination_name ?></td>
                <td>
                    <?
                    $t = count($value->size);
                    $i = 0;
                    $total_qty = 0;
                    foreach($value->size as $s){
                        echo $s->size_no;
                        $total_qty += Helpers::getPOSizeQuantity($value['id'], $s->id);
                        $i++;
                        echo ($i != $t)?', ':'';
                    }
                    ?>
                </td>
                <td><?= $value->color_name ?></td>
                <td><?= $total_qty ?> Pair</td>
                <td><?= $value->note ?></td>
            </tr>
                <?
                $total += $value->quantity;
            }
            ?>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><b>Total</b></td>
                <td><b><?=$total?> Pair</b></td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <a class="btn btn-default btn-md" href="<?= URL::to('sampleOrder/edit/'.$order->id);?>">Edit Order</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-default btn-md" href="#">Close</a>
    </div>
</div>

@stop