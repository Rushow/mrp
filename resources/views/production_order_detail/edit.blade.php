@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<script>
    $(function() {
        $("#order_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
        $("#cut_off").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
    });

</script>

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message){
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Edit Production Order Detail</h2>
            <?= Form::open(array('url' => 'productionOrderDetail/'.$order_detail->id, 'method' => 'put', 'id' => 'productionOrderDetailForm')); ?>
            <div class="col-md-4 sidebar" style="margin-right: 150px;">
                {{ Form::hidden('production_order_id', $order->id) }}
                <div class="form-group">
                    <?= Form::label('order_no', 'Order No', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('order_no', $order->order_no,array('class'=>'validate[required] form-control','id'=>'order_no', 'readonly'=>'readonly')) }}
                    </div>
                </div>
                <div class="form-group">
                    <?= Form::label('article_id', 'Article', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::select('article_id', $article, $order->article_id, $attributes = array('id'=>'article_id','data-rel'=>'chosen', 'class'=>'form-control', 'readonly'=>'readonly')); }}
                    </div>
                </div>
                <div class="form-group">
                    <?= Form::label('color_id', 'Color', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::select('color_id', $color, $order->color_id, $attributes = array('id'=>'color_id','data-rel'=>'chosen', 'class'=>'form-control')); }}
                    </div>
                </div>
                <div class="form-group">
                    <?= Form::label('destination_id', 'Destination', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::select('destination_id', $destination, (isset($order_detail))?$order_detail->destination_id:'', $attributes = array('id'=>'destination_id','data-rel'=>'chosen', 'class'=>'form-control')); }}
                    </div>
                </div>
                <div class="form-group">
                    <?= Form::label('season_id', 'Season', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::select('season_id', $season, $order->season_id, $attributes = array('id'=>'season_id','data-rel'=>'chosen', 'class'=>'form-control')); }}
                    </div>
                </div>
                <div class="form-group">
                    <?= Form::label('', 'Sizes', array('class' => 'control-label col-md-4 bold')); ?>
                    <?= Form::label('', 'Quantity', array('class' => 'control-label col-md-8 bold')); ?>
                    <div class="clearfix"></div>
                </div>
                <?php

                $i = 1;
                $total_qty = 0;
                if(isset($order_detail_sizes)){
                    foreach($order_detail_sizes as $s){
                        $qty = (Helpers::getPOSizeQuantity($order_detail->id, $s->id))?Helpers::getPOSizeQuantity($order_detail->id, $s->id):0;
                ?>
                    <div class="form-group">
                        <?= Form::label('size_no_'.$i, $s->size_no, array('class' => 'control-label col-md-4')); ?>
                        {{ Form::hidden('size_id_'.$i, $s->id) }}
                        <div class="controls col-md-8">
                            {{ Form::text('quantity_'.$i, $qty,array('class'=>'size_qty form-control Required','id'=>'quantity_'.$i)) }}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php
                    $total_qty += $qty;
                    $i++;
                    }
                }
                ?>
                <div class="form-group">
                    <?= Form::label('total', 'Total', array('class' => 'control-label col-md-4 bold')); ?>
                    <div class="controls col-md-8">
                        {{ Form::text('total_quantity', $total_qty,array('class'=>'form-control bold','id'=>'total_quantity','readonly'=>'readonly')) }}
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-6 sidebar">
                <div class="form-group">
                    <?= Form::label('order_date', 'TOD', array('class' => 'control-label col-md-4')); ?>
                    <div class="controls col-md-8">
                        {{ Form::text('order_date', date('Y-m-d', $order->order_date),array('class'=>'validate[required] form-control datepicker','id'=>'order_date', 'readonly'=>'readonly')) }}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <?= Form::label('days_ship_advance', 'No. of Days Need Ship Advance', array('class' => 'control-label col-md-4')); ?>
                    <div class="controls col-md-8">
                        {{ Form::text('days_ship_advance', (isset($order_detail))?$order_detail->days_ship_advance:'',array('class'=>'validate[required] form-control','id'=>'days_ship_advance')) }}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <?= Form::label('cut_off', 'Cut Off', array('class' => 'control-label col-md-4')); ?>
                    <div class="controls col-md-8">
                        {{ Form::text('cut_off', (isset($order_detail))?date('Y-m-d', $order_detail->cut_off):'',array('class'=>'validate[required] form-control datepicker','id'=>'cut_off', 'readonly'=>'readonly')) }}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <?= Form::label('remarks', 'Remarks', array('class' => 'control-label col-md-4')); ?>
                    <div class="controls col-md-8">
                        {{ Form::textarea('remarks', (isset($order_detail))?$order_detail->remarks:'',array('class'=>'form-control','id'=>'remarks')) }}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="submit" class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</button>
                </div>
            </div>
            <div class="clearfix"></div>

            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>

    $(document).ready(function() {
        $("#productionOrderDetailForm").validate({
            rules: {
                order_no: "required",
                order_date: "required",
                cut_off: "required"
            },
            messages: {
                order_no: "Please enter a Order No",
                order_date: "Please enter Order Date",
                cut_off: "Please enter Cut Off Date"
            }
        });

        $.validator.addClassRules("Required", {
            required: true
        });
    });

    $('.size_qty').change(function(){
        var total = 0;
        $('.size_qty').each(function(i, obj) {
            obj_id = obj.id;
            obj_val = ($(this).val())?$(this).val():0;
            total += parseInt(obj_val);
            console.log(total);
            $("#total_quantity").val(total);
        });
    });
</script>

<style>
    .widget{
        overflow: visible !important;
    }
</style>

@stop