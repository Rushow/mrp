@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')


<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message){
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Define Production Order Details
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Define Production Order Details</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'productionOrderDetail', 'method' => 'post', 'id' => 'productionOrderDetailForm')); ?>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('buyer_id', 'Buyer', array('class' => 'control-label')); ?>
                                    {{ Form::select('buyer_id', $buyer, '', $attributes = array('id'=>'buyer_id','data-rel'=>'chosen', 'class'=>'validate[required] form-control', 'onChange'=>'getOrderByBuyer(this);')); }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('production_order_id', 'Order', array('class' => 'control-label')); ?>
                                    {{ Form::select('production_order_id', $order, '', $attributes = array('id'=>'production_order_id','data-rel'=>'chosen', 'class'=>'form-control', 'onchange'=>'getOrderInfo(this);', 'disabled'=>'disabled')); }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('article_id', 'Article', array('class' => 'control-label')); ?>
                                    {{ Form::select('article_id', $article, '', $attributes = array('id'=>'article_id','data-rel'=>'chosen', 'class'=>'form-control', 'disabled'=>'disabled'    )); }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('destination_id', 'Destination', array('class' => 'control-label')); ?>
                                    {{ Form::select('destination_id', $destination, '', $attributes = array('id'=>'destination_id', 'name'=>'destination_id[]', 'multiple'=>'', 'data-rel'=>'chosen', 'class'=>'form-control select2', 'placeholder' => 'Select Destination')); }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('season_id', 'Season', array('class' => 'control-label')); ?>
                                    {{ Form::select('season_id', $season, '', $attributes = array('id'=>'season_id','data-rel'=>'chosen', 'class'=>'form-control', 'readonly'=>'readonly')); }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('', 'Sizes', array('class' => 'control-label col-md-4 bold')); ?>
                                    <?= Form::label('', 'Quantity', array('class' => 'control-label col-md-8 bold')); ?>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group" id="size_html"></div>
                                <?
                                /*$i = 1;
                                if($sizes){
                                foreach($sizes as $s){
                                    $size_no = (Helpers::getSizeNoById($s->size_id))?Helpers::getSizeNoById($s->size_id):'';
                */
                                ?>
                                {{--<div class="form-group">--}}
                                {{--<?= Form::label('size_no_'.$i, $size_no, array('class' => 'control-label col-md-4')); ?>--}}
                                {{--{{ Form::hidden('size_id_'.$i, $s->size_id) }}--}}
                                {{--<div class="controls col-md-8">--}}
                                {{--{{ Form::text('quantity_'.$i, '',array('class'=>'size_qty form-control Required','id'=>'quantity_'.$i)) }}--}}
                                {{--</div>--}}
                                {{--<div class="clearfix"></div>--}}
                                {{--</div>--}}
                                <?
                                /*$i++;
                            }
                            }*/
                                ?>
                                <div class="form-group">
                                    <?= Form::label('total', 'Total', array('class' => 'control-label col-md-4 bold')); ?>
                                    <div class="controls col-md-8">
                                        {{ Form::text('total_quantity', '',array('class'=>'form-control bold','id'=>'total_quantity','readonly'=>'readonly')) }}
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('order_date', 'TOD', array('class' => 'control-label')); ?>
                                    {{ Form::text('order_date', '',array('class'=>'validate[required] form-control datepicker','id'=>'order_date', 'readonly'=>'readonly')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('days_ship_advance', 'No. of Days Need Ship Advance', array('class' => 'control-label')); ?>
                                    {{ Form::text('days_ship_advance','',array('class'=>'validate[required] form-control','id'=>'days_ship_advance')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('cut_off', 'Cut Off', array('class' => 'control-label')); ?>
                                    {{ Form::text('cut_off', '',array('class'=>'validate[required] form-control datepicker','id'=>'cut_off', 'readonly'=>'readonly')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('remarks', 'Remarks', array('class' => 'control-label')); ?>
                                    {{ Form::textarea('remarks','',array('class'=>'form-control','id'=>'remarks')) }}
                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <div class="col-md-4 col-md-offset-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                            </div>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

@stop

@section('page-script')
    <!-- Select2 -->
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js'); }}
@stop

@section('plugin-script')
<script>
    $(function() {
        $(".select2").select2();

        $("#order_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
        $("#cut_off").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
    });

    $(document).ready(function() {
        $("#productionOrderDetailForm").validate({
            rules: {
                production_order_id: "required",
                order_date: "required",
                cut_off: "required"
            },
            messages: {
                production_order_id: "Please enter a Order No",
                order_date: "Please enter Order Date",
                cut_off: "Please enter Cut Off Date"
            }
        });

        $.validator.addClassRules("Required", {
            required: true
        });
    });

    function size_change(){
        var total = 0;
        $('.size_qty').each(function(i, obj) {
            obj_id = obj.id;
            obj_val = ($(this).val())?$(this).val():0;
            total += parseInt(obj_val);
//            console.log(total);
            $("#total_quantity").val(total);
        });
    }

    function getOrderByBuyer(item){
        var val = $(item).val();
        console.log(val);

        if(val != ""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("productionOrderDetail/getOrderByBuyer") ?>',
                data: 'buyer_id='+val,
                success: function(data){
                    console.log(data);
                    var el = $('#production_order_id');
                    el.prop("disabled", false);
                    el.empty(); // removing old options
                    el.append($("<option></option>").attr("value", "").text("Select Order"));
                    $.each(data.orders, function(value, key){
                        el.append($("<option></option>").attr("value", value).text(key));
                    })
                }
            })
        }
    }

    function getOrderInfo(item){
        var val = $(item).val();
        console.log(val);

        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("productionOrderDetail/getOrderInfo") ?>',
                data: 'order_id='+val,
                success: function(data){
                    console.log(data);
                    $('#article_id').val(data.article_id);
                    $('#article_id').attr('disabled','disabled');
                    $('#season_id').val(data.season_id);
                    $('#season_id').attr('disabled','disabled');
                    $('#order_date').val(data.order_date);
                    $('#order_date').attr('disabled','disabled');
                    $('#size_html').html(data.size_html);
                    $('#total_quantity').val('');
                }
            });
        }
    }
</script>

@stop