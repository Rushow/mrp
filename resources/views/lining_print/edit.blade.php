@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">
    <div class="col-md-6 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Update Lining Print</h2>
            <?= Form::open(array('url' => 'liningPrint/'.$lining_print->id, 'method' => 'put', 'id' => 'liningPrintForm')); ?>
            <div class="form-group">
                <?= Form::label('lining_print', 'Lining Print', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::text('lining_print', $lining_print->lining_print, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <?= Form::label('status', 'Status', array('class' => 'control-label')); ?>
                <div class="controls">
                    {{ Form::select('status', $status, $lining_print->status,$attributes = array('id'=>'status','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'changeStatus(this)')); }}
                </div>
            </div>
            <div class="form-group">
                <?= Form::label('enable_date', 'Enable Date', array('class' => 'control-label')); ?>
                <div class="controls">
                    {{ Form::text('enable_date', ($lining_print->status)?date('d.m.Y', $lining_print->enable_date):"",array('class'=>'validate[required] form-control datepicker','id'=>'enable_date')) }}
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="submit" class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</button>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(function() {
        $("#enable_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });
    });
</script>

<script>
    $(document).ready(function() {
        changeStatus()

        $("#liningPrintForm").validate({
            rules: {
                lining_print: "required",
                status: "required"
            },
            messages: {
                lining_print: "Please enter Lining Print",
                status: "Please select Status"
            }
        });
    });

    function changeStatus(){
        var status = $('#status').val();

        if(status == 1){
            $('#enable_date').removeAttr("disabled");
        }
        else{
            $('#enable_date').val('');
            $('#enable_date').attr("disabled", true);
        }
    }
</script>
@stop