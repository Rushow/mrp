@extends('layouts.master')
@section('head')

    @parent
    {{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')

    <div class="row">

        <div class="col-md-12 sidebar">
            <?php
            $error_messages = Session::get('error_messages');

            if(isset($error_messages)){
                foreach($error_messages as $message){
                    echo $message;
                }
            }

            ?>
                    <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Sub-Division List
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Sub-Department/Sub-Division List</h3>
                                <a href="<?=URL::to('subdivision/create')?>" class="btn btn-warning btn-sm">Add Sub-Division </a>

                            </div><!-- /.box-header -->
                            <div class="box-body">

                                <div class="col-md-12">
                                    <table class="table table-first-column-number data-table display" id="example1">
                                        <thead>
                                        <tr>
                                            <th>SL No.</th>
                                            <th class="col-md-2">Division Name</th>
                                            <th class="col-md-1">Sub Division Name</th>
                                            <th class="col-md-1">Sub Division Description</th>
                                            <th class="col-md-2">Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?
                                        $total = 0;
                                        foreach($subdivision as $key => $value){
                                        ?>
                                        <tr>
                                            <td><?=$value->id?></td>
                                            <td><?=$value->division->name?></td>
                                            <td><?=$value->subdivision_name?></td>
                                            <td><?=$value->subdivision_description?></td>


                                            <td>

                                                <a class="btn btn-info btn-sm" href="<?=URL::to('subdivision/'.$value->id.'/edit')?>">Edit</a>
                                                {{ Form::open(['route' => ['subdivision.destroy', $value->id], 'method' => 'delete']) }}
                                                {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                                {{ Form::close() }}


                                            </td>


                                        </tr>
                                        <? } ?>

                                        </tbody>
                                    </table>
                                </div>

                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>




        </div>







        @stop
        @section('page-script')

    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js') }}

    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}
@stop
        @section('plugin-script')
            <script type="text/javascript">
                $(function () {
                    $('#example1').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "columns": [
                            { "width": "10%" },
                            null,
                            null,
                                null,
                                null
                        ]
                    });

                });
            </script>

        @stop