@extends('layouts.master')
@section('head')

    @parent

@stop
@section('header')
@stop
@section('content')


    <div class="row">

        <div class="col-md-12 sidebar">
            <?php
            $error_messages = Session::get('error_messages');

            if(isset($error_messages)){
                foreach($error_messages as $message){
                    echo $message;
                }
            }


            ?>
            <div class="widget">
                <h2 class="topheadertext"> Add Sub-Department/Sub-Division :</h2>
                <?= Form::open(array('url' => 'subdivision', 'method' => 'post', 'id' => 'addSubDivisionForm')); ?>
                <div class="col-md-4 sidebar" style="margin-right: 150px;">
                    <div class="form-group">
                        <?= Form::label('division_name', 'Division Name', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('division_name', $division, '', $attributes =array('class'=>'validate[required] form-control','id'=>'division_name')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('sub_division_name', 'Sub Division Name ', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('sub_division_name','',array('class'=>'validate[required] form-control','id'=>'sub_division_name')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('description', 'Description', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('description', '', array('id'=>'description', 'class'=>'validate[required] form-control')) }}
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="controls">
                            <?= Form::submit('Save', array('class' => 'btn btn-success')); ?>
                        </div>
                    </div>
                </div>


                <?= Form::close(); ?>
            </div>
        </div>
    </div>



    <style>
        .sidebar{
            padding-bottom: 20px;
        }
        form{
            margin: 15px;
        }
    </style>

    <script>

        $(document).ready(function() {
            $("#addSubDivisionForm").validate({
                rules: {
                    division_name: "required",
                    sub_division_name:"required",
                    description: "required",

                }
            });

            $.validator.addClassRules("Required", {
                required: true
            });

        });

    </script>

    <style>
        .widget{
            overflow: visible !important;
        }
    </style>

@stop