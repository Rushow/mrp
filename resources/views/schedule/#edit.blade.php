@extends('layouts.master')
@section('head')

    @parent

@stop
@section('header')
@stop
@section('content')
    {{ HTML::style('codebase/dhtmlxgantt.css') }}

    {{ HTML::script('codebase/dhtmlxgantt.js') }}
    {{--{{ HTML::script('dhtmlxGantt/codebase/ext/dhtmlxgantt_quick_info.js') }}--}}
    {{--{{ HTML::script('dhtmlxGantt/codebase/common/testdata.js') }}--}}
    {{ HTML::script('codebase/api.js') }}
    {{ HTML::script('codebase/dhtmlxmenu.js') }}
    {{ HTML::script('codebase/ext/dhtmlxgantt_marker.js') }}
    {{--{{ HTML::script('codebase/ext/dhtmlxgantt_keyboard_navigation.js') }}--}}
    {{ HTML::script('codebase/ext/dhtmlxgantt_smart_rendering.js') }}
    {{ HTML::script('codebase/ext/dhtmlxgantt_undo.js') }}
    {{--{{ HTML::script('codebase/ext/dhtmlxgantt_multiselect.js')}}--}}

    <body>
    <div class="row">
        <div class="col-md-9">
            <h3>Order No : <?if(isset($id)){echo $id;}else{echo "ALL";}?></h3>
        </div>
        <div class="col-md-3" style="margin-top: 10px;">
            <a href="#" onclick='gantt.exportToPDF()'  class="btn btn-success"> Export PDF</a>
        </div>

    </div>

    <div id="gantt_here" style='width:100%; min-height:800px;'></div>
    <script type="text/javascript">

        $(document).ready(function() {

            gantt.templates.scale_cell_class = function (date) {
                if (date.getDay() == 6 || date.getDay() == 5) {
                    return "weekend";
                }
            };
            gantt.templates.task_cell_class = function (item, date) {
                if (date.getDay() == 6 || date.getDay() == 5) {
                    return "weekend";
                }
            };
            gantt.templates.task_class  = function(start, end, task){
                switch (task.sub_division_id){
                    case "1":
                        return "high";
                        alert('hey');
                        break;
                    case "2":
                        return "medium";
                        break;
                    case "3":
                        return "low";
                        break;
                }
            };

            gantt.config.undo = true;
            gantt.config.undo_actions = {
                update:"update",
                remove:"remove", // remove an item from datastore
                add:"add"
            };
            gantt.config.undo_steps = 1;
            gantt.config.undo_types = {
                task:"task"
            };
            //gantt.locale.labels.section_description=["Process Description"];
            //gantt.getLightboxSection('description').setValue('Description');
            gantt.config.details_on_dblclick = true;

            gantt.locale.labels.section_order_id=["Order"];
            gantt.locale.labels.section_article_no = ["Article"];
            gantt.locale.labels.section_quantity = ["Target Quantities"];
            gantt.locale.labels.section_division_id = ["Division"];
            gantt.locale.labels.section_sub_division_id = ["Sub Division"];
            gantt.locale.labels.section_machine_id = ["Machine"];
            gantt.locale.labels.section_style_id = ["Style/Construction"];
            gantt.locale.labels.section_complexity_id = ["Complexity"];


            gantt.config.columns = [
                {name: "id", label: "id", width: 10,  align: "left",hide:true },
                {name: "order_id", label: "Order Id", width: 90,  align: "center",resize: true },
                {name: "text", label: "Process", width: 90,align: "center", resize: true },
                {name: "start_date", label: "Start time", template: function (obj) {
                    return gantt.templates.date_grid(obj.start_date);
                }, align: "center", width: 90, resize: true },
                {name: "duration", label: "Duration", align: "center", width: 60, resize: true},

                {name: "add", label: "", width: 30,align: "right", resize: true }
            ];

            gantt.config.grid_resize = true;
            gantt.config.grid_width = 600;

            gantt.config.scale_height = 60;
            gantt.config.subscales = [
                { unit: "week", step: 1, date: "Week #%W"}
            ];
            gantt.config.xml_date = "%Y-%m-%d %H:%i:%s";
            gantt.config.step = 1;
            gantt.config.scale_unit= "day";

            var date = new Date(), y = date.getFullYear(), m = date.getMonth();

            <? if(isset($planning)){

                $start = "";
                $end = "";
                foreach ($planning as $p){
                    $start =$p->start;
                    $end = $p->end;
                }
             }
            ?>
            <? if(isset($id)&&($id!=0)) { ?>

                gantt.init("gantt_here", new Date('<?=$start?>'), new Date('<?=$end?>'));
            gantt.load("{{url('gantt_data/'.$id)}}", "json");
            var dp = new gantt.dataProcessor("{{url('gantt_data/'.$id)}}");

            <? }else if(isset($id)&&($id==0)){ ?>
                gantt.init("gantt_here", new Date(date.getFullYear(),date.getMonth(),1), new Date(date.getFullYear(),date.getMonth()+3,1));
            gantt.load("{{url('gantt_data/'.$id)}}", "json");
            var dp = new gantt.dataProcessor("{{url('gantt_data/')}}");
            <? }else{ ?>
                gantt.init("gantt_here", new Date('<?=$start?>'), new Date('<?=$end?>'));
            gantt.load("{{url('gantt_data/')}}", "json");
            var dp = new gantt.dataProcessor("{{url('gantt_data/')}}");
            <? } ?>

            dp.init(gantt);
            dp.setTransactionMode("REST");
            //dp.setTransactionMode("POST", false);
            gantt.attachEvent("onTaskLoading", function (task) {
                //task.order_id = 'order_id';

                return true;
            });



            gantt.attachEvent("onTaskCreated", function(task){
                //any custom logic here

                return true;
            });
            var opts = [<? foreach ($order as $o) {?>{ key:<?=$o->order_no?>, label: '<?=$o->order_no?>' },<? } ?>
            ];

            var opts_art = [<? foreach ($article as $o) {?>{ key:<?=$o->article_no?>, label: '<?=$o->article_no?>' },<? } ?>
            ];

            var opts_div = [<? foreach ($division as $div) {?>{ key:<?=$div->id?>, label: '<?=$div->name?>' },<? } ?>
            ];

            var opts_sub_div = [<? foreach ($subdivision as $sdiv) {?>{ key:<?=$sdiv->id?>, label: '<?=$sdiv->subdivision_name?>' },<? } ?>
            ];

            var opts_machine = [<? foreach ($machine as $m) {?>{ key:<?=$m->id?>, label: '<?=$m->machine_id?>' },<? } ?>
            ];

            var opts_style = [<? foreach ($style as $m) {?>{ key:<?=$m->id?>, label: '<?=$m->style_name?>' },<? } ?>
            ];
            var opts_complexity = [<? foreach ($complexity as $m) {?>{ key:<?=$m->id?>, label: '<?=$m->complexity?>' },<? } ?>
            ];


            gantt.config.lightbox.sections = [
                {name:"description", height:38, map_to:"text", type:"textarea", focus:true,default_value:"Low"},
                {name:"order_id",    height:38, map_to:"order_id", type:"select", options:opts},
                {name:"article_no",    height:38, map_to:"article_no", type:"select", options:opts_art},
                {name:"division_id",    height:38, map_to:"division_id", type:"select", options:opts_div},
                {name:"sub_division_id",    height:38, map_to:"sub_division_id", type:"select", options:opts_sub_div},
                {name:"style_id",    height:38, map_to:"style_id", type:"select", options:opts_style},
                {name:"complexity_id",    height:38, map_to:"complexity_id", type:"select", options:opts_complexity},
                {name:"quantity",    height:38, map_to:"quantity", type:"textarea", default_value:"100"},
                {name:"time",        height:72, map_to:"auto", type:"duration"}
            ];



            dp.attachEvent("onBeforeDataSending", function(id, state, data){
                //console.log(data);
                return true;
            });

            gantt.attachEvent("onBeforeTaskUpdate", function(id,item){
                //console.log(item);
//            var taskId = gantt.addTask({
//                id:item.id,
//                text:item.text,
//                division_id:item.division_name,
//                sub_division_id:item.sub_division_id,
//                article_no:item.article_no,
//                start_date:item.start_date,
//                end_date:item.end_date,
//                duration:item.duration,
//                parent:item.parent,
//            });
//
//            gantt.updateTask(taskId);
                return true;
            });
            //prevent moving to another sub-branch:
            gantt.attachEvent("onAfterTaskDrag", function(id,item, mode, e){
                gantt.message("Checking if date is available ...");

                return true;
            });
//        dp.attachEvent("onAfterUpdateFinish",function(){
//            alert("single row updated")
//        });
            dp.defineAction("invalid",function(response){
                var message = response.getAttribute("message");
                gantt.alert(message);
                gantt.message({
                    type: "error",
                    text: response.getAttribute("message")
                });

                //gantt.undo();
                //gantt.refreshData();
                return true;
            })

            //gantt.message("Right click on a header of the Grid");
            gantt.attachEvent("onAfterTaskAdd", function(id,item){
                //any custom logic here
                var taskId = id;
                dp.defineAction("invalid",function(tag){
                    var message = tag.getAttribute("message");
                    //var message = tag.getAttribute("message");
                    gantt.alert(message);
                    gantt.deleteTask(taskId);


                });
            });
        });


    </script>

    <style type="text/css" media="screen">
        html, body {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }

        .weekend {
            background: lightgoldenrodyellow !important;
            color: white;
        }

        .gantt_selected .weekend {
            background: lightgoldenrodyellow !important;
        }

        .controls_bar {
            border-top: 1px solid #bababa;
            border-bottom: 1px solid #bababa;
            clear: both;
            margin-top: 90px;
            height: 28px;
            background: #f1f1f1;
            color: #494949;
            font-family: Arial, sans-serif;
            font-size: 13px;
            padding-left: 10px;
            line-height: 25px
        }
        .status_line{
            background-color: #0ca30a;
        }
        .line1{
            background-color: red;
        }
        .line2{
            background-color: green;
        }
        .line3{
            background-color: grey;
        }
        .line4{
            background: olive;
        }
        .line5{
            background-color: #00a7d0;
        }
        html, body{ height:100%; padding:0px; margin:0px; }

    </style>


@stop
