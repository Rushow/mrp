@extends('layouts.master')
@section('head')

    @parent

@stop
@section('header')
@stop
@section('content')
{{ HTML::style('codebase/dhtmlxgantt.css') }}
{{ HTML::style('codebase/dhtmlxscheduler.css') }}
{{ HTML::script('codebase/dhtmlxgantt.js') }}
{{ HTML::script('codebase/dhtmlxscheduler.js') }}
{{ HTML::script('codebase/ext/dhtmlxscheduler_year_view.js') }}
{{--{{ HTML::script('dhtmlxGantt/codebase/ext/dhtmlxgantt_quick_info.js') }}--}}
{{--{{ HTML::script('dhtmlxGantt/codebase/common/testdata.js') }}--}}
{{ HTML::script('codebase/api.js') }}
{{ HTML::script('codebase/dhtmlxmenu.js') }}
{{ HTML::script('codebase/ext/dhtmlxgantt_marker.js') }}
{{--{{ HTML::script('codebase/ext/dhtmlxgantt_keyboard_navigation.js') }}--}}
{{ HTML::script('codebase/ext/dhtmlxgantt_smart_rendering.js') }}
{{ HTML::script('codebase/ext/dhtmlxgantt_undo.js') }}
{{--{{ HTML::script('codebase/ext/dhtmlxgantt_multiselect.js')}}--}}


<div class="row">

    <div class="col-md-12" style="margin-top: 10px;;text-align: right;">
        <a href="#" onclick='gantt.exportToPDF()'  class="btn btn-primary"> Export PDF</a>
    </div>
    <div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px;text-align: right;">
        <? foreach ($line as $ln){?>
        <span class="line{{$ln->id}} boxes">{{$ln->line_name}}</span>
        <?}?>

    </div>
</div>

<div class="row">
        <div class="col-md-12" id="calender">
            <div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:50%;'>
                <div class="dhx_cal_navline">
                    <div class="dhx_cal_prev_button">&nbsp;</div>
                    <div class="dhx_cal_next_button">&nbsp;</div>
                    <div class="dhx_cal_today_button"></div>
                    <div class="dhx_cal_date"></div>

                    <div class="dhx_cal_tab" name="year_tab" style="left:63px!important;"></div>
                    <div class="dhx_cal_tab" name="month_tab" style="right:140px;"></div>

                </div>
                <div class="dhx_cal_header">
                </div>
                <div class="dhx_cal_data">
                </div>
            </div>
        </div>
        <br>
        <div class="col-md-12">
            <h3 style="text-align: center;"><? if(isset($id)&&($id==0)){ echo "Make Planning";}?></h3>
        </div>
    </div>
<div class="row">
    <div class="col-md-12 topbox">

        <div class="col-md-3">

            <h3> <? if(isset($id)){ if($id!=0){echo "Order No :".$id;}else{echo "";}}else{echo "Order No :ALL";}?></h3>

        </div>
        <div class="col-md-9">

            <?= Form::label('start_date', 'Start Date :', array('class' => 'control-label')); ?>
            {{ Form::text('start_date', '', '', array('class'=>'validate[required] form-inline datepicker','id'=>'start_date')) }}

            <?= Form::label('end_date', 'End Date :', array('class' => 'control-label')); ?>
            {{ Form::text('end_date', '', '', array('class'=>'validate[required] form-inline datepicker','id'=>'end_date')) }}

            <?= Form::label('line_id', 'Line', array('class' => 'control-label')); ?>
            {{ Form::select('line_id', $line_a, '', $attributes =array('class'=>'validate[required] form-inline','id'=>'line_id')) }}

            <a id="checkifavailable" href="#" class=" btn btn btn-warning">Check Available?</a>


        </div>



    </div>
</div>
<div id="gantt_here" style='width:100%; min-height:800px;'></div>
<script type="text/javascript">
    $(document).ready(function() {

        scheduler.templates.event_class = function(start, end, ev){

            switch(ev.line_id) {
                case 1:
                    return "line1";
                    break;
                case 2:
                    return "line2";
                    break;
                case 3:
                    return "line3";
                    break;
                case 4:
                    return "line4";
                    break;
                case 5:
                    return "line5";
                    break;
                case 6:
                    return "line6";
                    break;
                case 7:
                    return "line7";
                    break;
                case 8:
                    return "line8";
                    break;
                default:
                    return "white";
            }
        }

        var dateobject = new Date();
        scheduler.config.multi_day = true;
        scheduler.config.prevent_cache = true;
        scheduler.config.readonly = true;
        //scheduler.locale.labels.year_tab ="Year";

        //the Year view will display only 6 months
        scheduler.config.year_x = 2; //2 months in a row
        scheduler.config.year_y = 6; //3 months in a column
        scheduler.config.show_loading = true;

        //scheduler.parse(events, "json");
        scheduler.setLoadMode("month");
        scheduler.config.show_loading = true;
        scheduler.config.xml_date = "%Y-%m-%d %H:%i";
        scheduler.init('scheduler_here', new Date(dateobject.getFullYear(),dateobject.getMonth(), 1), "month");
        scheduler.load("<?=url('eventdata')?>", "json");
        var dpscheduler = new dataProcessor("<?=url('eventdata')?>");
        dpscheduler.init(scheduler);


    });

    $(document).ready(function() {

        gantt.templates.scale_cell_class = function (date) {
            if (date.getDay() == 6 || date.getDay() == 5) {
                return "weekend";
            }
        };
        gantt.templates.task_cell_class = function (item, date) {
            if (date.getDay() == 6 || date.getDay() == 5) {
                return "weekend";
            }
        };
        gantt.templates.task_class  = function(start, end, task){
            switch (task.line_id){
                case 1:
                    return "line1";
                    break;
                case 2:
                    return "line2";
                    break;
                case 3:
                    return "line3";
                    break;
                case 4:
                    return "line4";
                    break;
                case 5:
                    return "line5";
                    break;
                case 6:
                    return "line6";
                    break;
                case 7:
                    return "line7";
                    break;
                case 8:
                    return "line8";
                    break;
                default:
                    return "white";
            }
        };

        gantt.config.undo = true;
        gantt.config.undo_actions = {
            update:"update",
            remove:"remove", // remove an item from datastore
            add:"add"
        };
        gantt.config.undo_steps = 1;
        gantt.config.undo_types = {
            task:"task"
        };
        //gantt.locale.labels.section_description=["Process Description"];
        //gantt.getLightboxSection('description').setValue('Description');
        gantt.config.details_on_dblclick = true;

        gantt.locale.labels.section_order_id=["Order"];
        gantt.locale.labels.section_article_no = ["Article"];
        gantt.locale.labels.section_quantity = ["Target Quantities"];
        gantt.locale.labels.section_division_id = ["Division"];
        gantt.locale.labels.section_sub_division_id = ["Sub Division"];
        gantt.locale.labels.section_machine_id = ["Machine"];
        gantt.locale.labels.section_line_id = ["Line"];
        gantt.locale.labels.section_complexity_id = ["Complexity"];


        gantt.config.columns = [
            {name: "id", label: "id", width: 10,  align: "left",hide:true },
            {name: "order_id", label: "Order Id", width: 90,  align: "center",resize: true },
            {name: "text", label: "Process", width: 90,align: "center", resize: true },
            {name: "start_date", label: "Start time", template: function (obj) {
                return gantt.templates.date_grid(obj.start_date);
            }, align: "center", width: 90, resize: true },
            {name: "duration", label: "Duration", align: "center", width: 60, resize: true},

            {name: "add", label: "", width: 30,align: "right", resize: true }
        ];

        gantt.config.grid_resize = true;
        gantt.config.grid_width = 600;

        gantt.config.scale_height = 60;
        gantt.config.subscales = [
            { unit: "week", step: 1, date: "Week #%W"}
        ];
        gantt.config.xml_date = "%Y-%m-%d %H:%i:%s";
        gantt.config.step = 1;
        gantt.config.scale_unit= "day";

        var date = new Date(), y = date.getFullYear(), m = date.getMonth();

        <? if(isset($planning)){

            $start = "";
            $end = "";
            foreach ($planning as $p){
                $start =$p->start;
                $end = $p->end;
            }
         }
        ?>
        <? if(isset($id)&&($id!=0)) { ?>

            gantt.init("gantt_here", new Date('<?=$start?>'), new Date('<?=$end?>'));
            gantt.load("{{url('gantt_data/'.$id)}}", "json");
            var dp = new gantt.dataProcessor("{{url('gantt_data/'.$id)}}");

        <? }else if(isset($id)&&($id==0)){ ?>
            gantt.init("gantt_here", new Date(date.getFullYear(),date.getMonth(),1), new Date(date.getFullYear(),date.getMonth()+3,1));
            gantt.load("{{url('gantt_data/'.$id)}}", "json");
            var dp = new gantt.dataProcessor("{{url('gantt_data/')}}");
        <? }else{ ?>
            gantt.init("gantt_here", new Date('<?=$start?>'), new Date('<?=$end?>'));
            gantt.load("{{url('gantt_data/')}}", "json");
            var dp = new gantt.dataProcessor("{{url('gantt_data/')}}");
        <? } ?>

        dp.init(gantt);
        dp.setTransactionMode("REST");
        //dp.setTransactionMode("POST", false);
        gantt.attachEvent("onTaskLoading", function (task) {
            //task.order_id = 'order_id';

            return true;
        });



        gantt.attachEvent("onTaskCreated", function(task){
            //any custom logic here

            return true;
        });
        var opts = [<? foreach ($order as $o) {?>{ key:<?=$o->order_no?>, label: '<?=$o->order_no?>' },<? } ?>
        ];

        var opts_art = [<? foreach ($article as $o) {?>{ key:'<?=$o->article_no?>', label: '<?=$o->article_no?>' },<? } ?>
        ];

        var opts_div = [<? foreach ($division as $div) {?>{ key:<?=$div->id?>, label: '<?=$div->name?>' },<? } ?>
        ];

        var opts_sub_div = [<? foreach ($subdivision as $sdiv) {?>{ key:<?=$sdiv->id?>, label: '<?=$sdiv->subdivision_name?>' },<? } ?>
        ];

        var opts_machine = [<? foreach ($machine as $m) {?>{ key:<?=$m->id?>, label: '<?=$m->machine_id?>' },<? } ?>
        ];

        var opts_style = [<? foreach ($line as $m) {?>{ key:<?=$m->id?>, label: '<?=$m->line_name?>' },<? } ?>
        ];
        var opts_complexity = [<? foreach ($complexity as $m) {?>{ key:<?=$m->id?>, label: '<?=$m->complexity?>' },<? } ?>
        ];


        gantt.config.lightbox.sections = [
            {name:"description", height:38, map_to:"text", type:"textarea", focus:true,default_value:"Low"},
            {name:"order_id",    height:38, map_to:"order_id", type:"select", options:opts},
            {name:"article_no",    height:38, map_to:"article_no", type:"select", options:opts_art},
            {name:"division_id",    height:38, map_to:"division_id", type:"select", options:opts_div},
            {name:"sub_division_id",    height:38, map_to:"sub_division_id", type:"select", options:opts_sub_div},
            {name:"line_id",    height:38, map_to:"line_id", type:"select", options:opts_style},
            {name:"complexity_id",    height:38, map_to:"complexity_id", type:"select", options:opts_complexity},
            {name:"quantity",    height:38, map_to:"quantity", type:"textarea", default_value:"100"},
            {name:"time",        height:72, map_to:"auto", type:"duration"}
        ];



        dp.attachEvent("onBeforeDataSending", function(id, state, data){
            //console.log(data);
            return true;
        });

        gantt.attachEvent("onBeforeTaskUpdate", function(id,item){
            //console.log(item);
//            var taskId = gantt.addTask({
//                id:item.id,
//                text:item.text,
//                division_id:item.division_name,
//                sub_division_id:item.sub_division_id,
//                article_no:item.article_no,
//                start_date:item.start_date,
//                end_date:item.end_date,
//                duration:item.duration,
//                parent:item.parent,
//            });
//
//            gantt.updateTask(taskId);
            return true;
        });
        //prevent moving to another sub-branch:
        gantt.attachEvent("onAfterTaskDrag", function(id,item, mode, e){
            gantt.message("Checking if date is available ...");

            return true;
        });
//        dp.attachEvent("onAfterUpdateFinish",function(){
//            alert("single row updated")
//        });
        dp.defineAction("invalid",function(response){
            var message = response.getAttribute("message");
            gantt.alert(message);
            gantt.message({
                type: "error",
                text: response.getAttribute("message")
            });

            //gantt.undo();
            //gantt.refreshData();
            return true;
        })

        //gantt.message("Right click on a header of the Grid");
        gantt.attachEvent("onAfterTaskAdd", function(id,item){
            //any custom logic here
            var taskId = id;
            dp.defineAction("invalid",function(tag){
                var message = tag.getAttribute("message");
                //var message = tag.getAttribute("message");
                gantt.alert(message);
                gantt.deleteTask(taskId);


            });
            scheduler.setCurrentView();
        });
    });
    $(function() {


        $("#start_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'yyyy-mm-dd'
        });
        $("#end_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'yyyy-mm-dd'
        });

    });

    $( "#checkifavailable" ).bind( "click", function() {

        var start = $("#start_date").val();
        var end = $("#start_date").val();
        var line = $("#line_id").val();//alert(subdivison);
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
        $.ajax({
            method: "POST",
            dataType: 'json',
            url: "{{url('checkifavailable')}}",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({start_date: start, end_date: end,line_id:line}),
            async: true,
            success: function (data) {
                //alert("Data : " + data.start);
                gantt.alert(data.message);

            }

        });


    });



</script>
<style>
    .line1{
        background-color: palevioletred;
    }
    .line2{
        background-color: lightseagreen;
    }
    .line3{
        background-color: grey;
    }
    .line4{
        background: lightsalmon;
    }
    .line5{
        background-color: #00a7d0;
    }
    .line6{
        background-color: lavender;
    }
    .line7{
        background-color: darkcyan;
    }
    .line8{
        background-color: violet;
    }
</style>



@stop
