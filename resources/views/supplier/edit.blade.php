@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">
    <div class="col-md-6 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Update Supplier</h2>
            <?= Form::open(array('url' => 'supplier/'.$supplier->id, 'method' => 'put', 'id' => 'supplierForm')); ?>
            <div class="form-group">
                <?= Form::label('supplier_name', 'Supplier Name', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::text('supplier_name', $supplier->supplier_name, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="widget">
                <h2>Address</h2>
                <div class="form-group">
                    <?= Form::label('floor', 'Floor', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?= Form::text('floor', $address['floor'], array('class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Form::label('street_address', 'Street Address', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?= Form::textarea('street_address', $address['street_address'], array('class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Form::label('city', 'City', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?= Form::text('city', $address['city'], array('class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Form::label('state', 'State', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?= Form::text('state', $address['state'], array('class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Form::label('zip', 'Zip', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?= Form::text('zip', $address['zip'], array('class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Form::label('country', 'Country', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?= Form::text('country', $address['country'], array('class' => 'form-control')); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?= Form::label('remarks', 'Remarks', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::textarea('remarks', $supplier->remarks, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Submit', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#supplierForm").validate({
            rules: {
                supplier_name: "required",
                supplier_location: "required",
                remarks: "required"
            },
            messages: {
                supplier_name: "Please enter Supplier Name",
                supplier_location : "Please enter Supplier Location",
                remarks : "Please enter Remarks"
            }
        });
    });
</script>

@stop