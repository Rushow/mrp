<html lang="en">
<head>
    <meta charset="utf-8">
    <title><? if(isset($pagetitle)){echo $pagetitle;}?> Fortuna MRP</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

        {{ HTML::style('css/style.css?t=1') }}
        {{ HTML::style('http://fonts.googleapis.com/css?family=Aguafina+Script') }}
        {{--{{ HTML::style('awesome-admin/lib/bootstrap3/dist/css/bootstrap.css') }}--}}
        {{ HTML::style('awesome-admin/lib/FontAwesome/css/font-awesome.css') }}
        {{--{{ HTML::style('awesome-admin/stylesheets/theme.css') }}--}}
        <!-- Select2 -->
        {{ HTML::style('admin-lte/plugins/select2/select2.min.css') }}

        {{ HTML::style('admin-lte/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') }}
        {{ HTML::style('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') }}
        {{ HTML::style('admin-lte/dist/css/AdminLTE.min.css') }}
        {{ HTML::style('admin-lte/dist/css/skins/_all-skins.min.css?t=1') }}
        {{ HTML::style('admin-lte/plugins/iCheck/flat/blue.css') }}

        {{ HTML::style('facebox/facebox.css') }}
        {{ HTML::style('bootstrap-multiselect/css/bootstrap-multiselect.css') }}
        {{ HTML::style('js/jquery-ui-1.11.0/jquery-ui.css') }}
        {{ HTML::style('js/jquery-tags-input/jquery.tagsinput.css') }}
        {{ HTML::style('admin-lte/plugins/datepicker/datepicker3.css') }}

        {{ HTML::script('admin-lte/plugins/jQuery/jQuery-2.1.4.min.js') }}
        {{ HTML::script('js/jquery-validation-1.13.0/dist/jquery.validate.js') }}
        {{ HTML::script('js/script.js') }}
        {{ HTML::script('bootstrap-multiselect/js/bootstrap-multiselect.js') }}
        {{ HTML::script('facebox/facebox.js') }}

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        {{ HTML::script('https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js') }}
        {{ HTML::script('https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}
        <![endif]-->


    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('a[rel*=facebox]').facebox({
                loadingImage : 'facebox/loading.gif',
                closeImage   : 'facebox/closelabel.png'
            });
        });
    </script>
    @section('head')
    @show

</head>

<!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
<!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
<!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
<!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body class="skin-blue sidebar-mini">
<!--<![endif]-->


<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?= url('/dashboard')?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>MRP</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Fortuna</b>MRP</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ asset('admin-lte/dist/img/user1-128x128.jpg') }}" class="user-image" alt="User Image" />
                            <span class="hidden-xs"><?=Auth::user()->first_name.' '.Auth::user()->last_name ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{ asset('admin-lte/dist/img/user1-128x128.jpg') }}" class="img-circle" alt="User Image" />
                                <p>
                                    <?=Auth::user()->first_name.' '.Auth::user()->last_name ?>
                                    <small>Member since <?=date('M, Y', strtotime(Auth::user()->created_at))?></small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?= url('logout')?>" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ asset('admin-lte/dist/img/user1-128x128.jpg') }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?=Auth::user()->first_name.' '.Auth::user()->last_name ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search..." />
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                {{--<li class="header">MAIN NAVIGATION</li>--}}
                <li class="active treeview">
                    <a href="<?= url('/dashboard')?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i>
                        <span>Merchandising</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= url('article')?>"><i class="fa fa-circle-o"></i>Article Info</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i>Costing</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i>Quotation</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i>Production Instruction</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i>Purchase Requisition</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i>Cost Analysis</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i>Purchase Analysis</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>Production Order</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>

                    <ul class="treeview-menu">
                        <li><a href="<?= url('productionOrder')?>"><i class="fa fa-circle-o"></i>PO Lists</a></li>
                        <li><a href="<?= url('productionOrder/create')?>"><i class="fa fa-circle-o"></i>Create PO</a></li>
                        <li><a href="<?= url('productionOrder/orderbreakdownreport')?>"><i class="fa fa-circle-o"></i>Order Breakdown Report</a></li>
                        <li><a href="<?= url('productionOrder/sizeBreakdownProductionReport')?>"><i class="fa fa-circle-o"></i>Size Breakdown Report</a></li>
                        <li><a href="<?= url('productionOrder/overallTodReport')?>"><i class="fa fa-circle-o"></i>TOD Report</a></li>
                        <li><a href="<?= url('productionOrder/seasonalOrderValueReportGroupbyOrder')?>"><i class="fa fa-circle-o"></i>Seasonal Order & Value Report</a></li>
                        <li><a href="<?= url('productionOrder/seasonalConcentrationReport')?>"><i class="fa fa-circle-o"></i>Seasonal Concentration Report</a></li>
                        <li><a href="<?= url('productionOrder/shipmentschedule')?>"><i class="fa fa-circle-o"></i>Shipment Schedule</a></li>
                        <li><a href="<?= url('productionOrder/salescontract')?>"><i class="fa fa-circle-o"></i>Sales Contract</a></li>
                        <li><a href="<?= url('productionOrder/overallproductionreport')?>"><i class="fa fa-circle-o"></i>Overall Production Overview</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-reorder"></i>
                        <span>Daily Production</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>

                    <ul class="treeview-menu">


                        @include('dailyproductionmenu.productionmenu')
                        <li class="treeview">
                            <a href="#"><i class="fa fa-newspaper-o"></i>Admin</a>
                            <ul class="treeview-menu">
                                <li><a href="<?= url('dailyproduction/component')?>"><i class="fa fa-circle-o"></i>Component</a></li>
                                <li><a href="<?= url('dailyproduction/component/create')?>"><i class="fa fa-circle-o"></i>Add Component</a></li>
                                <li><a href="<?= url('dailyproduction/instruction')?>"><i class="fa fa-circle-o"></i>Instruction</a></li>
                                <li><a href="<?= url('dailyproduction/addinstruction')?>"><i class="fa fa-circle-o"></i>Add Instruction</a></li>

                            </ul>
                        </li>
                    </ul>


                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-empire"></i>
                        <span>Planning</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">

                        <li><a href="<?= url('listofplan')?>"><i class="fa fa-circle-o"></i>List of planning by order</a></li>
                        <li><a href="<?= url('allplanning')?>"><i class="fa fa-circle-o"></i>All of planning of all order</a></li>
                        <li><a href="<?= url('makeschedule/create')?>"><i class="fa fa-circle-o"></i>Make Schedule/Planning</a></li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-newspaper-o"></i>Admin</a>
                            <ul class="treeview-menu">
                                <li><a href="<?= url('showdivision')?>"><i class="fa fa-circle-o"></i>Department/Division List</a></li>
                                <li><a href="<?= url('adddivision')?>"><i class="fa fa-circle-o"></i>Add Department/Division</a></li>
                                <li><a href="<?= url('subdivision')?>"><i class="fa fa-circle-o"></i>Sub-Division</a></li>
                                <li><a href="<?= url('subdivision/create')?>"><i class="fa fa-circle-o"></i>Add Sub-Division</a></li>
                                <li><a href="<?= url('machine')?>"><i class="fa fa-circle-o"></i>Machine</a></li>
                                <li><a href="<?= url('machine/create')?>"><i class="fa fa-circle-o"></i>Add Machine</a></li>
                                <li><a href="<?= url('line')?>"><i class="fa fa-circle-o"></i>Line</a></li>
                                <li><a href="<?= url('line/create')?>"><i class="fa fa-circle-o"></i>Add Line</a></li>
                                <li><a href="<?= url('complexity')?>"><i class="fa fa-circle-o"></i>Complexity</a></li>
                                <li><a href="<?= url('complexity/create')?>"><i class="fa fa-circle-o"></i>Add Complexity</a></li>
                                <li><a href="<?= url('capacity')?>"><i class="fa fa-circle-o"></i>Capacity</a></li>
                                <li><a href="<?= url('capacity/create')?>"><i class="fa fa-circle-o"></i>Add Capacity</a></li>

                            </ul>
                        </li>

                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-edit"></i>
                        <span>Sample</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= url('sampleOrder')?>"><i class="fa fa-circle-o"></i>Sample Order</a></li>
                        <li><a href="<?= url('sampleOrderSpec')?>"><i class="fa fa-circle-o"></i>Sample Order Spec</a></li>
                        <li><a href="<?= url('sampleOrderMds')?>"><i class="fa fa-circle-o"></i>Sample Order MDS & TDS</a></li>
                        <li><a href="<?= url('sampleStatus')?>"><i class="fa fa-circle-o"></i>Sample Status</a></li>
                        <li><a href="<?= url('sampleSchedule')?>"><i class="fa fa-circle-o"></i>Sample Schedule</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i>
                        <span>Warehouse</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= url('item/create')?>"><i class="fa fa-circle-o"></i>Add Item</a></li>
                        <li><a href="<?= url('item')?>"><i class="fa fa-circle-o"></i>Item List</a></li>
                        <li><a href="<?= url('storeIn/create')?>"><i class="fa fa-circle-o"></i>Add Store In</a></li>
                        <li><a href="<?= url('storeOut/create')?>"><i class="fa fa-circle-o"></i>Add Store Out</a></li>
                        <li><a href="<?= url('storeIn')?>"><i class="fa fa-circle-o"></i>Store In Report</a></li>
                        <li><a href="<?= url('storeOut')?>"><i class="fa fa-circle-o"></i>Store Out Report</a></li>
                        <li><a href="<?= url('inventory') ?>"><i class="fa fa-circle-o"></i>Inventory Report</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-inbox"></i>
                        <span>Quality</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-cogs"></i>
                        <span>Administration</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= url('additionalPrint')?>"><i class="fa fa-circle-o"></i>Additional Print</a></li>
                        <li><a href="<?= url('additionalStickerInfo')?>"><i class="fa fa-circle-o"></i>Additional Sticker Information</a></li>
                        <li><a href="<?= url('articleType')?>"><i class="fa fa-circle-o"></i>Appearance</a></li>
                        <li><a href="<?= url('blockTtNo')?>"><i class="fa fa-circle-o"></i>Block TT No</a></li>
                        <li><a href="<?= url('bank')?>"><i class="fa fa-circle-o"></i>Bank</a></li>
                        <li><a href="<?= url('buyer')?>"><i class="fa fa-circle-o"></i>Buyer</a></li>
                        @if ((Auth::user()->IsSuperAdmin()))
                            <li><a href="<?= url('buyerTemplate')?>"><i class="fa fa-circle-o"></i>Buyer Template</a></li>
                        @endif
                        <li><a href="<?= url('category')?>"><i class="fa fa-circle-o"></i>Category</a></li>
                        <li><a href="<?= url('color')?>"><i class="fa fa-circle-o"></i>Color</a></li>
                        <li><a href="<?= url('component')?>"><i class="fa fa-circle-o"></i>Component</a></li>
                        <li><a href="<?= url('construction')?>"><i class="fa fa-circle-o"></i>Construction</a></li>
                        <li><a href="<?= url('destination')?>"><i class="fa fa-circle-o"></i>Country</a></li>
                        <li><a href="<?= url('customerGroup')?>"><i class="fa fa-circle-o"></i>Customer Group</a></li>
                        <li><a href="<?= url('department')?>"><i class="fa fa-circle-o"></i>Department</a></li>
                        <li><a href="<?= url('documentType')?>"><i class="fa fa-circle-o"></i>Document Type</a></li>
                        <li><a href="<?= url('forwarder')?>"><i class="fa fa-circle-o"></i>Forwarder</a></li>
                        <li><a href="<?= url('heelNo')?>"><i class="fa fa-circle-o"></i>Heel No</a></li>
                        <li><a href="<?= url('itemGroup')?>"><i class="fa fa-circle-o"></i>Item Group</a></li>
                        <li><a href="<?= url('itemSubGroup')?>"><i class="fa fa-circle-o"></i>Item Sub Group</a></li>
                        <li><a href="<?= url('lastNo')?>"><i class="fa fa-circle-o"></i>Last No</a></li>
                        <li><a href="<?= url('liningPrint')?>"><i class="fa fa-circle-o"></i>Lining Print</a></li>
                        <li><a href="<?= url('outsoleNo')?>"><i class="fa fa-circle-o"></i>Outsole No</a></li>
                        <li><a href="<?= url('parameter')?>"><i class="fa fa-circle-o"></i>Parameter</a></li>
                        <li><a href="<?= url('paymentTerm')?>"><i class="fa fa-circle-o"></i>Payment Term</a></li>
                        <li><a href="<?= url('pictogram')?>"><i class="fa fa-circle-o"></i>Pictogram</a></li>
                        <li><a href="<?= url('polySticker')?>"><i class="fa fa-circle-o"></i>Poly Sticker</a></li>
                        <li><a href="<?= url('pmTag')?>"><i class="fa fa-circle-o"></i>PM Tag</a></li>
                        <li><a href="<?= url('priceTag')?>"><i class="fa fa-circle-o"></i>Price Tag</a></li>
                        <li><a href="<?= url('sampleType')?>"><i class="fa fa-circle-o"></i>Sample Type</a></li>
                        <li><a href="<?= url('season')?>"><i class="fa fa-circle-o"></i>Season</a></li>
                        <li><a href="<?= url('shippingMode')?>"><i class="fa fa-circle-o"></i>Shipping Mode</a></li>
                        <li><a href="<?= url('size')?>"><i class="fa fa-circle-o"></i>Size</a></li>
                        <li><a href="<?= url('sizeGroup')?>"><i class="fa fa-circle-o"></i>Size Group</a></li>
                        <li><a href="<?= url('sockPrint')?>"><i class="fa fa-circle-o"></i>Sock Print</a></li>
                        <li><a href="<?= url('supplier')?>"><i class="fa fa-circle-o"></i>Supplier</a></li>
                        <li><a href="<?= url('termsOfDelivery')?>"><i class="fa fa-circle-o"></i>Terms of Delivery</a></li>
                        <li><a href="<?= url('unit')?>"><i class="fa fa-circle-o"></i>Unit</a></li>
                        @if ((Auth::user()->IsSuperAdmin()))
                            <li><a href="<?= url('registration')?>"><i class="fa fa-circle-o"></i>User</a></li>
                            <li><a href="<?= url('userType')?>"><i class="fa fa-circle-o"></i>User Type</a></li>
                            <li><a href="<?= url('import')?>"><i class="fa fa-circle-o"></i>Import</a></li>
                        @endif
                    </ul>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>



<div class="content-wrapper">
    @yield('content')

</div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.2.0
        </div>
        <strong>Copyright &copy 2013-2014 <a href="http://www.bluebd.com">Infrablue Technology</a>.</strong> All rights reserved.
    </footer>
{{--<footer>--}}
    {{--<hr>--}}
    {{--<p>&copy 2013 <a href="http://www.bluebd.com">Infrablue Technology</a></p>--}}
{{--</footer>--}}
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
            </div><!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                    <h3 class="control-sidebar-heading">General Settings</h3>

            </div><!-- /.tab-pane -->
        </div>
    </aside><!-- /.control-sidebar -->

</div>



{{ HTML::script('https://code.jquery.com/ui/1.11.4/jquery-ui.min.js') }}
{{ HTML::script('js/jquery-ui-1.11.0/jquery-ui.js') }}
<script type="text/javascript">
    $.widget.bridge('uibutton', $.ui.button)
</script>
{{ HTML::script('admin-lte/bootstrap/js/bootstrap.min.js') }}
{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') }}
{{--{{ HTML::script('admin-lte/plugins/morris/morris.min.js') }}--}}
{{ HTML::script('admin-lte/plugins/sparkline/jquery.sparkline.min.js') }}
{{ HTML::script('admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}
{{ HTML::script('admin-lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}
{{ HTML::script('admin-lte/plugins/knob/jquery.knob.js') }}
{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js') }}
{{ HTML::script('admin-lte/plugins/daterangepicker/daterangepicker.js') }}
{{ HTML::script('admin-lte/plugins/datepicker/bootstrap-datepicker.js') }}
{{ HTML::script('admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}
{{ HTML::script('admin-lte/plugins/slimScroll/jquery.slimscroll.min.js') }}
{{ HTML::script('admin-lte/plugins/fastclick/fastclick.min.js') }}
{{ HTML::script('admin-lte/dist/js/app.min.js') }}
{{ HTML::script('admin-lte/dist/js/pages/dashboard.js') }}
{{ HTML::script('js/jquery-tags-input/jquery.tagsinput.js') }}

@yield('page-script')
@yield('plugin-script')

</body>
</html>


