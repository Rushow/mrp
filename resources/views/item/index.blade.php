@extends('layouts.master')
@section('head')

@parent
{{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')

<?
    $item_code_val = (isset($post_val['item_code'])) ? $post_val['item_code'] : null;
    $group_val = (isset($post_val['group_id'])) ? $post_val['group_id'] : null;
    $sub_group_val = (isset($post_val['sub_group_id'])) ? $post_val['sub_group_id'] : null;
    $parameter_val = (isset($post_val['parameter_id'])) ? $post_val['parameter_id'] : null;
    $item_name_val = (isset($post_val['item_name'])) ? $post_val['item_name'] : null;
    $color_val = (isset($post_val['color_id'])) ? $post_val['color_id'] : null;
?>


<div class="row">


    <div class="col-md-12 sidebar">

        <?php if(Session::has('message')){ ?>
        <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
        <script>
            $('#alert').delay(2000).fadeOut(400)
        </script>
        <?php } ?>
        <data class="row">
            <div class="col-sm-8">
                @if (!Auth::user()->isViewer())
                    <a href="<?= URL::to('item/create')?>" class="btn btn-warning">Create Item</a>
                @endif
            </div>
            <div class="col-sm-4">
                <a href="<?= URL::to('excel/item')?>" class="btn btn-primary">Download excel file</a>
                <a href="<?= URL::to('pdf/item')?>" class="btn btn-info">Download pdf file</a>
            </div>
        </data>
        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <!-- form start -->
                        <?= Form::open(array('url' => 'item/search', 'method' => 'item/search')); ?>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('item_code', 'Go to Code', array('class' => 'control-label')); ?>
                                    {{ Form::select('item_code', $item_code, $item_code_val, $attributes = array('id'=>'item_code','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('group_id', 'Material Group', array('class' => 'control-label')); ?>
                                    {{ Form::select('group_id', $item_group, $group_val, $attributes = array('id'=>'group_id','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'generateSubGroup(this);'))}}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('sub_group_id', 'Sub Group', array('class' => 'control-label')); ?>
                                    {{ Form::select('sub_group_id', $item_sub_group, $sub_group_val, $attributes = array('id'=>'sub_group_id','data-rel'=>'chosen', 'class'=>'form-control', 'readonly'=>'readonly')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('parameter_id', 'Parameter', array('class' => 'control-label')); ?>
                                    {{ Form::select('parameter_id', $parameter, $parameter_val, $attributes = array('id'=>'parameter_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('item_name', 'Material Name', array('class' => 'control-label')); ?>
                                    {{ Form::select('item_name', $item_name, $item_name_val, $attributes = array('id'=>'item_name','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('color_id', 'Color', array('class' => 'control-label')); ?>
                                    {{ Form::select('color_id', $color, $color_val, $attributes = array('id'=>'color_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-2 col-md-offset-11">
                                <button type="submit" class="btn btn-success">Search</button>
                            </div>

                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Item List
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Item List</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Material Code<span class="sort-icon"><span></th>
                                    <th>Material Name<span class="sort-icon"><span></th>
                                    <th>Group<span class="sort-icon"><span></th>
                                    <th>Sub Group<span class="sort-icon"><span></th>
                                    <th>Parameter<span class="sort-icon"><span></th>
                                    <th>Color<span class="sort-icon"><span></th>
                                    <th>Store Unit<span class="sort-icon"><span></th>
                                    <th>Purchase Unit<span class="sort-icon"><span></th>
                                    <th>Note<span class="sort-icon"><span></th>
                                    @if (!Auth::user()->isViewer())
                                        <th>Action</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($items as $key => $value){ ?>
                                <tr>
                                    <td><?= $value->item_code ?></td>
                                    <td><?= $value->item_name ?></td>
                                    <td><?= $value->group_name ?></td>
                                    <td><?= $value->sub_group_name ?></td>
                                    <td><?= $value->parameter_name ?></td>
                                    <td><?= $value->color_name ?></td>
                                    <td><?= $value->StoreUnit->unit_name ?></td>
                                    <td><?= $value->PurchaseUnit->unit_name ?></td>
                                    <td><?= $value->remarks ?></td>
                                    @if (!Auth::user()->isViewer())
                                        <td>
                                            <a class="btn btn-info btn-sm" href="<?= URL::to('item/'.$value->id).'/edit';?>">Edit</a>
                                            {{ Form::open(array('class' => 'btn btn-danger btn-xs','url' => 'item/'.$value->id, 'method' => 'delete', 'style' => 'padding: 3px 0; border-color: transparent !important;')) }}
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs', 'style' => 'display:block;')) }}
                                            {{ Form::close() }}
                                        </td>
                                    @endif
                                </tr>
                                <?php } ?>

                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
        </section>


    </div>
</div>

@stop

@section('page-script')
    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js')}}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js')}}
@stop

@section('plugin-script')
    <script type="text/javascript">
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });

    function generateSubGroup(item){
        var val = $(item).val();
        if(val!=""){
        console.log(val);            
            $.ajax({                
                type: "POST",
                dataType: 'json',
                url: '<?= Url("item/subGroup") ?>',
                data: 'group_id='+val,
                success: function(data){
                    $("#sub_group_id").removeAttr('readonly');
                    $("#sub_group_id").removeAttr('disabled');
                    $("#sub_group_id").html(data.sub_group);

                }
            });
        }
        else{
            $("#sub_group_id").html('<option value="">Select</option>');
            $("#sub_group_id").attr('readonly', 'readonly');
            $("#sub_group_id").attr('disabled', 'disabled');
        }
    }
</script>
@stop