@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-10 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message){
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Add Item</h2>
            <?= Form::open(array('url' => 'item', 'method' => 'post','files'=>true)); ?>
            <div class="col-md-4 sidebar" style="margin-right: 150px;">
                <div class="form-group">
                    <?= Form::label('item_id', 'Material ID', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('item_id',$item_id,array('class'=>'form-control','id'=>'item_id', 'readonly'=>'readonly')) }}
                    </div>
                </div>
                <div class="form-group">
                    <?= Form::label('item_name', 'Material Name', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('item_name','',array('class'=>'validate[required] form-control','id'=>'item_name')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('group_id', 'Material Group', array('class' => 'validate[required] control-label')); ?>
                    <div class="controls">
                        {{ Form::select('group_id', $item_group, null, $attributes = array('id'=>'group_id','data-rel'=>'chosen', 'class'=>'form-control', 'onChange'=>'generateSubGroup(this);'))}}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('sub_group_id', 'Sub Group', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::select('sub_group_id', $item_sub_group, null, $attributes = array('id'=>'sub_group_id','data-rel'=>'chosen', 'class'=>'validate[required] form-control', 'disabled'=>'disabled')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('parameter_id', 'Parameter', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::select('parameter_id', $parameter, null, $attributes = array('id'=>'parameter_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('color_id', 'Color', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::select('color_id', $color, null, $attributes = array('id'=>'color_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group">
                    <?= Form::label('remarks', 'Remarks', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::textarea('remarks','',array('class'=>'form-control','id'=>'remarks')) }}
                    </div>
                </div>
            </div>
            <div class="col-md-6 sidebar">
                <div class="form-group">
                    <?= Form::label('item_code', 'Material Code', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('item_code',$item_code,array('class'=>'form-control','id'=>'item_code', 'readonly'=>'readonly')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('construction_id', 'Cutability Factor', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::label('cutability_yes','Yes') }}
                        {{ Form::radio('cutability_factor','1','',array('id'=>'cutability_yes', 'onclick'=>'cutabilityChange(this);')) }}
                        {{ Form::label('cutability_no','No') }}
                        {{ Form::radio('cutability_factor','0','',array('id'=>'cutability_no', 'onclick'=>'cutabilityChange(this);')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('cutable_width', 'Cutable Width', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('cutable_width', null,array('class'=>'form-control left','id'=>'cutable_width', 'style' => 'width: 100px;')) }}
                        {{ Form::select('cutable_width_unit', $unit, null, array('id'=>'cutable_width_unit','data-rel'=>'chosen', 'class'=>'form-control left', 'style' => 'width: 150px; margin-left: 15px')) }}
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('store_unit', 'Store Unit', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::select('store_unit', $unit, null, array('id'=>'store_unit','data-rel'=>'chosen', 'class'=>'validate[required] form-control', 'onChange'=>'storeUnitChange();')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('purchase_unit', 'Purchase Unit', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::select('purchase_unit', $unit, null, array('id'=>'purchase_unit','data-rel'=>'chosen', 'class'=>'validate[required] form-control', 'onChange'=>'purchaseUnitChange();')) }}
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('', 'Unit Conversion', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::text('store_conversion',$item_code, array('class'=>'form-control left','id'=>'store_conversion', 'style' => 'width: 100px;')) }}
                        <?= Form::label('', 'Unit Conversion', array('class' => 'control-label left', 'style' => 'margin-left: 8px;', 'id' => 'unit_conversion_store_label')); ?>
                        <?= Form::label('', '=', array('class' => 'control-label left', 'style' => 'margin-left: 8px;')); ?>
                        {{ Form::text('purchase_conversion',$item_code, array('class'=>'form-control left','id'=>'purchase_conversion', 'style' => 'width: 100px; margin-left: 8px;')) }}
                        <?= Form::label('', 'Unit Conversion', array('class' => 'control-label left', 'style' => 'margin-left: 8px;', 'id' => 'unit_conversion_purchase_label')); ?>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="control-group">
                    <?= Form::label('item_pic', 'Picture', array('class' => 'control-label')); ?>
                    <div class="controls">
                        {{ Form::file('item_pic', array('class'=>'', 'id'=>'item_pic')) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <?= Form::submit('Add Item', array('class' => 'btn btn-success')); ?>
                    </div>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>

    $(document).ready(function(){
        $('#cutability_yes').prop('checked', true);
        $('#cutability_yes').prop('checked', true);
        $("#store_unit").val("2");
        $("#purchase_unit").val("3");
        storeUnitChange();
        purchaseUnitChange();

        $("#articleForm").validate({
            rules: {
                group_id: "required",
                sub_group_id: "required",
                cutability_factor: "required",
                store_unit: "required",
                purchase_unit: "required"
            },
            messages: {
                group_id: "Please select a group!",
                sub_group_id: "Please select a sub-group!",
                cutability_factor: "Please provide the cutability factor!",
                store_unit: "Please select a store unit!",
                purchase_unit: "Please select a purchase unit!",
            }
        });
    });

    function cutabilityChange(item){
        var item_id = $(item).attr('id');
        if(item_id == 'cutability_no'){
            $('#cutable_width').attr("disabled", "disabled");
            $('#cutable_width_unit').attr("disabled", "disabled");
        }
        else if(item_id == 'cutability_yes'){
            $('#cutable_width').removeAttr("disabled");
            $('#cutable_width_unit').removeAttr("disabled");
        }
    }

    function storeUnitChange(){
        var val = $( "#store_unit option:selected" ).text();
        $('#unit_conversion_store_label').html(val);
    }

    function purchaseUnitChange(){
        var val = $( "#purchase_unit option:selected" ).text();
        $('#unit_conversion_purchase_label').html(val);
    }
    function generateSubGroup(item){
        var val = $(item).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("item/subGroup") ?>',
                data: 'group_id='+val,
                success: function(data){
                    $("#sub_group_id").removeAttr('readonly');
                    $("#sub_group_id").removeAttr('disabled');
                    $("#sub_group_id").html(data.sub_group);
                }
            });
        }
        else{
            $("#sub_group_id").html('<option value="">Select</option>');
            $("#sub_group_id").attr('readonly', 'readonly');
            $("#sub_group_id").attr('disabled', 'disabled');
        }
    }
</script>
@stop