@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')


<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Update Forwarder
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Update Forwarder</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'forwarder/'.$forwarder->id, 'method' => 'put', 'id' => 'forwarderForm')); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <?= Form::label('forwarder_name', 'Forwarder Name', array('class' => 'control-label')); ?>
                                <?= Form::text('forwarder_name', $forwarder->forwarder_name, array('class' => 'form-control')); ?>
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#forwarderForm").validate({
            rules: {
                forwarder_name: "required"
            },
            messages: {
                forwarder_name: "Please enter Forwarder Name"
            }
        });
    });
</script>
@stop