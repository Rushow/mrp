@extends('layouts.master')
@section('head')

    @parent
    {{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')


    <div class="row">
        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">

                <?php if(Session::has('message')){ ?>
                <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
                <script>
                    $('#alert').delay(2000).fadeOut(400)
                </script>
                <?php } ?>

                <h1>
                    Import List
                </h1>
                <br>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-success">
                            <div class="box-header">
                                <h3 class="box-title">Import List</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Import<span class="sort-icon"><span></th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Appearance</td>
                                        <td>
                                            <a class="btn btn-info btn-sm" href="<?= URL::to('importAppearance') ?>">Import Appearance</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Buyer</td>
                                        <td>
                                            <a class="btn btn-info btn-sm" href="<?= URL::to('importBuyer') ?>">Import Buyer</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Color</td>
                                        <td>
                                            <a class="btn btn-info btn-sm" href="<?= URL::to('importColor') ?>">Import Color</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Construction</td>
                                        <td>
                                            <a class="btn btn-info btn-sm" href="<?= URL::to('importConstruction') ?>">Import Construction</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Customer Group</td>
                                        <td>
                                            <a class="btn btn-info btn-sm" href="<?= URL::to('importCustomerGroup') ?>">Import Customer Group</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Destination</td>
                                        <td>
                                            <a class="btn btn-info btn-sm" href="<?= URL::to('importDestination') ?>">Import Destination</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Season</td>
                                        <td>
                                            <a class="btn btn-info btn-sm" href="<?= URL::to('importSeason') ?>">Import Season</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>


        </div>
    </div>

@stop

@section('page-script')
    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}
@stop

@section('plugin-script')
    <script type="text/javascript">
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@stop