@extends('layouts.master')
@section('head')

    @parent

@stop
@section('header')
@stop
@section('content')

    <div class="row">

        <div class="col-md-12 sidebar">
            <?php
            $error_messages = Session::get('error_messages');

            if(isset($error_messages)){
                foreach($error_messages as $message)
                {
                    echo $message;
                }
            }
            ?>
            <?php if(Session::has('message')){ ?>
                <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
                <script>
                    $('#alert').delay(2000).fadeOut(400)
                </script>
            <?php } ?>
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Import Color
                </h1>
            </section>
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Import Color</h3>
                            </div><!-- /.box-header -->
                            <!-- form start -->
                            <?= Form::open(array('url' => 'importColorSave', 'id' => 'importColorForm', 'method' => 'post','files'=>true)); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <?= Form::label('import_file', 'Import Color', array('class' => 'control-label')); ?>
                                    {{ Form::file('import_file',array('class'=>'','id'=>'import_file', 'accept'=>'excel/*')) }}
                                </div>
                            </div><!-- /.box-body -->

                            <div class="box-footer">
                                <div class="col-md-4 col-md-offset-10">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                    <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                                </div>
                            </div>
                            <?= Form::close(); ?>
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </div>



    <style>
        .sidebar{
            padding-bottom: 20px;
        }
        form{
            margin: 15px;
        }
    </style>

    <script>
        $(document).ready(function() {
            $("#importColorForm").validate({
                rules: {
                    import_file: {
                        required: true,
                        extension: "xls|xlsx|csv"
                    }
                },
                messages: {
                    import_file: "The file must be a type of: xls, xlsx, csv."
                }
            });
        });
    </script>
@stop