@extends('layouts.master')
@section('head')

    @parent

@stop
@section('header')
@stop
@section('content')



    <div class="row">

        <div class="col-md-12 sidebar">
            <?php
            $error_messages = Session::get('error_messages');

            if(isset($error_messages)){
                foreach($error_messages as $message){
                    echo $message;
                }
            }


            ?>
            <div class="widget">
                <h2 class="topheadertext"> Add Department/Division :</h2>
                <?= Form::open(array('url' => 'adddivision', 'method' => 'post', 'id' => 'addDivisionForm')); ?>
                <div class="col-md-4 sidebar" style="margin-right: 150px;">
                    <div class="form-group">
                        <?= Form::label('division_name', 'Division Name ', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('division_name','',array('class'=>'validate[required] form-control','id'=>'division_name')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('description', 'Description', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('description', '', array('id'=>'description', 'class'=>'validate[required] form-control')) }}
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="controls">
                            <?= Form::submit('Save', array('class' => 'btn btn-success')); ?>
                        </div>
                    </div>
                </div>


                <?= Form::close(); ?>
            </div>
        </div>
    </div>



    <style>
        .sidebar{
            padding-bottom: 20px;
        }
        form{
            margin: 15px;
        }

        .widget{
            overflow: visible !important;
        }
    </style>
    <script>
        $(document).ready(function() {
            $("#addDivisionForm").validate({
                rules: {
                    division_name: "required",
                    description: "required",

                }
            });

            $.validator.addClassRules("Required", {
                required: true
            });

        });
    </script>

@stop