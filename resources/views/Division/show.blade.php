@extends('layouts.master')
@section('head')

    @parent
    {{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')

    <div class="row">

        <div class="col-md-12 sidebar">
            <?php
                        $error_messages = Session::get('error_messages');

                        if(isset($error_messages)){
                            foreach($error_messages as $message){
                                echo $message;
                            }
                        }

            ?>
                    <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Division List
                </h1>
                <data class="row">
                    <div class="col-sm-8">
                        @if (!Auth::user()->isViewer())
                            <a href="<?=URL::to('adddivision/')?>" class="btn btn-warning btn-sm">Create Division</a>

                        @endif
                    </div>
                </data>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Department/Division List</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">

                                    <div class="col-md-12">
                                        <table class="table table-first-column-number data-table display" id="example1">
                                            <thead>
                                            <tr>
                                                <th>Division No.</th>
                                                <th class="col-md-2">Name</th>
                                                <th class="col-md-1">Description</th>
                                                <th class="col-md-1">Sub-Division</th>
                                                <th class="col-md-1">Sub-Division List</th>
                                                <th class="col-md-2">Action</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?
                                            $total = 0;
                                            foreach($division as $key => $value){
                                            ?>
                                            <tr>

                                                <td><?=$value->id?></td>
                                                <td><?=$value->name?></td>
                                                <td><?=$value->description?></td>
                                                <td><?=$value->subdivision->count()

                                                    ?></td>
                                                <td>
                                                    @foreach($value->subdivision as $subname)
                                                        {{ $subname->subdivision_name }}
                                                    @endforeach
                                                </td>

                                                <td>

                                                    <a class="btn btn-info btn-sm" href="<?=URL::to('editdivision/'.$value->id)?>">Edit</a>
                                                    <a class="btn btn-danger btn-sm" href="<?=URL::to('deletedivision/'.$value->id)?>">Delete</a>

                                                </td>


                                            </tr>
                                            <? } ?>

                                            </tbody>
                                        </table>
                                    </div>

                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>




        </div>







@stop
@section('page-script')
        <!-- Select2 -->
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js') }}

    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}
@stop

@section('plugin-script')
            <script type="text/javascript">
                $(function () {
                    $('#example1').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false
                    });

                });
            </script>

@stop