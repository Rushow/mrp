@extends('layouts.master')
@section('head')

    @parent

@stop
@section('header')
@stop
@section('content')


    <div class="row">

        <div class="col-md-12 sidebar">
            <?php
            $error_messages = Session::get('error_messages');

            if(isset($error_messages)){
                foreach($error_messages as $message){
                    echo $message;
                }
            }


            ?>
            <div class="widget">
                <h2 class="topheadertext"> Add Machine :</h2>
                <?= Form::open(array('url' => 'machine', 'method' => 'post', 'id' => 'addMachineForm')); ?>
                <div class="col-md-4 sidebar" style="margin-right: 150px;">
                    <div class="form-group">
                        <?= Form::label('machine_id', 'Machine ID', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('machine_id','',array('class'=>'validate[required] form-control','id'=>'machine_id'))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('machine_name', 'Machine Name ', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('machine_name','',array('class'=>'validate[required] form-control','id'=>'machine_name')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('department_id', 'Department/Division', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('department_id', $division, '', $attributes =array('class'=>'validate[required] form-control','id'=>'department_id'))}}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('sub_division_id', 'Sub-Department/Sub-Division', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('sub_division_id', $subdivision, '', $attributes =array('class'=>'validate[required] form-control','id'=>'sub_division_id'))}}

                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="controls">
                            <?= Form::submit('Save', array('class' => 'btn btn-success')); ?>
                        </div>
                    </div>
                </div>


                <?= Form::close(); ?>
            </div>
        </div>
    </div>



    <style>
        .sidebar{
            padding-bottom: 20px;
        }
        form{
            margin: 15px;
        }
    </style>

    <script>


        $(document).ready(function() {
            $("#addMachineForm").validate({
                rules: {
                    machine_id:"required",
                    machine_name: "required",
                    department_id:"required",
                    sub_division_id: "required",

                }
            });

            $.validator.addClassRules("Required", {
                required: true
            });

        });

    </script>


    <style>
        .widget{
            overflow: visible !important;
        }
    </style>

@stop