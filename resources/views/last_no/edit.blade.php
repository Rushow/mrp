@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">
    <div class="col-md-6 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Update Last No</h2>
            <?= Form::open(array('url' => 'lastNo/'.$last_no->id, 'method' => 'put', 'id' => 'lastNoForm')); ?>
            <div class="form-group">
                <?= Form::label('last_no', 'Last No', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::text('last_no', $last_no->last_no, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Submit', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#lastNoForm").validate({
            rules: {
                last_no: "required"
            },
            messages: {
                last_no: "Please enter Last No"
            }
        });
    });
</script>

@stop