@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Terms Of Delivery
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Terms Of Delivery</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'termsOfDelivery', 'method' => 'post', 'id' => 'termsOfDeliveryForm')); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <?= Form::label('terms_of_delivery', 'Terms Of Delivery', array('class' => 'control-label')); ?>
                                    {{ Form::text('terms_of_delivery','',array('class'=>'validate[required] form-control','id'=>'terms_of_delivery', 'placeholder'=>'Terms Of Delivery')) }}
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="submit" class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</button>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>



<script>
    $(document).ready(function() {
        $("#termsOfDeliveryForm").validate({
            rules: {
                terms_of_delivery: "required"
            },
            messages: {
                terms_of_delivery: "Please enter Terms Of Delivery"
            }
        });
    });
</script>
@stop