@extends('layouts.master')
@section('head')

@parent
{{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css')}}
@stop
@section('header')
@stop
@section('content')

<?php
    $article_no_val = (isset($post_val['article_no'])) ? $post_val['article_no'] : null;
    $article_type_val = (isset($post_val['article_type_id'])) ? $post_val['article_type_id'] : null;
    $construction_val = (isset($post_val['construction_id'])) ? $post_val['construction_id'] : null;
    $customer_group_val = (isset($post_val['customer_group_id'])) ? $post_val['customer_group_id'] : null;
?>
<div class="row">


    <div class="col-md-12 sidebar">

        <?php if(Session::has('message')){ ?>
        <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
        <script>
            $('#alert').delay(2000).fadeOut(400)
        </script>
        <?php } ?>
        <data class="row">
            <div class="col-sm-8">
                @if (!Auth::user()->isViewer())
                    <a href="<?= URL::to('article/create')?>" class="btn btn-warning">Create Article</a>
                @endif
            </div>
            <div class="col-sm-4">

            </div>
        </data>
        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <!-- form start -->
                        <?= Form::open(array('url' => 'article/search', 'method' => 'post', 'id' => 'articleForm', 'role' => 'form')); ?>

                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('article_no', 'Article No', array('class' => 'control-label')); ?>
                                    {{ Form::select('article_no', $article_no, $article_no_val, $attributes = array('id'=>'article_no','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('article_type_id', 'Appearance', array('class' => 'control-label')); ?>
                                    {{ Form::select('article_type_id', $article_type, $article_type_val, $attributes = array('id'=>'article_type_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('construction_id', 'Construction', array('class' => 'control-label')); ?>
                                    {{ Form::select('construction_id', $construction, $construction_val, $attributes = array('id'=>'construction_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('customer_group_id', 'Customer Group', array('class' => 'control-label')); ?>
                                        {{ Form::select('customer_group_id', $customer_group, $customer_group_val, $attributes = array('id'=>'customer_group_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-2 col-md-offset-11">
                                <button type="submit" class="btn btn-success">Search</button>
                            </div>

                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Article List
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Article List</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Article No<span class="sort-icon"><span></th>
                                    <th>Article Ref<span class="sort-icon"><span></th>
                                    <th>Appearance<span class="sort-icon"><span></th>
                                    <th>Construction<span class="sort-icon"><span></th>
                                    <th>Customer Group<span class="sort-icon"><span></th>
                                    <th>Picture<span class="sort-icon"><span></th>
                                    <th>Note<span class="sort-icon"><span></th>
                                    @if (!Auth::user()->isViewer())
                                        <th>Action</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($articles as $key => $value){ ?>
                                <tr>
                                    <td><?= $value->id ?></td>
                                    <td><?= $value->article_no ?></td>
                                    <td><?= $value->article_ref ?></td>
                                    <td><?= $value->article_type ?></td>
                                    <td><?= $value->construction ?></td>
                                    <td><?= $value->CustomerGroup->customer_group ?></td>
                                    <?php
                                    if(file_exists(asset('images/article/160X160').'/'.$value->article_pic)) {
                                    ?>
                                    <td><img src="{{ asset('images/article/160X160').'/'.$value->article_pic }}" style="height: 75px; width: 75px;" /></td>
                                    <?php }else{ ?>
                                    <td><img src="{{ asset('images/sample-article.png') }}" style="height: 75px; width: 75px;" /></td>
                                    <?php } ?>
                                    <td><?= $value->note ?></td>
                                    @if (!Auth::user()->isViewer())
                                        <td>
                                            <a class="btn btn-info btn-sm" href="<?= URL::to('article/'.$value->id).'/edit';?>">Edit</a>
                                            {{ Form::open(array('class' => 'btn btn-danger btn-xs','url' => 'article/'.$value->id, 'method' => 'delete', 'style' => 'padding: 3px 0; border-color: transparent !important;')) }}
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs', 'style' => 'display:block;')) }}
                                            {{ Form::close() }}
                                        </td>
                                    @endif
                                </tr>
                                <?php } ?>

                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
        </section>


    </div>
</div>

@stop

@section('page-script')
    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js')}}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js')}}
    {{HTML::script('https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js')}}
    {{HTML::script('https://cdn.datatables.net/buttons/1.3.1/js/buttons.bootstrap.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')}}
    {{HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js')}}
    {{HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js')}}
    {{HTML::script('//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js')}}
@stop

@section('plugin-script')
    <script type="text/javascript">

        var table=$('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "scrollX": true,
            "lengthChange": true,
            "buttons": [  {
                extend: 'excel',
                message: 'This print was produced using the Print button for DataTables',
                exportOptions: {
                    columns: ':visible',
                }
            }, 'pdf', 'colvis' ]
        });
        table.buttons().container()
                .appendTo( '#example1_wrapper .col-sm-6:eq(0)' );
    </script>
@stop
