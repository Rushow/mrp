@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

    <div class="col-md-12 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Article
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Article</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?= Form::open(array('url' => 'article', 'id' => 'articleForm', 'method' => 'post','files'=>true)); ?>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('article_id', 'Article ID', array('class' => 'control-label')); ?>
                                    {{ Form::text('article_id',$article_id,array('class'=>'validate[required] form-control','id'=>'article_id', 'readonly'=>'readonly')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('article_no', 'Article No', array('class' => 'control-label')); ?>
                                    {{ Form::text('article_no','',array('class'=>'validate[required] form-control','id'=>'article_no')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('article_ref', 'Article Ref', array('class' => 'control-label')); ?>
                                    {{ Form::text('article_ref','',array('class'=>'form-control','id'=>'article_ref')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('article_type_id', 'Appearance', array('class' => 'control-label')); ?>
                                    {{ Form::select('article_type_id', $article_type,null,$attributes = array('id'=>'article_type_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('note', 'Note', array('class' => 'control-label')); ?>
                                    {{ Form::textarea('note','',array('class'=>'form-control','id'=>'note', 'size' => '30x5')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Form::label('construction_id', 'Construction', array('class' => 'control-label')); ?>
                                    {{ Form::select('construction_id', $construction,null,$attributes = array('id'=>'construction_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('customer_group_id', 'Customer Group', array('class' => 'control-label')); ?>
                                    {{ Form::select('customer_group_id', $customer_group,null,$attributes = array('id'=>'customer_group_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    <?= Form::label('article_pic', 'Picture', array('class' => 'control-label')); ?>
                                    {{ Form::file('article_pic',array('class'=>'','id'=>'article_pic', 'accept'=>'image/*')) }}
                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <div class="col-md-4 col-md-offset-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</a>
                            </div>
                        </div>
                        <?= Form::close(); ?>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#articleForm").validate({
            rules: {
                article_no: "required",
                // article_ref: "required",
                article_type_id: "required",
                // note: "required",
                construction_id: "required",
                customer_group_id: "required"
                // article_pic: {
                //     required: true,
                //     extension: "jpg|jpeg|png|bmp|JPG"
                // }
            },
            messages: {
                article_no: "Please enter Article No",
                // article_ref: "Please enter Article Ref",
                article_type_id: "Please enter Appearance",
                // note: "Please enter Note",
                construction_id: "Please select construction type",
                customer_group_id: "Please select customer group"
                // article_pic: "The article pic must be a file of type: jpeg, jpg, bmp, png, JPG."
            }
        });
    });
</script>
@stop