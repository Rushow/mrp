@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">
    <div class="col-md-6 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Update Size</h2>
            <?= Form::open(array('url' => 'size/'.$size->id, 'method' => 'put', 'id' => 'sizeForm')); ?>
            <div class="form-group">
                <?= Form::label('size_no', 'Size No', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?= Form::text('size_no', $size->size_no, array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Submit', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#sizeForm").validate({
            rules: {
                size_no: "required"
            },
            messages: {
                size_no: "Please enter Size No"
            }
        });
    });
</script>
@stop