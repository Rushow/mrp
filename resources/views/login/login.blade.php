<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Fortuna MRP</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    @section('head')
    {{ HTML::style('admin-lte/bootstrap/css/bootstrap.min.css') }}
    {{ HTML::style('awesome-admin/lib/FontAwesome/css/font-awesome.css') }}
    {{ HTML::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') }}
    {{ HTML::style('admin-lte/dist/css/AdminLTE.min.css') }}
    {{ HTML::style('css/style.css') }}
    {{ HTML::style('admin-lte/plugins/iCheck/square/blue.css') }}
    @show


</head>

<!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
<!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
<!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
<!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body class="login-page">
<!--<![endif]-->


<div class="login-box">
    <div class="dialog">
        <div class="login-logo">
            <a href="#"><b>Fortuna</b>MRP</a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Sign In</p>
            {{ Form::open(array('action'=>'LoginController@checkLogin','method'=>'post', 'class'=>''))}}

            <div class="form-group has-feedback">
                {{ Form::text('email','',array('id'=>'email','placeholder'=>'Email','class'=>'form-control')) }}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

            </div>
            <div class="form-group has-feedback">
                {{ Form::password('password',array('id'=>'password','placeholder'=>'Password','class'=>'form-control')) }}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8"></div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div><!-- /.col -->
            </div>
            {{ Form::close()}}
        </div>
    </div>
</div>


<footer>
    <hr>
    <p>&copy 2013 <a href="http://www.bluebd.com">Infrablue Technology LTD.</a></p>
</footer>


{{ HTML::script('js/jquery-2.0.3.js') }}
{{ HTML::script('admin-lte/bootstrap/js/bootstrap.min.js') }}
{{ HTML::script('admin-lte/plugins/iCheck/icheck.min.js') }}
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>


