@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<div class="row">

<div class="col-md-12 sidebar">
<?php
$error_messages = Session::get('error_messages');

if(isset($error_messages)){
    foreach($error_messages as $message){
        echo $message;
    }
}
?>
<div class="widget">
    <h2 style="margin-bottom: 15px">Material Description and Technical Data Sheet</h2>
    <div class="col-md-4 sidebar">
        {{ Form::hidden('sample_order_details_id', $order_details['id']) }}
        <div class="form-group">
            <?= Form::label('mds_no', 'MDS No', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$sample_mds['mds_no']?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('sample_order_spec_no', 'Sample Order & Spec No', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$order['sample_order_no'].'/'.$sample_spec['spec_no']?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('buyer_id', 'Buyer', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$order['buyer_name']?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('article_id', 'Article No', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$order_details['article_no']?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('sample_type_id', 'Sample Type', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$order_details['sample_type']?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('last_no_id', 'Last No', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$sample_mds['last_no']->last_no?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('outsole_no_id', 'Outsole No', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$sample_mds['outsole_no']->outsole_no?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('heel_no_id', 'Heel No', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$sample_mds['heel_no']->heel_no?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('', 'Sizes', array('class' => 'control-label col-md-4 bold')); ?>
            <?= Form::label('', 'Quantity', array('class' => 'control-label col-md-8 bold')); ?>
            <div class="clearfix"></div>
        </div>
        <?
        $i = 1;
        foreach($order_details['size'] as $s){
            ?>
            <div class="form-group">
                <?= Form::label('size_no_'.$i, $s['size']['size_no'], array('class' => 'control-label col-md-4')); ?>
                <div class="controls col-md-8">
                    <?=$s['quantity']?>
                </div>
                <div class="clearfix"></div>
            </div>
            <?
            $i++;
        }
        ?>
        <div class="form-group">
            <?= Form::label('total', 'Total', array('class' => 'control-label col-md-4 bold')); ?>
            <div class="controls col-md-8">
                <?=$order_details['quantity']?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-md-4" style="text-align:center;">
        <img src="{{ asset('images/article/300X200').'/'.$order_details['article_pic'] }}" />
    </div>
    <div class="col-md-4 sidebar">
        <div class="form-group">
            <?= Form::label('order_date', 'Order Date', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$order['order_date']?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('delivery_date', 'Delivery Date', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$order['delivery_date']?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('logo_position', 'Logo Position', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$sample_mds['logo_position']?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('logo_type', 'Logo Type', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$sample_mds['logo_type']?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('print_type', 'Print Type', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$sample_mds['print_type']?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('print_color', 'Print Color', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$sample_mds['print_color']?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <?= Form::label('note', 'Note', array('class' => 'control-label col-md-4')); ?>
            <div class="controls col-md-8">
                <?=$sample_mds['note']?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="col-md-12">
        <table border="1">
            <th>
                <td colspan="10" text-align="center">Material Description</td>
            </th>
            <tr>
                <td class="col-md-1 bold">Component</td>
                <td class="col-md-2 bold">Material Group</td>
                <td class="col-md-2 bold">Sub Group</td>
                <td class="col-md-1 bold">Parameter</td>
                <td class="col-md-2 bold">Material Name</td>
                <td class="col-md-1 bold">Color</td>
                <td class="col-md-1 bold">Area</td>
                <td class="col-md-1 bold">Wastage</td>
                <td class="col-md-1 bold">Consumption</td>
                <td class="col-md-2 bold">Note</td>
            </tr>
            <?
            foreach($mds_desc as $desc){
                ?>
                <tr>
                    <td>
                        <?=$desc['component_name']?>
                    </td>
                    <td>
                        <?=$desc['group_name']?>
                    </td>
                    <td>
                        <?=$desc['sub_group_name']?>
                    </td>
                    <td>
                        <?=$desc['parameter_name']?>
                    </td>
                    <td>
                        <?=$desc['item_name']?>
                    </td>
                    <td>
                        <?=$desc['color_name']?>
                    </td>
                    <td>
                        <?=$desc['area']?>
                    </td>
                    <td>
                        <?=$desc['wastage']?>
                    </td>
                    <td>
                        <?=$desc['consumption']?>
                    </td>
                    <td>
                        <?=$desc['note']?>
                    </td>
                </tr>
                <? } ?>
        </table>
    </div>
    <div class="clearfix"></div>

    <div class="col-md-12" style="margin-top: 20px;">
        <table border="1">
            <th>
                <td colspan="3" text-align="center">TDS</td>
            </th>
            <tr>
                <td class="col-md-1 bold">Particulars</td>
                <td class="col-md-2 bold">Tolerance</td>
                <td class="col-md-2 bold">Remarks</td>
            </tr>
            <?
            foreach($sample_tds as $t){
                ?>
                <tr>
                    <td>
                        <?=$t['particulars']?>
                    </td>
                    <td>
                        <?=$t['tolerance']?>
                    </td>
                    <td>
                        <?=$t['remarks']?>
                    </td>
                    </td>
                </tr>
                <? } ?>
        </table>
    </div>
    <div class="clearfix"></div>
</div>
</div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
    .widget{
        overflow: visible !important;
        padding: 15px;
    }
</style>

@stop