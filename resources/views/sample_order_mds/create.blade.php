@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<script>
    $(function() {
        $("#order_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });

        $("#delivery_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });

    });

</script>
<div class="row">

<div class="col-md-12 sidebar">
<?php
$error_messages = Session::get('error_messages');

if(isset($error_messages)){
    foreach($error_messages as $message){
        echo $message;
    }
}
?>
<div class="widget">
<h2>Create Material Description and Technical Data Sheet</h2>
<?= Form::open(array('url' => 'sampleOrderMds', 'method' => 'post', 'id' => 'sampleOrderMdsForm')); ?>
<div class="col-md-4 sidebar">
    {{ Form::hidden('sample_order_detail_id', $order_details['id']) }}
    {{ Form::hidden('id', $id) }}
    <div class="form-group">
        <?= Form::label('mds_no', 'MDS No', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::text('mds_no',$mds_no,array('class'=>'validate[required] form-control','id'=>'mds_no', 'readonly'=>'readonly')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('sample_order_spec_no', 'Sample Order & Spec No', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::text('sample_order_spec_no',$order['sample_order_no'].'/'.$sample_spec['spec_no'],array('class'=>'validate[required] form-control','id'=>'sample_order_spec_no', 'readonly'=>'readonly')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('buyer_id', 'Buyer', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::text('buyer_id',$order['buyer_name'],array('class'=>'validate[required] form-control','id'=>'buyer_id', 'readonly'=>'readonly')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('article_id', 'Article No', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::text('article_id',$order_details['article_no'],array('class'=>'validate[required] form-control','id'=>'article_id', 'readonly'=>'readonly')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('sample_type_id', 'Sample Type', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::text('sample_type_id',$order_details['sample_type'],array('class'=>'validate[required] form-control','id'=>'sample_type_id', 'readonly'=>'readonly')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('last_no_id', 'Last No', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::select('last_no_id', $last_no, '', $attributes = array('id'=>'last_no_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('outsole_no_id', 'Outsole No', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::select('outsole_no_id', $outsole_no, '', $attributes = array('id'=>'outsole_no_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('heel_no_id', 'Heel No', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::select('heel_no_id', $heel_no, '', $attributes = array('id'=>'heel_no_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('', 'Sizes', array('class' => 'control-label col-md-4 bold')); ?>
        <?= Form::label('', 'Quantity', array('class' => 'control-label col-md-8 bold')); ?>
        <div class="clearfix"></div>
    </div>
    <?
    $i = 1;
    foreach($order_details['size'] as $s){
        ?>
        <div class="form-group">
            <?= Form::label('size_no_'.$i, $s['size_no'], array('class' => 'control-label col-md-4')); ?>
            {{ Form::hidden('size_id_'.$i, $s['id']) }}
            <div class="controls col-md-8">
                {{ Form::text('quantity_'.$i, '',array('class'=>'size_qty form-control Required','id'=>'quantity_'.$i)) }}
            </div>
            <div class="clearfix"></div>
        </div>
        <?
        $i++;
    }
    ?>
    <div class="form-group">
        <?= Form::label('total', 'Total', array('class' => 'control-label col-md-4 bold')); ?>
        <div class="controls col-md-8">
            {{ Form::text('total_quantity', $order_details['quantity'],array('class'=>'form-control bold','id'=>'total_quantity', 'readonly'=>'readonly')) }}
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="col-md-4" style="text-align:center;">
    <img src="{{ asset('images/article/300X200').'/'.$order_details['article_pic'] }}" />
</div>
<div class="col-md-4 sidebar">
    <div class="form-group">
        <?= Form::label('order_date', 'Order Date', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::text('order_date', $order['order_date'],array('class'=>'validate[required] form-control datepicker','id'=>'order_date', 'readonly'=>'readonly')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('delivery_date', 'Delivery Date', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::text('delivery_date', $order['delivery_date'],array('class'=>'validate[required] form-control datepicker','id'=>'delivery_date', 'readonly'=>'readonly')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('logo_position', 'Logo Position', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::text('logo_position','',array('class'=>'validate[required] form-control','id'=>'logo_position')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('logo_type', 'Logo Type', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::text('logo_type','',array('class'=>'validate[required] form-control','id'=>'logo_type')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('print_type', 'Print Type', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::text('print_type','',array('class'=>'validate[required] form-control','id'=>'print_type')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('print_color', 'Print Color', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::text('print_color','',array('class'=>'validate[required] form-control','id'=>'print_color')) }}
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <?= Form::label('note', 'Note', array('class' => 'control-label col-md-4')); ?>
        <div class="controls col-md-8">
            {{ Form::textarea('note','',array('class'=>'form-control','id'=>'note')) }}
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="clearfix"></div>

<div class="col-md-12">
    <table>
        <tr>Material Description</tr>
        <tr>
            <td class="col-md-1">Component</td>
            <td class="col-md-2">Material Group</td>
            <td class="col-md-2">Sub Group</td>
            <td class="col-md-1">Parameter</td>
            <td class="col-md-2">Material Name</td>
            <td class="col-md-1">Color</td>
            <td class="col-md-1">Area</td>
            <td class="col-md-1">Wastage(%)</td>
            <td class="col-md-1">Consumption(Per Pair)</td>
            <td class="col-md-2">Note</td>
            <td></td>
        </tr>
        <tr>
            {{ Form::hidden('total_row', 1, array('id'=> 'total_row')) }}
            <td>
                {{ Form::select('component_id_1', $component, '', $attributes = array('id'=>'component_id_1','data-rel'=>'chosen', 'class'=>'form-control Required')) }}
            </td>
            <td>
                {{ Form::select('group_id_1', $item_group, '', $attributes = array('id'=>'group_id_1','data-rel'=>'chosen', 'class'=>'form-control Required', 'onChange'=>'generateSubGroup(this);')) }}
            </td>
            <td>
                {{ Form::select('sub_group_id_1', $item_sub_group, null, $attributes = array('id'=>'sub_group_id_1','data-rel'=>'chosen', 'class'=>'form-control Required', 'disabled'=>'disabled', 'onChange'=>'generateItem(this);')) }}
            </td>
            <td>
                {{ Form::select('parameter_id_1', $parameter, null, $attributes = array('id'=>'parameter_id_1','data-rel'=>'chosen', 'class'=>'form-control Required')) }}
            </td>
            <td>
                {{ Form::select('item_id_1', $item, null, $attributes = array('id'=>'item_id_1','data-rel'=>'chosen', 'class'=>'form-control Required', 'disabled'=>'disabled')) }}
            </td>
            <td>
                {{ Form::select('color_id_1', $color, '', $attributes = array('id'=>'color_id_1','data-rel'=>'chosen', 'class'=>'form-control Required')) }}
            </td>
            <td>
                {{ Form::text('area_1','',array('class'=>'Required form-control','id'=>'area_1')) }}
            </td>
            <td>
                {{ Form::text('wastage_1','',array('class'=>'Required form-control','id'=>'wastage_1')) }}
            </td>
            <td>
                {{ Form::text('consumption_1','',array('class'=>'Required form-control','id'=>'consumption_1')) }}
            </td>
            <td>
                {{ Form::text('note_1', '',array('class'=>'form-control','id'=>'note_1')) }}
            </td>
            <td>
                <a href="#" onclick="return checkDelete(this);">
                    <span class="badge badge-important">X</span>
                </a>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><label onclick="addMdsDescRow(this);" style="cursor: pointer;" class="badge badge-info">+Add More</label> </td>
        </tr>
    </table>
</div>
<div class="clearfix"></div>

<div class="col-md-12">
    <table>
        <tr>Technical Data Sheet</tr>
        <tr>
            <td class="col-md-2">Particulars/Sizes</td>
            <td class="col-md-2">Tolerance(%)</td>
            <td class="col-md-2">Remarks</td>
            <td></td>
        </tr>
        <tr>
            {{ Form::hidden('tds_total_row', 1, array('id'=> 'tds_total_row')) }}
            <td>
                {{ Form::text('tds_particulars_1','',array('class'=>'Required form-control','id'=>'tds_particulars_1')) }}
            </td>
            <td>
                {{ Form::text('tds_tolerance_1','',array('class'=>'Required form-control','id'=>'tds_tolerance_1')) }}
            </td>
            <td>
                {{ Form::text('tds_remarks_1','',array('class'=>'form-control','id'=>'tds_remarks_1')) }}
            <td>
                <a href="#" onclick="return checkDelete(this);">
                    <span class="badge badge-important">X</span>
                </a>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><label onclick="addTdsRow(this);" style="cursor: pointer;" class="badge badge-info">+Add More</label> </td>
        </tr>
    </table>
</div>
<div class="clearfix"></div>
<div class="form-group">
    <div class="controls">
        <?= Form::submit('Save', array('class' => 'btn btn-success')); ?>
    </div>
</div>
<?= Form::close(); ?>
</div>
</div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    function addMdsDescRow(item){
        var last_id = $("#total_row").val();
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '<?= Url("sampleOrderMds/addMdsDescRow") ?>',
            data: 'last_id='+last_id,
            success: function(data){
                if(data.error == false){
                    $(item).parents('tr').before('<tr>'+data.html+'</tr>');
                    last_id = parseInt(last_id) + 1;
                    $('#total_row').val(last_id);
                    $('#size_id_'+last_id).multiselect();
                }
                else{
                    alert(data.message);
                }
            }
        });
    }
    function addTdsRow(item){
        var last_id = $("#tds_total_row").val();
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '<?= Url("sampleOrderMds/addTdsRow") ?>',
            data: 'last_id='+last_id,
            success: function(data){
                if(data.error == false){
                    $(item).parents('tr').before('<tr>'+data.html+'</tr>');
                    last_id = parseInt(last_id) + 1;
                    $('#tds_total_row').val(last_id);
                }
                else{
                    alert(data.message);
                }
            }
        });
    }


    $(document).ready(function() {
        $.validator.addMethod("quantityCheck", function(value, element){
            var total = 0;
            $('.size_qty').each(function(i, obj) {
                obj_id = obj.id;
                obj_val = $(this).val();
                total += parseInt(obj_val);
            });
            var quantity = $("#total_quantity").val();
            if(total == quantity)
            {
                return true;
            }
        }, 'Total quantity of sizes should be equal to the Total Quantity.');

        $("#sampleOrderMdsForm").validate({
            rules: {
                spec_no: "required",
                last_no_id: "required",
                outsole_no_id: "required",
                heel_no_id: "required",
                order_date: "required",
                delivery_date: "required",
                logo_position: "required",
                logo_type: "required",
                print_type: "required",
                print_color: "required",
                total_quantity: {
                    quantityCheck: true
                }
            },
            messages: {
                spec_no: "Please Enter Spec No",
                last_no_id: "Please Select a Last No",
                outsole_no_id: "Please Select a Outsole No",
                heel_no_id: "Please Select a Heel No",
                order_date: "Please enter Order Date",
                delivery_date: "Please enter Delivery Date",
                logo_position: "Please enter Logo Position",
                logo_type: "Please enter Logo Type",
                print_type: "Please enter Print Type",
                print_color: "Please enter Print Color"
            }
        });

        $.validator.addClassRules("Required", {
            required: true
        });
    });


    function generateSubGroup(item){
        var val = $(item).val();
        var group_id = $(item).attr('id');
        var grp_arr = group_id.split('_');
        var id = grp_arr[2];

        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("item/subGroup") ?>',
                data: 'group_id='+val,
                success: function(data){
                    $("#sub_group_id_"+id).html(data.sub_group);
                    $("#sub_group_id_"+id).removeAttr('disabled');

                }
            });
        }
        else{
            $("#sub_group_id").attr('readonly', 'readonly');
        }
    }

    function generateItem(item){
        var val = $(item).val();
        var sub_group_id = $(item).attr('id');
        var sub_grp_arr = sub_group_id.split('_');
        var id = sub_grp_arr[3];

        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("item/item") ?>',
                data: 'sub_group_id='+val,
                success: function(data){
                    $("#item_id_"+id).html(data.item);
                    $("#item_id_"+id).removeAttr('disabled');

                }
            });
        }
        else{
            $("#sub_group_id").attr('readonly', 'readonly');
        }
    }
</script>

<style>
    .widget{
        overflow: visible !important;
    }
</style>

@stop