@extends('layouts.master')
@section('head')

    @parent

@stop
@section('header')
@stop
@section('content')


    <div class="row">
        <div class="col-md-12 sidebar">
            <?php
            $error_messages = Session::get('error_messages');

            if(isset($error_messages)){
                foreach($error_messages as $message){
                    echo $message;
                }
            }
            ?>
            <div class="widget">
                <h2 class="topheadertext"> Add Component :</h2>
                <?= Form::open(array('url' => 'dailyproduction/component', 'method' => 'post', 'id' => 'addcomponentForm')); ?>
                <div class="col-md-4 sidebar" style="margin-right: 150px;">
                    <div class="form-group">
                        <?= Form::label('component_name', 'Component Name ', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('component_name','',array('class'=>'validate[required] form-control','id'=>'component_name')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('component_type', 'Component Type', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('component_type',array('0' => 'Single', '1' => 'Combination'),$combination, $attributes =array('id'=>'type', 'class'=>'validate[required] form-control')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('sub_component_id', 'Sub Component (if Component Type is Combination)', array('class' => 'control-label')); ?>
                        {{ Form::select('sub_component_id', $sub_component, '', $attributes = array('id'=>'sub_component_id', 'name'=>'sub_component_ids[]', 'multiple'=>'', 'class'=>'form-control select2', 'data-placeholder' => 'Select Sub Component')) }}
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="controls">
                            <?= Form::submit('Save', array('class' => 'btn btn-success')); ?>
                        </div>
                    </div>
                </div>


                <?= Form::close(); ?>
            </div>
        </div>
    </div>



@stop
@section('page-script')
        <!-- Select2 -->
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js') }}
@stop
@section('plugin-script')
    <style>
        .sidebar{
            padding-bottom: 20px;
        }
        form{
            margin: 15px;
        }

        .widget{
            overflow: visible !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2").select2();
            $("#addcomponentForm").validate({
                rules: {
                    component_name: "required",
                    description: "required",

                }
            });
            $.validator.addClassRules("Required", {
                required: true
            });
        });
    </script>

@stop