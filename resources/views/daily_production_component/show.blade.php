@extends('layouts.master')
@section('head')
    @parent
    {{ HTML::style('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}
@stop
@section('header')
@stop
@section('content')

    <div class="row">

        <div class="col-md-12 sidebar">
            <?php
                        $error_messages = Session::get('error_messages');

                        if(isset($error_messages)){
                            foreach($error_messages as $message){
                                echo $message;
                            }
                        }
            ?>
                    <!-- Content Header (Page header) -->
            <section class="content-header">

                <data class="row">
                    <div class="col-sm-8">
                        @if (!Auth::user()->isViewer())
                            <a href="<?=URL::to('dailyproduction/component/create')?>" class="btn btn-warning btn-sm">Create component</a>

                        @endif
                    </div>
                </data>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Component List</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">

                                    <div class="col-md-12">
                                        <table class="table table-first-column-number data-table display" id="example1">
                                            <thead>
                                            <tr>
                                                <th>SL No.</th>
                                                <th class="col-md-2">Name</th>
                                                <th>Component Type</th>
                                                <th class="col-md-1">Sub Component List</th>
                                                <th class="col-md-2">Action</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?
                                            $total = 0;
                                            foreach($dailyProductionComponent as $key => $value){
                                            ?>
                                            <tr>
                                                <td><?=$value->id?></td>
                                                <td><?=$value->component_name?></td>
                                                <td><? if($value->component_type){echo "Combination";}else{echo "Single";}?></td>
                                                <td>
                                                    <? if($value->component_type){
                                                        foreach ($value->Combination as $k=>$combo){
                                                            if($k==0){
                                                                echo $combo->Comboname->component_name;
                                                            }else{
                                                                echo ",".$combo->Comboname->component_name;
                                                            }
                                                        }
                                                    }?>
                                                </td>
                                                <td>
                                                    <a class="btn btn-info btn-sm" href="<?=URL::to('dailyproduction/component/'.$value->id.'/edit')?>">Edit</a>
                                                </td>

                                            </tr>
                                            <? } ?>

                                            </tbody>
                                        </table>
                                    </div>

                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </section>




        </div>







@stop
@section('page-script')
        <!-- Select2 -->
    {{ HTML::script('admin-lte/plugins/select2/select2.full.min.js') }}

    {{ HTML::script('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}
@stop

@section('plugin-script')
            <script type="text/javascript">
                $(function () {
                    $('#example1').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false
                    });

                });
            </script>

@stop