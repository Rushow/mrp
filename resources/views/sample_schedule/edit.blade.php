@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<script>
    $(function() {

        $("#schedule_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });

    });

</script>

<div class="row">
    <div class="col-md-10 sidebar">
        <?php
        $error_messages = Session::get('error_messages');

        if(isset($error_messages)){
            foreach($error_messages as $message)
            {
                echo $message;
            }
        }
        ?>
        <div class="widget">
            <h2>Update Sample Schedule Info</h2>
            <?= Form::open(array('url' => 'sampleSchedule/edit/'.$sample_schedule->id, 'method' => 'post', 'id' => 'sampleScheduleForm')); ?>
            <table class="table table-first-column-number data-table display sort" style="margin-bottom: 20px;">
                <thead>
                <tr>
                    <th>Sample Order No<span class="sort-icon"><span></th>
                    <th>Article No<span class="sort-icon"><span></th>
                    <th>Buyer Name<span class="sort-icon"><span></th>
                    <th>Picture<span class="sort-icon"><span></th>
                    <th>Color<span class="sort-icon"><span></th>
                    <th>Sizes<span class="sort-icon"><span></th>
                    <th>Qty/Size<span class="sort-icon"><span></th>
                    <th>Sample Type<span class="sort-icon"><span></th>
                    <th>Order Date<span class="sort-icon"><span></th>
                    <th>Customer Req. Delivery Date<span class="sort-icon"><span></th>
                    <th>Scheduled Delivery Date<span class="sort-icon"><span></th>
                    <th>Note<span class="sort-icon"><span></th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $sample_schedule->SampleOrderDetail->SampleOrder->sample_order_no ?></td>
                        <td><?= $sample_schedule->SampleOrderDetail->Article->article_no ?></td>
                        <td><?= $sample_schedule->SampleOrderDetail->SampleOrder->Buyer->buyer_name ?></td>
                        <td>Picture</td>
                        <td><?= $sample_schedule->SampleOrderDetail->Color->color_name ?></td>
                        <td><? $sizes = $sample_schedule->SampleOrderDetail->Sizes;
                        foreach ($sizes as $key => $size) {
                            echo $size->size_no;
                            echo (sizeof($sizes) != (1 + $key))? ', ' : "";
                        } ?></td>
                        <td><?= $sample_schedule->SampleOrderDetail->quantity ?></td>
                        <td><?= $sample_schedule->SampleOrderDetail->sample_type->sample_type ?></td>
                        <td><?= $sample_schedule->SampleOrderDetail->SampleOrder->order_date ?></td>
                        <td><?= $sample_schedule->SampleOrderDetail->SampleOrder->delivery_date ?></td>                        
                        <td>
                            <div class="control-group">
                                <div class="controls">
                                    {{ Form::text('schedule_date', $sample_schedule->schedule_date,array('class'=>'validate[required] form-control datepicker','id'=>'schedule_date')) }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <div class="controls">
                                    {{ Form::textarea('note', $sample_schedule->note,array('class'=>'validate[required] form-control','id'=>'note')) }}
                                </div>
                            </div>
                        </td>
                    </tr> 
                </tbody>
            </table>
            <div class="form-group">
                <div class="controls">
                    <?= Form::submit('Update Schedule Info', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?= Form::close(); ?>
        </div>
    </div>
</div>



<style>
    .sidebar{
        padding-bottom: 20px;
    }
    form{
        margin: 15px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#sampleScheduleForm").validate({
            rules: {
                schedule_date: "required",
                note: "required"
            },
            messages: {
                schedule_date: "Please enter scheduled date",
                note: "Please enter a note"
            }
        });
    });
</script>

@stop