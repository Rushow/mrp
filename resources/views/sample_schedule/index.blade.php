@extends('layouts.master')
@section('head')

@parent

@stop
@section('header')
@stop
@section('content')

<script>
    $(function() {
        $("#order_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });

        $("#delivery_date").datepicker({
            changeMonth: true,
            changeYear: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            format: 'dd-mm-yyyy'
        });

    });

</script>
<?
    $sample_order_no_val = (isset($sample_order_no)) ? $sample_order_no : null;
    $buyer_val = (isset($buyer_id)) ? $buyer_id : null;
    $article_val = (isset($article_no)) ? $article_no : null;
    $sample_type_val = (isset($sample_type_id)) ? $sample_type_id : null;
    $order_date_val = (isset($order_date)) ? $order_date : null;
    $delivery_date_val = (isset($delivery_date)) ? $delivery_date : null;
?>
<div class="row">
    <?php if(Session::has('message')){ ?>
    <div id="alert" class="alert alert-info"><?= Session::get('message') ?></div>
    <script>
        $('#alert').delay(2000).fadeOut(400)
    </script>
    <?php } ?>
    <div class="col-sm-10">
    <div class="row" style="margin-top: 10px;">
        <div class="col-sm-10">
            <a href="<?= URL::to('excel/sampleSchedule')?>" class="grid btn">Download excel file</a>
            <a href="<?= URL::to('pdf/sampleSchedule')?>" target="_blank" class="grid btn">Download pdf file</a>
            <div class="widget" style="margin-top: 15px;">
                <?= Form::open(array('url' => 'sampleSchedule/search', 'method' => 'post')); ?>
                <div class="col-md-4" style="margin-right: 150px;">
                    <div class="form-group">
                        <?= Form::label('sample_order_no', 'Select Sample Order No', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('sample_order_no', $sample_order_no, $sample_order_no_val, $attributes = array('id'=>'sample_order_no','data-rel'=>'chosen', 'class'=>'form-control')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('buyer_id', 'Buyer', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('buyer_id', $buyer, $buyer_val, $attributes = array('id'=>'buyer_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('article_id', 'Article No', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('article_id', $article, $article_val, $attributes = array('id'=>'article_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="control-group">
                        <?= Form::label('sample_type_id', 'Type of Sample', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::select('sample_type_id', $sample_type, $sample_type_val, $attributes = array('id'=>'sample_type_id','data-rel'=>'chosen', 'class'=>'form-control')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('order_date', 'Order Date', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('order_date', $order_date_val,array('class'=>'validate[required] form-control datepicker','id'=>'order_date')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <?= Form::label('delivery_date', 'Delivery Date', array('class' => 'control-label')); ?>
                        <div class="controls">
                            {{ Form::text('delivery_date', $delivery_date_val,array('class'=>'validate[required] form-control datepicker','id'=>'delivery_date')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="controls">
                            <?= Form::submit('Search', array('class' => 'btn btn-success')); ?>
                        </div>
                    </div>
                </div>
                <?= Form::close(); ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    </div>


    <div class="col-sm-10">
        <h2>Sample Shecdule List</h2>
        <table class="table table-first-column-number data-table display full" style="margin-bottom: 20px;">
            <thead>
            <tr>
                <th>Sl. No<span class="sort-icon"><span></th>
                <th>Sample Order No<span class="sort-icon"><span></th>
                <th>Article No<span class="sort-icon"><span></th>
                <th>Buyer Name<span class="sort-icon"><span></th>
                <th>Picture<span class="sort-icon"><span></th>
                <th>Color<span class="sort-icon"><span></th>
                <th>Sizes<span class="sort-icon"><span></th>
                <th>Qty/Size<span class="sort-icon"><span></th>
                <th>Sample Type<span class="sort-icon"><span></th>
                <th>Order Date<span class="sort-icon"><span></th>
                <th>Customer Req. Delivery Date<span class="sort-icon"><span></th>
                <th>Scheduled Delivery Date<span class="sort-icon"><span></th>
                <th>Note<span class="sort-icon"><span></th>
                @if (!Auth::user()->isViewer())
                    <th>Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
            <?php foreach ($details as $detail_count => $detail) { ?>
            <tr>
                <td><?= $detail->id ?>&nbsp;</td>
                <td><?= $detail->SampleOrder->sample_order_no ?>&nbsp;</td>
                <td><?= $detail->Article->article_no ?>&nbsp;</td>
                <td><?= $detail->SampleOrder->Buyer->buyer_name ?>&nbsp;</td>
                <td><img url="<?= $detail->Article->article_pic ?>" />&nbsp;</td>
                <td><?= $detail->Color->color_name ?>&nbsp;</td>
                <td><? $sizes = $detail->Sizes;
                foreach ($sizes as $key => $size) {
                    echo $size->size_no;
                    echo (sizeof($sizes) != (1 + $key))? ', ' : "";
                } ?>&nbsp;</td>
                <td><?= $detail->quantity ?>&nbsp;</td>
                <td><?= $detail->sample_type->sample_type ?>&nbsp;</td>
                <td><?= $detail->SampleOrder->order_date ?>&nbsp;</td>
                <td><?= $detail->SampleOrder->delivery_date ?>&nbsp;</td>
                <td><?= ($detail->schedule != null)? $detail->schedule->schedule_date:$order->delivery_date ?>&nbsp;</td>
                <td style="max-width:250px; word-wrap:break-word;"><?= ($detail->schedule != null)? $detail->schedule->note:"Add a note" ?>&nbsp;</td>
                @if (!Auth::user()->isViewer())
                    <td>
                        <?= ($detail->schedule != null)? '<a class="" href="' . URL::to('sampleSchedule/edit/'.$detail->schedule->id) . '"><span class="label label-warning">Edit</span></a>':'<a class="" href="' . URL::to('sampleSchedule/create/') . '"><span class="label label-warning">Create</span></a>' ?>
                    </td>
                @endif
            </tr> 
            <?php } ?>
        </table>
    </div>

</div>


<style>
    .btn{
        width: 200px!important;
        margin-top: 15px;
    }
</style>
<script>

    function generateSubGroup(item){
        var val = $(item).val();
        if(val!=""){
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?= Url("item/subGroup") ?>',
                data: 'group_id='+val,
                success: function(data){
                    $("#sub_group_id").html(data.sub_group);
                    $("#sub_group_id").removeAttr('readonly');

                }
            });
        }
        else{
            $("#sub_group_id").attr('readonly', 'readonly');
        }
    }
</script>

@stop